// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/Parametry__pf1851508772.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeParametry__pf1851508772() {}
// Cross Module References
	NATIVIZEDASSETS_API UScriptStruct* Z_Construct_UScriptStruct_FParametry__pf1851508772();
// End Cross Module References
class UScriptStruct* FParametry__pf1851508772::StaticStruct()
{
	class UPackage* StructPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Car/Parametry"));
	class UScriptStruct* Singleton = Cast<UScriptStruct>(StaticFindObjectFast(UScriptStruct::StaticClass(), StructPackage, TEXT("Parametry")));
	if (!Singleton)
	{
		extern NATIVIZEDASSETS_API uint32 Get_Z_Construct_UScriptStruct_FParametry__pf1851508772_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FParametry__pf1851508772, StructPackage, TEXT("Parametry"), sizeof(FParametry__pf1851508772), Get_Z_Construct_UScriptStruct_FParametry__pf1851508772_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FParametry__pf1851508772(FParametry__pf1851508772::StaticStruct, TEXT("/Game/Blueprints/Car/Parametry"), TEXT("Parametry"), true, TEXT("/Game/Blueprints/Car/Parametry"), TEXT("/Game/Blueprints/Car/Parametry.Parametry"));
static struct FScriptStruct_NativizedAssets_StaticRegisterNativesFParametry__pf1851508772
{
	FScriptStruct_NativizedAssets_StaticRegisterNativesFParametry__pf1851508772()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("Parametry")),new UScriptStruct::TCppStructOps<FParametry__pf1851508772>);
	}
} ScriptStruct_NativizedAssets_StaticRegisterNativesFParametry__pf1851508772;
	struct Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics
	{
		static UObject* OuterFuncGetter();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
	UObject* Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::OuterFuncGetter()
	{
		return FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Car/Parametry"));	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Parametry__pf1851508772.h" },
		{ "OverrideNativeName", "Parametry" },
		{ "ReplaceConverted", "/Game/Blueprints/Car/Parametry.Parametry" },
	};
#endif
	void* Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FParametry__pf1851508772>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf_MetaData[] = {
		{ "Category", "Parametry__pf1851508772" },
		{ "DisplayName", "DrivingForce" },
		{ "ModuleRelativePath", "Public/Parametry__pf1851508772.h" },
		{ "OverrideNativeName", "DrivingForce_7_9788A03E425C90DD905F5CA4274F056A" },
		{ "Tooltip", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf = { UE4CodeGen_Private::EPropertyClass::Float, "DrivingForce_7_9788A03E425C90DD905F5CA4274F056A", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(FParametry__pf1851508772, bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf), METADATA_PARAMS(Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf_MetaData[] = {
		{ "Category", "Parametry__pf1851508772" },
		{ "DisplayName", "SteeringSensitivity" },
		{ "ModuleRelativePath", "Public/Parametry__pf1851508772.h" },
		{ "OverrideNativeName", "SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76" },
		{ "Tooltip", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf = { UE4CodeGen_Private::EPropertyClass::Float, "SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(FParametry__pf1851508772, bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf), METADATA_PARAMS(Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::ReturnStructParams = {
		(UObject* (*)())&OuterFuncGetter,
		nullptr,
		&NewStructOps,
		"Parametry",
		RF_Public|RF_Transient,
		EStructFlags(0x00000001),
		sizeof(FParametry__pf1851508772),
		alignof(FParametry__pf1851508772),
		Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FParametry__pf1851508772()
	{
		extern uint32 Get_Z_Construct_UScriptStruct_FParametry__pf1851508772_CRC();
		UPackage* Outer = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Car/Parametry"));
		UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("Parametry"), sizeof(FParametry__pf1851508772), Get_Z_Construct_UScriptStruct_FParametry__pf1851508772_CRC(), true);
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FParametry__pf1851508772_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FParametry__pf1851508772_CRC() { return 3151565557U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
