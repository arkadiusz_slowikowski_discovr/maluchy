// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/MainUI__pf3053510930.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMainUI__pf3053510930() {}
// Cross Module References
	NATIVIZEDASSETS_API UFunction* Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_UMainUI_C__pf3053510930();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_UMainUI_C__pf3053510930_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateColor();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FGeometry();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UTextBlock_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UCanvasPanel_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UProgressBar_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UImage_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UHorizontalBox_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Event GameStarted__DelegateSignature" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "Event GameStarted__DelegateSignature", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00120000, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Event GameStarted__DelegateSignature") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static FName NAME_UMainUI_C__pf3053510930_bpf__Construct__pf = FName(TEXT("Construct"));
	void UMainUI_C__pf3053510930::eventbpf__Construct__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_UMainUI_C__pf3053510930_bpf__Construct__pf),NULL);
	}
	static FName NAME_UMainUI_C__pf3053510930_bpf__Tick__pf = FName(TEXT("Tick"));
	void UMainUI_C__pf3053510930::eventbpf__Tick__pf(FGeometry bpp__MyGeometry__pf, float bpp__InDeltaTime__pf)
	{
		MainUI_C__pf3053510930_eventbpf__Tick__pf_Parms Parms;
		Parms.bpp__MyGeometry__pf=bpp__MyGeometry__pf;
		Parms.bpp__InDeltaTime__pf=bpp__InDeltaTime__pf;
		ProcessEvent(FindFunctionChecked(NAME_UMainUI_C__pf3053510930_bpf__Tick__pf),&Parms);
	}
	void UMainUI_C__pf3053510930::StaticRegisterNativesUMainUI_C__pf3053510930()
	{
		UClass* Class = UMainUI_C__pf3053510930::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Construct", &UMainUI_C__pf3053510930::execbpf__Construct__pf },
			{ "Event HandleGameStarted", &UMainUI_C__pf3053510930::execbpf__EventxHandleGameStarted__pfT },
			{ "GetCarSpeed", &UMainUI_C__pf3053510930::execbpf__GetCarSpeed__pf },
			{ "GetColorAndOpacity_0", &UMainUI_C__pf3053510930::execbpf__GetColorAndOpacity_0__pf },
			{ "GetPercent_0", &UMainUI_C__pf3053510930::execbpf__GetPercent_0__pf },
			{ "GetText_0", &UMainUI_C__pf3053510930::execbpf__GetText_0__pf },
			{ "GetText_1", &UMainUI_C__pf3053510930::execbpf__GetText_1__pf },
			{ "GetText_2", &UMainUI_C__pf3053510930::execbpf__GetText_2__pf },
			{ "HandleGameStarted", &UMainUI_C__pf3053510930::execbpf__HandleGameStarted__pf },
			{ "Tick", &UMainUI_C__pf3053510930::execbpf__Tick__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "User Interface" },
		{ "CppFromBpEvent", "" },
		{ "Keywords", "Begin Play" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Construct" },
		{ "ToolTip", "Called after the underlying slate widget is constructed.  Depending on how the slate object is usedthis event may be called multiple times due to adding and removing from the hierarchy." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "Construct", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C08, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Construct") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Event HandleGameStarted" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "Event HandleGameStarted", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Event HandleGameStarted") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics
	{
		struct MainUI_C__pf3053510930_eventbpf__GetCarSpeed__pf_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MainUI_C__pf3053510930_eventbpf__GetCarSpeed__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "GetCarSpeed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "GetCarSpeed", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(MainUI_C__pf3053510930_eventbpf__GetCarSpeed__pf_Parms), Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetCarSpeed") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics
	{
		struct MainUI_C__pf3053510930_eventbpf__GetColorAndOpacity_0__pf_Parms
		{
			FSlateColor ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Struct, "ReturnValue", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MainUI_C__pf3053510930_eventbpf__GetColorAndOpacity_0__pf_Parms, ReturnValue), Z_Construct_UScriptStruct_FSlateColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "GetColorAndOpacity_0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "GetColorAndOpacity_0", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x14020400, sizeof(MainUI_C__pf3053510930_eventbpf__GetColorAndOpacity_0__pf_Parms), Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetColorAndOpacity_0") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics
	{
		struct MainUI_C__pf3053510930_eventbpf__GetPercent_0__pf_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MainUI_C__pf3053510930_eventbpf__GetPercent_0__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "GetPercent_0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "GetPercent_0", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x14020400, sizeof(MainUI_C__pf3053510930_eventbpf__GetPercent_0__pf_Parms), Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetPercent_0") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics
	{
		struct MainUI_C__pf3053510930_eventbpf__GetText_0__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Text, "ReturnValue", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MainUI_C__pf3053510930_eventbpf__GetText_0__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "GetText_0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "GetText_0", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x14020400, sizeof(MainUI_C__pf3053510930_eventbpf__GetText_0__pf_Parms), Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetText_0") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics
	{
		struct MainUI_C__pf3053510930_eventbpf__GetText_1__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Text, "ReturnValue", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MainUI_C__pf3053510930_eventbpf__GetText_1__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "GetText_1" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "GetText_1", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x14020400, sizeof(MainUI_C__pf3053510930_eventbpf__GetText_1__pf_Parms), Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetText_1") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics
	{
		struct MainUI_C__pf3053510930_eventbpf__GetText_2__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Text, "ReturnValue", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(MainUI_C__pf3053510930_eventbpf__GetText_2__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "GetText_2" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "GetText_2", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x14020400, sizeof(MainUI_C__pf3053510930_eventbpf__GetText_2__pf_Parms), Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetText_2") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "HandleGameStarted" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "HandleGameStarted", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("HandleGameStarted") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__InDeltaTime__pf;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__MyGeometry__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::NewProp_bpp__InDeltaTime__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__InDeltaTime__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainUI_C__pf3053510930_eventbpf__Tick__pf_Parms, bpp__InDeltaTime__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::NewProp_bpp__MyGeometry__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__MyGeometry__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainUI_C__pf3053510930_eventbpf__Tick__pf_Parms, bpp__MyGeometry__pf), Z_Construct_UScriptStruct_FGeometry, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::NewProp_bpp__InDeltaTime__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::NewProp_bpp__MyGeometry__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "User Interface" },
		{ "CppFromBpEvent", "" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Tick" },
		{ "ToolTip", "Ticks this widget.  Override in derived classes, but always call the parent implementation.@param  MyGeometry The space allotted for this widget@param  InDeltaTime  Real time passed since last tick" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMainUI_C__pf3053510930, "Tick", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C08, sizeof(MainUI_C__pf3053510930_eventbpf__Tick__pf_Parms), Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf()
	{
		UObject* Outer = Z_Construct_UClass_UMainUI_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Tick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMainUI_C__pf3053510930_NoRegister()
	{
		return UMainUI_C__pf3053510930::StaticClass();
	}
	struct Z_Construct_UClass_UMainUI_C__pf3053510930_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_InDeltaTime__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_Event_InDeltaTime__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_MyGeometry__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_Event_MyGeometry__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_CreateDelegate_OutputDelegate__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_b0l__K2Node_CreateDelegate_OutputDelegate__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_619__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_619__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_123__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_123__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_56__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_56__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Speedometer__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Speedometer__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ProgressBar_0__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__ProgressBar_0__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Pointer__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Pointer__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__InitInfo__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__InitInfo__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_202__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_202__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_46__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_46__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__FuelBox__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__FuelBox__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DistanceBox__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__DistanceBox__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__CoinsBox__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__CoinsBox__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Circle__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Circle__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Construct__pf, "Construct" }, // 973428882
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__EventxHandleGameStarted__pfT, "Event HandleGameStarted" }, // 2686293447
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetCarSpeed__pf, "GetCarSpeed" }, // 1459955102
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetColorAndOpacity_0__pf, "GetColorAndOpacity_0" }, // 3158400269
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetPercent_0__pf, "GetPercent_0" }, // 137800068
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_0__pf, "GetText_0" }, // 1036521049
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_1__pf, "GetText_1" }, // 1984804379
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__GetText_2__pf, "GetText_2" }, // 2864938717
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__HandleGameStarted__pf, "HandleGameStarted" }, // 1335887691
		{ &Z_Construct_UFunction_UMainUI_C__pf3053510930_bpf__Tick__pf, "Tick" }, // 3194658644
		{ &Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature, "Event GameStarted__DelegateSignature" }, // 2591847740
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MainUI__pf3053510930.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "MainUI_C" },
		{ "ReplaceConverted", "/Game/Blueprints/UI/MainUI.MainUI_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess" },
	};
#endif
	void Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj)
	{
		((UMainUI_C__pf3053510930*)Obj)->b0l__K2Node_DynamicCast_bSuccess__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UMainUI_C__pf3053510930), &Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsMaluchy_Game_Mode" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsMaluchy_Game_Mode", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf), Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_InDeltaTime__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "K2Node_Event_InDeltaTime" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_InDeltaTime__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_Event_InDeltaTime", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, b0l__K2Node_Event_InDeltaTime__pf), METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_InDeltaTime__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_InDeltaTime__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_MyGeometry__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "K2Node_Event_MyGeometry" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_MyGeometry__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_Event_MyGeometry", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, b0l__K2Node_Event_MyGeometry__pf), Z_Construct_UScriptStruct_FGeometry, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_MyGeometry__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_MyGeometry__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_CreateDelegate_OutputDelegate__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "K2Node_CreateDelegate_OutputDelegate" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_CreateDelegate_OutputDelegate__pf = { UE4CodeGen_Private::EPropertyClass::Delegate, "K2Node_CreateDelegate_OutputDelegate", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000282000, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, b0l__K2Node_CreateDelegate_OutputDelegate__pf), Z_Construct_UDelegateFunction_UMainUI_C__pf3053510930_EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_CreateDelegate_OutputDelegate__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_CreateDelegate_OutputDelegate__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_619__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_619" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_619" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_619__pf = { UE4CodeGen_Private::EPropertyClass::Object, "TextBlock_619", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000080008, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__TextBlock_619__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_619__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_619__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_123__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_123" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_123" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_123__pf = { UE4CodeGen_Private::EPropertyClass::Object, "TextBlock_123", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000080008, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__TextBlock_123__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_123__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_123__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_56__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_56" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_56" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_56__pf = { UE4CodeGen_Private::EPropertyClass::Object, "TextBlock_56", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000080008, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__TextBlock_56__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_56__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_56__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_1__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_1" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "TextBlock_1", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000080008, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__TextBlock_1__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Speedometer__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "Speedometer" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Speedometer" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Speedometer__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Speedometer", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__Speedometer__pf), Z_Construct_UClass_UCanvasPanel_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Speedometer__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Speedometer__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__ProgressBar_0__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "ProgressBar_0" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "ProgressBar_0" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__ProgressBar_0__pf = { UE4CodeGen_Private::EPropertyClass::Object, "ProgressBar_0", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__ProgressBar_0__pf), Z_Construct_UClass_UProgressBar_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__ProgressBar_0__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__ProgressBar_0__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Pointer__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "Pointer" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Pointer" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Pointer__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Pointer", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__Pointer__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Pointer__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Pointer__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__InitInfo__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "InitInfo" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "InitInfo" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__InitInfo__pf = { UE4CodeGen_Private::EPropertyClass::Object, "InitInfo", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__InitInfo__pf), Z_Construct_UClass_UCanvasPanel_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__InitInfo__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__InitInfo__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_202__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "Image_202" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Image_202" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_202__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Image_202", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__Image_202__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_202__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_202__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_46__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "Image_46" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Image_46" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_46__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Image_46", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__Image_46__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_46__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_46__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__FuelBox__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "FuelBox" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "FuelBox" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__FuelBox__pf = { UE4CodeGen_Private::EPropertyClass::Object, "FuelBox", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__FuelBox__pf), Z_Construct_UClass_UHorizontalBox_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__FuelBox__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__FuelBox__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__DistanceBox__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "DistanceBox" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "DistanceBox" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__DistanceBox__pf = { UE4CodeGen_Private::EPropertyClass::Object, "DistanceBox", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__DistanceBox__pf), Z_Construct_UClass_UHorizontalBox_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__DistanceBox__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__DistanceBox__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__CoinsBox__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "CoinsBox" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "CoinsBox" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__CoinsBox__pf = { UE4CodeGen_Private::EPropertyClass::Object, "CoinsBox", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__CoinsBox__pf), Z_Construct_UClass_UHorizontalBox_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__CoinsBox__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__CoinsBox__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Circle__pf_MetaData[] = {
		{ "Category", "MainUI" },
		{ "DisplayName", "Circle" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MainUI__pf3053510930.h" },
		{ "OverrideNativeName", "Circle" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Circle__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Circle", RF_Public|RF_Transient, (EPropertyFlags)0x001000000008000c, 1, nullptr, STRUCT_OFFSET(UMainUI_C__pf3053510930, bpv__Circle__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Circle__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Circle__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_InDeltaTime__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_Event_MyGeometry__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_b0l__K2Node_CreateDelegate_OutputDelegate__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_619__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_123__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_56__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__TextBlock_1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Speedometer__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__ProgressBar_0__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Pointer__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__InitInfo__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_202__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Image_46__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__FuelBox__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__DistanceBox__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__CoinsBox__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::NewProp_bpv__Circle__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMainUI_C__pf3053510930>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::ClassParams = {
		&UMainUI_C__pf3053510930::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00A010A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMainUI_C__pf3053510930()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/UI/MainUI"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("MainUI_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMainUI_C__pf3053510930_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(UMainUI_C__pf3053510930, TEXT("MainUI_C"), 1223646990);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMainUI_C__pf3053510930(Z_Construct_UClass_UMainUI_C__pf3053510930, &UMainUI_C__pf3053510930::StaticClass, TEXT("/Game/Blueprints/UI/MainUI"), TEXT("MainUI_C"), true, TEXT("/Game/Blueprints/UI/MainUI"), TEXT("/Game/Blueprints/UI/MainUI.MainUI_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMainUI_C__pf3053510930);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
