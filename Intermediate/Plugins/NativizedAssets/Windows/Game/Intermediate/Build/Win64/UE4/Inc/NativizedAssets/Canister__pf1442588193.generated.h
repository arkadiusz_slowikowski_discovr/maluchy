// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef NATIVIZEDASSETS_Canister__pf1442588193_generated_h
#error "Canister__pf1442588193.generated.h already included, missing '#pragma once' in Canister__pf1442588193.h"
#endif
#define NATIVIZEDASSETS_Canister__pf1442588193_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__Rotate__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Rotate__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorBeginOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorBeginOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__Rotate__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Rotate__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorBeginOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorBeginOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_EVENT_PARMS \
	struct Canister_C__pf1442588193_eventbpf__ReceiveActorBeginOverlap__pf_Parms \
	{ \
		AActor* bpp__OtherActor__pf; \
	}; \
	struct Canister_C__pf1442588193_eventbpf__ReceiveTick__pf_Parms \
	{ \
		float bpp__DeltaSeconds__pf; \
	};


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf); \
 \
	void eventbpf__ReceiveBeginPlay__pf(); \
 \
	void eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf); \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACanister_C__pf1442588193(); \
	friend struct Z_Construct_UClass_ACanister_C__pf1442588193_Statics; \
public: \
	DECLARE_CLASS(ACanister_C__pf1442588193, ARotatable_C__pf2962636854, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/Collectables/Canister/Canister"), NO_API) \
	DECLARE_SERIALIZER(ACanister_C__pf1442588193)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACanister_C__pf1442588193(); \
	friend struct Z_Construct_UClass_ACanister_C__pf1442588193_Statics; \
public: \
	DECLARE_CLASS(ACanister_C__pf1442588193, ARotatable_C__pf2962636854, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/Collectables/Canister/Canister"), NO_API) \
	DECLARE_SERIALIZER(ACanister_C__pf1442588193)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACanister_C__pf1442588193(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACanister_C__pf1442588193) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACanister_C__pf1442588193); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACanister_C__pf1442588193); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACanister_C__pf1442588193(ACanister_C__pf1442588193&&); \
	NO_API ACanister_C__pf1442588193(const ACanister_C__pf1442588193&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACanister_C__pf1442588193(ACanister_C__pf1442588193&&); \
	NO_API ACanister_C__pf1442588193(const ACanister_C__pf1442588193&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACanister_C__pf1442588193); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACanister_C__pf1442588193); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACanister_C__pf1442588193)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_8_PROLOG \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_EVENT_PARMS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h_12_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Canister__pf1442588193_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
