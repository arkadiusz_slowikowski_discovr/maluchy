// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NATIVIZEDASSETS_MaluchyGameMode__pf2132744816_generated_h
#error "MaluchyGameMode__pf2132744816.generated.h already included, missing '#pragma once' in MaluchyGameMode__pf2132744816.h"
#endif
#define NATIVIZEDASSETS_MaluchyGameMode__pf2132744816_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_15_DELEGATE \
static inline void FEventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate_DelegateWrapper(const FMulticastScriptDelegate& EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate) \
{ \
	EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__RotateAllRotatables__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaTime__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RotateAllRotatables__pf(Z_Param_bpp__DeltaTime__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ResetGame__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ResetGame__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GameEnded__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GameEnded__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ResetEnviroment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ResetEnviroment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__RotateAllRotatables__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaTime__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RotateAllRotatables__pf(Z_Param_bpp__DeltaTime__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ResetGame__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ResetGame__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GameEnded__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GameEnded__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ResetEnviroment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ResetEnviroment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_EVENT_PARMS \
	struct MaluchyGameMode_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms \
	{ \
		float bpp__DeltaSeconds__pf; \
	};


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveBeginPlay__pf(); \
 \
	void eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf); \
 \
	void eventbpf__UserConstructionScript__pf(); \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMaluchyGameMode_C__pf2132744816(); \
	friend struct Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics; \
public: \
	DECLARE_CLASS(AMaluchyGameMode_C__pf2132744816, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Game/Blueprints/MaluchyGameMode"), NO_API) \
	DECLARE_SERIALIZER(AMaluchyGameMode_C__pf2132744816)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAMaluchyGameMode_C__pf2132744816(); \
	friend struct Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics; \
public: \
	DECLARE_CLASS(AMaluchyGameMode_C__pf2132744816, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Game/Blueprints/MaluchyGameMode"), NO_API) \
	DECLARE_SERIALIZER(AMaluchyGameMode_C__pf2132744816)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMaluchyGameMode_C__pf2132744816(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMaluchyGameMode_C__pf2132744816) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMaluchyGameMode_C__pf2132744816); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMaluchyGameMode_C__pf2132744816); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMaluchyGameMode_C__pf2132744816(AMaluchyGameMode_C__pf2132744816&&); \
	NO_API AMaluchyGameMode_C__pf2132744816(const AMaluchyGameMode_C__pf2132744816&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMaluchyGameMode_C__pf2132744816(AMaluchyGameMode_C__pf2132744816&&); \
	NO_API AMaluchyGameMode_C__pf2132744816(const AMaluchyGameMode_C__pf2132744816&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMaluchyGameMode_C__pf2132744816); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMaluchyGameMode_C__pf2132744816); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMaluchyGameMode_C__pf2132744816)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_9_PROLOG \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_EVENT_PARMS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h_13_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchyGameMode__pf2132744816_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
