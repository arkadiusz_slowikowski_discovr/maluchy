// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/Coin__pf2962636854.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCoin__pf2962636854() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ACoin_C__pf2962636854_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ACoin_C__pf2962636854();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARotatable_C__pf2962636854();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf = FName(TEXT("ReceiveActorBeginOverlap"));
	void ACoin_C__pf2962636854::eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf)
	{
		Coin_C__pf2962636854_eventbpf__ReceiveActorBeginOverlap__pf_Parms Parms;
		Parms.bpp__OtherActor__pf=bpp__OtherActor__pf;
		ProcessEvent(FindFunctionChecked(NAME_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf),&Parms);
	}
	static FName NAME_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void ACoin_C__pf2962636854::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_ACoin_C__pf2962636854_bpf__ReceiveTick__pf = FName(TEXT("ReceiveTick"));
	void ACoin_C__pf2962636854::eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
	{
		Coin_C__pf2962636854_eventbpf__ReceiveTick__pf_Parms Parms;
		Parms.bpp__DeltaSeconds__pf=bpp__DeltaSeconds__pf;
		ProcessEvent(FindFunctionChecked(NAME_ACoin_C__pf2962636854_bpf__ReceiveTick__pf),&Parms);
	}
	void ACoin_C__pf2962636854::StaticRegisterNativesACoin_C__pf2962636854()
	{
		UClass* Class = ACoin_C__pf2962636854::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ReceiveActorBeginOverlap", &ACoin_C__pf2962636854::execbpf__ReceiveActorBeginOverlap__pf },
			{ "ReceiveBeginPlay", &ACoin_C__pf2962636854::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveTick", &ACoin_C__pf2962636854::execbpf__ReceiveTick__pf },
			{ "Rotate", &ACoin_C__pf2962636854::execbpf__Rotate__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__OtherActor__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__OtherActor__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Coin_C__pf2962636854_eventbpf__ReceiveActorBeginOverlap__pf_Parms, bpp__OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "Collision" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "ActorBeginOverlap" },
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "ReceiveActorBeginOverlap" },
		{ "ToolTip", "Event when this actor overlaps another actor, for example a player walking into a trigger.For events when objects have a blocking collision, for example a player hitting a wall, see 'Hit' events.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACoin_C__pf2962636854, "ReceiveActorBeginOverlap", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(Coin_C__pf2962636854_eventbpf__ReceiveActorBeginOverlap__pf_Parms), Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf()
	{
		UObject* Outer = Z_Construct_UClass_ACoin_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveActorBeginOverlap") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACoin_C__pf2962636854, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_ACoin_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaSeconds__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaSeconds__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Coin_C__pf2962636854_eventbpf__ReceiveTick__pf_Parms, bpp__DeltaSeconds__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Tick" },
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "ReceiveTick" },
		{ "ToolTip", "Event called every frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACoin_C__pf2962636854, "ReceiveTick", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(Coin_C__pf2962636854_eventbpf__ReceiveTick__pf_Parms), Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf()
	{
		UObject* Outer = Z_Construct_UClass_ACoin_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveTick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics
	{
		struct Coin_C__pf2962636854_eventbpf__Rotate__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Coin_C__pf2962636854_eventbpf__Rotate__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "Rotate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACoin_C__pf2962636854, "Rotate", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(Coin_C__pf2962636854_eventbpf__Rotate__pf_Parms), Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf()
	{
		UObject* Outer = Z_Construct_UClass_ACoin_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Rotate") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACoin_C__pf2962636854_NoRegister()
	{
		return ACoin_C__pf2962636854::StaticClass();
	}
	struct Z_Construct_UClass_ACoin_C__pf2962636854_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_MetaData[];
#endif
		static void NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b1l__K2Node_DynamicCast_bSuccess__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b1l__K2Node_Event_DeltaSeconds__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b1l__K2Node_Event_DeltaSeconds__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b1l__K2Node_Event_OtherActor__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b1l__K2Node_Event_OtherActor__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACoin_C__pf2962636854_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ARotatable_C__pf2962636854,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACoin_C__pf2962636854_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf, "ReceiveActorBeginOverlap" }, // 922480462
		{ &Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 2346115050
		{ &Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__ReceiveTick__pf, "ReceiveTick" }, // 719200111
		{ &Z_Construct_UFunction_ACoin_C__pf2962636854_bpf__Rotate__pf, "Rotate" }, // 1115341174
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_C__pf2962636854_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Coin__pf2962636854.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "Coin_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Collectables/Coin.Coin_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess" },
	};
#endif
	void Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj)
	{
		((ACoin_C__pf2962636854*)Obj)->b1l__K2Node_DynamicCast_bSuccess__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ACoin_C__pf2962636854), &Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsDisco_Maluch" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsDisco_Maluch", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ACoin_C__pf2962636854, b1l__K2Node_DynamicCast_AsDisco_Maluch__pf), Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "K2Node_Event_DeltaSeconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_Event_DeltaSeconds", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ACoin_C__pf2962636854, b1l__K2Node_Event_DeltaSeconds__pf), METADATA_PARAMS(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "K2Node_Event_OtherActor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_OtherActor", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ACoin_C__pf2962636854, b1l__K2Node_Event_OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_bpv__StaticMesh__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Coin__pf2962636854.h" },
		{ "OverrideNativeName", "StaticMesh" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_bpv__StaticMesh__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ACoin_C__pf2962636854, bpv__StaticMesh__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_bpv__StaticMesh__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_bpv__StaticMesh__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACoin_C__pf2962636854_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_C__pf2962636854_Statics::NewProp_bpv__StaticMesh__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACoin_C__pf2962636854_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACoin_C__pf2962636854>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACoin_C__pf2962636854_Statics::ClassParams = {
		&ACoin_C__pf2962636854::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ACoin_C__pf2962636854_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACoin_C__pf2962636854_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACoin_C__pf2962636854()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Collectables/Coin"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("Coin_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACoin_C__pf2962636854_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ACoin_C__pf2962636854, TEXT("Coin_C"), 1766810556);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACoin_C__pf2962636854(Z_Construct_UClass_ACoin_C__pf2962636854, &ACoin_C__pf2962636854::StaticClass, TEXT("/Game/Blueprints/Collectables/Coin"), TEXT("Coin_C"), true, TEXT("/Game/Blueprints/Collectables/Coin"), TEXT("/Game/Blueprints/Collectables/Coin.Coin_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACoin_C__pf2962636854);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
