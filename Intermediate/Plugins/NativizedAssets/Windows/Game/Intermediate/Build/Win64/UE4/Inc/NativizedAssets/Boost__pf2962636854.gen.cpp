// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/Boost__pf2962636854.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBoost__pf2962636854() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABoost_C__pf2962636854_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABoost_C__pf2962636854();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
// End Cross Module References
	static FName NAME_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf = FName(TEXT("ReceiveActorBeginOverlap"));
	void ABoost_C__pf2962636854::eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf)
	{
		Boost_C__pf2962636854_eventbpf__ReceiveActorBeginOverlap__pf_Parms Parms;
		Parms.bpp__OtherActor__pf=bpp__OtherActor__pf;
		ProcessEvent(FindFunctionChecked(NAME_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf),&Parms);
	}
	static FName NAME_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void ABoost_C__pf2962636854::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf),NULL);
	}
	void ABoost_C__pf2962636854::StaticRegisterNativesABoost_C__pf2962636854()
	{
		UClass* Class = ABoost_C__pf2962636854::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ReceiveActorBeginOverlap", &ABoost_C__pf2962636854::execbpf__ReceiveActorBeginOverlap__pf },
			{ "UserConstructionScript", &ABoost_C__pf2962636854::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__OtherActor__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__OtherActor__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Boost_C__pf2962636854_eventbpf__ReceiveActorBeginOverlap__pf_Parms, bpp__OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "Collision" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "ActorBeginOverlap" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "ReceiveActorBeginOverlap" },
		{ "ToolTip", "Event when this actor overlaps another actor, for example a player walking into a trigger.For events when objects have a blocking collision, for example a player hitting a wall, see 'Hit' events.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABoost_C__pf2962636854, "ReceiveActorBeginOverlap", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(Boost_C__pf2962636854_eventbpf__ReceiveActorBeginOverlap__pf_Parms), Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABoost_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveActorBeginOverlap") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABoost_C__pf2962636854, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABoost_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABoost_C__pf2962636854_NoRegister()
	{
		return ABoost_C__pf2962636854::StaticClass();
	}
	struct Z_Construct_UClass_ABoost_C__pf2962636854_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_Event_OtherActor__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__BoostValue__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__BoostValue__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DefaultSceneRoot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__DefaultSceneRoot__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__arrow_Box__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__arrow_Box__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__arrow_Box2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__arrow_Box2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__arrow_Box3__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__arrow_Box3__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__arrow_Box4__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__arrow_Box4__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__arrow_Box5__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__arrow_Box5__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__arrow_Box6__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__arrow_Box6__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__arrow_Box7__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__arrow_Box7__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__arrow_Box1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__arrow_Box1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Box__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Box__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABoost_C__pf2962636854_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABoost_C__pf2962636854_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__ReceiveActorBeginOverlap__pf, "ReceiveActorBeginOverlap" }, // 4261474122
		{ &Z_Construct_UFunction_ABoost_C__pf2962636854_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 3106964944
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Boost__pf2962636854.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "Boost_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Collectables/Boost.Boost_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess" },
	};
#endif
	void Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj)
	{
		((ABoost_C__pf2962636854*)Obj)->b0l__K2Node_DynamicCast_bSuccess__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ABoost_C__pf2962636854), &Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsDisco_Maluch" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsDisco_Maluch", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, b0l__K2Node_DynamicCast_AsDisco_Maluch__pf), Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "K2Node_Event_OtherActor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_OtherActor", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, b0l__K2Node_Event_OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__BoostValue__pf_MetaData[] = {
		{ "Category", "Vars" },
		{ "DisplayName", "Boost Value" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "BoostValue" },
		{ "tooltip", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__BoostValue__pf = { UE4CodeGen_Private::EPropertyClass::Float, "BoostValue", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__BoostValue__pf), METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__BoostValue__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__BoostValue__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "DefaultSceneRoot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf = { UE4CodeGen_Private::EPropertyClass::Object, "DefaultSceneRoot", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__DefaultSceneRoot__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "arrow_Box" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box__pf = { UE4CodeGen_Private::EPropertyClass::Object, "arrow_Box", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__arrow_Box__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box2__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "arrow_Box2" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box2__pf = { UE4CodeGen_Private::EPropertyClass::Object, "arrow_Box2", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__arrow_Box2__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box2__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box3__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "arrow_Box3" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box3__pf = { UE4CodeGen_Private::EPropertyClass::Object, "arrow_Box3", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__arrow_Box3__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box3__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box3__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box4__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "arrow_Box4" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box4__pf = { UE4CodeGen_Private::EPropertyClass::Object, "arrow_Box4", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__arrow_Box4__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box4__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box4__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box5__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "arrow_Box5" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box5__pf = { UE4CodeGen_Private::EPropertyClass::Object, "arrow_Box5", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__arrow_Box5__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box5__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box5__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box6__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "arrow_Box6" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box6__pf = { UE4CodeGen_Private::EPropertyClass::Object, "arrow_Box6", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__arrow_Box6__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box6__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box6__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box7__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "arrow_Box7" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box7__pf = { UE4CodeGen_Private::EPropertyClass::Object, "arrow_Box7", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__arrow_Box7__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box7__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box7__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box1__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "arrow_Box1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "arrow_Box1", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__arrow_Box1__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__Box__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Boost__pf2962636854.h" },
		{ "OverrideNativeName", "Box" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__Box__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Box", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ABoost_C__pf2962636854, bpv__Box__pf), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__Box__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__Box__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABoost_C__pf2962636854_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__BoostValue__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box3__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box4__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box5__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box6__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box7__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__arrow_Box1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_C__pf2962636854_Statics::NewProp_bpv__Box__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABoost_C__pf2962636854_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABoost_C__pf2962636854>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABoost_C__pf2962636854_Statics::ClassParams = {
		&ABoost_C__pf2962636854::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ABoost_C__pf2962636854_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ABoost_C__pf2962636854_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABoost_C__pf2962636854()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Collectables/Boost"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("Boost_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABoost_C__pf2962636854_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ABoost_C__pf2962636854, TEXT("Boost_C"), 4230143741);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABoost_C__pf2962636854(Z_Construct_UClass_ABoost_C__pf2962636854, &ABoost_C__pf2962636854::StaticClass, TEXT("/Game/Blueprints/Collectables/Boost"), TEXT("Boost_C"), true, TEXT("/Game/Blueprints/Collectables/Boost"), TEXT("/Game/Blueprints/Collectables/Boost.Boost_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABoost_C__pf2962636854);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
