// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/Canister__pf1442588193.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCanister__pf1442588193() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ACanister_C__pf1442588193_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ACanister_C__pf1442588193();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARotatable_C__pf2962636854();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf = FName(TEXT("ReceiveActorBeginOverlap"));
	void ACanister_C__pf1442588193::eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf)
	{
		Canister_C__pf1442588193_eventbpf__ReceiveActorBeginOverlap__pf_Parms Parms;
		Parms.bpp__OtherActor__pf=bpp__OtherActor__pf;
		ProcessEvent(FindFunctionChecked(NAME_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf),&Parms);
	}
	static FName NAME_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void ACanister_C__pf1442588193::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_ACanister_C__pf1442588193_bpf__ReceiveTick__pf = FName(TEXT("ReceiveTick"));
	void ACanister_C__pf1442588193::eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
	{
		Canister_C__pf1442588193_eventbpf__ReceiveTick__pf_Parms Parms;
		Parms.bpp__DeltaSeconds__pf=bpp__DeltaSeconds__pf;
		ProcessEvent(FindFunctionChecked(NAME_ACanister_C__pf1442588193_bpf__ReceiveTick__pf),&Parms);
	}
	void ACanister_C__pf1442588193::StaticRegisterNativesACanister_C__pf1442588193()
	{
		UClass* Class = ACanister_C__pf1442588193::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ReceiveActorBeginOverlap", &ACanister_C__pf1442588193::execbpf__ReceiveActorBeginOverlap__pf },
			{ "ReceiveBeginPlay", &ACanister_C__pf1442588193::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveTick", &ACanister_C__pf1442588193::execbpf__ReceiveTick__pf },
			{ "Rotate", &ACanister_C__pf1442588193::execbpf__Rotate__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__OtherActor__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__OtherActor__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Canister_C__pf1442588193_eventbpf__ReceiveActorBeginOverlap__pf_Parms, bpp__OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "Collision" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "ActorBeginOverlap" },
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "ReceiveActorBeginOverlap" },
		{ "ToolTip", "Event when this actor overlaps another actor, for example a player walking into a trigger.For events when objects have a blocking collision, for example a player hitting a wall, see 'Hit' events.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACanister_C__pf1442588193, "ReceiveActorBeginOverlap", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(Canister_C__pf1442588193_eventbpf__ReceiveActorBeginOverlap__pf_Parms), Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf()
	{
		UObject* Outer = Z_Construct_UClass_ACanister_C__pf1442588193();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveActorBeginOverlap") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACanister_C__pf1442588193, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_ACanister_C__pf1442588193();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaSeconds__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaSeconds__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Canister_C__pf1442588193_eventbpf__ReceiveTick__pf_Parms, bpp__DeltaSeconds__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Tick" },
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "ReceiveTick" },
		{ "ToolTip", "Event called every frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACanister_C__pf1442588193, "ReceiveTick", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(Canister_C__pf1442588193_eventbpf__ReceiveTick__pf_Parms), Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf()
	{
		UObject* Outer = Z_Construct_UClass_ACanister_C__pf1442588193();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveTick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics
	{
		struct Canister_C__pf1442588193_eventbpf__Rotate__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Canister_C__pf1442588193_eventbpf__Rotate__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "Rotate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACanister_C__pf1442588193, "Rotate", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(Canister_C__pf1442588193_eventbpf__Rotate__pf_Parms), Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf()
	{
		UObject* Outer = Z_Construct_UClass_ACanister_C__pf1442588193();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Rotate") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACanister_C__pf1442588193_NoRegister()
	{
		return ACanister_C__pf1442588193::StaticClass();
	}
	struct Z_Construct_UClass_ACanister_C__pf1442588193_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_MetaData[];
#endif
		static void NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b1l__K2Node_DynamicCast_bSuccess__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b1l__K2Node_Event_DeltaSeconds__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b1l__K2Node_Event_DeltaSeconds__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b1l__K2Node_Event_OtherActor__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b1l__K2Node_Event_OtherActor__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__canister_Reservekanister_001__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__canister_Reservekanister_001__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__canister_Group_001__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__canister_Group_001__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__canister_Group_003__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__canister_Group_003__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACanister_C__pf1442588193_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ARotatable_C__pf2962636854,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACanister_C__pf1442588193_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveActorBeginOverlap__pf, "ReceiveActorBeginOverlap" }, // 1272667729
		{ &Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 787301684
		{ &Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__ReceiveTick__pf, "ReceiveTick" }, // 3716801904
		{ &Z_Construct_UFunction_ACanister_C__pf1442588193_bpf__Rotate__pf, "Rotate" }, // 141730155
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACanister_C__pf1442588193_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Canister__pf1442588193.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "Canister_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Collectables/Canister/Canister.Canister_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess" },
	};
#endif
	void Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj)
	{
		((ACanister_C__pf1442588193*)Obj)->b1l__K2Node_DynamicCast_bSuccess__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ACanister_C__pf1442588193), &Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsDisco_Maluch" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsDisco_Maluch", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ACanister_C__pf1442588193, b1l__K2Node_DynamicCast_AsDisco_Maluch__pf), Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "K2Node_Event_DeltaSeconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_Event_DeltaSeconds", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ACanister_C__pf1442588193, b1l__K2Node_Event_DeltaSeconds__pf), METADATA_PARAMS(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "K2Node_Event_OtherActor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_OtherActor", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ACanister_C__pf1442588193, b1l__K2Node_Event_OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Reservekanister_001__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "canister_Reservekanister_001" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Reservekanister_001__pf = { UE4CodeGen_Private::EPropertyClass::Object, "canister_Reservekanister_001", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ACanister_C__pf1442588193, bpv__canister_Reservekanister_001__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Reservekanister_001__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Reservekanister_001__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_001__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "canister_Group_001" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_001__pf = { UE4CodeGen_Private::EPropertyClass::Object, "canister_Group_001", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ACanister_C__pf1442588193, bpv__canister_Group_001__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_001__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_001__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_003__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Canister__pf1442588193.h" },
		{ "OverrideNativeName", "canister_Group_003" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_003__pf = { UE4CodeGen_Private::EPropertyClass::Object, "canister_Group_003", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ACanister_C__pf1442588193, bpv__canister_Group_003__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_003__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_003__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACanister_C__pf1442588193_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_bSuccess__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_DynamicCast_AsDisco_Maluch__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_DeltaSeconds__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_b1l__K2Node_Event_OtherActor__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Reservekanister_001__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_001__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACanister_C__pf1442588193_Statics::NewProp_bpv__canister_Group_003__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACanister_C__pf1442588193_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACanister_C__pf1442588193>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACanister_C__pf1442588193_Statics::ClassParams = {
		&ACanister_C__pf1442588193::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ACanister_C__pf1442588193_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACanister_C__pf1442588193_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACanister_C__pf1442588193()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Collectables/Canister/Canister"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("Canister_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACanister_C__pf1442588193_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ACanister_C__pf1442588193, TEXT("Canister_C"), 1170173309);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACanister_C__pf1442588193(Z_Construct_UClass_ACanister_C__pf1442588193, &ACanister_C__pf1442588193::StaticClass, TEXT("/Game/Blueprints/Collectables/Canister/Canister"), TEXT("Canister_C"), true, TEXT("/Game/Blueprints/Collectables/Canister/Canister"), TEXT("/Game/Blueprints/Collectables/Canister/Canister.Canister_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACanister_C__pf1442588193);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
