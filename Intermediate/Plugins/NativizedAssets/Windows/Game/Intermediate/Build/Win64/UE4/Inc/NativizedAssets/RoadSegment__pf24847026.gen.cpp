// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/RoadSegment__pf24847026.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRoadSegment__pf24847026() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARoadSegment_C__pf24847026_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARoadSegment_C__pf24847026();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
// End Cross Module References
	static FName NAME_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf = FName(TEXT("ReceiveActorBeginOverlap"));
	void ARoadSegment_C__pf24847026::eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf)
	{
		RoadSegment_C__pf24847026_eventbpf__ReceiveActorBeginOverlap__pf_Parms Parms;
		Parms.bpp__OtherActor__pf=bpp__OtherActor__pf;
		ProcessEvent(FindFunctionChecked(NAME_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf),&Parms);
	}
	static FName NAME_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf = FName(TEXT("ReceiveActorEndOverlap"));
	void ARoadSegment_C__pf24847026::eventbpf__ReceiveActorEndOverlap__pf(AActor* bpp__OtherActor__pf)
	{
		RoadSegment_C__pf24847026_eventbpf__ReceiveActorEndOverlap__pf_Parms Parms;
		Parms.bpp__OtherActor__pf=bpp__OtherActor__pf;
		ProcessEvent(FindFunctionChecked(NAME_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf),&Parms);
	}
	static FName NAME_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void ARoadSegment_C__pf24847026::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf = FName(TEXT("ReceiveTick"));
	void ARoadSegment_C__pf24847026::eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
	{
		RoadSegment_C__pf24847026_eventbpf__ReceiveTick__pf_Parms Parms;
		Parms.bpp__DeltaSeconds__pf=bpp__DeltaSeconds__pf;
		ProcessEvent(FindFunctionChecked(NAME_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf),&Parms);
	}
	static FName NAME_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void ARoadSegment_C__pf24847026::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf),NULL);
	}
	void ARoadSegment_C__pf24847026::StaticRegisterNativesARoadSegment_C__pf24847026()
	{
		UClass* Class = ARoadSegment_C__pf24847026::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddToResetables", &ARoadSegment_C__pf24847026::execbpf__AddToResetables__pf },
			{ "DestroyRoadSegment", &ARoadSegment_C__pf24847026::execbpf__DestroyRoadSegment__pf },
			{ "GetOriginForNext", &ARoadSegment_C__pf24847026::execbpf__GetOriginForNext__pf },
			{ "GetSpawnableLocation", &ARoadSegment_C__pf24847026::execbpf__GetSpawnableLocation__pf },
			{ "IsActorMainCar", &ARoadSegment_C__pf24847026::execbpf__IsActorMainCar__pf },
			{ "MoveSegment", &ARoadSegment_C__pf24847026::execbpf__MoveSegment__pf },
			{ "PutAnotherSegment", &ARoadSegment_C__pf24847026::execbpf__PutAnotherSegment__pf },
			{ "RandomizeObjects", &ARoadSegment_C__pf24847026::execbpf__RandomizeObjects__pf },
			{ "ReceiveActorBeginOverlap", &ARoadSegment_C__pf24847026::execbpf__ReceiveActorBeginOverlap__pf },
			{ "ReceiveActorEndOverlap", &ARoadSegment_C__pf24847026::execbpf__ReceiveActorEndOverlap__pf },
			{ "ReceiveBeginPlay", &ARoadSegment_C__pf24847026::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveTick", &ARoadSegment_C__pf24847026::execbpf__ReceiveTick__pf },
			{ "RemoveFromResetables", &ARoadSegment_C__pf24847026::execbpf__RemoveFromResetables__pf },
			{ "ResetLocation", &ARoadSegment_C__pf24847026::execbpf__ResetLocation__pf },
			{ "SpawnChild", &ARoadSegment_C__pf24847026::execbpf__SpawnChild__pf },
			{ "UserConstructionScript", &ARoadSegment_C__pf24847026::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "AddToResetables" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "AddToResetables", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("AddToResetables") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "DestroyRoadSegment" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "DestroyRoadSegment", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("DestroyRoadSegment") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics
	{
		struct RoadSegment_C__pf24847026_eventbpf__GetOriginForNext__pf_Parms
		{
			FVector bpp__Origin__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Origin__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::NewProp_bpp__Origin__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__Origin__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__GetOriginForNext__pf_Parms, bpp__Origin__pf), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::NewProp_bpp__Origin__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "GetOriginForNext" },
		{ "ToolTip", "out" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "GetOriginForNext", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04C20400, sizeof(RoadSegment_C__pf24847026_eventbpf__GetOriginForNext__pf_Parms), Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetOriginForNext") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics
	{
		struct RoadSegment_C__pf24847026_eventbpf__GetSpawnableLocation__pf_Parms
		{
			FVector bpp__Location__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Location__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::NewProp_bpp__Location__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__Location__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__GetSpawnableLocation__pf_Parms, bpp__Location__pf), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::NewProp_bpp__Location__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "GetSpawnableLocation" },
		{ "ToolTip", "out" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "GetSpawnableLocation", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04C20400, sizeof(RoadSegment_C__pf24847026_eventbpf__GetSpawnableLocation__pf_Parms), Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetSpawnableLocation") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics
	{
		struct RoadSegment_C__pf24847026_eventbpf__IsActorMainCar__pf_Parms
		{
			AActor* bpp__Actor__pf;
			bool bpp__Is__pf;
		};
		static void NewProp_bpp__Is__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpp__Is__pf;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__Actor__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::NewProp_bpp__Is__pf_SetBit(void* Obj)
	{
		((RoadSegment_C__pf24847026_eventbpf__IsActorMainCar__pf_Parms*)Obj)->bpp__Is__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::NewProp_bpp__Is__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "bpp__Is__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(RoadSegment_C__pf24847026_eventbpf__IsActorMainCar__pf_Parms), &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::NewProp_bpp__Is__pf_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::NewProp_bpp__Actor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__Actor__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__IsActorMainCar__pf_Parms, bpp__Actor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::NewProp_bpp__Is__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::NewProp_bpp__Actor__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "IsActorMainCar" },
		{ "ToolTip", "out" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "IsActorMainCar", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04420400, sizeof(RoadSegment_C__pf24847026_eventbpf__IsActorMainCar__pf_Parms), Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("IsActorMainCar") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "MoveSegment" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "MoveSegment", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("MoveSegment") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "PutAnotherSegment" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "PutAnotherSegment", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("PutAnotherSegment") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "RandomizeObjects" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "RandomizeObjects", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("RandomizeObjects") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__OtherActor__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__OtherActor__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__ReceiveActorBeginOverlap__pf_Parms, bpp__OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "Collision" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "ActorBeginOverlap" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "ReceiveActorBeginOverlap" },
		{ "ToolTip", "Event when this actor overlaps another actor, for example a player walking into a trigger.For events when objects have a blocking collision, for example a player hitting a wall, see 'Hit' events.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "ReceiveActorBeginOverlap", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(RoadSegment_C__pf24847026_eventbpf__ReceiveActorBeginOverlap__pf_Parms), Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveActorBeginOverlap") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__OtherActor__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::NewProp_bpp__OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__OtherActor__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__ReceiveActorEndOverlap__pf_Parms, bpp__OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::NewProp_bpp__OtherActor__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "Collision" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "ActorEndOverlap" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "ReceiveActorEndOverlap" },
		{ "ToolTip", "Event when an actor no longer overlaps another actor, and they have separated.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "ReceiveActorEndOverlap", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(RoadSegment_C__pf24847026_eventbpf__ReceiveActorEndOverlap__pf_Parms), Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveActorEndOverlap") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaSeconds__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaSeconds__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__ReceiveTick__pf_Parms, bpp__DeltaSeconds__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Tick" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "ReceiveTick" },
		{ "ToolTip", "Event called every frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "ReceiveTick", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(RoadSegment_C__pf24847026_eventbpf__ReceiveTick__pf_Parms), Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveTick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "RemoveFromResetables" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "RemoveFromResetables", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("RemoveFromResetables") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics
	{
		struct RoadSegment_C__pf24847026_eventbpf__ResetLocation__pf_Parms
		{
			float bpp__By__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__By__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::NewProp_bpp__By__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__By__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__ResetLocation__pf_Parms, bpp__By__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::NewProp_bpp__By__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "ResetLocation" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "ResetLocation", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(RoadSegment_C__pf24847026_eventbpf__ResetLocation__pf_Parms), Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ResetLocation") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics
	{
		struct RoadSegment_C__pf24847026_eventbpf__SpawnChild__pf_Parms
		{
			UClass* bpp__Class__pf;
			FTransform bpp__SpawnTransform__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__SpawnTransform__pf;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_bpp__Class__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::NewProp_bpp__SpawnTransform__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__SpawnTransform__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__SpawnChild__pf_Parms, bpp__SpawnTransform__pf), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::NewProp_bpp__Class__pf = { UE4CodeGen_Private::EPropertyClass::Class, "bpp__Class__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RoadSegment_C__pf24847026_eventbpf__SpawnChild__pf_Parms, bpp__Class__pf), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::NewProp_bpp__SpawnTransform__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::NewProp_bpp__Class__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "SpawnChild" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "SpawnChild", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04820400, sizeof(RoadSegment_C__pf24847026_eventbpf__SpawnChild__pf_Parms), Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("SpawnChild") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoadSegment_C__pf24847026, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARoadSegment_C__pf24847026();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ARoadSegment_C__pf24847026_NoRegister()
	{
		return ARoadSegment_C__pf24847026::StaticClass();
	}
	struct Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf_MetaData[];
#endif
		static void NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_Event_OtherActor__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_IsActorMainCar_Is__pf_MetaData[];
#endif
		static void NewProp_b0l__CallFunc_IsActorMainCar_Is__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__CallFunc_IsActorMainCar_Is__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_OtherActor1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_Event_OtherActor1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_Event_DeltaSeconds__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__NextSegments__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__NextSegments__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ObjectsPerSegment__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__ObjectsPerSegment__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__MinDistanceFromAnother__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__MinDistanceFromAnother__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ObjectsToRandomize__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_bpv__ObjectsToRandomize__pf;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_bpv__ObjectsToRandomize__pf_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__CurrObjects__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_bpv__CurrObjects__pf;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__CurrObjects__pf_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Previous__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Previous__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Next__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Next__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__IsStart__pf_MetaData[];
#endif
		static void NewProp_bpv__IsStart__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__IsStart__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DefaultSceneRoot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__DefaultSceneRoot__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Box__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Box__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Asphalt__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Asphalt__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Line0__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Line0__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Line1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Line1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SmolLine0__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SmolLine0__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SmolLine1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SmolLine1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Border0__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Border0__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Border1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Border1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__low_poly_buildings_Highrise_17__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__low_poly_buildings_Highrise_17__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__low_poly_buildings_Highrise_9__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__low_poly_buildings_Highrise_9__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__low_poly_buildings_Highrise_2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__low_poly_buildings_Highrise_2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__low_poly_buildings_Highrise_16__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__low_poly_buildings_Highrise_16__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__low_poly_buildings_Highrise_11__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__low_poly_buildings_Highrise_11__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh3__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh3__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh4__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh4__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh5__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh5__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh6__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh6__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Cube1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Cube1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh7__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh7__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Cube__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Cube__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__low_poly_buildings_Highrise_1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__low_poly_buildings_Highrise_1__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__AddToResetables__pf, "AddToResetables" }, // 2149446440
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__DestroyRoadSegment__pf, "DestroyRoadSegment" }, // 1160506119
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetOriginForNext__pf, "GetOriginForNext" }, // 1734579724
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__GetSpawnableLocation__pf, "GetSpawnableLocation" }, // 4049305904
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__IsActorMainCar__pf, "IsActorMainCar" }, // 1070109610
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__MoveSegment__pf, "MoveSegment" }, // 1937275576
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__PutAnotherSegment__pf, "PutAnotherSegment" }, // 2922682177
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RandomizeObjects__pf, "RandomizeObjects" }, // 776519001
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorBeginOverlap__pf, "ReceiveActorBeginOverlap" }, // 2148696414
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveActorEndOverlap__pf, "ReceiveActorEndOverlap" }, // 3862151262
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 3533450258
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ReceiveTick__pf, "ReceiveTick" }, // 1907098919
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__RemoveFromResetables__pf, "RemoveFromResetables" }, // 984544533
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__ResetLocation__pf, "ResetLocation" }, // 515204620
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__SpawnChild__pf, "SpawnChild" }, // 110695817
		{ &Z_Construct_UFunction_ARoadSegment_C__pf24847026_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 556139667
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "RoadSegment__pf24847026.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "RoadSegment_C" },
		{ "ReplaceConverted", "/Game/Blueprints/RoadSegment/RoadSegment.RoadSegment_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess" },
	};
#endif
	void Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj)
	{
		((ARoadSegment_C__pf24847026*)Obj)->b0l__K2Node_DynamicCast_bSuccess__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoadSegment_C__pf24847026), &Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsMaluchy_Game_Mode" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsMaluchy_Game_Mode", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf), Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "CallFunc_IsActorMainCar_Is1" },
	};
#endif
	void Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf_SetBit(void* Obj)
	{
		((ARoadSegment_C__pf24847026*)Obj)->b0l__CallFunc_IsActorMainCar_Is1__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "CallFunc_IsActorMainCar_Is1", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoadSegment_C__pf24847026), &Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "K2Node_Event_OtherActor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_OtherActor", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, b0l__K2Node_Event_OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "CallFunc_IsActorMainCar_Is" },
	};
#endif
	void Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is__pf_SetBit(void* Obj)
	{
		((ARoadSegment_C__pf24847026*)Obj)->b0l__CallFunc_IsActorMainCar_Is__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "CallFunc_IsActorMainCar_Is", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoadSegment_C__pf24847026), &Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "K2Node_Event_OtherActor1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_OtherActor1", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, b0l__K2Node_Event_OtherActor1__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "K2Node_Event_DeltaSeconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_Event_DeltaSeconds", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, b0l__K2Node_Event_DeltaSeconds__pf), METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__NextSegments__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Next Segments" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "NextSegments" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__NextSegments__pf = { UE4CodeGen_Private::EPropertyClass::Int, "NextSegments", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__NextSegments__pf), METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__NextSegments__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__NextSegments__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsPerSegment__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Objects Per Segment" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "ObjectsPerSegment" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsPerSegment__pf = { UE4CodeGen_Private::EPropertyClass::Int, "ObjectsPerSegment", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__ObjectsPerSegment__pf), METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsPerSegment__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsPerSegment__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__MinDistanceFromAnother__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Min Distance from Another" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "MinDistanceFromAnother" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__MinDistanceFromAnother__pf = { UE4CodeGen_Private::EPropertyClass::Float, "MinDistanceFromAnother", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__MinDistanceFromAnother__pf), METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__MinDistanceFromAnother__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__MinDistanceFromAnother__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsToRandomize__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Objects to Randomize" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "ObjectsToRandomize" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsToRandomize__pf = { UE4CodeGen_Private::EPropertyClass::Array, "ObjectsToRandomize", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__ObjectsToRandomize__pf), METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsToRandomize__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsToRandomize__pf_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsToRandomize__pf_Inner = { UE4CodeGen_Private::EPropertyClass::Class, "bpv__ObjectsToRandomize__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__CurrObjects__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Curr Objects" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "CurrObjects" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__CurrObjects__pf = { UE4CodeGen_Private::EPropertyClass::Array, "CurrObjects", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__CurrObjects__pf), METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__CurrObjects__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__CurrObjects__pf_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__CurrObjects__pf_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "bpv__CurrObjects__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Previous__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Previous" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Previous" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Previous__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Previous", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000805, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Previous__pf), Z_Construct_UClass_ARoadSegment_C__pf24847026_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Previous__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Previous__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Next__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Next" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Next" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Next__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Next", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000805, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Next__pf), Z_Construct_UClass_ARoadSegment_C__pf24847026_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Next__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Next__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__IsStart__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Is Start" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "IsStart" },
	};
#endif
	void Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__IsStart__pf_SetBit(void* Obj)
	{
		((ARoadSegment_C__pf24847026*)Obj)->bpv__IsStart__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__IsStart__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "IsStart", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoadSegment_C__pf24847026), &Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__IsStart__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__IsStart__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__IsStart__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "DefaultSceneRoot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__DefaultSceneRoot__pf = { UE4CodeGen_Private::EPropertyClass::Object, "DefaultSceneRoot", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__DefaultSceneRoot__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh1__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "StaticMesh1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh1", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__StaticMesh1__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh__pf_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "StaticMesh" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__StaticMesh__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Box__pf_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Box" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Box__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Box", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Box__pf), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Box__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Box__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Asphalt__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Asphalt" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Asphalt__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Asphalt", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Asphalt__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Asphalt__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Asphalt__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line0__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Line0" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line0__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Line0", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Line0__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line0__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line0__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line1__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Line1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Line1", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Line1__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine0__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "SmolLine0" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine0__pf = { UE4CodeGen_Private::EPropertyClass::Object, "SmolLine0", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__SmolLine0__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine0__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine0__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine1__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "SmolLine1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "SmolLine1", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__SmolLine1__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border0__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Border0" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border0__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Border0", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Border0__pf), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border0__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border0__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border1__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Border1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Border1", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Border1__pf), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_17__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "low_poly_buildings_Highrise_17" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_17__pf = { UE4CodeGen_Private::EPropertyClass::Object, "low_poly_buildings_Highrise_17", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__low_poly_buildings_Highrise_17__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_17__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_17__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_9__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "low_poly_buildings_Highrise_9" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_9__pf = { UE4CodeGen_Private::EPropertyClass::Object, "low_poly_buildings_Highrise_9", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__low_poly_buildings_Highrise_9__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_9__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_9__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_2__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "low_poly_buildings_Highrise_2" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_2__pf = { UE4CodeGen_Private::EPropertyClass::Object, "low_poly_buildings_Highrise_2", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__low_poly_buildings_Highrise_2__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_2__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_16__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "low_poly_buildings_Highrise_16" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_16__pf = { UE4CodeGen_Private::EPropertyClass::Object, "low_poly_buildings_Highrise_16", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__low_poly_buildings_Highrise_16__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_16__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_16__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_11__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "low_poly_buildings_Highrise_11" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_11__pf = { UE4CodeGen_Private::EPropertyClass::Object, "low_poly_buildings_Highrise_11", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__low_poly_buildings_Highrise_11__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_11__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_11__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh3__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "StaticMesh3" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh3__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh3", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__StaticMesh3__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh3__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh3__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh4__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "StaticMesh4" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh4__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh4", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__StaticMesh4__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh4__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh4__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh5__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "StaticMesh5" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh5__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh5", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__StaticMesh5__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh5__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh5__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh6__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "StaticMesh6" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh6__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh6", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__StaticMesh6__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh6__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh6__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube1__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Cube1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Cube1", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Cube1__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh7__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "StaticMesh7" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh7__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh7", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__StaticMesh7__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh7__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh7__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh2__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "StaticMesh2" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh2__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh2", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__StaticMesh2__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh2__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "Cube" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Cube", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__Cube__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_1__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RoadSegment__pf24847026.h" },
		{ "OverrideNativeName", "low_poly_buildings_Highrise_1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "low_poly_buildings_Highrise_1", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARoadSegment_C__pf24847026, bpv__low_poly_buildings_Highrise_1__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_1__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__CallFunc_IsActorMainCar_Is__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_OtherActor1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__NextSegments__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsPerSegment__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__MinDistanceFromAnother__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsToRandomize__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__ObjectsToRandomize__pf_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__CurrObjects__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__CurrObjects__pf_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Previous__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Next__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__IsStart__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__DefaultSceneRoot__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Box__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Asphalt__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line0__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Line1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine0__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__SmolLine1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border0__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Border1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_17__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_9__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_16__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_11__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh3__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh4__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh5__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh6__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh7__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__StaticMesh2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__Cube__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::NewProp_bpv__low_poly_buildings_Highrise_1__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARoadSegment_C__pf24847026>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::ClassParams = {
		&ARoadSegment_C__pf24847026::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARoadSegment_C__pf24847026()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/RoadSegment/RoadSegment"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("RoadSegment_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ARoadSegment_C__pf24847026, TEXT("RoadSegment_C"), 3205966033);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARoadSegment_C__pf24847026(Z_Construct_UClass_ARoadSegment_C__pf24847026, &ARoadSegment_C__pf24847026::StaticClass, TEXT("/Game/Blueprints/RoadSegment/RoadSegment"), TEXT("RoadSegment_C"), true, TEXT("/Game/Blueprints/RoadSegment/RoadSegment"), TEXT("/Game/Blueprints/RoadSegment/RoadSegment.RoadSegment_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARoadSegment_C__pf24847026);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
