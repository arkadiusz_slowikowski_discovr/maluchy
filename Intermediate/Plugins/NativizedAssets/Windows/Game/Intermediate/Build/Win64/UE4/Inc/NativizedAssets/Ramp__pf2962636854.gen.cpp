// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/Ramp__pf2962636854.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRamp__pf2962636854() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARamp_C__pf2962636854_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARamp_C__pf2962636854();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UChildActorComponent_NoRegister();
// End Cross Module References
	static FName NAME_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void ARamp_C__pf2962636854::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void ARamp_C__pf2962636854::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf),NULL);
	}
	void ARamp_C__pf2962636854::StaticRegisterNativesARamp_C__pf2962636854()
	{
		UClass* Class = ARamp_C__pf2962636854::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ReceiveBeginPlay", &ARamp_C__pf2962636854::execbpf__ReceiveBeginPlay__pf },
			{ "UserConstructionScript", &ARamp_C__pf2962636854::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/Ramp__pf2962636854.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARamp_C__pf2962636854, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARamp_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/Ramp__pf2962636854.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARamp_C__pf2962636854, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARamp_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ARamp_C__pf2962636854_NoRegister()
	{
		return ARamp_C__pf2962636854::StaticClass();
	}
	struct Z_Construct_UClass_ARamp_C__pf2962636854_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DefaultSceneRoot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__DefaultSceneRoot__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Cube__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Cube__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Boost__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Boost__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARamp_C__pf2962636854_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ARamp_C__pf2962636854_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 1593582271
		{ &Z_Construct_UFunction_ARamp_C__pf2962636854_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 2115335491
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARamp_C__pf2962636854_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Ramp__pf2962636854.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Ramp__pf2962636854.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "Ramp_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Collectables/Ramp.Ramp_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Ramp__pf2962636854.h" },
		{ "OverrideNativeName", "DefaultSceneRoot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf = { UE4CodeGen_Private::EPropertyClass::Object, "DefaultSceneRoot", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARamp_C__pf2962636854, bpv__DefaultSceneRoot__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Cube__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Ramp__pf2962636854.h" },
		{ "OverrideNativeName", "Cube" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Cube__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Cube", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARamp_C__pf2962636854, bpv__Cube__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Cube__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Cube__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Boost__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Ramp__pf2962636854.h" },
		{ "OverrideNativeName", "Boost" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Boost__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Boost", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARamp_C__pf2962636854, bpv__Boost__pf), Z_Construct_UClass_UChildActorComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Boost__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Boost__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARamp_C__pf2962636854_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Cube__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARamp_C__pf2962636854_Statics::NewProp_bpv__Boost__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARamp_C__pf2962636854_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARamp_C__pf2962636854>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARamp_C__pf2962636854_Statics::ClassParams = {
		&ARamp_C__pf2962636854::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ARamp_C__pf2962636854_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARamp_C__pf2962636854_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARamp_C__pf2962636854()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Collectables/Ramp"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("Ramp_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARamp_C__pf2962636854_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ARamp_C__pf2962636854, TEXT("Ramp_C"), 3274268604);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARamp_C__pf2962636854(Z_Construct_UClass_ARamp_C__pf2962636854, &ARamp_C__pf2962636854::StaticClass, TEXT("/Game/Blueprints/Collectables/Ramp"), TEXT("Ramp_C"), true, TEXT("/Game/Blueprints/Collectables/Ramp"), TEXT("/Game/Blueprints/Collectables/Ramp.Ramp_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARamp_C__pf2962636854);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
