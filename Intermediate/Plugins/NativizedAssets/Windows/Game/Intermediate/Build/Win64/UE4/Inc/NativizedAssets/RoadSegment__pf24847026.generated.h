// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
struct FTransform;
struct FVector;
class AActor;
#ifdef NATIVIZEDASSETS_RoadSegment__pf24847026_generated_h
#error "RoadSegment__pf24847026.generated.h already included, missing '#pragma once' in RoadSegment__pf24847026.h"
#endif
#define NATIVIZEDASSETS_RoadSegment__pf24847026_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__SpawnChild__pf) \
	{ \
		P_GET_OBJECT(UClass,Z_Param_bpp__Class__pf); \
		P_GET_STRUCT(FTransform,Z_Param_bpp__SpawnTransform__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__SpawnChild__pf(Z_Param_bpp__Class__pf,Z_Param_bpp__SpawnTransform__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetSpawnableLocation__pf) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_bpp__Location__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GetSpawnableLocation__pf(Z_Param_Out_bpp__Location__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__DestroyRoadSegment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__DestroyRoadSegment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ResetLocation__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__By__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ResetLocation__pf(Z_Param_bpp__By__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__RemoveFromResetables__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RemoveFromResetables__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__AddToResetables__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__AddToResetables__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__RandomizeObjects__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RandomizeObjects__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__IsActorMainCar__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__Actor__pf); \
		P_GET_UBOOL_REF(Z_Param_Out_bpp__Is__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__IsActorMainCar__pf(Z_Param_bpp__Actor__pf,Z_Param_Out_bpp__Is__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__PutAnotherSegment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__PutAnotherSegment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetOriginForNext__pf) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_bpp__Origin__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GetOriginForNext__pf(Z_Param_Out_bpp__Origin__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__MoveSegment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__MoveSegment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorBeginOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorBeginOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorEndOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorEndOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__SpawnChild__pf) \
	{ \
		P_GET_OBJECT(UClass,Z_Param_bpp__Class__pf); \
		P_GET_STRUCT(FTransform,Z_Param_bpp__SpawnTransform__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__SpawnChild__pf(Z_Param_bpp__Class__pf,Z_Param_bpp__SpawnTransform__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetSpawnableLocation__pf) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_bpp__Location__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GetSpawnableLocation__pf(Z_Param_Out_bpp__Location__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__DestroyRoadSegment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__DestroyRoadSegment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ResetLocation__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__By__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ResetLocation__pf(Z_Param_bpp__By__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__RemoveFromResetables__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RemoveFromResetables__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__AddToResetables__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__AddToResetables__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__RandomizeObjects__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RandomizeObjects__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__IsActorMainCar__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__Actor__pf); \
		P_GET_UBOOL_REF(Z_Param_Out_bpp__Is__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__IsActorMainCar__pf(Z_Param_bpp__Actor__pf,Z_Param_Out_bpp__Is__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__PutAnotherSegment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__PutAnotherSegment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetOriginForNext__pf) \
	{ \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_bpp__Origin__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GetOriginForNext__pf(Z_Param_Out_bpp__Origin__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__MoveSegment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__MoveSegment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorBeginOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorBeginOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorEndOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorEndOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_EVENT_PARMS \
	struct RoadSegment_C__pf24847026_eventbpf__ReceiveActorBeginOverlap__pf_Parms \
	{ \
		AActor* bpp__OtherActor__pf; \
	}; \
	struct RoadSegment_C__pf24847026_eventbpf__ReceiveActorEndOverlap__pf_Parms \
	{ \
		AActor* bpp__OtherActor__pf; \
	}; \
	struct RoadSegment_C__pf24847026_eventbpf__ReceiveTick__pf_Parms \
	{ \
		float bpp__DeltaSeconds__pf; \
	};


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf); \
 \
	void eventbpf__ReceiveActorEndOverlap__pf(AActor* bpp__OtherActor__pf); \
 \
	void eventbpf__ReceiveBeginPlay__pf(); \
 \
	void eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf); \
 \
	void eventbpf__UserConstructionScript__pf(); \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARoadSegment_C__pf24847026(); \
	friend struct Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics; \
public: \
	DECLARE_CLASS(ARoadSegment_C__pf24847026, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/RoadSegment/RoadSegment"), NO_API) \
	DECLARE_SERIALIZER(ARoadSegment_C__pf24847026)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_INCLASS \
private: \
	static void StaticRegisterNativesARoadSegment_C__pf24847026(); \
	friend struct Z_Construct_UClass_ARoadSegment_C__pf24847026_Statics; \
public: \
	DECLARE_CLASS(ARoadSegment_C__pf24847026, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/RoadSegment/RoadSegment"), NO_API) \
	DECLARE_SERIALIZER(ARoadSegment_C__pf24847026)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARoadSegment_C__pf24847026(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARoadSegment_C__pf24847026) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARoadSegment_C__pf24847026); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARoadSegment_C__pf24847026); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARoadSegment_C__pf24847026(ARoadSegment_C__pf24847026&&); \
	NO_API ARoadSegment_C__pf24847026(const ARoadSegment_C__pf24847026&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARoadSegment_C__pf24847026(ARoadSegment_C__pf24847026&&); \
	NO_API ARoadSegment_C__pf24847026(const ARoadSegment_C__pf24847026&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARoadSegment_C__pf24847026); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARoadSegment_C__pf24847026); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARoadSegment_C__pf24847026)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_13_PROLOG \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_EVENT_PARMS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_RoadSegment__pf24847026_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
