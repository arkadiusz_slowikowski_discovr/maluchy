// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NATIVIZEDASSETS_MaluchABP__pf1851508772_generated_h
#error "MaluchABP__pf1851508772.generated.h already included, missing '#pragma once' in MaluchABP__pf1851508772.h"
#endif
#define NATIVIZEDASSETS_MaluchABP__pf1851508772_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__ExecuteUbergraph_MaluchABP__pf) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_bpp__EntryPoint__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ExecuteUbergraph_MaluchABP__pf(Z_Param_bpp__EntryPoint__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__ExecuteUbergraph_MaluchABP__pf) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_bpp__EntryPoint__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ExecuteUbergraph_MaluchABP__pf(Z_Param_bpp__EntryPoint__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMaluchABP_C__pf1851508772(); \
	friend struct Z_Construct_UClass_UMaluchABP_C__pf1851508772_Statics; \
public: \
	DECLARE_CLASS(UMaluchABP_C__pf1851508772, UVehicleAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Game/Blueprints/Car/MaluchABP"), NO_API) \
	DECLARE_SERIALIZER(UMaluchABP_C__pf1851508772) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUMaluchABP_C__pf1851508772(); \
	friend struct Z_Construct_UClass_UMaluchABP_C__pf1851508772_Statics; \
public: \
	DECLARE_CLASS(UMaluchABP_C__pf1851508772, UVehicleAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Game/Blueprints/Car/MaluchABP"), NO_API) \
	DECLARE_SERIALIZER(UMaluchABP_C__pf1851508772) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMaluchABP_C__pf1851508772(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMaluchABP_C__pf1851508772) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMaluchABP_C__pf1851508772); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMaluchABP_C__pf1851508772); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMaluchABP_C__pf1851508772(UMaluchABP_C__pf1851508772&&); \
	NO_API UMaluchABP_C__pf1851508772(const UMaluchABP_C__pf1851508772&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMaluchABP_C__pf1851508772(UMaluchABP_C__pf1851508772&&); \
	NO_API UMaluchABP_C__pf1851508772(const UMaluchABP_C__pf1851508772&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMaluchABP_C__pf1851508772); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMaluchABP_C__pf1851508772); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMaluchABP_C__pf1851508772)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_10_PROLOG
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h_14_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MaluchABP__pf1851508772_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
