// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FSlateColor;
struct FGeometry;
#ifdef NATIVIZEDASSETS_MainUI__pf3053510930_generated_h
#error "MainUI__pf3053510930.generated.h already included, missing '#pragma once' in MainUI__pf3053510930.h"
#endif
#define NATIVIZEDASSETS_MainUI__pf3053510930_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_20_DELEGATE \
static inline void FEventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate_DelegateWrapper(const FScriptDelegate& EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate) \
{ \
	EventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate.ProcessDelegate<UObject>(NULL); \
}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__GetText_2__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->bpf__GetText_2__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__HandleGameStarted__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__HandleGameStarted__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetCarSpeed__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->bpf__GetCarSpeed__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetText_1__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->bpf__GetText_1__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetColorAndOpacity_0__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FSlateColor*)Z_Param__Result=P_THIS->bpf__GetColorAndOpacity_0__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetPercent_0__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->bpf__GetPercent_0__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetText_0__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->bpf__GetText_0__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__Construct__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Construct__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__Tick__pf) \
	{ \
		P_GET_STRUCT(FGeometry,Z_Param_bpp__MyGeometry__pf); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__InDeltaTime__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Tick__pf(Z_Param_bpp__MyGeometry__pf,Z_Param_bpp__InDeltaTime__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__EventxHandleGameStarted__pfT) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__EventxHandleGameStarted__pfT(); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__GetText_2__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->bpf__GetText_2__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__HandleGameStarted__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__HandleGameStarted__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetCarSpeed__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->bpf__GetCarSpeed__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetText_1__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->bpf__GetText_1__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetColorAndOpacity_0__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FSlateColor*)Z_Param__Result=P_THIS->bpf__GetColorAndOpacity_0__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetPercent_0__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->bpf__GetPercent_0__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetText_0__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->bpf__GetText_0__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__Construct__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Construct__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__Tick__pf) \
	{ \
		P_GET_STRUCT(FGeometry,Z_Param_bpp__MyGeometry__pf); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__InDeltaTime__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Tick__pf(Z_Param_bpp__MyGeometry__pf,Z_Param_bpp__InDeltaTime__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__EventxHandleGameStarted__pfT) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__EventxHandleGameStarted__pfT(); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_EVENT_PARMS \
	struct MainUI_C__pf3053510930_eventbpf__Tick__pf_Parms \
	{ \
		FGeometry bpp__MyGeometry__pf; \
		float bpp__InDeltaTime__pf; \
	};


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_CALLBACK_WRAPPERS \
	void eventbpf__Construct__pf(); \
 \
	void eventbpf__Tick__pf(FGeometry bpp__MyGeometry__pf, float bpp__InDeltaTime__pf); \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMainUI_C__pf3053510930(); \
	friend struct Z_Construct_UClass_UMainUI_C__pf3053510930_Statics; \
public: \
	DECLARE_CLASS(UMainUI_C__pf3053510930, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/UI/MainUI"), NO_API) \
	DECLARE_SERIALIZER(UMainUI_C__pf3053510930) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMainUI_C__pf3053510930(); \
	friend struct Z_Construct_UClass_UMainUI_C__pf3053510930_Statics; \
public: \
	DECLARE_CLASS(UMainUI_C__pf3053510930, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/UI/MainUI"), NO_API) \
	DECLARE_SERIALIZER(UMainUI_C__pf3053510930) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMainUI_C__pf3053510930(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMainUI_C__pf3053510930) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMainUI_C__pf3053510930); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMainUI_C__pf3053510930); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMainUI_C__pf3053510930(UMainUI_C__pf3053510930&&); \
	NO_API UMainUI_C__pf3053510930(const UMainUI_C__pf3053510930&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMainUI_C__pf3053510930(UMainUI_C__pf3053510930&&); \
	NO_API UMainUI_C__pf3053510930(const UMainUI_C__pf3053510930&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMainUI_C__pf3053510930); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMainUI_C__pf3053510930); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMainUI_C__pf3053510930)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_14_PROLOG \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_EVENT_PARMS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainUI__pf3053510930_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
