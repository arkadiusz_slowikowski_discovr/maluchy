// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/Rotatable__pf2962636854.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRotatable__pf2962636854() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARotatable_C__pf2962636854_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARotatable_C__pf2962636854();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	static FName NAME_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void ARotatable_C__pf2962636854::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf),NULL);
	}
	void ARotatable_C__pf2962636854::StaticRegisterNativesARotatable_C__pf2962636854()
	{
		UClass* Class = ARotatable_C__pf2962636854::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddToRotatables", &ARotatable_C__pf2962636854::execbpf__AddToRotatables__pf },
			{ "RandomStartRot", &ARotatable_C__pf2962636854::execbpf__RandomStartRot__pf },
			{ "RemoveFromRotatables", &ARotatable_C__pf2962636854::execbpf__RemoveFromRotatables__pf },
			{ "UserConstructionScript", &ARotatable_C__pf2962636854::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/Rotatable__pf2962636854.h" },
		{ "OverrideNativeName", "AddToRotatables" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARotatable_C__pf2962636854, "AddToRotatables", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARotatable_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("AddToRotatables") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/Rotatable__pf2962636854.h" },
		{ "OverrideNativeName", "RandomStartRot" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARotatable_C__pf2962636854, "RandomStartRot", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARotatable_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("RandomStartRot") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/Rotatable__pf2962636854.h" },
		{ "OverrideNativeName", "RemoveFromRotatables" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARotatable_C__pf2962636854, "RemoveFromRotatables", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARotatable_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("RemoveFromRotatables") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/Rotatable__pf2962636854.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARotatable_C__pf2962636854, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_ARotatable_C__pf2962636854();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ARotatable_C__pf2962636854_NoRegister()
	{
		return ARotatable_C__pf2962636854::StaticClass();
	}
	struct Z_Construct_UClass_ARotatable_C__pf2962636854_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__RotatingSpeed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__RotatingSpeed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DefaultSceneRoot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__DefaultSceneRoot__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__AddToRotatables__pf, "AddToRotatables" }, // 427208875
		{ &Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RandomStartRot__pf, "RandomStartRot" }, // 2680009040
		{ &Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__RemoveFromRotatables__pf, "RemoveFromRotatables" }, // 1034944723
		{ &Z_Construct_UFunction_ARotatable_C__pf2962636854_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 2986013125
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Rotatable__pf2962636854.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Rotatable__pf2962636854.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "Rotatable_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Collectables/Rotatable.Rotatable_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__RotatingSpeed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Rotating Speed" },
		{ "ModuleRelativePath", "Public/Rotatable__pf2962636854.h" },
		{ "OverrideNativeName", "RotatingSpeed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__RotatingSpeed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "RotatingSpeed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ARotatable_C__pf2962636854, bpv__RotatingSpeed__pf), METADATA_PARAMS(Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__RotatingSpeed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__RotatingSpeed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Rotatable__pf2962636854.h" },
		{ "OverrideNativeName", "DefaultSceneRoot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf = { UE4CodeGen_Private::EPropertyClass::Object, "DefaultSceneRoot", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ARotatable_C__pf2962636854, bpv__DefaultSceneRoot__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__RotatingSpeed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::NewProp_bpv__DefaultSceneRoot__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARotatable_C__pf2962636854>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::ClassParams = {
		&ARotatable_C__pf2962636854::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARotatable_C__pf2962636854()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Collectables/Rotatable"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("Rotatable_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARotatable_C__pf2962636854_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ARotatable_C__pf2962636854, TEXT("Rotatable_C"), 3112663192);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARotatable_C__pf2962636854(Z_Construct_UClass_ARotatable_C__pf2962636854, &ARotatable_C__pf2962636854::StaticClass, TEXT("/Game/Blueprints/Collectables/Rotatable"), TEXT("Rotatable_C"), true, TEXT("/Game/Blueprints/Collectables/Rotatable"), TEXT("/Game/Blueprints/Collectables/Rotatable.Rotatable_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARotatable_C__pf2962636854);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
