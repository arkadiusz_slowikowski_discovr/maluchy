// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NATIVIZEDASSETS_DiscoMaluch__pf1851508772_generated_h
#error "DiscoMaluch__pf1851508772.generated.h already included, missing '#pragma once' in DiscoMaluch__pf1851508772.h"
#endif
#define NATIVIZEDASSETS_DiscoMaluch__pf1851508772_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_18_DELEGATE \
static inline void FBecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate_DelegateWrapper(const FMulticastScriptDelegate& BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate) \
{ \
	BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__ReadFromJson__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReadFromJson__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__DecreaseMultipier__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaTime__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__DecreaseMultipier__pf(Z_Param_bpp__DeltaTime__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBoost__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__BoostValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBoost__pf(Z_Param_bpp__BoostValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CountDistance__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CountDistance__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CheckIfGrounded__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CheckIfGrounded__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ChangeGear__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ChangeGear__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CallResettingEnviroment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CallResettingEnviroment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__NewSteer__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__NewSteer__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__OldSteer__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__OldSteer__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__Drive__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Drive__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetTotalMass__pf) \
	{ \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_bpp__TotalxMass__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GetTotalMass__pf(Z_Param_Out_bpp__TotalxMass__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CantDrive__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CantDrive__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__DecreaseFuel__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__DecreaseFuel__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CollectFuel__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__HowxMuch__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CollectFuel__pf(Z_Param_bpp__HowxMuch__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__AddUIWidget__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__AddUIWidget__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CollectCoin__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CollectCoin__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__HitByObstacle__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__HitByObstacle__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__SetAsMainCar__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__SetAsMainCar__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__Move__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Move__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__ReadFromJson__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReadFromJson__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__DecreaseMultipier__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaTime__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__DecreaseMultipier__pf(Z_Param_bpp__DeltaTime__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBoost__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__BoostValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBoost__pf(Z_Param_bpp__BoostValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CountDistance__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CountDistance__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CheckIfGrounded__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CheckIfGrounded__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ChangeGear__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ChangeGear__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CallResettingEnviroment__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CallResettingEnviroment__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__NewSteer__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__NewSteer__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__OldSteer__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__OldSteer__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__Drive__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Drive__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GetTotalMass__pf) \
	{ \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_bpp__TotalxMass__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GetTotalMass__pf(Z_Param_Out_bpp__TotalxMass__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CantDrive__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CantDrive__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__DecreaseFuel__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__DecreaseFuel__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CollectFuel__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__HowxMuch__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CollectFuel__pf(Z_Param_bpp__HowxMuch__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__AddUIWidget__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__AddUIWidget__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__CollectCoin__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__CollectCoin__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__HitByObstacle__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__HitByObstacle__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__SetAsMainCar__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__SetAsMainCar__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__Move__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaxTime__pfT); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Move__pf(Z_Param_bpp__DeltaxTime__pfT); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_EVENT_PARMS \
	struct DiscoMaluch_C__pf1851508772_eventbpf__ReceiveTick__pf_Parms \
	{ \
		float bpp__DeltaSeconds__pf; \
	};


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveBeginPlay__pf(); \
 \
	void eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf); \
 \
	void eventbpf__UserConstructionScript__pf(); \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADiscoMaluch_C__pf1851508772(); \
	friend struct Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics; \
public: \
	DECLARE_CLASS(ADiscoMaluch_C__pf1851508772, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/Car/DiscoMaluch"), NO_API) \
	DECLARE_SERIALIZER(ADiscoMaluch_C__pf1851508772)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_INCLASS \
private: \
	static void StaticRegisterNativesADiscoMaluch_C__pf1851508772(); \
	friend struct Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics; \
public: \
	DECLARE_CLASS(ADiscoMaluch_C__pf1851508772, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/Car/DiscoMaluch"), NO_API) \
	DECLARE_SERIALIZER(ADiscoMaluch_C__pf1851508772)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADiscoMaluch_C__pf1851508772(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADiscoMaluch_C__pf1851508772) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADiscoMaluch_C__pf1851508772); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADiscoMaluch_C__pf1851508772); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADiscoMaluch_C__pf1851508772(ADiscoMaluch_C__pf1851508772&&); \
	NO_API ADiscoMaluch_C__pf1851508772(const ADiscoMaluch_C__pf1851508772&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADiscoMaluch_C__pf1851508772(ADiscoMaluch_C__pf1851508772&&); \
	NO_API ADiscoMaluch_C__pf1851508772(const ADiscoMaluch_C__pf1851508772&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADiscoMaluch_C__pf1851508772); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADiscoMaluch_C__pf1851508772); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADiscoMaluch_C__pf1851508772)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_12_PROLOG \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_EVENT_PARMS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_DiscoMaluch__pf1851508772_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
