// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/MainCarController__pf1851508772.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMainCarController__pf1851508772() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMainCarController_C__pf1851508772_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMainCarController_C__pf1851508772();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister();
// End Cross Module References
	static FName NAME_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void AMainCarController_C__pf1851508772::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void AMainCarController_C__pf1851508772::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf),NULL);
	}
	void AMainCarController_C__pf1851508772::StaticRegisterNativesAMainCarController_C__pf1851508772()
	{
		UClass* Class = AMainCarController_C__pf1851508772::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "InpActEvt_YellowButton_K2Node_InputActionEvent_2", &AMainCarController_C__pf1851508772::execbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf },
			{ "InpActEvt_YellowButton_K2Node_InputActionEvent_3", &AMainCarController_C__pf1851508772::execbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf },
			{ "InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0", &AMainCarController_C__pf1851508772::execbpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf },
			{ "InpAxisEvt_Vertical_K2Node_InputAxisEvent_1", &AMainCarController_C__pf1851508772::execbpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf },
			{ "MoveHor", &AMainCarController_C__pf1851508772::execbpf__MoveHor__pf },
			{ "ReceiveBeginPlay", &AMainCarController_C__pf1851508772::execbpf__ReceiveBeginPlay__pf },
			{ "Restart", &AMainCarController_C__pf1851508772::execbpf__Restart__pf },
			{ "RollCar", &AMainCarController_C__pf1851508772::execbpf__RollCar__pf },
			{ "UserConstructionScript", &AMainCarController_C__pf1851508772::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics
	{
		struct MainCarController_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Parms
		{
			FKey bpp__Key__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Key__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::NewProp_bpp__Key__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__Key__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainCarController_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Parms, bpp__Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::NewProp_bpp__Key__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "InpActEvt_YellowButton_K2Node_InputActionEvent_2" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "InpActEvt_YellowButton_K2Node_InputActionEvent_2", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020400, sizeof(MainCarController_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Parms), Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpActEvt_YellowButton_K2Node_InputActionEvent_2") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics
	{
		struct MainCarController_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Parms
		{
			FKey bpp__Key__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Key__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::NewProp_bpp__Key__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__Key__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainCarController_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Parms, bpp__Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::NewProp_bpp__Key__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "InpActEvt_YellowButton_K2Node_InputActionEvent_3" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "InpActEvt_YellowButton_K2Node_InputActionEvent_3", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020400, sizeof(MainCarController_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Parms), Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpActEvt_YellowButton_K2Node_InputActionEvent_3") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics
	{
		struct MainCarController_C__pf1851508772_eventbpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Parms
		{
			float bpp__AxisValue__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__AxisValue__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::NewProp_bpp__AxisValue__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__AxisValue__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainCarController_C__pf1851508772_eventbpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Parms, bpp__AxisValue__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::NewProp_bpp__AxisValue__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020400, sizeof(MainCarController_C__pf1851508772_eventbpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Parms), Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics
	{
		struct MainCarController_C__pf1851508772_eventbpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Parms
		{
			float bpp__AxisValue__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__AxisValue__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::NewProp_bpp__AxisValue__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__AxisValue__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainCarController_C__pf1851508772_eventbpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Parms, bpp__AxisValue__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::NewProp_bpp__AxisValue__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "InpAxisEvt_Vertical_K2Node_InputAxisEvent_1" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "InpAxisEvt_Vertical_K2Node_InputAxisEvent_1", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020400, sizeof(MainCarController_C__pf1851508772_eventbpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Parms), Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpAxisEvt_Vertical_K2Node_InputAxisEvent_1") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics
	{
		struct MainCarController_C__pf1851508772_eventbpf__MoveHor__pf_Parms
		{
			float bpp__Input__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__Input__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::NewProp_bpp__Input__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__Input__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainCarController_C__pf1851508772_eventbpf__MoveHor__pf_Parms, bpp__Input__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::NewProp_bpp__Input__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "MoveHor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "MoveHor", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(MainCarController_C__pf1851508772_eventbpf__MoveHor__pf_Parms), Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("MoveHor") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "Restart" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "Restart", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Restart") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics
	{
		struct MainCarController_C__pf1851508772_eventbpf__RollCar__pf_Parms
		{
			float bpp__Input__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__Input__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::NewProp_bpp__Input__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__Input__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainCarController_C__pf1851508772_eventbpf__RollCar__pf_Parms, bpp__Input__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::NewProp_bpp__Input__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "RollCar" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "RollCar", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(MainCarController_C__pf1851508772_eventbpf__RollCar__pf_Parms), Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("RollCar") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCarController_C__pf1851508772, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMainCarController_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMainCarController_C__pf1851508772_NoRegister()
	{
		return AMainCarController_C__pf1851508772::StaticClass();
	}
	struct Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputAxisEvent_AxisValue1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_InputAxisEvent_AxisValue1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__Temp_struct_Variable__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__Temp_struct_Variable__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_InputActionEvent_Key__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputActionEvent_Key1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_InputActionEvent_Key1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__YellowTimer__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__YellowTimer__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__YellowPressed__pf_MetaData[];
#endif
		static void NewProp_bpv__YellowPressed__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__YellowPressed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__RollingSensitivity__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__RollingSensitivity__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TurningSpeed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__TurningSpeed__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf, "InpActEvt_YellowButton_K2Node_InputActionEvent_2" }, // 3023026082
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf, "InpActEvt_YellowButton_K2Node_InputActionEvent_3" }, // 663108551
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf, "InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0" }, // 1531582688
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf, "InpAxisEvt_Vertical_K2Node_InputAxisEvent_1" }, // 974882487
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__MoveHor__pf, "MoveHor" }, // 2497734493
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 3556164347
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__Restart__pf, "Restart" }, // 4133810780
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__RollCar__pf, "RollCar" }, // 2934698041
		{ &Z_Construct_UFunction_AMainCarController_C__pf1851508772_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 165191972
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MainCarController__pf1851508772.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "MainCarController_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Car/MainCarController.MainCarController_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess3" },
	};
#endif
	void Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf_SetBit(void* Obj)
	{
		((AMainCarController_C__pf1851508772*)Obj)->b0l__K2Node_DynamicCast_bSuccess3__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess3", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMainCarController_C__pf1851508772), &Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsMaluchy_Game_Mode" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsMaluchy_Game_Mode", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf), Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess2" },
	};
#endif
	void Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf_SetBit(void* Obj)
	{
		((AMainCarController_C__pf1851508772*)Obj)->b0l__K2Node_DynamicCast_bSuccess2__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess2", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMainCarController_C__pf1851508772), &Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsDisco_Maluch2" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsDisco_Maluch2", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf), Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess1" },
	};
#endif
	void Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf_SetBit(void* Obj)
	{
		((AMainCarController_C__pf1851508772*)Obj)->b0l__K2Node_DynamicCast_bSuccess1__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess1", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMainCarController_C__pf1851508772), &Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsDisco_Maluch1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsDisco_Maluch1", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf), Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_InputAxisEvent_AxisValue" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_InputAxisEvent_AxisValue", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__K2Node_InputAxisEvent_AxisValue__pf), METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_InputAxisEvent_AxisValue1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue1__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_InputAxisEvent_AxisValue1", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__K2Node_InputAxisEvent_AxisValue1__pf), METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "Temp_struct_Variable" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "Temp_struct_Variable", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__Temp_struct_Variable__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_InputActionEvent_Key" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_InputActionEvent_Key", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__K2Node_InputActionEvent_Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess" },
	};
#endif
	void Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj)
	{
		((AMainCarController_C__pf1851508772*)Obj)->b0l__K2Node_DynamicCast_bSuccess__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMainCarController_C__pf1851508772), &Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsDisco_Maluch" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsDisco_Maluch", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__K2Node_DynamicCast_AsDisco_Maluch__pf), Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_InputActionEvent_Key1" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_InputActionEvent_Key1", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, b0l__K2Node_InputActionEvent_Key1__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Yellow Timer" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "YellowTimer" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf = { UE4CodeGen_Private::EPropertyClass::Float, "YellowTimer", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, bpv__YellowTimer__pf), METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Yellow Pressed" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "YellowPressed" },
	};
#endif
	void Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_SetBit(void* Obj)
	{
		((AMainCarController_C__pf1851508772*)Obj)->bpv__YellowPressed__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "YellowPressed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMainCarController_C__pf1851508772), &Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__RollingSensitivity__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Rolling Sensitivity" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "RollingSensitivity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__RollingSensitivity__pf = { UE4CodeGen_Private::EPropertyClass::Float, "RollingSensitivity", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, bpv__RollingSensitivity__pf), METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__RollingSensitivity__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__RollingSensitivity__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__TurningSpeed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Turning Speed" },
		{ "ModuleRelativePath", "Public/MainCarController__pf1851508772.h" },
		{ "OverrideNativeName", "TurningSpeed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__TurningSpeed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "TurningSpeed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(AMainCarController_C__pf1851508772, bpv__TurningSpeed__pf), METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__TurningSpeed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__TurningSpeed__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess3__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__RollingSensitivity__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::NewProp_bpv__TurningSpeed__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMainCarController_C__pf1851508772>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::ClassParams = {
		&AMainCarController_C__pf1851508772::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008002A4u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::PropPointers),
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMainCarController_C__pf1851508772()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Car/MainCarController"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("MainCarController_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(AMainCarController_C__pf1851508772, TEXT("MainCarController_C"), 2999109533);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMainCarController_C__pf1851508772(Z_Construct_UClass_AMainCarController_C__pf1851508772, &AMainCarController_C__pf1851508772::StaticClass, TEXT("/Game/Blueprints/Car/MainCarController"), TEXT("MainCarController_C"), true, TEXT("/Game/Blueprints/Car/MainCarController"), TEXT("/Game/Blueprints/Car/MainCarController.MainCarController_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMainCarController_C__pf1851508772);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
