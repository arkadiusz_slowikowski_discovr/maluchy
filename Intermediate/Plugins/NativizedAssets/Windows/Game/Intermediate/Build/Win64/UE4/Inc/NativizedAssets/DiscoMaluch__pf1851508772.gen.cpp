// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/DiscoMaluch__pf1851508772.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDiscoMaluch__pf1851508772() {}
// Cross Module References
	NATIVIZEDASSETS_API UFunction* Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPhysicsConstraintComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "BecameUnableToDrive__DelegateSignature" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "BecameUnableToDrive__DelegateSignature", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00130000, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("BecameUnableToDrive__DelegateSignature") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static FName NAME_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void ADiscoMaluch_C__pf1851508772::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf = FName(TEXT("ReceiveTick"));
	void ADiscoMaluch_C__pf1851508772::eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
	{
		DiscoMaluch_C__pf1851508772_eventbpf__ReceiveTick__pf_Parms Parms;
		Parms.bpp__DeltaSeconds__pf=bpp__DeltaSeconds__pf;
		ProcessEvent(FindFunctionChecked(NAME_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf),&Parms);
	}
	static FName NAME_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void ADiscoMaluch_C__pf1851508772::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf),NULL);
	}
	void ADiscoMaluch_C__pf1851508772::StaticRegisterNativesADiscoMaluch_C__pf1851508772()
	{
		UClass* Class = ADiscoMaluch_C__pf1851508772::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddUIWidget", &ADiscoMaluch_C__pf1851508772::execbpf__AddUIWidget__pf },
			{ "CallResettingEnviroment", &ADiscoMaluch_C__pf1851508772::execbpf__CallResettingEnviroment__pf },
			{ "CantDrive", &ADiscoMaluch_C__pf1851508772::execbpf__CantDrive__pf },
			{ "ChangeGear", &ADiscoMaluch_C__pf1851508772::execbpf__ChangeGear__pf },
			{ "CheckIfGrounded", &ADiscoMaluch_C__pf1851508772::execbpf__CheckIfGrounded__pf },
			{ "CollectCoin", &ADiscoMaluch_C__pf1851508772::execbpf__CollectCoin__pf },
			{ "CollectFuel", &ADiscoMaluch_C__pf1851508772::execbpf__CollectFuel__pf },
			{ "CountDistance", &ADiscoMaluch_C__pf1851508772::execbpf__CountDistance__pf },
			{ "DecreaseFuel", &ADiscoMaluch_C__pf1851508772::execbpf__DecreaseFuel__pf },
			{ "DecreaseMultipier", &ADiscoMaluch_C__pf1851508772::execbpf__DecreaseMultipier__pf },
			{ "Drive", &ADiscoMaluch_C__pf1851508772::execbpf__Drive__pf },
			{ "GetTotalMass", &ADiscoMaluch_C__pf1851508772::execbpf__GetTotalMass__pf },
			{ "HitByObstacle", &ADiscoMaluch_C__pf1851508772::execbpf__HitByObstacle__pf },
			{ "Move", &ADiscoMaluch_C__pf1851508772::execbpf__Move__pf },
			{ "NewSteer", &ADiscoMaluch_C__pf1851508772::execbpf__NewSteer__pf },
			{ "OldSteer", &ADiscoMaluch_C__pf1851508772::execbpf__OldSteer__pf },
			{ "ReadFromJson", &ADiscoMaluch_C__pf1851508772::execbpf__ReadFromJson__pf },
			{ "ReceiveBeginPlay", &ADiscoMaluch_C__pf1851508772::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveBoost", &ADiscoMaluch_C__pf1851508772::execbpf__ReceiveBoost__pf },
			{ "ReceiveTick", &ADiscoMaluch_C__pf1851508772::execbpf__ReceiveTick__pf },
			{ "SetAsMainCar", &ADiscoMaluch_C__pf1851508772::execbpf__SetAsMainCar__pf },
			{ "UserConstructionScript", &ADiscoMaluch_C__pf1851508772::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "AddUIWidget" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "AddUIWidget", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("AddUIWidget") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CallResettingEnviroment" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "CallResettingEnviroment", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CallResettingEnviroment") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CantDrive" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "CantDrive", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CantDrive") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__ChangeGear__pf_Parms
		{
			float bpp__Input__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__Input__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::NewProp_bpp__Input__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__Input__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__ChangeGear__pf_Parms, bpp__Input__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::NewProp_bpp__Input__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "ChangeGear" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "ChangeGear", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__ChangeGear__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ChangeGear") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__CheckIfGrounded__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__CheckIfGrounded__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CheckIfGrounded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "CheckIfGrounded", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__CheckIfGrounded__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CheckIfGrounded") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CollectCoin" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "CollectCoin", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CollectCoin") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__CollectFuel__pf_Parms
		{
			float bpp__HowxMuch__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__HowxMuch__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::NewProp_bpp__HowxMuch__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__HowxMuch__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__CollectFuel__pf_Parms, bpp__HowxMuch__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::NewProp_bpp__HowxMuch__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CollectFuel" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "CollectFuel", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__CollectFuel__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CollectFuel") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CountDistance" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "CountDistance", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CountDistance") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__DecreaseFuel__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__DecreaseFuel__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "DecreaseFuel" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "DecreaseFuel", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__DecreaseFuel__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("DecreaseFuel") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__DecreaseMultipier__pf_Parms
		{
			float bpp__DeltaTime__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaTime__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::NewProp_bpp__DeltaTime__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaTime__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__DecreaseMultipier__pf_Parms, bpp__DeltaTime__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::NewProp_bpp__DeltaTime__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "DecreaseMultipier" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "DecreaseMultipier", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__DecreaseMultipier__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("DecreaseMultipier") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__Drive__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__Drive__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "Drive" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "Drive", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__Drive__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Drive") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__GetTotalMass__pf_Parms
		{
			float bpp__TotalxMass__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__TotalxMass__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::NewProp_bpp__TotalxMass__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__TotalxMass__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__GetTotalMass__pf_Parms, bpp__TotalxMass__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::NewProp_bpp__TotalxMass__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "GetTotalMass" },
		{ "ToolTip", "out" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "GetTotalMass", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04420400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__GetTotalMass__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetTotalMass") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "HitByObstacle" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "HitByObstacle", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("HitByObstacle") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__Move__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__Move__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "Move" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "Move", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__Move__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Move") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__NewSteer__pf_Parms
		{
			float bpp__Input__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__Input__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::NewProp_bpp__Input__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__Input__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__NewSteer__pf_Parms, bpp__Input__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::NewProp_bpp__Input__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "NewSteer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "NewSteer", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__NewSteer__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("NewSteer") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__OldSteer__pf_Parms
		{
			float bpp__Input__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__Input__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::NewProp_bpp__Input__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__Input__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__OldSteer__pf_Parms, bpp__Input__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::NewProp_bpp__Input__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "OldSteer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "OldSteer", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__OldSteer__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("OldSteer") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "ReadFromJson" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "ReadFromJson", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReadFromJson") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics
	{
		struct DiscoMaluch_C__pf1851508772_eventbpf__ReceiveBoost__pf_Parms
		{
			float bpp__BoostValue__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__BoostValue__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::NewProp_bpp__BoostValue__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__BoostValue__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__ReceiveBoost__pf_Parms, bpp__BoostValue__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::NewProp_bpp__BoostValue__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "ReceiveBoost" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "ReceiveBoost", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__ReceiveBoost__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBoost") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaSeconds__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaSeconds__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluch_C__pf1851508772_eventbpf__ReceiveTick__pf_Parms, bpp__DeltaSeconds__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Tick" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "ReceiveTick" },
		{ "ToolTip", "Event called every frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "ReceiveTick", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(DiscoMaluch_C__pf1851508772_eventbpf__ReceiveTick__pf_Parms), Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveTick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "SetAsMainCar" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "SetAsMainCar", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("SetAsMainCar") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluch_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister()
	{
		return ADiscoMaluch_C__pf1851508772::StaticClass();
	}
	struct Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_Event_DeltaSeconds__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakVector_Z__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakVector_Z__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakVector_Y__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakVector_Y__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakVector_X__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakVector_X__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Velocity__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__Velocity__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__MultipierDecreaser__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__MultipierDecreaser__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__YellowPressed__pf_MetaData[];
#endif
		static void NewProp_bpv__YellowPressed__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__YellowPressed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__YellowTimer__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__YellowTimer__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__PrevX__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__PrevX__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__GameMode__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__GameMode__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DrivingMultipier__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__DrivingMultipier__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__IsGrounded__pf_MetaData[];
#endif
		static void NewProp_bpv__IsGrounded__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__IsGrounded__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__PrevSpeed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__PrevSpeed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__BecameUnableToDrive__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_bpv__BecameUnableToDrive__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__AbleToDrive__pf_MetaData[];
#endif
		static void NewProp_bpv__AbleToDrive__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__AbleToDrive__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ChangingGearTrashhold__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__ChangingGearTrashhold__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ResettingDistance__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__ResettingDistance__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__CameraSteeringSensitivity__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__CameraSteeringSensitivity__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SteeringSensitivity__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__SteeringSensitivity__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DrivingForce__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__DrivingForce__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__FuelDescreasingSpeed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__FuelDescreasingSpeed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StartRot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpv__StartRot__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Fuel__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__Fuel__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__CoinsCollected__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__CoinsCollected__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DistancePassed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__DistancePassed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Camera__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Camera__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Scene__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Scene__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SpringArm__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SpringArm__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FL__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FL__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RL__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RL__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RR__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RR__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FR__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FR__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__PhysicsConstraintFL__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__PhysicsConstraintFL__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__PhysicsConstraintRL__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__PhysicsConstraintRL__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__PhysicsConstraintFR__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__PhysicsConstraintFR__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__PhysicsConstraintRR__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__PhysicsConstraintRR__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::FuncInfo[] = {
		{ &Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature, "BecameUnableToDrive__DelegateSignature" }, // 1725314575
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__AddUIWidget__pf, "AddUIWidget" }, // 2951138528
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CallResettingEnviroment__pf, "CallResettingEnviroment" }, // 952741501
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CantDrive__pf, "CantDrive" }, // 3650175390
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ChangeGear__pf, "ChangeGear" }, // 1947566039
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CheckIfGrounded__pf, "CheckIfGrounded" }, // 4120348726
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectCoin__pf, "CollectCoin" }, // 2514214097
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CollectFuel__pf, "CollectFuel" }, // 3963337847
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__CountDistance__pf, "CountDistance" }, // 1452688267
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseFuel__pf, "DecreaseFuel" }, // 2794483078
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__DecreaseMultipier__pf, "DecreaseMultipier" }, // 3066359751
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Drive__pf, "Drive" }, // 2390934454
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__GetTotalMass__pf, "GetTotalMass" }, // 2958946561
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__HitByObstacle__pf, "HitByObstacle" }, // 1408561476
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__Move__pf, "Move" }, // 2935386516
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__NewSteer__pf, "NewSteer" }, // 3518409483
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__OldSteer__pf, "OldSteer" }, // 1213352333
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReadFromJson__pf, "ReadFromJson" }, // 2340655547
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 1951519413
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveBoost__pf, "ReceiveBoost" }, // 931952401
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__ReceiveTick__pf, "ReceiveTick" }, // 2095384473
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__SetAsMainCar__pf, "SetAsMainCar" }, // 990832328
		{ &Z_Construct_UFunction_ADiscoMaluch_C__pf1851508772_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 292537126
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "DiscoMaluch__pf1851508772.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "DiscoMaluch_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Car/DiscoMaluch.DiscoMaluch_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess" },
	};
#endif
	void Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj)
	{
		((ADiscoMaluch_C__pf1851508772*)Obj)->b0l__K2Node_DynamicCast_bSuccess__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ADiscoMaluch_C__pf1851508772), &Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsMaluchy_Game_Mode" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsMaluchy_Game_Mode", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf), Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_Event_DeltaSeconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_Event_DeltaSeconds", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, b0l__K2Node_Event_DeltaSeconds__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CallFunc_BreakVector_Z" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf = { UE4CodeGen_Private::EPropertyClass::Float, "CallFunc_BreakVector_Z", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, b0l__CallFunc_BreakVector_Z__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CallFunc_BreakVector_Y" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf = { UE4CodeGen_Private::EPropertyClass::Float, "CallFunc_BreakVector_Y", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, b0l__CallFunc_BreakVector_Y__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CallFunc_BreakVector_X" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf = { UE4CodeGen_Private::EPropertyClass::Float, "CallFunc_BreakVector_X", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, b0l__CallFunc_BreakVector_X__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Velocity__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Velocity" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "Velocity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Velocity__pf = { UE4CodeGen_Private::EPropertyClass::Float, "Velocity", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__Velocity__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Velocity__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Velocity__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__MultipierDecreaser__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Multipier Decreaser" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "MultipierDecreaser" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__MultipierDecreaser__pf = { UE4CodeGen_Private::EPropertyClass::Float, "MultipierDecreaser", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__MultipierDecreaser__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__MultipierDecreaser__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__MultipierDecreaser__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Yellow Pressed" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "YellowPressed" },
	};
#endif
	void Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_SetBit(void* Obj)
	{
		((ADiscoMaluch_C__pf1851508772*)Obj)->bpv__YellowPressed__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "YellowPressed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ADiscoMaluch_C__pf1851508772), &Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Yellow Timer" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "YellowTimer" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf = { UE4CodeGen_Private::EPropertyClass::Float, "YellowTimer", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__YellowTimer__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Prev X" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "PrevX" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf = { UE4CodeGen_Private::EPropertyClass::Float, "PrevX", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__PrevX__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Game Mode" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "GameMode" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf = { UE4CodeGen_Private::EPropertyClass::Object, "GameMode", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__GameMode__pf), Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Driving Multipier" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "DrivingMultipier" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf = { UE4CodeGen_Private::EPropertyClass::Float, "DrivingMultipier", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__DrivingMultipier__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Is Grounded" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "IsGrounded" },
	};
#endif
	void Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_SetBit(void* Obj)
	{
		((ADiscoMaluch_C__pf1851508772*)Obj)->bpv__IsGrounded__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "IsGrounded", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ADiscoMaluch_C__pf1851508772), &Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Prev Speed" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "PrevSpeed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "PrevSpeed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__PrevSpeed__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Became Unable to Drive" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "BecameUnableToDrive" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf = { UE4CodeGen_Private::EPropertyClass::MulticastDelegate, "BecameUnableToDrive", RF_Public|RF_Transient, (EPropertyFlags)0x0010100010090005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__BecameUnableToDrive__pf), Z_Construct_UDelegateFunction_ADiscoMaluch_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluch_C__pf__MulticastDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Able to Drive" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "AbleToDrive" },
	};
#endif
	void Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_SetBit(void* Obj)
	{
		((ADiscoMaluch_C__pf1851508772*)Obj)->bpv__AbleToDrive__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "AbleToDrive", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ADiscoMaluch_C__pf1851508772), &Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Changing Gear Trashhold" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "ChangingGearTrashhold" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf = { UE4CodeGen_Private::EPropertyClass::Float, "ChangingGearTrashhold", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__ChangingGearTrashhold__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Resetting Distance" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "ResettingDistance" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf = { UE4CodeGen_Private::EPropertyClass::Float, "ResettingDistance", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__ResettingDistance__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Camera Steering Sensitivity" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CameraSteeringSensitivity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf = { UE4CodeGen_Private::EPropertyClass::Float, "CameraSteeringSensitivity", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__CameraSteeringSensitivity__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Steering Sensitivity" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "SteeringSensitivity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf = { UE4CodeGen_Private::EPropertyClass::Float, "SteeringSensitivity", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__SteeringSensitivity__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Driving Force" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "DrivingForce" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf = { UE4CodeGen_Private::EPropertyClass::Float, "DrivingForce", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__DrivingForce__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Fuel Descreasing Speed" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "FuelDescreasingSpeed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "FuelDescreasingSpeed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__FuelDescreasingSpeed__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Start Rot" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "StartRot" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "StartRot", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__StartRot__pf), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Fuel" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "Fuel" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf = { UE4CodeGen_Private::EPropertyClass::Float, "Fuel", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__Fuel__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Coins Collected" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "CoinsCollected" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf = { UE4CodeGen_Private::EPropertyClass::Int, "CoinsCollected", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__CoinsCollected__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Distance Passed" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "DistancePassed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "DistancePassed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__DistancePassed__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StaticMesh__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "StaticMesh" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StaticMesh__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__StaticMesh__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StaticMesh__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StaticMesh__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Camera__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "Camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Camera__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Camera", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__Camera__pf), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Camera__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Camera__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Scene__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "Scene" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Scene__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Scene", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__Scene__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Scene__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Scene__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "SpringArm" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf = { UE4CodeGen_Private::EPropertyClass::Object, "SpringArm", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__SpringArm__pf), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FL__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "SimplifiedMeshSimplifiedAsStatic_FL" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FL__pf = { UE4CodeGen_Private::EPropertyClass::Object, "SimplifiedMeshSimplifiedAsStatic_FL", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__SimplifiedMeshSimplifiedAsStatic_FL__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FL__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FL__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RL__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "SimplifiedMeshSimplifiedAsStatic_RL" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RL__pf = { UE4CodeGen_Private::EPropertyClass::Object, "SimplifiedMeshSimplifiedAsStatic_RL", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__SimplifiedMeshSimplifiedAsStatic_RL__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RL__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RL__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RR__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "SimplifiedMeshSimplifiedAsStatic_RR" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RR__pf = { UE4CodeGen_Private::EPropertyClass::Object, "SimplifiedMeshSimplifiedAsStatic_RR", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__SimplifiedMeshSimplifiedAsStatic_RR__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RR__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RR__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FR__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "SimplifiedMeshSimplifiedAsStatic_FR" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FR__pf = { UE4CodeGen_Private::EPropertyClass::Object, "SimplifiedMeshSimplifiedAsStatic_FR", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__SimplifiedMeshSimplifiedAsStatic_FR__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FR__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FR__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFL__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "PhysicsConstraintFL" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFL__pf = { UE4CodeGen_Private::EPropertyClass::Object, "PhysicsConstraintFL", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__PhysicsConstraintFL__pf), Z_Construct_UClass_UPhysicsConstraintComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFL__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFL__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRL__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "PhysicsConstraintRL" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRL__pf = { UE4CodeGen_Private::EPropertyClass::Object, "PhysicsConstraintRL", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__PhysicsConstraintRL__pf), Z_Construct_UClass_UPhysicsConstraintComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRL__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRL__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFR__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "PhysicsConstraintFR" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFR__pf = { UE4CodeGen_Private::EPropertyClass::Object, "PhysicsConstraintFR", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__PhysicsConstraintFR__pf), Z_Construct_UClass_UPhysicsConstraintComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFR__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFR__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRR__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluch__pf1851508772.h" },
		{ "OverrideNativeName", "PhysicsConstraintRR" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRR__pf = { UE4CodeGen_Private::EPropertyClass::Object, "PhysicsConstraintRR", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluch_C__pf1851508772, bpv__PhysicsConstraintRR__pf), Z_Construct_UClass_UPhysicsConstraintComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRR__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRR__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Velocity__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__MultipierDecreaser__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__StaticMesh__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Camera__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__Scene__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FL__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RL__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_RR__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__SimplifiedMeshSimplifiedAsStatic_FR__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFL__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRL__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintFR__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::NewProp_bpv__PhysicsConstraintRR__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADiscoMaluch_C__pf1851508772>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::ClassParams = {
		&ADiscoMaluch_C__pf1851508772::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::PropPointers),
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Car/DiscoMaluch"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("DiscoMaluch_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ADiscoMaluch_C__pf1851508772, TEXT("DiscoMaluch_C"), 3355642653);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADiscoMaluch_C__pf1851508772(Z_Construct_UClass_ADiscoMaluch_C__pf1851508772, &ADiscoMaluch_C__pf1851508772::StaticClass, TEXT("/Game/Blueprints/Car/DiscoMaluch"), TEXT("DiscoMaluch_C"), true, TEXT("/Game/Blueprints/Car/DiscoMaluch"), TEXT("/Game/Blueprints/Car/DiscoMaluch.DiscoMaluch_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADiscoMaluch_C__pf1851508772);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
