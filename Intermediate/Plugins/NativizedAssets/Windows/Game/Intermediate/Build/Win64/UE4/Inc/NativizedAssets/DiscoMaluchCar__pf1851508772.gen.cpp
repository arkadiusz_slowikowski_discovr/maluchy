// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/DiscoMaluchCar__pf1851508772.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDiscoMaluchCar__pf1851508772() {}
// Cross Module References
	NATIVIZEDASSETS_API UFunction* Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_NoRegister();
	PHYSXVEHICLES_API UClass* Z_Construct_UClass_AWheeledVehicle();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpotLightComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "BecameUnableToDrive__DelegateSignature" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "BecameUnableToDrive__DelegateSignature", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00130000, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("BecameUnableToDrive__DelegateSignature") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static FName NAME_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void ADiscoMaluchCar_C__pf1851508772::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf = FName(TEXT("ReceiveTick"));
	void ADiscoMaluchCar_C__pf1851508772::eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
	{
		DiscoMaluchCar_C__pf1851508772_eventbpf__ReceiveTick__pf_Parms Parms;
		Parms.bpp__DeltaSeconds__pf=bpp__DeltaSeconds__pf;
		ProcessEvent(FindFunctionChecked(NAME_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf),&Parms);
	}
	static FName NAME_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void ADiscoMaluchCar_C__pf1851508772::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf),NULL);
	}
	void ADiscoMaluchCar_C__pf1851508772::StaticRegisterNativesADiscoMaluchCar_C__pf1851508772()
	{
		UClass* Class = ADiscoMaluchCar_C__pf1851508772::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddUIWidget", &ADiscoMaluchCar_C__pf1851508772::execbpf__AddUIWidget__pf },
			{ "CallResettingEnviroment", &ADiscoMaluchCar_C__pf1851508772::execbpf__CallResettingEnviroment__pf },
			{ "CantDrive", &ADiscoMaluchCar_C__pf1851508772::execbpf__CantDrive__pf },
			{ "ChangeGear", &ADiscoMaluchCar_C__pf1851508772::execbpf__ChangeGear__pf },
			{ "CheckIfGrounded", &ADiscoMaluchCar_C__pf1851508772::execbpf__CheckIfGrounded__pf },
			{ "CollectCoin", &ADiscoMaluchCar_C__pf1851508772::execbpf__CollectCoin__pf },
			{ "CollectFuel", &ADiscoMaluchCar_C__pf1851508772::execbpf__CollectFuel__pf },
			{ "CountDistance", &ADiscoMaluchCar_C__pf1851508772::execbpf__CountDistance__pf },
			{ "DecreaseFuel", &ADiscoMaluchCar_C__pf1851508772::execbpf__DecreaseFuel__pf },
			{ "Drive", &ADiscoMaluchCar_C__pf1851508772::execbpf__Drive__pf },
			{ "GetTotalMass", &ADiscoMaluchCar_C__pf1851508772::execbpf__GetTotalMass__pf },
			{ "HitByObstacle", &ADiscoMaluchCar_C__pf1851508772::execbpf__HitByObstacle__pf },
			{ "InpActEvt_YellowButton_K2Node_InputActionEvent_2", &ADiscoMaluchCar_C__pf1851508772::execbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf },
			{ "InpActEvt_YellowButton_K2Node_InputActionEvent_3", &ADiscoMaluchCar_C__pf1851508772::execbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf },
			{ "Move", &ADiscoMaluchCar_C__pf1851508772::execbpf__Move__pf },
			{ "NewSteer", &ADiscoMaluchCar_C__pf1851508772::execbpf__NewSteer__pf },
			{ "OldSteer", &ADiscoMaluchCar_C__pf1851508772::execbpf__OldSteer__pf },
			{ "ReceiveBeginPlay", &ADiscoMaluchCar_C__pf1851508772::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveTick", &ADiscoMaluchCar_C__pf1851508772::execbpf__ReceiveTick__pf },
			{ "SetAsMainCar", &ADiscoMaluchCar_C__pf1851508772::execbpf__SetAsMainCar__pf },
			{ "UserConstructionScript", &ADiscoMaluchCar_C__pf1851508772::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "AddUIWidget" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "AddUIWidget", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("AddUIWidget") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CallResettingEnviroment" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "CallResettingEnviroment", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CallResettingEnviroment") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CantDrive" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "CantDrive", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CantDrive") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__ChangeGear__pf_Parms
		{
			float bpp__Input__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__Input__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::NewProp_bpp__Input__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__Input__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__ChangeGear__pf_Parms, bpp__Input__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::NewProp_bpp__Input__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "ChangeGear" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "ChangeGear", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__ChangeGear__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ChangeGear") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__CheckIfGrounded__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__CheckIfGrounded__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CheckIfGrounded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "CheckIfGrounded", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__CheckIfGrounded__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CheckIfGrounded") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CollectCoin" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "CollectCoin", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CollectCoin") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__CollectFuel__pf_Parms
		{
			float bpp__HowxMuch__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__HowxMuch__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::NewProp_bpp__HowxMuch__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__HowxMuch__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__CollectFuel__pf_Parms, bpp__HowxMuch__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::NewProp_bpp__HowxMuch__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CollectFuel" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "CollectFuel", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__CollectFuel__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CollectFuel") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CountDistance" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "CountDistance", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("CountDistance") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__DecreaseFuel__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__DecreaseFuel__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "DecreaseFuel" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "DecreaseFuel", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__DecreaseFuel__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("DecreaseFuel") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__Drive__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__Drive__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "Drive" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "Drive", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__Drive__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Drive") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__GetTotalMass__pf_Parms
		{
			float bpp__TotalxMass__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__TotalxMass__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::NewProp_bpp__TotalxMass__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__TotalxMass__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__GetTotalMass__pf_Parms, bpp__TotalxMass__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::NewProp_bpp__TotalxMass__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "GetTotalMass" },
		{ "ToolTip", "out" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "GetTotalMass", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04420400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__GetTotalMass__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetTotalMass") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "HitByObstacle" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "HitByObstacle", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("HitByObstacle") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Parms
		{
			FKey bpp__Key__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Key__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::NewProp_bpp__Key__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__Key__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Parms, bpp__Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::NewProp_bpp__Key__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "InpActEvt_YellowButton_K2Node_InputActionEvent_2" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "InpActEvt_YellowButton_K2Node_InputActionEvent_2", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpActEvt_YellowButton_K2Node_InputActionEvent_2") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Parms
		{
			FKey bpp__Key__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Key__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::NewProp_bpp__Key__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__Key__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Parms, bpp__Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::NewProp_bpp__Key__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "InpActEvt_YellowButton_K2Node_InputActionEvent_3" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "InpActEvt_YellowButton_K2Node_InputActionEvent_3", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpActEvt_YellowButton_K2Node_InputActionEvent_3") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__Move__pf_Parms
		{
			float bpp__DeltaxTime__pfT;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaxTime__pfT;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::NewProp_bpp__DeltaxTime__pfT = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaxTime__pfT", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__Move__pf_Parms, bpp__DeltaxTime__pfT), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::NewProp_bpp__DeltaxTime__pfT,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "Move" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "Move", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__Move__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Move") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__NewSteer__pf_Parms
		{
			float bpp__Input__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__Input__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::NewProp_bpp__Input__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__Input__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__NewSteer__pf_Parms, bpp__Input__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::NewProp_bpp__Input__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "NewSteer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "NewSteer", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__NewSteer__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("NewSteer") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics
	{
		struct DiscoMaluchCar_C__pf1851508772_eventbpf__OldSteer__pf_Parms
		{
			float bpp__Input__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__Input__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::NewProp_bpp__Input__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__Input__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__OldSteer__pf_Parms, bpp__Input__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::NewProp_bpp__Input__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "OldSteer" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "OldSteer", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__OldSteer__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("OldSteer") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaSeconds__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaSeconds__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(DiscoMaluchCar_C__pf1851508772_eventbpf__ReceiveTick__pf_Parms, bpp__DeltaSeconds__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Tick" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "ReceiveTick" },
		{ "ToolTip", "Event called every frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "ReceiveTick", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(DiscoMaluchCar_C__pf1851508772_eventbpf__ReceiveTick__pf_Parms), Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveTick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "SetAsMainCar" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "SetAsMainCar", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("SetAsMainCar") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_NoRegister()
	{
		return ADiscoMaluchCar_C__pf1851508772::StaticClass();
	}
	struct Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakVector_Z__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakVector_Z__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakVector_Y__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakVector_Y__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakVector_X__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakVector_X__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputActionEvent_Key1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_InputActionEvent_Key1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_InputActionEvent_Key__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__Temp_struct_Variable__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__Temp_struct_Variable__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_Event_DeltaSeconds__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__YellowPressed__pf_MetaData[];
#endif
		static void NewProp_bpv__YellowPressed__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__YellowPressed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__YellowTimer__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__YellowTimer__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__PrevX__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__PrevX__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__GameMode__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__GameMode__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DrivingMultipier__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__DrivingMultipier__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__IsGrounded__pf_MetaData[];
#endif
		static void NewProp_bpv__IsGrounded__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__IsGrounded__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__PrevSpeed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__PrevSpeed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__BecameUnableToDrive__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_bpv__BecameUnableToDrive__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__AbleToDrive__pf_MetaData[];
#endif
		static void NewProp_bpv__AbleToDrive__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__AbleToDrive__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ChangingGearTrashhold__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__ChangingGearTrashhold__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ResettingDistance__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__ResettingDistance__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__CameraSteeringSensitivity__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__CameraSteeringSensitivity__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SteeringSensitivity__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__SteeringSensitivity__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DrivingForce__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__DrivingForce__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__FuelDescreasingSpeed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__FuelDescreasingSpeed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StartRot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpv__StartRot__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Fuel__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__Fuel__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__CoinsCollected__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__CoinsCollected__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DistancePassed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__DistancePassed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SpringArm__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SpringArm__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Headlights__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Headlights__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Left__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Left__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Right__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Right__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Camera__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Camera__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AWheeledVehicle,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::FuncInfo[] = {
		{ &Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature, "BecameUnableToDrive__DelegateSignature" }, // 661389967
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__AddUIWidget__pf, "AddUIWidget" }, // 1981605881
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CallResettingEnviroment__pf, "CallResettingEnviroment" }, // 839860581
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CantDrive__pf, "CantDrive" }, // 1049172853
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ChangeGear__pf, "ChangeGear" }, // 4176331391
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CheckIfGrounded__pf, "CheckIfGrounded" }, // 3133344135
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectCoin__pf, "CollectCoin" }, // 240069542
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CollectFuel__pf, "CollectFuel" }, // 554503143
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__CountDistance__pf, "CountDistance" }, // 2102145658
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__DecreaseFuel__pf, "DecreaseFuel" }, // 2768106179
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Drive__pf, "Drive" }, // 3151272692
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__GetTotalMass__pf, "GetTotalMass" }, // 2902633405
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__HitByObstacle__pf, "HitByObstacle" }, // 1252371153
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf, "InpActEvt_YellowButton_K2Node_InputActionEvent_2" }, // 3177846534
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf, "InpActEvt_YellowButton_K2Node_InputActionEvent_3" }, // 164510751
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__Move__pf, "Move" }, // 1961049696
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__NewSteer__pf, "NewSteer" }, // 4049037244
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__OldSteer__pf, "OldSteer" }, // 1976116660
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 1758087227
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__ReceiveTick__pf, "ReceiveTick" }, // 3230176196
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__SetAsMainCar__pf, "SetAsMainCar" }, // 3979161530
		{ &Z_Construct_UFunction_ADiscoMaluchCar_C__pf1851508772_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 1054006801
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "DiscoMaluchCar__pf1851508772.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "DiscoMaluchCar_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Car/DiscoMaluchCar.DiscoMaluchCar_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CallFunc_BreakVector_Z" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf = { UE4CodeGen_Private::EPropertyClass::Float, "CallFunc_BreakVector_Z", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, b0l__CallFunc_BreakVector_Z__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CallFunc_BreakVector_Y" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf = { UE4CodeGen_Private::EPropertyClass::Float, "CallFunc_BreakVector_Y", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, b0l__CallFunc_BreakVector_Y__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CallFunc_BreakVector_X" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf = { UE4CodeGen_Private::EPropertyClass::Float, "CallFunc_BreakVector_X", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, b0l__CallFunc_BreakVector_X__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_InputActionEvent_Key1" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_InputActionEvent_Key1", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, b0l__K2Node_InputActionEvent_Key1__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_InputActionEvent_Key" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_InputActionEvent_Key", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, b0l__K2Node_InputActionEvent_Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "Temp_struct_Variable" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "Temp_struct_Variable", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, b0l__Temp_struct_Variable__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "K2Node_Event_DeltaSeconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_Event_DeltaSeconds", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, b0l__K2Node_Event_DeltaSeconds__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Yellow Pressed" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "YellowPressed" },
	};
#endif
	void Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_SetBit(void* Obj)
	{
		((ADiscoMaluchCar_C__pf1851508772*)Obj)->bpv__YellowPressed__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "YellowPressed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ADiscoMaluchCar_C__pf1851508772), &Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Yellow Timer" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "YellowTimer" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf = { UE4CodeGen_Private::EPropertyClass::Float, "YellowTimer", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__YellowTimer__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Prev X" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "PrevX" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf = { UE4CodeGen_Private::EPropertyClass::Float, "PrevX", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__PrevX__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Game Mode" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "GameMode" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf = { UE4CodeGen_Private::EPropertyClass::Object, "GameMode", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__GameMode__pf), Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Driving Multipier" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "DrivingMultipier" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf = { UE4CodeGen_Private::EPropertyClass::Float, "DrivingMultipier", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__DrivingMultipier__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Is Grounded" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "IsGrounded" },
	};
#endif
	void Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_SetBit(void* Obj)
	{
		((ADiscoMaluchCar_C__pf1851508772*)Obj)->bpv__IsGrounded__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "IsGrounded", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ADiscoMaluchCar_C__pf1851508772), &Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Prev Speed" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "PrevSpeed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "PrevSpeed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__PrevSpeed__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Became Unable to Drive" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "BecameUnableToDrive" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf = { UE4CodeGen_Private::EPropertyClass::MulticastDelegate, "BecameUnableToDrive", RF_Public|RF_Transient, (EPropertyFlags)0x0010100010090005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__BecameUnableToDrive__pf), Z_Construct_UDelegateFunction_ADiscoMaluchCar_C__pf1851508772_BecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Able to Drive" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "AbleToDrive" },
	};
#endif
	void Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_SetBit(void* Obj)
	{
		((ADiscoMaluchCar_C__pf1851508772*)Obj)->bpv__AbleToDrive__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "AbleToDrive", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ADiscoMaluchCar_C__pf1851508772), &Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Changing Gear Trashhold" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "ChangingGearTrashhold" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf = { UE4CodeGen_Private::EPropertyClass::Float, "ChangingGearTrashhold", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__ChangingGearTrashhold__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Resetting Distance" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "ResettingDistance" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf = { UE4CodeGen_Private::EPropertyClass::Float, "ResettingDistance", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__ResettingDistance__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Camera Steering Sensitivity" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CameraSteeringSensitivity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf = { UE4CodeGen_Private::EPropertyClass::Float, "CameraSteeringSensitivity", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__CameraSteeringSensitivity__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Steering Sensitivity" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "SteeringSensitivity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf = { UE4CodeGen_Private::EPropertyClass::Float, "SteeringSensitivity", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__SteeringSensitivity__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Driving Force" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "DrivingForce" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf = { UE4CodeGen_Private::EPropertyClass::Float, "DrivingForce", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__DrivingForce__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Fuel Descreasing Speed" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "FuelDescreasingSpeed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "FuelDescreasingSpeed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__FuelDescreasingSpeed__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Start Rot" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "StartRot" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "StartRot", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__StartRot__pf), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Fuel" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "Fuel" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf = { UE4CodeGen_Private::EPropertyClass::Float, "Fuel", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__Fuel__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Coins Collected" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "CoinsCollected" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf = { UE4CodeGen_Private::EPropertyClass::Int, "CoinsCollected", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__CoinsCollected__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Distance Passed" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "DistancePassed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "DistancePassed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__DistancePassed__pf), METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "SpringArm" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf = { UE4CodeGen_Private::EPropertyClass::Object, "SpringArm", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__SpringArm__pf), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Headlights__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "Headlights" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Headlights__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Headlights", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__Headlights__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Headlights__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Headlights__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Left__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "Left" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Left__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Left", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__Left__pf), Z_Construct_UClass_USpotLightComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Left__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Left__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Right__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "Right" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Right__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Right", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__Right__pf), Z_Construct_UClass_USpotLightComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Right__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Right__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Camera__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DiscoMaluchCar__pf1851508772.h" },
		{ "OverrideNativeName", "Camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Camera__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Camera", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(ADiscoMaluchCar_C__pf1851508772, bpv__Camera__pf), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Camera__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Camera__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Z__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_Y__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__CallFunc_BreakVector_X__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__Temp_struct_Variable__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowPressed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__YellowTimer__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevX__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__GameMode__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingMultipier__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__IsGrounded__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__PrevSpeed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__BecameUnableToDrive__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__AbleToDrive__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ChangingGearTrashhold__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__ResettingDistance__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CameraSteeringSensitivity__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SteeringSensitivity__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DrivingForce__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__FuelDescreasingSpeed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__StartRot__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Fuel__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__CoinsCollected__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__DistancePassed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__SpringArm__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Headlights__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Left__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Right__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::NewProp_bpv__Camera__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADiscoMaluchCar_C__pf1851508772>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::ClassParams = {
		&ADiscoMaluchCar_C__pf1851508772::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::PropPointers),
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Car/DiscoMaluchCar"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("DiscoMaluchCar_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ADiscoMaluchCar_C__pf1851508772, TEXT("DiscoMaluchCar_C"), 3380566758);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADiscoMaluchCar_C__pf1851508772(Z_Construct_UClass_ADiscoMaluchCar_C__pf1851508772, &ADiscoMaluchCar_C__pf1851508772::StaticClass, TEXT("/Game/Blueprints/Car/DiscoMaluchCar"), TEXT("DiscoMaluchCar_C"), true, TEXT("/Game/Blueprints/Car/DiscoMaluchCar"), TEXT("/Game/Blueprints/Car/DiscoMaluchCar.DiscoMaluchCar_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADiscoMaluchCar_C__pf1851508772);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
