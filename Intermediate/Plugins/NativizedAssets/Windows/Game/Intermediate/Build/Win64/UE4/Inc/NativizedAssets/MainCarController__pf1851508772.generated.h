// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FKey;
#ifdef NATIVIZEDASSETS_MainCarController__pf1851508772_generated_h
#error "MainCarController__pf1851508772.generated.h already included, missing '#pragma once' in MainCarController__pf1851508772.h"
#endif
#define NATIVIZEDASSETS_MainCarController__pf1851508772_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__Restart__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Restart__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__RollCar__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RollCar__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__MoveHor__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__MoveHor__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__Restart__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__Restart__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__RollCar__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RollCar__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__MoveHor__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__Input__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__MoveHor__pf(Z_Param_bpp__Input__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_EVENT_PARMS
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveBeginPlay__pf(); \
 \
	void eventbpf__UserConstructionScript__pf(); \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainCarController_C__pf1851508772(); \
	friend struct Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics; \
public: \
	DECLARE_CLASS(AMainCarController_C__pf1851508772, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Game/Blueprints/Car/MainCarController"), NO_API) \
	DECLARE_SERIALIZER(AMainCarController_C__pf1851508772)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMainCarController_C__pf1851508772(); \
	friend struct Z_Construct_UClass_AMainCarController_C__pf1851508772_Statics; \
public: \
	DECLARE_CLASS(AMainCarController_C__pf1851508772, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Game/Blueprints/Car/MainCarController"), NO_API) \
	DECLARE_SERIALIZER(AMainCarController_C__pf1851508772)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainCarController_C__pf1851508772(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainCarController_C__pf1851508772) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainCarController_C__pf1851508772); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainCarController_C__pf1851508772); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainCarController_C__pf1851508772(AMainCarController_C__pf1851508772&&); \
	NO_API AMainCarController_C__pf1851508772(const AMainCarController_C__pf1851508772&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainCarController_C__pf1851508772(AMainCarController_C__pf1851508772&&); \
	NO_API AMainCarController_C__pf1851508772(const AMainCarController_C__pf1851508772&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainCarController_C__pf1851508772); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainCarController_C__pf1851508772); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainCarController_C__pf1851508772)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_8_PROLOG \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_EVENT_PARMS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h_12_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_MainCarController__pf1851508772_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
