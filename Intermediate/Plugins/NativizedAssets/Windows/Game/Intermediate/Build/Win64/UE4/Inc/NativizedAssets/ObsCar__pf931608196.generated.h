// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef NATIVIZEDASSETS_ObsCar__pf931608196_generated_h
#error "ObsCar__pf931608196.generated.h already included, missing '#pragma once' in ObsCar__pf931608196.h"
#endif
#define NATIVIZEDASSETS_ObsCar__pf931608196_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__RandomizeMaterial__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RandomizeMaterial__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveHit__pf) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_bpp__MyComp__pf); \
		P_GET_OBJECT(AActor,Z_Param_bpp__Other__pf); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_bpp__OtherComp__pf); \
		P_GET_UBOOL(Z_Param_bpp__bSelfMoved__pf); \
		P_GET_STRUCT(FVector,Z_Param_bpp__HitLocation__pf); \
		P_GET_STRUCT(FVector,Z_Param_bpp__HitNormal__pf); \
		P_GET_STRUCT(FVector,Z_Param_bpp__NormalImpulse__pf); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_bpp__Hit__pf__const); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveHit__pf(Z_Param_bpp__MyComp__pf,Z_Param_bpp__Other__pf,Z_Param_bpp__OtherComp__pf,Z_Param_bpp__bSelfMoved__pf,Z_Param_bpp__HitLocation__pf,Z_Param_bpp__HitNormal__pf,Z_Param_bpp__NormalImpulse__pf,Z_Param_Out_bpp__Hit__pf__const); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorBeginOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorBeginOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__RandomizeMaterial__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RandomizeMaterial__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveHit__pf) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_bpp__MyComp__pf); \
		P_GET_OBJECT(AActor,Z_Param_bpp__Other__pf); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_bpp__OtherComp__pf); \
		P_GET_UBOOL(Z_Param_bpp__bSelfMoved__pf); \
		P_GET_STRUCT(FVector,Z_Param_bpp__HitLocation__pf); \
		P_GET_STRUCT(FVector,Z_Param_bpp__HitNormal__pf); \
		P_GET_STRUCT(FVector,Z_Param_bpp__NormalImpulse__pf); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_bpp__Hit__pf__const); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveHit__pf(Z_Param_bpp__MyComp__pf,Z_Param_bpp__Other__pf,Z_Param_bpp__OtherComp__pf,Z_Param_bpp__bSelfMoved__pf,Z_Param_bpp__HitLocation__pf,Z_Param_bpp__HitNormal__pf,Z_Param_bpp__NormalImpulse__pf,Z_Param_Out_bpp__Hit__pf__const); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorBeginOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorBeginOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_EVENT_PARMS \
	struct ObsCar_C__pf931608196_eventbpf__ReceiveActorBeginOverlap__pf_Parms \
	{ \
		AActor* bpp__OtherActor__pf; \
	}; \
	struct ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms \
	{ \
		UPrimitiveComponent* bpp__MyComp__pf; \
		AActor* bpp__Other__pf; \
		UPrimitiveComponent* bpp__OtherComp__pf; \
		bool bpp__bSelfMoved__pf; \
		FVector bpp__HitLocation__pf; \
		FVector bpp__HitNormal__pf; \
		FVector bpp__NormalImpulse__pf; \
		FHitResult bpp__Hit__pf__const; \
	};


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf); \
 \
	void eventbpf__ReceiveBeginPlay__pf(); \
 \
	void eventbpf__ReceiveHit__pf(UPrimitiveComponent* bpp__MyComp__pf, AActor* bpp__Other__pf, UPrimitiveComponent* bpp__OtherComp__pf, bool bpp__bSelfMoved__pf, FVector bpp__HitLocation__pf, FVector bpp__HitNormal__pf, FVector bpp__NormalImpulse__pf, FHitResult const& bpp__Hit__pf__const); \
 \
	void eventbpf__UserConstructionScript__pf(); \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAObsCar_C__pf931608196(); \
	friend struct Z_Construct_UClass_AObsCar_C__pf931608196_Statics; \
public: \
	DECLARE_CLASS(AObsCar_C__pf931608196, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/Car/OppCars/ObsCar"), NO_API) \
	DECLARE_SERIALIZER(AObsCar_C__pf931608196)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAObsCar_C__pf931608196(); \
	friend struct Z_Construct_UClass_AObsCar_C__pf931608196_Statics; \
public: \
	DECLARE_CLASS(AObsCar_C__pf931608196, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/Car/OppCars/ObsCar"), NO_API) \
	DECLARE_SERIALIZER(AObsCar_C__pf931608196)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AObsCar_C__pf931608196(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AObsCar_C__pf931608196) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObsCar_C__pf931608196); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObsCar_C__pf931608196); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObsCar_C__pf931608196(AObsCar_C__pf931608196&&); \
	NO_API AObsCar_C__pf931608196(const AObsCar_C__pf931608196&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObsCar_C__pf931608196(AObsCar_C__pf931608196&&); \
	NO_API AObsCar_C__pf931608196(const AObsCar_C__pf931608196&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObsCar_C__pf931608196); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObsCar_C__pf931608196); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AObsCar_C__pf931608196)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_14_PROLOG \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_EVENT_PARMS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_ObsCar__pf931608196_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
