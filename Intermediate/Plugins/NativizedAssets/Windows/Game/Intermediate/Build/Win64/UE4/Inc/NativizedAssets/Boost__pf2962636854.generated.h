// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef NATIVIZEDASSETS_Boost__pf2962636854_generated_h
#error "Boost__pf2962636854.generated.h already included, missing '#pragma once' in Boost__pf2962636854.h"
#endif
#define NATIVIZEDASSETS_Boost__pf2962636854_generated_h

#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorBeginOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorBeginOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__UserConstructionScript__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__UserConstructionScript__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveActorBeginOverlap__pf) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_bpp__OtherActor__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveActorBeginOverlap__pf(Z_Param_bpp__OtherActor__pf); \
		P_NATIVE_END; \
	}


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_EVENT_PARMS \
	struct Boost_C__pf2962636854_eventbpf__ReceiveActorBeginOverlap__pf_Parms \
	{ \
		AActor* bpp__OtherActor__pf; \
	};


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf); \
 \
	void eventbpf__UserConstructionScript__pf(); \



#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABoost_C__pf2962636854(); \
	friend struct Z_Construct_UClass_ABoost_C__pf2962636854_Statics; \
public: \
	DECLARE_CLASS(ABoost_C__pf2962636854, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/Collectables/Boost"), NO_API) \
	DECLARE_SERIALIZER(ABoost_C__pf2962636854)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABoost_C__pf2962636854(); \
	friend struct Z_Construct_UClass_ABoost_C__pf2962636854_Statics; \
public: \
	DECLARE_CLASS(ABoost_C__pf2962636854, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/Collectables/Boost"), NO_API) \
	DECLARE_SERIALIZER(ABoost_C__pf2962636854)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABoost_C__pf2962636854(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABoost_C__pf2962636854) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABoost_C__pf2962636854); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABoost_C__pf2962636854); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABoost_C__pf2962636854(ABoost_C__pf2962636854&&); \
	NO_API ABoost_C__pf2962636854(const ABoost_C__pf2962636854&); \
public:


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABoost_C__pf2962636854(ABoost_C__pf2962636854&&); \
	NO_API ABoost_C__pf2962636854(const ABoost_C__pf2962636854&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABoost_C__pf2962636854); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABoost_C__pf2962636854); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABoost_C__pf2962636854)


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_PRIVATE_PROPERTY_OFFSET
#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_10_PROLOG \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_EVENT_PARMS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_RPC_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_INCLASS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_PRIVATE_PROPERTY_OFFSET \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_CALLBACK_WRAPPERS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_INCLASS_NO_PURE_DECLS \
	maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h_14_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID maluchy_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_Boost__pf2962636854_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
