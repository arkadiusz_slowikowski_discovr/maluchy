// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/MaluchyGameMode__pf2132744816.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMaluchyGameMode__pf2132744816() {}
// Cross Module References
	NATIVIZEDASSETS_API UFunction* Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ARotatable_C__pf2962636854_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "Event GameStarted__DelegateSignature" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, "Event GameStarted__DelegateSignature", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00130000, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature()
	{
		UObject* Outer = Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Event GameStarted__DelegateSignature") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static FName NAME_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void AMaluchyGameMode_C__pf2132744816::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf = FName(TEXT("ReceiveTick"));
	void AMaluchyGameMode_C__pf2132744816::eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
	{
		MaluchyGameMode_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms Parms;
		Parms.bpp__DeltaSeconds__pf=bpp__DeltaSeconds__pf;
		ProcessEvent(FindFunctionChecked(NAME_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf),&Parms);
	}
	static FName NAME_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void AMaluchyGameMode_C__pf2132744816::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf),NULL);
	}
	void AMaluchyGameMode_C__pf2132744816::StaticRegisterNativesAMaluchyGameMode_C__pf2132744816()
	{
		UClass* Class = AMaluchyGameMode_C__pf2132744816::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GameEnded", &AMaluchyGameMode_C__pf2132744816::execbpf__GameEnded__pf },
			{ "ReceiveBeginPlay", &AMaluchyGameMode_C__pf2132744816::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveTick", &AMaluchyGameMode_C__pf2132744816::execbpf__ReceiveTick__pf },
			{ "ResetEnviroment", &AMaluchyGameMode_C__pf2132744816::execbpf__ResetEnviroment__pf },
			{ "ResetGame", &AMaluchyGameMode_C__pf2132744816::execbpf__ResetGame__pf },
			{ "RotateAllRotatables", &AMaluchyGameMode_C__pf2132744816::execbpf__RotateAllRotatables__pf },
			{ "UserConstructionScript", &AMaluchyGameMode_C__pf2132744816::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "GameEnded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, "GameEnded", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GameEnded") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaSeconds__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaSeconds__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MaluchyGameMode_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms, bpp__DeltaSeconds__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Tick" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "ReceiveTick" },
		{ "ToolTip", "Event called every frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, "ReceiveTick", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(MaluchyGameMode_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms), Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveTick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "ResetEnviroment" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, "ResetEnviroment", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ResetEnviroment") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "ResetGame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, "ResetGame", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ResetGame") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics
	{
		struct MaluchyGameMode_C__pf2132744816_eventbpf__RotateAllRotatables__pf_Parms
		{
			float bpp__DeltaTime__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaTime__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::NewProp_bpp__DeltaTime__pf = { UE4CodeGen_Private::EPropertyClass::Float, "bpp__DeltaTime__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MaluchyGameMode_C__pf2132744816_eventbpf__RotateAllRotatables__pf_Parms, bpp__DeltaTime__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::NewProp_bpp__DeltaTime__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "RotateAllRotatables" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, "RotateAllRotatables", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, sizeof(MaluchyGameMode_C__pf2132744816_eventbpf__RotateAllRotatables__pf_Parms), Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("RotateAllRotatables") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_NoRegister()
	{
		return AMaluchyGameMode_C__pf2132744816::StaticClass();
	}
	struct Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_Event_DeltaSeconds__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Rotatables__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_bpv__Rotatables__pf;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Rotatables__pf_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__GameAlreadyEnded__pf_MetaData[];
#endif
		static void NewProp_bpv__GameAlreadyEnded__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__GameAlreadyEnded__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__EventxGameStarted__pfT_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_bpv__EventxGameStarted__pfT;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__GameStarted__pf_MetaData[];
#endif
		static void NewProp_bpv__GameStarted__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__GameStarted__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__IsResetting__pf_MetaData[];
#endif
		static void NewProp_bpv__IsResetting__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__IsResetting__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Resetables__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_bpv__Resetables__pf;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Resetables__pf_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__RoadSegmentsSpeed__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__RoadSegmentsSpeed__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Car__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Car__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DefaultSceneRoot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__DefaultSceneRoot__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__GameEnded__pf, "GameEnded" }, // 3875111077
		{ &Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 2942219106
		{ &Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ReceiveTick__pf, "ReceiveTick" }, // 2997213801
		{ &Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetEnviroment__pf, "ResetEnviroment" }, // 2674381344
		{ &Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__ResetGame__pf, "ResetGame" }, // 1884216613
		{ &Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__RotateAllRotatables__pf, "RotateAllRotatables" }, // 1654968651
		{ &Z_Construct_UFunction_AMaluchyGameMode_C__pf2132744816_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 3077418940
		{ &Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature, "Event GameStarted__DelegateSignature" }, // 4284265410
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MaluchyGameMode__pf2132744816.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "MaluchyGameMode_C" },
		{ "ReplaceConverted", "/Game/Blueprints/MaluchyGameMode.MaluchyGameMode_C,/Game/Blueprints/CarGameMode.CarGameMode_C,/Game/CarGameMode.CarGameMode_C" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_Event_DeltaSeconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf = { UE4CodeGen_Private::EPropertyClass::Float, "K2Node_Event_DeltaSeconds", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AMaluchyGameMode_C__pf2132744816, b0l__K2Node_Event_DeltaSeconds__pf), METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Rotatables__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Rotatables" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "Rotatables" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Rotatables__pf = { UE4CodeGen_Private::EPropertyClass::Array, "Rotatables", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(AMaluchyGameMode_C__pf2132744816, bpv__Rotatables__pf), METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Rotatables__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Rotatables__pf_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Rotatables__pf_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "bpv__Rotatables__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ARotatable_C__pf2962636854_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameAlreadyEnded__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Game Already Ended" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "GameAlreadyEnded" },
	};
#endif
	void Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameAlreadyEnded__pf_SetBit(void* Obj)
	{
		((AMaluchyGameMode_C__pf2132744816*)Obj)->bpv__GameAlreadyEnded__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameAlreadyEnded__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "GameAlreadyEnded", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMaluchyGameMode_C__pf2132744816), &Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameAlreadyEnded__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameAlreadyEnded__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameAlreadyEnded__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__EventxGameStarted__pfT_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Event Game Started" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "Event GameStarted" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__EventxGameStarted__pfT = { UE4CodeGen_Private::EPropertyClass::MulticastDelegate, "Event GameStarted", RF_Public|RF_Transient, (EPropertyFlags)0x0010100010090005, 1, nullptr, STRUCT_OFFSET(AMaluchyGameMode_C__pf2132744816, bpv__EventxGameStarted__pfT), Z_Construct_UDelegateFunction_AMaluchyGameMode_C__pf2132744816_EventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__EventxGameStarted__pfT_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__EventxGameStarted__pfT_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameStarted__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Game Started" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "GameStarted" },
	};
#endif
	void Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameStarted__pf_SetBit(void* Obj)
	{
		((AMaluchyGameMode_C__pf2132744816*)Obj)->bpv__GameStarted__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameStarted__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "GameStarted", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMaluchyGameMode_C__pf2132744816), &Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameStarted__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameStarted__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameStarted__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__IsResetting__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Is Resetting" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "IsResetting" },
	};
#endif
	void Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__IsResetting__pf_SetBit(void* Obj)
	{
		((AMaluchyGameMode_C__pf2132744816*)Obj)->bpv__IsResetting__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__IsResetting__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "IsResetting", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AMaluchyGameMode_C__pf2132744816), &Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__IsResetting__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__IsResetting__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__IsResetting__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Resetables__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Resetables" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "Resetables" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Resetables__pf = { UE4CodeGen_Private::EPropertyClass::Array, "Resetables", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(AMaluchyGameMode_C__pf2132744816, bpv__Resetables__pf), METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Resetables__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Resetables__pf_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Resetables__pf_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "bpv__Resetables__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__RoadSegmentsSpeed__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Road Segments Speed" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "RoadSegmentsSpeed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__RoadSegmentsSpeed__pf = { UE4CodeGen_Private::EPropertyClass::Float, "RoadSegmentsSpeed", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AMaluchyGameMode_C__pf2132744816, bpv__RoadSegmentsSpeed__pf), METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__RoadSegmentsSpeed__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__RoadSegmentsSpeed__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Car__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Car" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "Car" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Car__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Car", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(AMaluchyGameMode_C__pf2132744816, bpv__Car__pf), Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Car__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Car__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MaluchyGameMode__pf2132744816.h" },
		{ "OverrideNativeName", "DefaultSceneRoot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf = { UE4CodeGen_Private::EPropertyClass::Object, "DefaultSceneRoot", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AMaluchyGameMode_C__pf2132744816, bpv__DefaultSceneRoot__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Rotatables__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Rotatables__pf_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameAlreadyEnded__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__EventxGameStarted__pfT,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__GameStarted__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__IsResetting__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Resetables__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Resetables__pf_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__RoadSegmentsSpeed__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__Car__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMaluchyGameMode_C__pf2132744816>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::ClassParams = {
		&AMaluchyGameMode_C__pf2132744816::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008002A8u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::PropPointers),
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/MaluchyGameMode"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("MaluchyGameMode_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(AMaluchyGameMode_C__pf2132744816, TEXT("MaluchyGameMode_C"), 3096440931);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMaluchyGameMode_C__pf2132744816(Z_Construct_UClass_AMaluchyGameMode_C__pf2132744816, &AMaluchyGameMode_C__pf2132744816::StaticClass, TEXT("/Game/Blueprints/MaluchyGameMode"), TEXT("MaluchyGameMode_C"), true, TEXT("/Game/Blueprints/MaluchyGameMode"), TEXT("/Game/Blueprints/MaluchyGameMode.MaluchyGameMode_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMaluchyGameMode_C__pf2132744816);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
