// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Private/NativizedAssets.h"
#include "NativizedAssets/Public/ObsCar__pf931608196.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeObsCar__pf931608196() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AObsCar_C__pf931608196_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AObsCar_C__pf931608196();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpotLightComponent_NoRegister();
// End Cross Module References
	static FName NAME_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf = FName(TEXT("ReceiveActorBeginOverlap"));
	void AObsCar_C__pf931608196::eventbpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf)
	{
		ObsCar_C__pf931608196_eventbpf__ReceiveActorBeginOverlap__pf_Parms Parms;
		Parms.bpp__OtherActor__pf=bpp__OtherActor__pf;
		ProcessEvent(FindFunctionChecked(NAME_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf),&Parms);
	}
	static FName NAME_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void AObsCar_C__pf931608196::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_AObsCar_C__pf931608196_bpf__ReceiveHit__pf = FName(TEXT("ReceiveHit"));
	void AObsCar_C__pf931608196::eventbpf__ReceiveHit__pf(UPrimitiveComponent* bpp__MyComp__pf, AActor* bpp__Other__pf, UPrimitiveComponent* bpp__OtherComp__pf, bool bpp__bSelfMoved__pf, FVector bpp__HitLocation__pf, FVector bpp__HitNormal__pf, FVector bpp__NormalImpulse__pf, FHitResult const& bpp__Hit__pf__const)
	{
		ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms Parms;
		Parms.bpp__MyComp__pf=bpp__MyComp__pf;
		Parms.bpp__Other__pf=bpp__Other__pf;
		Parms.bpp__OtherComp__pf=bpp__OtherComp__pf;
		Parms.bpp__bSelfMoved__pf=bpp__bSelfMoved__pf ? true : false;
		Parms.bpp__HitLocation__pf=bpp__HitLocation__pf;
		Parms.bpp__HitNormal__pf=bpp__HitNormal__pf;
		Parms.bpp__NormalImpulse__pf=bpp__NormalImpulse__pf;
		Parms.bpp__Hit__pf__const=bpp__Hit__pf__const;
		ProcessEvent(FindFunctionChecked(NAME_AObsCar_C__pf931608196_bpf__ReceiveHit__pf),&Parms);
	}
	static FName NAME_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf = FName(TEXT("UserConstructionScript"));
	void AObsCar_C__pf931608196::eventbpf__UserConstructionScript__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf),NULL);
	}
	void AObsCar_C__pf931608196::StaticRegisterNativesAObsCar_C__pf931608196()
	{
		UClass* Class = AObsCar_C__pf931608196::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "RandomizeMaterial", &AObsCar_C__pf931608196::execbpf__RandomizeMaterial__pf },
			{ "ReceiveActorBeginOverlap", &AObsCar_C__pf931608196::execbpf__ReceiveActorBeginOverlap__pf },
			{ "ReceiveBeginPlay", &AObsCar_C__pf931608196::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveHit", &AObsCar_C__pf931608196::execbpf__ReceiveHit__pf },
			{ "UserConstructionScript", &AObsCar_C__pf931608196::execbpf__UserConstructionScript__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "RandomizeMaterial" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AObsCar_C__pf931608196, "RandomizeMaterial", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020400, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf()
	{
		UObject* Outer = Z_Construct_UClass_AObsCar_C__pf931608196();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("RandomizeMaterial") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__OtherActor__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__OtherActor__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(ObsCar_C__pf931608196_eventbpf__ReceiveActorBeginOverlap__pf_Parms, bpp__OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::NewProp_bpp__OtherActor__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "Collision" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "ActorBeginOverlap" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "ReceiveActorBeginOverlap" },
		{ "ToolTip", "Event when this actor overlaps another actor, for example a player walking into a trigger.For events when objects have a blocking collision, for example a player hitting a wall, see 'Hit' events.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AObsCar_C__pf931608196, "ReceiveActorBeginOverlap", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, sizeof(ObsCar_C__pf931608196_eventbpf__ReceiveActorBeginOverlap__pf_Parms), Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf()
	{
		UObject* Outer = Z_Construct_UClass_AObsCar_C__pf931608196();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveActorBeginOverlap") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AObsCar_C__pf931608196, "ReceiveBeginPlay", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_AObsCar_C__pf931608196();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpp__Hit__pf__const_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Hit__pf__const;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__NormalImpulse__pf;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__HitNormal__pf;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__HitLocation__pf;
		static void NewProp_bpp__bSelfMoved__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpp__bSelfMoved__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpp__OtherComp__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__OtherComp__pf;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__Other__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpp__MyComp__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpp__MyComp__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__Hit__pf__const_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__Hit__pf__const = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__Hit__pf__const", RF_Public|RF_Transient, (EPropertyFlags)0x0010008008000182, 1, nullptr, STRUCT_OFFSET(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms, bpp__Hit__pf__const), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__Hit__pf__const_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__Hit__pf__const_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__NormalImpulse__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__NormalImpulse__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms, bpp__NormalImpulse__pf), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__HitNormal__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__HitNormal__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms, bpp__HitNormal__pf), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__HitLocation__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "bpp__HitLocation__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms, bpp__HitLocation__pf), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__bSelfMoved__pf_SetBit(void* Obj)
	{
		((ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms*)Obj)->bpp__bSelfMoved__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__bSelfMoved__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "bpp__bSelfMoved__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms), &Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__bSelfMoved__pf_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__OtherComp__pf_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__OtherComp__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__OtherComp__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000080080, 1, nullptr, STRUCT_OFFSET(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms, bpp__OtherComp__pf), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__OtherComp__pf_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__OtherComp__pf_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__Other__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__Other__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms, bpp__Other__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__MyComp__pf_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__MyComp__pf = { UE4CodeGen_Private::EPropertyClass::Object, "bpp__MyComp__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000080080, 1, nullptr, STRUCT_OFFSET(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms, bpp__MyComp__pf), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__MyComp__pf_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__MyComp__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__Hit__pf__const,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__NormalImpulse__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__HitNormal__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__HitLocation__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__bSelfMoved__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__OtherComp__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__Other__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::NewProp_bpp__MyComp__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "Collision" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Hit" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "ReceiveHit" },
		{ "ToolTip", "Event when this actor bumps into a blocking object, or blocks another actor that bumps into it.This could happen due to things like Character movement, using Set Location with 'sweep' enabled, or physics simulation.For events when objects overlap (e.g. walking into a trigger) see the 'Overlap' event.@note For collisions during physics simulation to generate hit events, 'Simulation Generates Hit Events' must be enabled.@note When receiving a hit from another object's movement (bSelfMoved is false), the directions of 'Hit.Normal' and 'Hit.ImpactNormal'will be adjusted to indicate force from the other object against this object.@note NormalImpulse will be filled in for physics-simulating bodies, but will be zero for swept-component blocking collisions." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AObsCar_C__pf931608196, "ReceiveHit", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x00C20C00, sizeof(ObsCar_C__pf931608196_eventbpf__ReceiveHit__pf_Parms), Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf()
	{
		UObject* Outer = Z_Construct_UClass_AObsCar_C__pf931608196();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveHit") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Construction Script" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "UserConstructionScript" },
		{ "ToolTip", "Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AObsCar_C__pf931608196, "UserConstructionScript", RF_Public|RF_Transient, nullptr, (EFunctionFlags)0x04020C00, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf()
	{
		UObject* Outer = Z_Construct_UClass_AObsCar_C__pf931608196();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("UserConstructionScript") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AObsCar_C__pf931608196_NoRegister()
	{
		return AObsCar_C__pf931608196::StaticClass();
	}
	struct Z_Construct_UClass_AObsCar_C__pf931608196_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_Hit__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_Event_Hit__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_NormalImpulse__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_Event_NormalImpulse__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_HitNormal__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_Event_HitNormal__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_HitLocation__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_Event_HitLocation__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_bSelfMoved__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_Event_bSelfMoved__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_Event_bSelfMoved__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_OtherComp__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_Event_OtherComp__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_Other__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_Event_Other__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_MyComp__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_Event_MyComp__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[];
#endif
		static void NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__K2Node_DynamicCast_bSuccess__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_b0l__K2Node_Event_OtherActor__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__MaterialsToDraw__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_bpv__MaterialsToDraw__pf;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__MaterialsToDraw__pf_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DefaultSceneRoot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__DefaultSceneRoot__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh3__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh3__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__StaticMesh4__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__StaticMesh4__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Headlights__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Headlights__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Left__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Left__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Right__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Right__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AObsCar_C__pf931608196_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AObsCar_C__pf931608196_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__RandomizeMaterial__pf, "RandomizeMaterial" }, // 1761557553
		{ &Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveActorBeginOverlap__pf, "ReceiveActorBeginOverlap" }, // 3338920057
		{ &Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 3064740671
		{ &Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__ReceiveHit__pf, "ReceiveHit" }, // 540484125
		{ &Z_Construct_UFunction_AObsCar_C__pf931608196_bpf__UserConstructionScript__pf, "UserConstructionScript" }, // 2569908032
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ObsCar__pf931608196.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "ObsCar_C" },
		{ "ReplaceConverted", "/Game/Blueprints/Car/OppCars/ObsCar.ObsCar_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Hit__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_Hit" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Hit__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_Event_Hit", RF_Public|RF_Transient, (EPropertyFlags)0x0010008000202000, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_Event_Hit__pf), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Hit__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Hit__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_NormalImpulse__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_NormalImpulse" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_NormalImpulse__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_Event_NormalImpulse", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_Event_NormalImpulse__pf), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_NormalImpulse__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_NormalImpulse__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitNormal__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_HitNormal" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitNormal__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_Event_HitNormal", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_Event_HitNormal__pf), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitNormal__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitNormal__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitLocation__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_HitLocation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitLocation__pf = { UE4CodeGen_Private::EPropertyClass::Struct, "K2Node_Event_HitLocation", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_Event_HitLocation__pf), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitLocation__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitLocation__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_bSelfMoved__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_bSelfMoved" },
	};
#endif
	void Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_bSelfMoved__pf_SetBit(void* Obj)
	{
		((AObsCar_C__pf931608196*)Obj)->b0l__K2Node_Event_bSelfMoved__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_bSelfMoved__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_Event_bSelfMoved", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AObsCar_C__pf931608196), &Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_bSelfMoved__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_bSelfMoved__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_bSelfMoved__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherComp__pf_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_OtherComp" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherComp__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_OtherComp", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000282008, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_Event_OtherComp__pf), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherComp__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherComp__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Other__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_Other" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Other__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_Other", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_Event_Other__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Other__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Other__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_MyComp__pf_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_MyComp" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_MyComp__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_MyComp", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000282008, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_Event_MyComp__pf), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_MyComp__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_MyComp__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_bSuccess" },
	};
#endif
	void Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit(void* Obj)
	{
		((AObsCar_C__pf931608196*)Obj)->b0l__K2Node_DynamicCast_bSuccess__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf = { UE4CodeGen_Private::EPropertyClass::Bool, "K2Node_DynamicCast_bSuccess", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AObsCar_C__pf931608196), &Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_DynamicCast_AsDisco_Maluch" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_DynamicCast_AsDisco_Maluch", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_DynamicCast_AsDisco_Maluch__pf), Z_Construct_UClass_ADiscoMaluch_C__pf1851508772_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "K2Node_Event_OtherActor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf = { UE4CodeGen_Private::EPropertyClass::Object, "K2Node_Event_OtherActor", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000202000, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, b0l__K2Node_Event_OtherActor__pf), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__MaterialsToDraw__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Materials to Draw" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "MaterialsToDraw" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__MaterialsToDraw__pf = { UE4CodeGen_Private::EPropertyClass::Array, "MaterialsToDraw", RF_Public|RF_Transient, (EPropertyFlags)0x0010000000010005, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__MaterialsToDraw__pf), METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__MaterialsToDraw__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__MaterialsToDraw__pf_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__MaterialsToDraw__pf_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "bpv__MaterialsToDraw__pf", RF_Public|RF_Transient, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "DefaultSceneRoot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__DefaultSceneRoot__pf = { UE4CodeGen_Private::EPropertyClass::Object, "DefaultSceneRoot", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__DefaultSceneRoot__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "StaticMesh" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__StaticMesh__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh1__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "StaticMesh1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh1__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh1", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__StaticMesh1__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh1__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh2__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "StaticMesh2" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh2__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh2", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__StaticMesh2__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh2__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh3__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "StaticMesh3" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh3__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh3", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__StaticMesh3__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh3__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh3__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh4__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "StaticMesh4" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh4__pf = { UE4CodeGen_Private::EPropertyClass::Object, "StaticMesh4", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__StaticMesh4__pf), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh4__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh4__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Headlights__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "Headlights" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Headlights__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Headlights", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__Headlights__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Headlights__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Headlights__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Left__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "Left" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Left__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Left", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__Left__pf), Z_Construct_UClass_USpotLightComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Left__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Left__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Right__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObsCar__pf931608196.h" },
		{ "OverrideNativeName", "Right" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Right__pf = { UE4CodeGen_Private::EPropertyClass::Object, "Right", RF_Public|RF_Transient, (EPropertyFlags)0x001000040008000c, 1, nullptr, STRUCT_OFFSET(AObsCar_C__pf931608196, bpv__Right__pf), Z_Construct_UClass_USpotLightComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Right__pf_MetaData, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Right__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AObsCar_C__pf931608196_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Hit__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_NormalImpulse__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitNormal__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_HitLocation__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_bSelfMoved__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherComp__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_Other__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_MyComp__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_bSuccess__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_DynamicCast_AsDisco_Maluch__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_b0l__K2Node_Event_OtherActor__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__MaterialsToDraw__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__MaterialsToDraw__pf_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__DefaultSceneRoot__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh3__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__StaticMesh4__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Headlights__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Left__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObsCar_C__pf931608196_Statics::NewProp_bpv__Right__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AObsCar_C__pf931608196_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AObsCar_C__pf931608196>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AObsCar_C__pf931608196_Statics::ClassParams = {
		&AObsCar_C__pf931608196::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AObsCar_C__pf931608196_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AObsCar_C__pf931608196_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AObsCar_C__pf931608196()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/Car/OppCars/ObsCar"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("ObsCar_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AObsCar_C__pf931608196_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(AObsCar_C__pf931608196, TEXT("ObsCar_C"), 705417808);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AObsCar_C__pf931608196(Z_Construct_UClass_AObsCar_C__pf931608196, &AObsCar_C__pf931608196::StaticClass, TEXT("/Game/Blueprints/Car/OppCars/ObsCar"), TEXT("ObsCar_C"), true, TEXT("/Game/Blueprints/Car/OppCars/ObsCar"), TEXT("/Game/Blueprints/Car/OppCars/ObsCar.ObsCar_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AObsCar_C__pf931608196);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
