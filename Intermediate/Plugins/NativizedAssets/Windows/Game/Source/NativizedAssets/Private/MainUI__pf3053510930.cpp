#include "NativizedAssets.h"
#include "MainUI__pf3053510930.h"
#include "GeneratedCodeHelpers.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/UMG/Public/Components/CanvasPanel.h"
#include "Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "Runtime/UMG/Public/Components/HorizontalBox.h"
#include "Runtime/UMG/Public/Components/HorizontalBoxSlot.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/Engine/Classes/Engine/Font.h"
#include "Runtime/UMG/Public/Components/ProgressBar.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/Engine/Classes/Engine/BlueprintGeneratedClass.h"
#include "MaluchyGameMode__pf2132744816.h"
#include "Runtime/Engine/Classes/Engine/SimpleConstructionScript.h"
#include "Runtime/CoreUObject/Public/UObject/NoExportTypes.h"
#include "Runtime/Engine/Classes/Engine/SCS_Node.h"
#include "Runtime/Engine/Classes/Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/EngineBaseTypes.h"
#include "Runtime/Engine/Classes/Engine/AssetUserData.h"
#include "Runtime/Engine/Classes/EdGraph/EdGraphPin.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Runtime/Engine/Classes/Engine/NetSerialization.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PhysicsVolume.h"
#include "Runtime/Engine/Classes/GameFramework/Volume.h"
#include "Runtime/Engine/Classes/Engine/Brush.h"
#include "Runtime/Engine/Classes/Components/BrushComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodyInstance.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterial.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsSettingsEnums.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterialPropertyBase.h"
#include "Runtime/Engine/Classes/Vehicles/TireType.h"
#include "Runtime/Engine/Classes/Engine/DataAsset.h"
#include "Runtime/InputCore/Classes/InputCoreTypes.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Engine/SubsurfaceProfile.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Engine/Public/MaterialShared.h"
#include "Runtime/Engine/Classes/Materials/MaterialExpression.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunction.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunctionInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollection.h"
#include "Runtime/Engine/Classes/Engine/BlendableInterface.h"
#include "Runtime/Engine/Classes/Engine/Texture.h"
#include "Runtime/Engine/Classes/Engine/TextureDefines.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_AssetUserData.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Materials/MaterialLayersFunctions.h"
#include "Runtime/Engine/Classes/Engine/FontImportOptions.h"
#include "Runtime/SlateCore/Public/Fonts/CompositeFont.h"
#include "Runtime/SlateCore/Public/Fonts/FontProviderInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceBasePropertyOverrides.h"
#include "Runtime/Engine/Public/StaticParameterSet.h"
#include "Runtime/Engine/Classes/GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"
#include "Runtime/Engine/Classes/GameFramework/Info.h"
#include "Runtime/Engine/Classes/Components/BillboardComponent.h"
#include "Runtime/Engine/Classes/GameFramework/LocalMessage.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineReplStructs.h"
#include "Runtime/CoreUObject/Public/UObject/CoreOnline.h"
#include "Runtime/Engine/Classes/GameFramework/EngineMessage.h"
#include "Runtime/Engine/Classes/GameFramework/DamageType.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/SkinnedMeshComponent.h"
#include "Runtime/Engine/Classes/Components/MeshComponent.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMesh.h"
#include "Runtime/Engine/Classes/Animation/Skeleton.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSocket.h"
#include "Runtime/Engine/Classes/Animation/SmartName.h"
#include "Runtime/Engine/Classes/Animation/BlendProfile.h"
#include "Runtime/Engine/Public/BoneContainer.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_PreviewMeshProvider.h"
#include "Runtime/Engine/Public/Components.h"
#include "Runtime/Engine/Public/PerPlatformProperties.h"
#include "Runtime/Engine/Public/SkeletalMeshReductionSettings.h"
#include "Runtime/Engine/Classes/Animation/AnimSequence.h"
#include "Runtime/Engine/Classes/Animation/AnimSequenceBase.h"
#include "Runtime/Engine/Classes/Animation/AnimationAsset.h"
#include "Runtime/Engine/Classes/Animation/AnimMetaData.h"
#include "Runtime/Engine/Public/Animation/AnimTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimLinkableElement.h"
#include "Runtime/Engine/Classes/Animation/AnimMontage.h"
#include "Runtime/Engine/Classes/Animation/AnimCompositeBase.h"
#include "Runtime/Engine/Public/AlphaBlend.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Curves/CurveBase.h"
#include "Runtime/Engine/Classes/Curves/RichCurve.h"
#include "Runtime/Engine/Classes/Curves/IndexedCurve.h"
#include "Runtime/Engine/Classes/Curves/KeyHandle.h"
#include "Runtime/Engine/Classes/Animation/AnimEnums.h"
#include "Runtime/Engine/Classes/Animation/TimeStretchCurve.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotify.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotifyState.h"
#include "Runtime/Engine/Public/Animation/AnimCurveTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetup.h"
#include "Runtime/Engine/Classes/PhysicsEngine/AggregateGeom.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphereElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ShapeElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BoxElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphylElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConvexElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/TaperedCapsuleElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetupEnums.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsAsset.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicalAnimationComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsConstraintTemplate.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintInstance.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintDrives.h"
#include "Runtime/Engine/Classes/EditorFramework/ThumbnailInfo.h"
#include "Runtime/Engine/Public/Animation/NodeMappingContainer.h"
#include "Runtime/Engine/Public/Animation/NodeMappingProviderInterface.h"
#include "Runtime/Engine/Classes/Animation/MorphTarget.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include "Runtime/Engine/Public/Animation/AnimNotifyQueue.h"
#include "Runtime/Engine/Public/Animation/PoseSnapshot.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingAssetInterface.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSampling.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshLODSettings.h"
#include "Runtime/Engine/Classes/Engine/Blueprint.h"
#include "Runtime/Engine/Classes/Engine/BlueprintCore.h"
#include "Runtime/Engine/Classes/Engine/TimelineTemplate.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "Runtime/Engine/Classes/Curves/CurveVector.h"
#include "Runtime/Engine/Classes/Curves/CurveLinearColor.h"
#include "Runtime/Engine/Classes/Engine/InheritableComponentHandler.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_CollisionDataProvider.h"
#include "Runtime/Engine/Classes/Animation/AnimBlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/DynamicBlueprintBinding.h"
#include "Runtime/Engine/Classes/Animation/AnimStateMachineTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimClassInterface.h"
#include "Runtime/Engine/Public/SingleAnimationPlayData.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationFactoryInterface.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationInteractor.h"
#include "Runtime/Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PawnMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/NavMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/MovementComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationTypes.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationAvoidanceTypes.h"
#include "Runtime/Engine/Classes/GameFramework/RootMotionSource.h"
#include "Runtime/Engine/Public/AI/RVOAvoidanceInterface.h"
#include "Runtime/Engine/Classes/Interfaces/NetworkPredictionInterface.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAreaBase.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Navigation/PathFollowingComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationData.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataInterface.h"
#include "Runtime/AIModule/Classes/AIResourceInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/PathFollowingAgentInterface.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardData.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include "Runtime/AIModule/Classes/Perception/AISenseConfig.h"
#include "Runtime/AIModule/Classes/Perception/AISense.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionSystem.h"
#include "Runtime/AIModule/Classes/Perception/AISenseEvent.h"
#include "Runtime/AIModule/Classes/Actions/PawnActionsComponent.h"
#include "Runtime/AIModule/Classes/Actions/PawnAction.h"
#include "Runtime/AIModule/Classes/AITypes.h"
#include "Runtime/GameplayTasks/Classes/GameplayTasksComponent.h"
#include "Runtime/GameplayTasks/Classes/GameplayTask.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskOwnerInterface.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskResource.h"
#include "Runtime/NavigationSystem/Public/NavFilters/NavigationQueryFilter.h"
#include "Runtime/NavigationSystem/Public/NavAreas/NavArea.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTCompositeNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTTaskNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTService.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTAuxiliaryNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTDecorator.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionListenerInterface.h"
#include "Runtime/AIModule/Classes/GenericTeamAgentInterface.h"
#include "Runtime/Engine/Public/VisualLogger/VisualLoggerDebugSnapshotInterface.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Engine/Player.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstDirector.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInst.h"
#include "Runtime/Engine/Classes/GameFramework/HUD.h"
#include "Runtime/Engine/Classes/Engine/Canvas.h"
#include "Runtime/Engine/Classes/Debug/ReporterGraph.h"
#include "Runtime/Engine/Classes/Debug/ReporterBase.h"
#include "Runtime/Engine/Classes/GameFramework/DebugTextInfo.h"
#include "Runtime/Engine/Classes/Camera/PlayerCameraManager.h"
#include "Runtime/Engine/Classes/Camera/CameraTypes.h"
#include "Runtime/Engine/Classes/Engine/Scene.h"
#include "Runtime/Engine/Classes/Engine/TextureCube.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier.h"
#include "Runtime/Engine/Classes/Particles/EmitterCameraLensEffectBase.h"
#include "Runtime/Engine/Classes/Particles/Emitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleEmitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleLODLevel.h"
#include "Runtime/Engine/Classes/Particles/ParticleModuleRequired.h"
#include "Runtime/Engine/Classes/Particles/ParticleModule.h"
#include "Runtime/Engine/Classes/Particles/ParticleSpriteEmitter.h"
#include "Runtime/Engine/Classes/Distributions/DistributionFloat.h"
#include "Runtime/Engine/Classes/Distributions/Distribution.h"
#include "Runtime/Engine/Classes/Particles/SubUVAnimation.h"
#include "Runtime/Engine/Classes/Particles/TypeData/ParticleModuleTypeDataBase.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawn.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawnBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventGenerator.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventSendToGame.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbit.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbitBase.h"
#include "Runtime/Engine/Classes/Distributions/DistributionVector.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventReceiverBase.h"
#include "Runtime/Engine/Public/ParticleHelper.h"
#include "Runtime/Engine/Classes/Engine/InterpCurveEdSetup.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemReplay.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier_CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraAnim.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroup.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrack.h"
#include "Runtime/Engine/Classes/Camera/CameraAnimInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackMove.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstMove.h"
#include "Runtime/Engine/Classes/Camera/CameraActor.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/GameFramework/CheatManager.h"
#include "Runtime/Engine/Classes/Engine/DebugCameraController.h"
#include "Runtime/Engine/Classes/Components/DrawFrustumComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerInput.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "Runtime/Engine/Classes/Engine/NetConnection.h"
#include "Runtime/Engine/Classes/Engine/ChildConnection.h"
#include "Runtime/Engine/Classes/Engine/PackageMapClient.h"
#include "Runtime/Engine/Classes/Engine/NetDriver.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Engine/Level.h"
#include "Runtime/Engine/Classes/Components/ModelComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelActorContainer.h"
#include "Runtime/Engine/Classes/Engine/LevelScriptActor.h"
#include "Runtime/Engine/Classes/Engine/NavigationObjectBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAgentInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataChunk.h"
#include "Runtime/Engine/Classes/Engine/MapBuildDataRegistry.h"
#include "Runtime/Engine/Classes/GameFramework/WorldSettings.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemConfig.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPhysicsVolume.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsCollisionHandler.h"
#include "Runtime/Engine/Classes/Sound/SoundBase.h"
#include "Runtime/Engine/Classes/Sound/SoundClass.h"
#include "Runtime/Engine/Classes/Sound/SoundMix.h"
#include "Runtime/Engine/Classes/Sound/SoundConcurrency.h"
#include "Runtime/Engine/Classes/Sound/SoundAttenuation.h"
#include "Runtime/Engine/Classes/Engine/Attenuation.h"
#include "Runtime/Engine/Public/IAudioExtensionPlugin.h"
#include "Runtime/Engine/Classes/Sound/SoundSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectPreset.h"
#include "Runtime/Engine/Public/IAmbisonicsMixer.h"
#include "Runtime/Engine/Classes/Sound/SoundWave.h"
#include "Runtime/AudioPlatformConfiguration/Public/AudioCompressionSettings.h"
#include "Runtime/Engine/Classes/Sound/SoundGroups.h"
#include "Runtime/Engine/Classes/Engine/CurveTable.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSource.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBusSend.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBus.h"
#include "Runtime/Engine/Classes/GameFramework/GameModeBase.h"
#include "Runtime/Engine/Classes/GameFramework/GameSession.h"
#include "Runtime/Engine/Classes/GameFramework/GameStateBase.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawn.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPawn.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshSocket.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavCollisionBase.h"
#include "Runtime/Engine/Classes/Engine/TextureStreamingTypes.h"
#include "Runtime/Engine/Classes/GameFramework/FloatingPawnMovement.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawnMovement.h"
#include "Runtime/Engine/Classes/Engine/ServerStatReplicator.h"
#include "Runtime/Engine/Classes/GameFramework/GameNetworkManager.h"
#include "Runtime/Engine/Classes/Sound/AudioVolume.h"
#include "Runtime/Engine/Classes/Sound/ReverbEffect.h"
#include "Runtime/Engine/Classes/Engine/BookMark.h"
#include "Runtime/Engine/Classes/Components/LineBatchComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelStreaming.h"
#include "Runtime/Engine/Classes/Engine/LevelStreamingVolume.h"
#include "Runtime/Engine/Classes/Engine/DemoNetDriver.h"
#include "Runtime/Engine/Classes/Particles/ParticleEventManager.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemBase.h"
#include "Runtime/Engine/Classes/AI/AISystemBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/AvoidanceManager.h"
#include "Runtime/Engine/Classes/Engine/GameInstance.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "Runtime/Engine/Classes/Engine/GameViewportClient.h"
#include "Runtime/Engine/Classes/Engine/ScriptViewportClient.h"
#include "Runtime/Engine/Classes/Engine/Console.h"
#include "Runtime/Engine/Classes/Engine/DebugDisplayProperty.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/Engine/Classes/GameFramework/GameUserSettings.h"
#include "Runtime/Engine/Classes/Engine/AssetManager.h"
#include "Runtime/Engine/Classes/Engine/EngineCustomTimeStep.h"
#include "Runtime/Engine/Classes/Engine/TimecodeProvider.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineSession.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollectionInstance.h"
#include "Runtime/Engine/Classes/Engine/WorldComposition.h"
#include "Runtime/Engine/Classes/Particles/WorldPSCPool.h"
#include "Runtime/Engine/Classes/Engine/ReplicationDriver.h"
#include "Runtime/Engine/Classes/Engine/Channel.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/GameFramework/TouchInterface.h"
#include "Runtime/UMG/Public/Components/Widget.h"
#include "Runtime/UMG/Public/Components/Visual.h"
#include "Runtime/UMG/Public/Components/PanelSlot.h"
#include "Runtime/UMG/Public/Components/PanelWidget.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "Runtime/UMG/Public/Slate/WidgetTransform.h"
#include "Runtime/SlateCore/Public/Layout/Clipping.h"
#include "Runtime/UMG/Public/Blueprint/WidgetNavigation.h"
#include "Runtime/SlateCore/Public/Input/NavigationReply.h"
#include "Runtime/SlateCore/Public/Types/SlateEnums.h"
#include "Runtime/UMG/Public/Binding/PropertyBinding.h"
#include "Runtime/UMG/Public/Binding/DynamicPropertyPath.h"
#include "Runtime/PropertyPath/Public/PropertyPathHelpers.h"
#include "Runtime/SlateCore/Public/Input/Events.h"
#include "Runtime/SlateCore/Public/Styling/SlateBrush.h"
#include "Runtime/SlateCore/Public/Layout/Margin.h"
#include "Runtime/SlateCore/Public/Styling/SlateTypes.h"
#include "Runtime/UMG/Public/Animation/UMGSequencePlayer.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimation.h"
#include "Runtime/MovieScene/Public/MovieSceneSequence.h"
#include "Runtime/MovieScene/Public/MovieSceneSignedObject.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackIdentifier.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSegment.h"
#include "Runtime/MovieScene/Public/MovieSceneTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvalTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackImplementation.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationField.h"
#include "Runtime/MovieScene/Public/MovieSceneFrameMigration.h"
#include "Runtime/MovieScene/Public/MovieSceneSequenceID.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationKey.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceHierarchy.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceTransform.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceInstanceData.h"
#include "Runtime/MovieScene/Public/MovieSceneSection.h"
#include "Runtime/MovieScene/Public/MovieScene.h"
#include "Runtime/MovieScene/Public/MovieSceneSpawnable.h"
#include "Runtime/MovieScene/Public/MovieScenePossessable.h"
#include "Runtime/MovieScene/Public/MovieSceneBinding.h"
#include "Runtime/MovieScene/Public/MovieSceneFwd.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimationBinding.h"
#include "Runtime/Slate/Public/Widgets/Layout/Anchors.h"
#include "Runtime/UMG/Public/Blueprint/DragDropOperation.h"
#include "Runtime/UMG/Public/Components/NamedSlotInterface.h"
#include "Runtime/Engine/Classes/Haptics/HapticFeedbackEffect_Base.h"
#include "Runtime/Engine/Classes/Engine/LatentActionManager.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavRelevantInterface.h"
#include "Runtime/Engine/Classes/Matinee/MatineeActor.h"
#include "Runtime/Engine/Classes/Matinee/InterpData.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupDirector.h"
#include "Runtime/Engine/Classes/Components/ChildActorComponent.h"
#include "Rotatable__pf2962636854.h"
#include "Runtime/Engine/Classes/Kismet/KismetArrayLibrary.h"
#include "Runtime/Engine/Classes/Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/Engine/Classes/Engine/CollisionProfile.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackAttenuation.h"
#include "Runtime/Engine/Classes/Components/ForceFeedbackComponent.h"
#include "Runtime/Engine/Classes/Sound/DialogueWave.h"
#include "Runtime/Engine/Classes/Sound/DialogueTypes.h"
#include "Runtime/Engine/Classes/Sound/DialogueVoice.h"
#include "Runtime/Engine/Classes/Sound/DialogueSoundWaveProxy.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SaveGame.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStaticsTypes.h"
#include "RoadSegment__pf24847026.h"
#include "DiscoMaluch__pf1851508772.h"
#include "MainCarController__pf1851508772.h"
#include "Runtime/Engine/Classes/Engine/Texture2DDynamic.h"
#include "Runtime/Engine/Public/Slate/SlateTextureAtlasInterface.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "Parametry__pf1851508772.h"
#include "Runtime/Engine/Classes/Kismet/KismetStringLibrary.h"
#include "Runtime/Engine/Classes/Kismet/DataTableFunctionLibrary.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Public/Slate/SGameLayerManager.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsConstraintComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetTextLibrary.h"
#include "Runtime/SlateCore/Public/Styling/SlateWidgetStyle.h"
#include "Runtime/SlateCore/Public/Styling/SlateWidgetStyleAsset.h"
#include "Runtime/SlateCore/Public/Styling/SlateWidgetStyleContainerBase.h"
#include "Runtime/SlateCore/Public/Styling/SlateWidgetStyleContainerInterface.h"
#include "Runtime/Slate/Public/Widgets/Notifications/SProgressBar.h"
#include "Runtime/UMG/Public/Components/TextWidgetTypes.h"
#include "Runtime/SlateCore/Public/Fonts/FontCache.h"
#include "Runtime/Slate/Public/Framework/Text/TextLayout.h"
#include "Runtime/SlateCore/Public/Fonts/SlateFontInfo.h"


#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
PRAGMA_DISABLE_OPTIMIZATION
UMainUI_C__pf3053510930::UMainUI_C__pf3053510930(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	if(HasAnyFlags(RF_ClassDefaultObject) && (UMainUI_C__pf3053510930::StaticClass() == GetClass()))
	{
		UMainUI_C__pf3053510930::__CustomDynamicClassInitialization(CastChecked<UDynamicClass>(GetClass()));
	}
	
	bpv__Circle__pf = nullptr;
	bpv__CoinsBox__pf = nullptr;
	bpv__DistanceBox__pf = nullptr;
	bpv__FuelBox__pf = nullptr;
	bpv__Image_46__pf = nullptr;
	bpv__Image_202__pf = nullptr;
	bpv__InitInfo__pf = nullptr;
	bpv__Pointer__pf = nullptr;
	bpv__ProgressBar_0__pf = nullptr;
	bpv__Speedometer__pf = nullptr;
	bpv__TextBlock_1__pf = nullptr;
	bpv__TextBlock_56__pf = nullptr;
	bpv__TextBlock_123__pf = nullptr;
	bpv__TextBlock_619__pf = nullptr;
	bHasScriptImplementedPaint = false;
}
PRAGMA_ENABLE_OPTIMIZATION
void UMainUI_C__pf3053510930::PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph)
{
	Super::PostLoadSubobjects(OuterInstanceGraph);
}
PRAGMA_DISABLE_OPTIMIZATION
void UMainUI_C__pf3053510930::__CustomDynamicClassInitialization(UDynamicClass* InDynamicClass)
{
	ensure(0 == InDynamicClass->ReferencedConvertedFields.Num());
	ensure(0 == InDynamicClass->MiscConvertedSubobjects.Num());
	ensure(0 == InDynamicClass->DynamicBindingObjects.Num());
	ensure(0 == InDynamicClass->ComponentTemplates.Num());
	ensure(0 == InDynamicClass->Timelines.Num());
	ensure(nullptr == InDynamicClass->AnimClassImplementation);
	InDynamicClass->AssembleReferenceTokenStream();
	// List of all referenced converted classes
	InDynamicClass->ReferencedConvertedFields.Add(AMaluchyGameMode_C__pf2132744816::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(ADiscoMaluch_C__pf1851508772::StaticClass());
	FConvertedBlueprintsDependencies::FillUsedAssetsInDynamicClass(InDynamicClass, &__StaticDependencies_DirectlyUsedAssets);
	auto __Local__0 = NewObject<UWidgetTree>(InDynamicClass, UWidgetTree::StaticClass(), TEXT("WidgetTree"));
	InDynamicClass->MiscConvertedSubobjects.Add(__Local__0);
	auto __Local__1 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("CanvasPanel_0"));
	auto& __Local__2 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__1), UPanelWidget::__PPO__Slots() )));
	__Local__2 = TArray<UPanelSlot*> ();
	__Local__2.Reserve(6);
	auto __Local__3 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_3"));
	__Local__3->LayoutData.Offsets.Top = 75.000000f;
	__Local__3->LayoutData.Offsets.Right = 150.000000f;
	__Local__3->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__3->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.000000);
	__Local__3->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.000000);
	__Local__3->LayoutData.Alignment = FVector2D(0.500000, 1.000000);
	__Local__3->Parent = __Local__1;
	auto __Local__4 = NewObject<UHorizontalBox>(__Local__0, UHorizontalBox::StaticClass(), TEXT("CoinsBox"));
	auto& __Local__5 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__4), UPanelWidget::__PPO__Slots() )));
	__Local__5 = TArray<UPanelSlot*> ();
	__Local__5.Reserve(2);
	auto __Local__6 = NewObject<UHorizontalBoxSlot>(__Local__4, UHorizontalBoxSlot::StaticClass(), TEXT("HorizontalBoxSlot_0"));
	__Local__6->Padding.Left = 4.000000f;
	__Local__6->Padding.Top = 2.000000f;
	__Local__6->Padding.Right = 4.000000f;
	__Local__6->Padding.Bottom = 2.000000f;
	__Local__6->HorizontalAlignment = EHorizontalAlignment::HAlign_Left;
	__Local__6->VerticalAlignment = EVerticalAlignment::VAlign_Center;
	__Local__6->Parent = __Local__4;
	auto __Local__7 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_615"));
	__Local__7->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("Monety:"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("4DBEA3424A5C93061225459E4F3E5EF2") /* Key */
	);
	auto& __Local__8 = (*(AccessPrivateProperty<FLinearColor >(&(__Local__7->ColorAndOpacity), FSlateColor::__PPO__SpecifiedColor() )));
	__Local__8 = FLinearColor(0.807292, 0.807292, 0.807292, 1.000000);
	__Local__7->Slot = __Local__6;
	__Local__6->Content = __Local__7;
	__Local__5.Add(__Local__6);
	auto __Local__9 = NewObject<UHorizontalBoxSlot>(__Local__4, UHorizontalBoxSlot::StaticClass(), TEXT("HorizontalBoxSlot_3"));
	__Local__9->Size.SizeRule = ESlateSizeRule::Type::Fill;
	__Local__9->HorizontalAlignment = EHorizontalAlignment::HAlign_Right;
	__Local__9->VerticalAlignment = EVerticalAlignment::VAlign_Center;
	__Local__9->Parent = __Local__4;
	auto __Local__10 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_619"));
	__Local__10->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("0"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("8319266240EA729D199995BAA76626B5") /* Key */
	);
	__Local__10->Slot = __Local__9;
	__Local__9->Content = __Local__10;
	__Local__5.Add(__Local__9);
	__Local__4->Slot = __Local__3;
	__Local__4->bIsVariable = true;
	__Local__4->RenderOpacity = 0.000000f;
	__Local__3->Content = __Local__4;
	__Local__2.Add(__Local__3);
	auto __Local__11 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_0"));
	__Local__11->LayoutData.Offsets.Top = 225.000000f;
	__Local__11->LayoutData.Offsets.Right = 300.000000f;
	__Local__11->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__11->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.000000);
	__Local__11->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.000000);
	__Local__11->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__11->Parent = __Local__1;
	auto __Local__12 = NewObject<UHorizontalBox>(__Local__0, UHorizontalBox::StaticClass(), TEXT("DistanceBox"));
	auto& __Local__13 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__12), UPanelWidget::__PPO__Slots() )));
	__Local__13 = TArray<UPanelSlot*> ();
	__Local__13.Reserve(2);
	auto __Local__14 = NewObject<UHorizontalBoxSlot>(__Local__12, UHorizontalBoxSlot::StaticClass(), TEXT("HorizontalBoxSlot_1"));
	__Local__14->HorizontalAlignment = EHorizontalAlignment::HAlign_Left;
	__Local__14->VerticalAlignment = EVerticalAlignment::VAlign_Center;
	__Local__14->Parent = __Local__12;
	auto __Local__15 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_0"));
	__Local__15->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("Dystans:"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("48CAB75C43AE4FC1E13BD68D5556AA54") /* Key */
	);
	auto& __Local__16 = (*(AccessPrivateProperty<FLinearColor >(&(__Local__15->ColorAndOpacity), FSlateColor::__PPO__SpecifiedColor() )));
	__Local__16 = FLinearColor(0.807292, 0.807292, 0.807292, 1.000000);
	auto& __Local__17 = (*(AccessPrivateProperty<TEnumAsByte<ETextJustify::Type> >((__Local__15), UTextLayoutWidget::__PPO__Justification() )));
	__Local__17 = ETextJustify::Type::Center;
	__Local__15->Slot = __Local__14;
	__Local__14->Content = __Local__15;
	__Local__13.Add(__Local__14);
	auto __Local__18 = NewObject<UHorizontalBoxSlot>(__Local__12, UHorizontalBoxSlot::StaticClass(), TEXT("HorizontalBoxSlot_2"));
	__Local__18->Size.SizeRule = ESlateSizeRule::Type::Fill;
	__Local__18->HorizontalAlignment = EHorizontalAlignment::HAlign_Right;
	__Local__18->VerticalAlignment = EVerticalAlignment::VAlign_Bottom;
	__Local__18->Parent = __Local__12;
	auto __Local__19 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_1"));
	__Local__19->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("123km"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("0CE4D23642A662BD3E64AC83EFD25373") /* Key */
	);
	auto& __Local__20 = (*(AccessPrivateProperty<FLinearColor >(&(__Local__19->ColorAndOpacity), FSlateColor::__PPO__SpecifiedColor() )));
	__Local__20 = FLinearColor(0.807292, 0.807292, 0.807292, 1.000000);
	auto& __Local__21 = (*(AccessPrivateProperty<TEnumAsByte<ETextJustify::Type> >((__Local__19), UTextLayoutWidget::__PPO__Justification() )));
	__Local__21 = ETextJustify::Type::Center;
	__Local__19->Slot = __Local__18;
	__Local__18->Content = __Local__19;
	__Local__13.Add(__Local__18);
	__Local__12->Slot = __Local__11;
	__Local__12->bIsVariable = true;
	__Local__12->RenderOpacity = 0.000000f;
	__Local__11->Content = __Local__12;
	__Local__2.Add(__Local__11);
	auto __Local__22 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_5"));
	__Local__22->LayoutData.Offsets.Top = 150.000000f;
	__Local__22->LayoutData.Offsets.Right = 400.000000f;
	__Local__22->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.000000);
	__Local__22->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.000000);
	__Local__22->LayoutData.Alignment = FVector2D(0.500000, 1.000000);
	__Local__22->Parent = __Local__1;
	auto __Local__23 = NewObject<UHorizontalBox>(__Local__0, UHorizontalBox::StaticClass(), TEXT("FuelBox"));
	auto& __Local__24 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__23), UPanelWidget::__PPO__Slots() )));
	__Local__24 = TArray<UPanelSlot*> ();
	__Local__24.Reserve(2);
	auto __Local__25 = NewObject<UHorizontalBoxSlot>(__Local__23, UHorizontalBoxSlot::StaticClass(), TEXT("HorizontalBoxSlot_1"));
	__Local__25->HorizontalAlignment = EHorizontalAlignment::HAlign_Left;
	__Local__25->VerticalAlignment = EVerticalAlignment::VAlign_Center;
	__Local__25->Parent = __Local__23;
	auto __Local__26 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_620"));
	__Local__26->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("Paliwo:"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("E63B50924B5FBFCB0F7A3797E6BF3FDC") /* Key */
	);
	auto& __Local__27 = (*(AccessPrivateProperty<FLinearColor >(&(__Local__26->ColorAndOpacity), FSlateColor::__PPO__SpecifiedColor() )));
	__Local__27 = FLinearColor(0.807292, 0.807292, 0.807292, 1.000000);
	auto& __Local__28 = (*(AccessPrivateProperty<TEnumAsByte<ETextJustify::Type> >((__Local__26), UTextLayoutWidget::__PPO__Justification() )));
	__Local__28 = ETextJustify::Type::Center;
	__Local__26->Slot = __Local__25;
	__Local__25->Content = __Local__26;
	__Local__24.Add(__Local__25);
	auto __Local__29 = NewObject<UHorizontalBoxSlot>(__Local__23, UHorizontalBoxSlot::StaticClass(), TEXT("HorizontalBoxSlot_0"));
	__Local__29->Padding.Left = 10.000000f;
	__Local__29->Size.SizeRule = ESlateSizeRule::Type::Fill;
	__Local__29->Parent = __Local__23;
	auto __Local__30 = NewObject<UProgressBar>(__Local__0, UProgressBar::StaticClass(), TEXT("ProgressBar_0"));
	__Local__30->Percent = 1.000000f;
	__Local__30->FillColorAndOpacity = FLinearColor(0.000000, 0.500000, 1.000000, 1.000000);
	__Local__30->Slot = __Local__29;
	__Local__29->Content = __Local__30;
	__Local__24.Add(__Local__29);
	__Local__23->Slot = __Local__22;
	__Local__23->bIsVariable = true;
	__Local__23->RenderOpacity = 0.000000f;
	__Local__22->Content = __Local__23;
	__Local__2.Add(__Local__22);
	auto __Local__31 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_2"));
	__Local__31->LayoutData.Offsets.Right = 300.000000f;
	__Local__31->LayoutData.Offsets.Bottom = 80.000000f;
	__Local__31->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__31->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__31->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__31->Parent = __Local__1;
	auto __Local__32 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_56"));
	__Local__32->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("GAME OVER"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("3418885A469C918E6E75A1A39B65497A") /* Key */
	);
	auto& __Local__33 = (*(AccessPrivateProperty<FLinearColor >(&(__Local__32->ColorAndOpacity), FSlateColor::__PPO__SpecifiedColor() )));
	__Local__33 = FLinearColor(1.000000, 1.000000, 1.000000, 0.000000);
	__Local__32->Font.Size = 106;
	auto& __Local__34 = (*(AccessPrivateProperty<TEnumAsByte<ETextJustify::Type> >((__Local__32), UTextLayoutWidget::__PPO__Justification() )));
	__Local__34 = ETextJustify::Type::Center;
	__Local__32->Slot = __Local__31;
	__Local__31->Content = __Local__32;
	__Local__2.Add(__Local__31);
	auto __Local__35 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_4"));
	__Local__35->LayoutData.Offsets.Left = -50.000000f;
	__Local__35->LayoutData.Offsets.Top = -50.000000f;
	__Local__35->LayoutData.Offsets.Right = 300.000000f;
	__Local__35->LayoutData.Offsets.Bottom = 300.000000f;
	__Local__35->LayoutData.Anchors.Minimum = FVector2D(1.000000, 1.000000);
	__Local__35->LayoutData.Anchors.Maximum = FVector2D(1.000000, 1.000000);
	__Local__35->LayoutData.Alignment = FVector2D(1.000000, 1.000000);
	__Local__35->Parent = __Local__1;
	auto __Local__36 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("Speedometer"));
	auto& __Local__37 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__36), UPanelWidget::__PPO__Slots() )));
	__Local__37 = TArray<UPanelSlot*> ();
	__Local__37.Reserve(3);
	auto __Local__38 = NewObject<UCanvasPanelSlot>(__Local__36, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_0"));
	__Local__38->LayoutData.Offsets.Right = 300.000000f;
	__Local__38->LayoutData.Offsets.Bottom = 300.000000f;
	__Local__38->LayoutData.Anchors.Minimum = FVector2D(1.000000, 1.000000);
	__Local__38->LayoutData.Anchors.Maximum = FVector2D(1.000000, 1.000000);
	__Local__38->LayoutData.Alignment = FVector2D(1.000000, 1.000000);
	__Local__38->Parent = __Local__36;
	auto __Local__39 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Circle"));
	__Local__39->Brush.ImageSize = FVector2D(1550.000000, 1550.000000);
	auto& __Local__40 = (*(AccessPrivateProperty<FLinearColor >(&(__Local__39->Brush.TintColor), FSlateColor::__PPO__SpecifiedColor() )));
	__Local__40 = FLinearColor(0.864583, 0.864583, 0.864583, 1.000000);
	auto& __Local__41 = (*(AccessPrivateProperty<UObject* >(&(__Local__39->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__41 = CastChecked<UObject>(CastChecked<UDynamicClass>(UMainUI_C__pf3053510930::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	__Local__39->Slot = __Local__38;
	__Local__38->Content = __Local__39;
	__Local__37.Add(__Local__38);
	auto __Local__42 = NewObject<UCanvasPanelSlot>(__Local__36, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_1"));
	__Local__42->LayoutData.Offsets.Top = -14.357568f;
	__Local__42->LayoutData.Offsets.Right = 138.800003f;
	__Local__42->LayoutData.Offsets.Bottom = 23.799999f;
	__Local__42->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__42->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__42->LayoutData.Alignment = FVector2D(0.835000, 0.500000);
	__Local__42->Parent = __Local__36;
	auto __Local__43 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Pointer"));
	__Local__43->Brush.ImageSize = FVector2D(694.000000, 119.000000);
	auto& __Local__44 = (*(AccessPrivateProperty<UObject* >(&(__Local__43->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__44 = CastChecked<UObject>(CastChecked<UDynamicClass>(UMainUI_C__pf3053510930::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed);
	__Local__43->Slot = __Local__42;
	__Local__43->RenderTransform.Angle = -28.000000f;
	__Local__43->RenderTransformPivot = FVector2D(0.835000, 0.500000);
	__Local__42->Content = __Local__43;
	__Local__37.Add(__Local__42);
	auto __Local__45 = NewObject<UCanvasPanelSlot>(__Local__36, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_2"));
	__Local__45->LayoutData.Offsets.Left = -148.401199f;
	__Local__45->LayoutData.Offsets.Top = -110.774368f;
	__Local__45->LayoutData.Offsets.Right = 96.645645f;
	__Local__45->LayoutData.Offsets.Bottom = 45.882481f;
	__Local__45->LayoutData.Anchors.Minimum = FVector2D(1.000000, 1.000000);
	__Local__45->LayoutData.Anchors.Maximum = FVector2D(1.000000, 1.000000);
	__Local__45->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__45->Parent = __Local__36;
	auto __Local__46 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_123"));
	__Local__46->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("000"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("EAA4167A4B9D92177F66CE9CB1AA9625") /* Key */
	);
	auto& __Local__47 = (*(AccessPrivateProperty<FLinearColor >(&(__Local__46->ColorAndOpacity), FSlateColor::__PPO__SpecifiedColor() )));
	__Local__47 = FLinearColor(0.026042, 0.026042, 0.026042, 1.000000);
	__Local__46->Font.Size = 38;
	auto& __Local__48 = (*(AccessPrivateProperty<TEnumAsByte<ETextJustify::Type> >((__Local__46), UTextLayoutWidget::__PPO__Justification() )));
	__Local__48 = ETextJustify::Type::Right;
	__Local__46->Slot = __Local__45;
	__Local__45->Content = __Local__46;
	__Local__37.Add(__Local__45);
	__Local__36->Slot = __Local__35;
	__Local__36->bIsVariable = true;
	__Local__35->Content = __Local__36;
	__Local__2.Add(__Local__35);
	auto __Local__49 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_6"));
	__Local__49->LayoutData.Offsets.Right = 0.000000f;
	__Local__49->LayoutData.Offsets.Bottom = 0.000000f;
	__Local__49->LayoutData.Anchors.Maximum = FVector2D(1.000000, 1.000000);
	__Local__49->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__49->Parent = __Local__1;
	auto __Local__50 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("InitInfo"));
	auto& __Local__51 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__50), UPanelWidget::__PPO__Slots() )));
	__Local__51 = TArray<UPanelSlot*> ();
	__Local__51.Reserve(4);
	auto __Local__52 = NewObject<UCanvasPanelSlot>(__Local__50, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_6"));
	__Local__52->LayoutData.Offsets.Right = 0.000000f;
	__Local__52->LayoutData.Offsets.Bottom = 0.000000f;
	__Local__52->LayoutData.Anchors.Maximum = FVector2D(1.000000, 1.000000);
	__Local__52->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__52->Parent = __Local__50;
	auto __Local__53 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_46"));
	auto& __Local__54 = (*(AccessPrivateProperty<FLinearColor >(&(__Local__53->Brush.TintColor), FSlateColor::__PPO__SpecifiedColor() )));
	__Local__54 = FLinearColor(0.000000, 0.000000, 0.000000, 0.755000);
	__Local__53->Slot = __Local__52;
	__Local__52->Content = __Local__53;
	__Local__51.Add(__Local__52);
	auto __Local__55 = NewObject<UCanvasPanelSlot>(__Local__50, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_0"));
	__Local__55->LayoutData.Offsets.Left = -37.831306f;
	__Local__55->LayoutData.Offsets.Right = 550.000000f;
	__Local__55->LayoutData.Offsets.Bottom = 100.000000f;
	__Local__55->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__55->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__55->Parent = __Local__50;
	auto __Local__56 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_82"));
	__Local__56->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("by rozpoczac gre"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("382DBAA54915032C2E8E8BA77545F9F5") /* Key */
	);
	__Local__56->Font.FontObject = CastChecked<UObject>(CastChecked<UDynamicClass>(UMainUI_C__pf3053510930::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
	__Local__56->Font.TypefaceFontName = FName(TEXT("Default"));
	__Local__56->Font.Size = 50;
	auto& __Local__57 = (*(AccessPrivateProperty<TEnumAsByte<ETextJustify::Type> >((__Local__56), UTextLayoutWidget::__PPO__Justification() )));
	__Local__57 = ETextJustify::Type::Center;
	__Local__56->Slot = __Local__55;
	__Local__55->Content = __Local__56;
	__Local__51.Add(__Local__55);
	auto __Local__58 = NewObject<UCanvasPanelSlot>(__Local__50, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_1"));
	__Local__58->LayoutData.Offsets.Left = -451.812531f;
	__Local__58->LayoutData.Offsets.Right = 250.000000f;
	__Local__58->LayoutData.Offsets.Bottom = 100.000000f;
	__Local__58->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__58->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__58->Parent = __Local__50;
	auto __Local__59 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_81"));
	__Local__59->Text = FInternationalization::ForUseOnlyByLocMacroAndGraphNodeTextLiterals_CreateText(
	TEXT("Nacisnij"), /* Literal Text */
	TEXT(""), /* Namespace */
	TEXT("ADF62B844DC959EA4F5B0EAFF16E1C3C") /* Key */
	);
	__Local__59->Font.FontObject = CastChecked<UObject>(CastChecked<UDynamicClass>(UMainUI_C__pf3053510930::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
	__Local__59->Font.TypefaceFontName = FName(TEXT("Default"));
	__Local__59->Font.Size = 50;
	auto& __Local__60 = (*(AccessPrivateProperty<TEnumAsByte<ETextJustify::Type> >((__Local__59), UTextLayoutWidget::__PPO__Justification() )));
	__Local__60 = ETextJustify::Type::Center;
	__Local__59->Slot = __Local__58;
	__Local__58->Content = __Local__59;
	__Local__51.Add(__Local__58);
	auto __Local__61 = NewObject<UCanvasPanelSlot>(__Local__50, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_2"));
	__Local__61->LayoutData.Offsets.Left = -180.453003f;
	__Local__61->LayoutData.Offsets.Bottom = 100.000000f;
	__Local__61->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__61->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__61->Parent = __Local__50;
	auto __Local__62 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_202"));
	__Local__62->Brush.ImageSize = FVector2D(64.000000, 64.000000);
	__Local__62->ColorAndOpacity = FLinearColor(0.848958, 0.848958, 0.000000, 1.000000);
	__Local__62->Slot = __Local__61;
	__Local__61->Content = __Local__62;
	__Local__51.Add(__Local__61);
	__Local__50->Slot = __Local__49;
	__Local__50->bIsVariable = true;
	__Local__50->RenderOpacity = 0.000000f;
	__Local__49->Content = __Local__50;
	__Local__2.Add(__Local__49);
	__Local__0->RootWidget = __Local__1;
}
PRAGMA_ENABLE_OPTIMIZATION
void UMainUI_C__pf3053510930::GetSlotNames(TArray<FName>& SlotNames) const
{
	TArray<FName>  __Local__63;
	SlotNames.Append(__Local__63);
}
void UMainUI_C__pf3053510930::InitializeNativeClassData()
{
	TArray<UWidgetAnimation*>  __Local__64;
	TArray<FDelegateRuntimeBinding>  __Local__65;
	__Local__65.AddUninitialized(5);
	FDelegateRuntimeBinding::StaticStruct()->InitializeStruct(__Local__65.GetData(), 5);
	auto& __Local__66 = __Local__65[0];
	__Local__66.ObjectName = FString(TEXT("TextBlock_619"));
	__Local__66.PropertyName = FName(TEXT("Text"));
	__Local__66.FunctionName = FName(TEXT("GetText_0"));
	auto& __Local__67 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__66.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__67 = TArray<FPropertyPathSegment> ();
	__Local__67.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__67.GetData(), 1);
	auto& __Local__68 = __Local__67[0];
	__Local__68.Name = FName(TEXT("GetText_0"));
	auto& __Local__69 = __Local__65[1];
	__Local__69.ObjectName = FString(TEXT("ProgressBar_0"));
	__Local__69.PropertyName = FName(TEXT("Percent"));
	__Local__69.FunctionName = FName(TEXT("GetPercent_0"));
	auto& __Local__70 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__69.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__70 = TArray<FPropertyPathSegment> ();
	__Local__70.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__70.GetData(), 1);
	auto& __Local__71 = __Local__70[0];
	__Local__71.Name = FName(TEXT("GetPercent_0"));
	auto& __Local__72 = __Local__65[2];
	__Local__72.ObjectName = FString(TEXT("TextBlock_56"));
	__Local__72.PropertyName = FName(TEXT("ColorAndOpacity"));
	__Local__72.FunctionName = FName(TEXT("GetColorAndOpacity_0"));
	auto& __Local__73 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__72.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__73 = TArray<FPropertyPathSegment> ();
	__Local__73.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__73.GetData(), 1);
	auto& __Local__74 = __Local__73[0];
	__Local__74.Name = FName(TEXT("GetColorAndOpacity_0"));
	auto& __Local__75 = __Local__65[3];
	__Local__75.ObjectName = FString(TEXT("TextBlock_123"));
	__Local__75.PropertyName = FName(TEXT("Text"));
	__Local__75.FunctionName = FName(TEXT("GetText_1"));
	auto& __Local__76 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__75.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__76 = TArray<FPropertyPathSegment> ();
	__Local__76.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__76.GetData(), 1);
	auto& __Local__77 = __Local__76[0];
	__Local__77.Name = FName(TEXT("GetText_1"));
	auto& __Local__78 = __Local__65[4];
	__Local__78.ObjectName = FString(TEXT("TextBlock_1"));
	__Local__78.PropertyName = FName(TEXT("Text"));
	__Local__78.FunctionName = FName(TEXT("GetText_2"));
	auto& __Local__79 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__78.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__79 = TArray<FPropertyPathSegment> ();
	__Local__79.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__79.GetData(), 1);
	auto& __Local__80 = __Local__79[0];
	__Local__80.Name = FName(TEXT("GetText_2"));
	UWidgetBlueprintGeneratedClass::InitializeWidgetStatic(this, GetClass(), false, true, CastChecked<UWidgetTree>(CastChecked<UDynamicClass>(UMainUI_C__pf3053510930::StaticClass())->MiscConvertedSubobjects[0]), __Local__64, __Local__65);
}
void UMainUI_C__pf3053510930::PreSave(const class ITargetPlatform* TargetPlatform)
{
	Super::PreSave(TargetPlatform);
	TArray<FName> LocalNamedSlots;
	GetSlotNames(LocalNamedSlots);
	RemoveObsoleteBindings(LocalNamedSlots);
}
void UMainUI_C__pf3053510930::bpf__ExecuteUbergraph_MainUI__pf_0(int32 bpp__EntryPoint__pf)
{
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	check(bpp__EntryPoint__pf == 2);
	// optimized KCST_UnconditionalGoto
	if(::IsValid(bpv__InitInfo__pf))
	{
		bpv__InitInfo__pf->UWidget::SetRenderOpacity(1.000000);
	}
	bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
	b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
	b0l__K2Node_DynamicCast_bSuccess__pf = (b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
	if (!b0l__K2Node_DynamicCast_bSuccess__pf)
	{
		return; //KCST_GotoReturnIfNot
	}
	b0l__K2Node_CreateDelegate_OutputDelegate__pf.BindUFunction(this,FName(TEXT("Event HandleGameStarted")));
	if(::IsValid(b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf))
	{
		b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__EventxGameStarted__pfT.AddUnique(b0l__K2Node_CreateDelegate_OutputDelegate__pf);
	}
	return; // KCST_GotoReturn
}
void UMainUI_C__pf3053510930::bpf__ExecuteUbergraph_MainUI__pf_1(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 9);
	// optimized KCST_UnconditionalGoto
	bpf__HandleGameStarted__pf();
	return; // KCST_GotoReturn
}
void UMainUI_C__pf3053510930::bpf__ExecuteUbergraph_MainUI__pf_2(int32 bpp__EntryPoint__pf)
{
	float bpfv__CallFunc_GetCarSpeed_ReturnValue__pf{};
	float bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf{};
	check(bpp__EntryPoint__pf == 6);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_GetCarSpeed_ReturnValue__pf = bpf__GetCarSpeed__pf();
	bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Subtract_FloatFloat(bpfv__CallFunc_GetCarSpeed_ReturnValue__pf, 28.000000);
	if(::IsValid(bpv__Pointer__pf))
	{
		bpv__Pointer__pf->UWidget::SetRenderAngle(bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf);
	}
	return; // KCST_GotoReturn
}
void UMainUI_C__pf3053510930::bpf__EventxHandleGameStarted__pfT()
{
	bpf__ExecuteUbergraph_MainUI__pf_1(9);
}
void UMainUI_C__pf3053510930::bpf__Tick__pf(FGeometry bpp__MyGeometry__pf, float bpp__InDeltaTime__pf)
{
	b0l__K2Node_Event_MyGeometry__pf = bpp__MyGeometry__pf;
	b0l__K2Node_Event_InDeltaTime__pf = bpp__InDeltaTime__pf;
	bpf__ExecuteUbergraph_MainUI__pf_2(6);
}
void UMainUI_C__pf3053510930::bpf__Construct__pf()
{
	bpf__ExecuteUbergraph_MainUI__pf_0(2);
}
FText  UMainUI_C__pf3053510930::bpf__GetText_0__pf()
{
	FText bpp__ReturnValue__pf{};
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				int32  __Local__81 = 0;
				bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf) && ::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf->bpv__CoinsCollected__pf) : (__Local__81)), false, true, 1, 324);
				bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
	return bpp__ReturnValue__pf;
}
float  UMainUI_C__pf3053510930::bpf__GetPercent_0__pf()
{
	float bpp__ReturnValue__pf{};
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				float  __Local__82 = 0.000000;
				bpp__ReturnValue__pf = ((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf) && ::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf->bpv__Fuel__pf) : (__Local__82));
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
	return bpp__ReturnValue__pf;
}
FSlateColor  UMainUI_C__pf3053510930::bpf__GetColorAndOpacity_0__pf()
{
	FSlateColor bpp__ReturnValue__pf{};
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	float bpfv__CallFunc_Conv_BoolToFloat_ReturnValue__pf{};
	FLinearColor bpfv__CallFunc_MakeColor_ReturnValue__pf(EForceInit::ForceInit);
	FSlateColor bpfv__K2Node_MakeStruct_SlateColor__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				bool  __Local__83 = false;
				bpfv__CallFunc_Conv_BoolToFloat_ReturnValue__pf = UKismetMathLibrary::Conv_BoolToFloat(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__GameAlreadyEnded__pf) : (__Local__83)));
				bpfv__CallFunc_MakeColor_ReturnValue__pf = UKismetMathLibrary::MakeColor(1.000000, 1.000000, 1.000000, bpfv__CallFunc_Conv_BoolToFloat_ReturnValue__pf);
				(*(AccessPrivateProperty<FLinearColor >(&(bpfv__K2Node_MakeStruct_SlateColor__pf), FSlateColor::__PPO__SpecifiedColor() ))) = bpfv__CallFunc_MakeColor_ReturnValue__pf;
				(*(AccessPrivateProperty<TEnumAsByte<ESlateColorStylingMode::Type> >(&(bpfv__K2Node_MakeStruct_SlateColor__pf), FSlateColor::__PPO__ColorUseRule() ))) = ESlateColorStylingMode::UseColor_Specified;
				bpp__ReturnValue__pf = bpfv__K2Node_MakeStruct_SlateColor__pf;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
	return bpp__ReturnValue__pf;
}
FText  UMainUI_C__pf3053510930::bpf__GetText_1__pf()
{
	FText bpp__ReturnValue__pf{};
	float bpfv__CallFunc_GetCarSpeed_ReturnValue__pf{};
	int32 bpfv__CallFunc_FCeil_ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	bpfv__CallFunc_GetCarSpeed_ReturnValue__pf = bpf__GetCarSpeed__pf();
	bpfv__CallFunc_FCeil_ReturnValue__pf = UKismetMathLibrary::FCeil(bpfv__CallFunc_GetCarSpeed_ReturnValue__pf);
	bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(bpfv__CallFunc_FCeil_ReturnValue__pf, false, true, 1, 324);
	bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
	return bpp__ReturnValue__pf;
}
float  UMainUI_C__pf3053510930::bpf__GetCarSpeed__pf()
{
	float bpp__ReturnValue__pf{};
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	float bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				float  __Local__84 = 0.000000;
				bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf = FCustomThunkTemplates::Divide_FloatFloat(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf) && ::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf->bpv__Velocity__pf) : (__Local__84)), 180.000000);
				bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_FloatFloat(bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf, 180.000000);
				bpp__ReturnValue__pf = bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
	return bpp__ReturnValue__pf;
}
void UMainUI_C__pf3053510930::bpf__HandleGameStarted__pf()
{
	if(::IsValid(bpv__Speedometer__pf))
	{
		bpv__Speedometer__pf->UWidget::SetRenderOpacity(1.000000);
	}
	if(::IsValid(bpv__FuelBox__pf))
	{
		bpv__FuelBox__pf->UWidget::SetRenderOpacity(1.000000);
	}
	if(::IsValid(bpv__CoinsBox__pf))
	{
		bpv__CoinsBox__pf->UWidget::SetRenderOpacity(1.000000);
	}
	if(::IsValid(bpv__DistanceBox__pf))
	{
		bpv__DistanceBox__pf->UWidget::SetRenderOpacity(1.000000);
	}
	if(::IsValid(bpv__InitInfo__pf))
	{
		bpv__InitInfo__pf->UWidget::SetRenderOpacity(0.000000);
	}
}
FText  UMainUI_C__pf3053510930::bpf__GetText_2__pf()
{
	FText bpp__ReturnValue__pf{};
	int32 bpfv__Temp_int_Array_Index_Variable__pf{};
	int32 bpfv__Temp_int_Loop_Counter_Variable__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue1__pf{};
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	float bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf{};
	FString bpfv__CallFunc_Conv_FloatToString_ReturnValue__pf{};
	TArray<FString> bpfv__CallFunc_GetCharacterArrayFromString_ReturnValue__pf{};
	FString bpfv__CallFunc_GetSubstring_ReturnValue__pf{};
	FString bpfv__CallFunc_Array_Get_Item__pf{};
	FString bpfv__CallFunc_Concat_StrStr_ReturnValue__pf{};
	bool bpfv__CallFunc_EqualEqual_StrStr_ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_StringToText_ReturnValue__pf{};
	int32 bpfv__CallFunc_Array_Length_ReturnValue__pf{};
	bool bpfv__CallFunc_Less_IntInt_ReturnValue__pf{};
	TArray< int32, TInlineAllocator<8> > __StateStack;

	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 2:
			{
				bpfv__Temp_int_Loop_Counter_Variable__pf = 0;
			}
		case 3:
			{
				bpfv__Temp_int_Array_Index_Variable__pf = 0;
			}
		case 4:
			{
				float  __Local__85 = 0.000000;
				bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf = FCustomThunkTemplates::Divide_FloatFloat(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf) && ::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf->bpv__DistancePassed__pf) : (__Local__85)), 100000.000000);
				bpfv__CallFunc_Conv_FloatToString_ReturnValue__pf = UKismetStringLibrary::Conv_FloatToString(bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf);
				bpfv__CallFunc_GetCharacterArrayFromString_ReturnValue__pf = UKismetStringLibrary::GetCharacterArrayFromString(bpfv__CallFunc_Conv_FloatToString_ReturnValue__pf);
				bpfv__CallFunc_Array_Length_ReturnValue__pf = FCustomThunkTemplates::Array_Length(bpfv__CallFunc_GetCharacterArrayFromString_ReturnValue__pf);
				bpfv__CallFunc_Less_IntInt_ReturnValue__pf = UKismetMathLibrary::Less_IntInt(bpfv__Temp_int_Loop_Counter_Variable__pf, bpfv__CallFunc_Array_Length_ReturnValue__pf);
				if (!bpfv__CallFunc_Less_IntInt_ReturnValue__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 5:
			{
				bpfv__Temp_int_Array_Index_Variable__pf = bpfv__Temp_int_Loop_Counter_Variable__pf;
			}
		case 6:
			{
				__StateStack.Push(9);
			}
		case 7:
			{
				float  __Local__86 = 0.000000;
				bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf = FCustomThunkTemplates::Divide_FloatFloat(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf) && ::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf->bpv__DistancePassed__pf) : (__Local__86)), 100000.000000);
				bpfv__CallFunc_Conv_FloatToString_ReturnValue__pf = UKismetStringLibrary::Conv_FloatToString(bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf);
				bpfv__CallFunc_GetCharacterArrayFromString_ReturnValue__pf = UKismetStringLibrary::GetCharacterArrayFromString(bpfv__CallFunc_Conv_FloatToString_ReturnValue__pf);
				FCustomThunkTemplates::Array_Get(bpfv__CallFunc_GetCharacterArrayFromString_ReturnValue__pf, bpfv__Temp_int_Array_Index_Variable__pf, /*out*/ bpfv__CallFunc_Array_Get_Item__pf);
				bpfv__CallFunc_EqualEqual_StrStr_ReturnValue__pf = UKismetStringLibrary::EqualEqual_StrStr(bpfv__CallFunc_Array_Get_Item__pf, FString(TEXT(".")));
				if (!bpfv__CallFunc_EqualEqual_StrStr_ReturnValue__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 8:
			{
				bpfv__CallFunc_Add_IntInt_ReturnValue__pf = UKismetMathLibrary::Add_IntInt(bpfv__Temp_int_Array_Index_Variable__pf, 3);
				float  __Local__87 = 0.000000;
				bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf = FCustomThunkTemplates::Divide_FloatFloat(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf) && ::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf->bpv__DistancePassed__pf) : (__Local__87)), 100000.000000);
				bpfv__CallFunc_Conv_FloatToString_ReturnValue__pf = UKismetStringLibrary::Conv_FloatToString(bpfv__CallFunc_Divide_FloatFloat_ReturnValue__pf);
				bpfv__CallFunc_GetSubstring_ReturnValue__pf = UKismetStringLibrary::GetSubstring(bpfv__CallFunc_Conv_FloatToString_ReturnValue__pf, 0, bpfv__CallFunc_Add_IntInt_ReturnValue__pf);
				bpfv__CallFunc_Concat_StrStr_ReturnValue__pf = UKismetStringLibrary::Concat_StrStr(bpfv__CallFunc_GetSubstring_ReturnValue__pf, FString(TEXT("km")));
				bpfv__CallFunc_Conv_StringToText_ReturnValue__pf = UKismetTextLibrary::Conv_StringToText(bpfv__CallFunc_Concat_StrStr_ReturnValue__pf);
				bpp__ReturnValue__pf = bpfv__CallFunc_Conv_StringToText_ReturnValue__pf;
				__CurrentState = -1;
				break;
			}
		case 9:
			{
				bpfv__CallFunc_Add_IntInt_ReturnValue1__pf = UKismetMathLibrary::Add_IntInt(bpfv__Temp_int_Loop_Counter_Variable__pf, 1);
				bpfv__Temp_int_Loop_Counter_Variable__pf = bpfv__CallFunc_Add_IntInt_ReturnValue1__pf;
				__CurrentState = 4;
				break;
			}
		default:
			check(false); // Invalid state
			break;
		}
	} while( __CurrentState != -1 );
	return bpp__ReturnValue__pf;
}
PRAGMA_DISABLE_OPTIMIZATION
void UMainUI_C__pf3053510930::__StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{43, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Assets/speedometer.speedometer 
		{44, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Assets/needle.needle 
		{45, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Font /Game/Fonts/Jaapokki-Regular_Font.Jaapokki-Regular_Font 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
PRAGMA_DISABLE_OPTIMIZATION
void UMainUI_C__pf3053510930::__StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	__StaticDependencies_DirectlyUsedAssets(AssetsToLoad);
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{46, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Font /Engine/EngineFonts/Roboto.Roboto 
		{1, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.PointerToUberGraphFrame 
		{47, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/SlateCore.Geometry 
		{2, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.GameModeBase 
		{48, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.CanvasPanel 
		{49, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.Widget 
		{7, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.GameplayStatics 
		{4, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetMathLibrary 
		{50, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.Image 
		{38, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.UserWidget 
		{34, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetStringLibrary 
		{3, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetArrayLibrary 
		{51, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetTextLibrary 
		{52, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.HorizontalBox 
		{53, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/SlateCore.SlateColor 
		{54, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.ProgressBar 
		{55, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.TextBlock 
		{40, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/MaluchyGameMode.MaluchyGameMode_C 
		{19, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/Car/DiscoMaluch.DiscoMaluch_C 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
struct FRegisterHelper__UMainUI_C__pf3053510930
{
	FRegisterHelper__UMainUI_C__pf3053510930()
	{
		FConvertedBlueprintsDependencies::Get().RegisterConvertedClass(TEXT("/Game/Blueprints/UI/MainUI"), &UMainUI_C__pf3053510930::__StaticDependenciesAssets);
	}
	static FRegisterHelper__UMainUI_C__pf3053510930 Instance;
};
FRegisterHelper__UMainUI_C__pf3053510930 FRegisterHelper__UMainUI_C__pf3053510930::Instance;
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
