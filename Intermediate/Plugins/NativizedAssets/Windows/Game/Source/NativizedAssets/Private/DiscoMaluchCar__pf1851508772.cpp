#include "NativizedAssets.h"
#include "DiscoMaluchCar__pf1851508772.h"
#include "GeneratedCodeHelpers.h"
#include "Runtime/Engine/Classes/Engine/InputActionDelegateBinding.h"
#include "Runtime/Engine/Classes/Engine/SimpleConstructionScript.h"
#include "Runtime/Engine/Classes/Engine/SCS_Node.h"
#include "Runtime/Engine/Classes/GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/EngineBaseTypes.h"
#include "Runtime/Engine/Classes/Engine/AssetUserData.h"
#include "Runtime/Engine/Classes/EdGraph/EdGraphPin.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Runtime/Engine/Classes/Engine/NetSerialization.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerInput.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"
#include "Runtime/Engine/Classes/GameFramework/Info.h"
#include "Runtime/Engine/Classes/Components/BillboardComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodyInstance.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterial.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsSettingsEnums.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterialPropertyBase.h"
#include "Runtime/Engine/Classes/Vehicles/TireType.h"
#include "Runtime/Engine/Classes/Engine/DataAsset.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Engine/SubsurfaceProfile.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Engine/Public/MaterialShared.h"
#include "Runtime/Engine/Classes/Materials/MaterialExpression.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunction.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunctionInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollection.h"
#include "Runtime/Engine/Classes/Engine/BlendableInterface.h"
#include "Runtime/Engine/Classes/Engine/Texture.h"
#include "Runtime/Engine/Classes/Engine/TextureDefines.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_AssetUserData.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Materials/MaterialLayersFunctions.h"
#include "Runtime/Engine/Classes/Engine/Font.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/Engine/Classes/Engine/FontImportOptions.h"
#include "Runtime/SlateCore/Public/Fonts/CompositeFont.h"
#include "Runtime/SlateCore/Public/Fonts/FontProviderInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceBasePropertyOverrides.h"
#include "Runtime/Engine/Public/StaticParameterSet.h"
#include "Runtime/Engine/Classes/GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/GameFramework/PawnMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/NavMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/MovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PhysicsVolume.h"
#include "Runtime/Engine/Classes/GameFramework/Volume.h"
#include "Runtime/Engine/Classes/Engine/Brush.h"
#include "Runtime/Engine/Classes/Components/BrushComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetup.h"
#include "Runtime/Engine/Classes/PhysicsEngine/AggregateGeom.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphereElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ShapeElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BoxElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphylElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConvexElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/TaperedCapsuleElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetupEnums.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationTypes.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAgentInterface.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Navigation/PathFollowingComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationData.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataInterface.h"
#include "Runtime/AIModule/Classes/AIResourceInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/PathFollowingAgentInterface.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardData.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include "Runtime/AIModule/Classes/Perception/AISenseConfig.h"
#include "Runtime/AIModule/Classes/Perception/AISense.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionSystem.h"
#include "Runtime/AIModule/Classes/Perception/AISenseEvent.h"
#include "Runtime/AIModule/Classes/Actions/PawnActionsComponent.h"
#include "Runtime/AIModule/Classes/Actions/PawnAction.h"
#include "Runtime/AIModule/Classes/AITypes.h"
#include "Runtime/GameplayTasks/Classes/GameplayTasksComponent.h"
#include "Runtime/GameplayTasks/Classes/GameplayTask.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskOwnerInterface.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskResource.h"
#include "Runtime/NavigationSystem/Public/NavFilters/NavigationQueryFilter.h"
#include "Runtime/NavigationSystem/Public/NavAreas/NavArea.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAreaBase.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTCompositeNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTTaskNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTService.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTAuxiliaryNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTDecorator.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionListenerInterface.h"
#include "Runtime/AIModule/Classes/GenericTeamAgentInterface.h"
#include "Runtime/Engine/Public/VisualLogger/VisualLoggerDebugSnapshotInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavRelevantInterface.h"
#include "Runtime/Engine/Classes/GameFramework/LocalMessage.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineReplStructs.h"
#include "Runtime/CoreUObject/Public/UObject/CoreOnline.h"
#include "Runtime/Engine/Classes/GameFramework/EngineMessage.h"
#include "Runtime/Engine/Classes/GameFramework/DamageType.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/SkinnedMeshComponent.h"
#include "Runtime/Engine/Classes/Components/MeshComponent.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMesh.h"
#include "Runtime/Engine/Classes/Animation/Skeleton.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSocket.h"
#include "Runtime/Engine/Classes/Animation/SmartName.h"
#include "Runtime/Engine/Classes/Animation/BlendProfile.h"
#include "Runtime/Engine/Public/BoneContainer.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_PreviewMeshProvider.h"
#include "Runtime/Engine/Public/Components.h"
#include "Runtime/Engine/Public/PerPlatformProperties.h"
#include "Runtime/Engine/Public/SkeletalMeshReductionSettings.h"
#include "Runtime/Engine/Classes/Animation/AnimSequence.h"
#include "Runtime/Engine/Classes/Animation/AnimSequenceBase.h"
#include "Runtime/Engine/Classes/Animation/AnimationAsset.h"
#include "Runtime/Engine/Classes/Animation/AnimMetaData.h"
#include "Runtime/Engine/Public/Animation/AnimTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimLinkableElement.h"
#include "Runtime/Engine/Classes/Animation/AnimMontage.h"
#include "Runtime/Engine/Classes/Animation/AnimCompositeBase.h"
#include "Runtime/Engine/Public/AlphaBlend.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Curves/CurveBase.h"
#include "Runtime/Engine/Classes/Curves/RichCurve.h"
#include "Runtime/Engine/Classes/Curves/IndexedCurve.h"
#include "Runtime/Engine/Classes/Curves/KeyHandle.h"
#include "Runtime/Engine/Classes/Animation/AnimEnums.h"
#include "Runtime/Engine/Classes/Animation/TimeStretchCurve.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotify.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotifyState.h"
#include "Runtime/Engine/Public/Animation/AnimCurveTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsAsset.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicalAnimationComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsConstraintTemplate.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintInstance.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintDrives.h"
#include "Runtime/Engine/Classes/EditorFramework/ThumbnailInfo.h"
#include "Runtime/Engine/Public/Animation/NodeMappingContainer.h"
#include "Runtime/Engine/Public/Animation/NodeMappingProviderInterface.h"
#include "Runtime/Engine/Classes/Animation/MorphTarget.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include "Runtime/Engine/Public/Animation/AnimNotifyQueue.h"
#include "Runtime/Engine/Public/Animation/PoseSnapshot.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingAssetInterface.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSampling.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshLODSettings.h"
#include "Runtime/Engine/Classes/Engine/Blueprint.h"
#include "Runtime/Engine/Classes/Engine/BlueprintCore.h"
#include "Runtime/Engine/Classes/Engine/BlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/TimelineTemplate.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "Runtime/Engine/Classes/Curves/CurveVector.h"
#include "Runtime/Engine/Classes/Curves/CurveLinearColor.h"
#include "Runtime/Engine/Classes/Engine/InheritableComponentHandler.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_CollisionDataProvider.h"
#include "Runtime/Engine/Classes/Animation/AnimBlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/DynamicBlueprintBinding.h"
#include "Runtime/Engine/Classes/Animation/AnimStateMachineTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimClassInterface.h"
#include "Runtime/Engine/Public/SingleAnimationPlayData.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationFactoryInterface.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationInteractor.h"
#include "Runtime/Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationAvoidanceTypes.h"
#include "Runtime/Engine/Classes/GameFramework/RootMotionSource.h"
#include "Runtime/Engine/Public/AI/RVOAvoidanceInterface.h"
#include "Runtime/Engine/Classes/Interfaces/NetworkPredictionInterface.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Runtime/Engine/Classes/Engine/Player.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstDirector.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInst.h"
#include "Runtime/Engine/Classes/GameFramework/HUD.h"
#include "Runtime/Engine/Classes/Engine/Canvas.h"
#include "Runtime/Engine/Classes/Debug/ReporterGraph.h"
#include "Runtime/Engine/Classes/Debug/ReporterBase.h"
#include "Runtime/Engine/Classes/GameFramework/DebugTextInfo.h"
#include "Runtime/Engine/Classes/Camera/PlayerCameraManager.h"
#include "Runtime/Engine/Classes/Camera/CameraTypes.h"
#include "Runtime/Engine/Classes/Engine/Scene.h"
#include "Runtime/Engine/Classes/Engine/TextureCube.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier.h"
#include "Runtime/Engine/Classes/Particles/EmitterCameraLensEffectBase.h"
#include "Runtime/Engine/Classes/Particles/Emitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleEmitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleLODLevel.h"
#include "Runtime/Engine/Classes/Particles/ParticleModuleRequired.h"
#include "Runtime/Engine/Classes/Particles/ParticleModule.h"
#include "Runtime/Engine/Classes/Particles/ParticleSpriteEmitter.h"
#include "Runtime/Engine/Classes/Distributions/DistributionFloat.h"
#include "Runtime/Engine/Classes/Distributions/Distribution.h"
#include "Runtime/Engine/Classes/Particles/SubUVAnimation.h"
#include "Runtime/Engine/Classes/Particles/TypeData/ParticleModuleTypeDataBase.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawn.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawnBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventGenerator.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventSendToGame.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbit.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbitBase.h"
#include "Runtime/Engine/Classes/Distributions/DistributionVector.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventReceiverBase.h"
#include "Runtime/Engine/Public/ParticleHelper.h"
#include "Runtime/Engine/Classes/Engine/InterpCurveEdSetup.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemReplay.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier_CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraAnim.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroup.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrack.h"
#include "Runtime/Engine/Classes/Camera/CameraAnimInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackMove.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstMove.h"
#include "Runtime/Engine/Classes/Camera/CameraActor.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/GameFramework/CheatManager.h"
#include "Runtime/Engine/Classes/Engine/DebugCameraController.h"
#include "Runtime/Engine/Classes/Components/DrawFrustumComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "Runtime/Engine/Classes/Engine/NetConnection.h"
#include "Runtime/Engine/Classes/Engine/ChildConnection.h"
#include "Runtime/Engine/Classes/Engine/PackageMapClient.h"
#include "Runtime/Engine/Classes/Engine/NetDriver.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Engine/Level.h"
#include "Runtime/Engine/Classes/Components/ModelComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelActorContainer.h"
#include "Runtime/Engine/Classes/Engine/LevelScriptActor.h"
#include "Runtime/Engine/Classes/Engine/NavigationObjectBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataChunk.h"
#include "Runtime/Engine/Classes/Engine/MapBuildDataRegistry.h"
#include "Runtime/Engine/Classes/GameFramework/WorldSettings.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemConfig.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPhysicsVolume.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsCollisionHandler.h"
#include "Runtime/Engine/Classes/Sound/SoundBase.h"
#include "Runtime/Engine/Classes/Sound/SoundClass.h"
#include "Runtime/Engine/Classes/Sound/SoundMix.h"
#include "Runtime/Engine/Classes/Sound/SoundConcurrency.h"
#include "Runtime/Engine/Classes/Sound/SoundAttenuation.h"
#include "Runtime/Engine/Classes/Engine/Attenuation.h"
#include "Runtime/Engine/Public/IAudioExtensionPlugin.h"
#include "Runtime/Engine/Classes/Sound/SoundSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectPreset.h"
#include "Runtime/Engine/Public/IAmbisonicsMixer.h"
#include "Runtime/Engine/Classes/Sound/SoundWave.h"
#include "Runtime/AudioPlatformConfiguration/Public/AudioCompressionSettings.h"
#include "Runtime/Engine/Classes/Sound/SoundGroups.h"
#include "Runtime/Engine/Classes/Engine/CurveTable.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSource.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBusSend.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBus.h"
#include "Runtime/Engine/Classes/GameFramework/GameModeBase.h"
#include "Runtime/Engine/Classes/GameFramework/GameSession.h"
#include "Runtime/Engine/Classes/GameFramework/GameStateBase.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawn.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPawn.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshSocket.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavCollisionBase.h"
#include "Runtime/Engine/Classes/Engine/TextureStreamingTypes.h"
#include "Runtime/Engine/Classes/GameFramework/FloatingPawnMovement.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawnMovement.h"
#include "Runtime/Engine/Classes/Engine/ServerStatReplicator.h"
#include "Runtime/Engine/Classes/GameFramework/GameNetworkManager.h"
#include "Runtime/Engine/Classes/Sound/AudioVolume.h"
#include "Runtime/Engine/Classes/Sound/ReverbEffect.h"
#include "Runtime/Engine/Classes/Engine/BookMark.h"
#include "Runtime/Engine/Classes/Components/LineBatchComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelStreaming.h"
#include "Runtime/Engine/Classes/Engine/LevelStreamingVolume.h"
#include "Runtime/Engine/Classes/Engine/DemoNetDriver.h"
#include "Runtime/Engine/Classes/Particles/ParticleEventManager.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemBase.h"
#include "Runtime/Engine/Classes/AI/AISystemBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/AvoidanceManager.h"
#include "Runtime/Engine/Classes/Engine/GameInstance.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "Runtime/Engine/Classes/Engine/GameViewportClient.h"
#include "Runtime/Engine/Classes/Engine/ScriptViewportClient.h"
#include "Runtime/Engine/Classes/Engine/Console.h"
#include "Runtime/Engine/Classes/Engine/DebugDisplayProperty.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/Engine/Classes/GameFramework/GameUserSettings.h"
#include "Runtime/Engine/Classes/Engine/AssetManager.h"
#include "Runtime/Engine/Classes/Engine/EngineCustomTimeStep.h"
#include "Runtime/Engine/Classes/Engine/TimecodeProvider.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineSession.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollectionInstance.h"
#include "Runtime/Engine/Classes/Engine/WorldComposition.h"
#include "Runtime/Engine/Classes/Particles/WorldPSCPool.h"
#include "Runtime/Engine/Classes/Engine/ReplicationDriver.h"
#include "Runtime/Engine/Classes/Engine/Channel.h"
#include "Runtime/Engine/Classes/GameFramework/TouchInterface.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Components/Widget.h"
#include "Runtime/UMG/Public/Components/Visual.h"
#include "Runtime/UMG/Public/Components/PanelSlot.h"
#include "Runtime/UMG/Public/Components/PanelWidget.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "Runtime/UMG/Public/Slate/WidgetTransform.h"
#include "Runtime/SlateCore/Public/Layout/Clipping.h"
#include "Runtime/UMG/Public/Blueprint/WidgetNavigation.h"
#include "Runtime/SlateCore/Public/Input/NavigationReply.h"
#include "Runtime/SlateCore/Public/Types/SlateEnums.h"
#include "Runtime/UMG/Public/Binding/PropertyBinding.h"
#include "Runtime/UMG/Public/Binding/DynamicPropertyPath.h"
#include "Runtime/PropertyPath/Public/PropertyPathHelpers.h"
#include "Runtime/SlateCore/Public/Layout/Geometry.h"
#include "Runtime/SlateCore/Public/Input/Events.h"
#include "Runtime/SlateCore/Public/Styling/SlateColor.h"
#include "Runtime/SlateCore/Public/Styling/SlateBrush.h"
#include "Runtime/SlateCore/Public/Layout/Margin.h"
#include "Runtime/SlateCore/Public/Styling/SlateTypes.h"
#include "Runtime/UMG/Public/Animation/UMGSequencePlayer.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimation.h"
#include "Runtime/MovieScene/Public/MovieSceneSequence.h"
#include "Runtime/MovieScene/Public/MovieSceneSignedObject.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackIdentifier.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSegment.h"
#include "Runtime/MovieScene/Public/MovieSceneTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvalTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackImplementation.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationField.h"
#include "Runtime/MovieScene/Public/MovieSceneFrameMigration.h"
#include "Runtime/MovieScene/Public/MovieSceneSequenceID.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationKey.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceHierarchy.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceTransform.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceInstanceData.h"
#include "Runtime/MovieScene/Public/MovieSceneSection.h"
#include "Runtime/MovieScene/Public/MovieScene.h"
#include "Runtime/MovieScene/Public/MovieSceneSpawnable.h"
#include "Runtime/MovieScene/Public/MovieScenePossessable.h"
#include "Runtime/MovieScene/Public/MovieSceneBinding.h"
#include "Runtime/MovieScene/Public/MovieSceneFwd.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimationBinding.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/Slate/Public/Widgets/Layout/Anchors.h"
#include "Runtime/UMG/Public/Blueprint/DragDropOperation.h"
#include "Runtime/UMG/Public/Components/NamedSlotInterface.h"
#include "Runtime/Engine/Classes/Haptics/HapticFeedbackEffect_Base.h"
#include "Runtime/Engine/Classes/Engine/LatentActionManager.h"
#include "Runtime/Engine/Classes/Matinee/MatineeActor.h"
#include "Runtime/Engine/Classes/Matinee/InterpData.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupDirector.h"
#include "Runtime/Engine/Classes/Components/ChildActorComponent.h"
#include "Runtime/Engine/Classes/Components/SpotLightComponent.h"
#include "Runtime/Engine/Classes/Components/PointLightComponent.h"
#include "Runtime/Engine/Classes/Components/LocalLightComponent.h"
#include "Runtime/Engine/Classes/Components/LightComponent.h"
#include "Runtime/Engine/Classes/Components/LightComponentBase.h"
#include "Runtime/Engine/Classes/Engine/TextureLightProfile.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Kismet/BlueprintFunctionLibrary.h"
#include "MaluchyGameMode__pf2132744816.h"
#include "Rotatable__pf2962636854.h"
#include "Runtime/Engine/Classes/Kismet/KismetArrayLibrary.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/Engine/Classes/Engine/CollisionProfile.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackAttenuation.h"
#include "Runtime/Engine/Classes/Components/ForceFeedbackComponent.h"
#include "Runtime/Engine/Classes/Sound/DialogueWave.h"
#include "Runtime/Engine/Classes/Sound/DialogueTypes.h"
#include "Runtime/Engine/Classes/Sound/DialogueVoice.h"
#include "Runtime/Engine/Classes/Sound/DialogueSoundWaveProxy.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SaveGame.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStaticsTypes.h"
#include "RoadSegment__pf24847026.h"
#include "DiscoMaluch__pf1851508772.h"
#include "MainCarController__pf1851508772.h"
#include "../Plugins/Runtime/PhysXVehicles/Source/PhysXVehicles/Public/WheeledVehicleMovementComponent.h"
#include "../Plugins/Runtime/PhysXVehicles/Source/PhysXVehicles/Public/VehicleWheel.h"
#include "../Plugins/Runtime/PhysXVehicles/Source/PhysXVehicles/Public/TireConfig.h"
#include "Runtime/ClothingSystemRuntime/Public/ClothingSimulationFactory.h"
#include "../Plugins/Runtime/PhysXVehicles/Source/PhysXVehicles/Public/WheeledVehicleMovementComponent4W.h"
#include "MainUI__pf3053510930.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Public/Slate/SGameLayerManager.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "MaluchABP__pf1851508772.h"


#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
PRAGMA_DISABLE_OPTIMIZATION
ADiscoMaluchCar_C__pf1851508772::ADiscoMaluchCar_C__pf1851508772(const FObjectInitializer& ObjectInitializer) : Super()
{
	if(HasAnyFlags(RF_ClassDefaultObject) && (ADiscoMaluchCar_C__pf1851508772::StaticClass() == GetClass()))
	{
		ADiscoMaluchCar_C__pf1851508772::__CustomDynamicClassInitialization(CastChecked<UDynamicClass>(GetClass()));
	}
	
	auto __Local__0 = CastChecked<USkeletalMeshComponent>(this->GetDefaultSubobjectByName(TEXT("VehicleMesh")), ECastCheckedType::NullAllowed);
	auto __Local__1 = CastChecked<UWheeledVehicleMovementComponent4W>(this->GetDefaultSubobjectByName(TEXT("MovementComp")), ECastCheckedType::NullAllowed);
	if(__Local__0)
	{
		// --- Default subobject 'VehicleMesh' //
		__Local__0->AnimClass = UMaluchABP_C__pf1851508772::StaticClass();
		__Local__0->SkeletalMesh = CastChecked<USkeletalMesh>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
		__Local__0->OverrideMaterials = TArray<UMaterialInterface*> ();
		__Local__0->OverrideMaterials.Reserve(5);
		__Local__0->OverrideMaterials.Add(nullptr);
		__Local__0->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed));
		__Local__0->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed));
		__Local__0->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed));
		__Local__0->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed));
		__Local__0->RelativeScale3D = FVector(0.500000, 0.500000, 0.500000);
		if(!__Local__0->IsTemplate())
		{
			__Local__0->BodyInstance.FixupData(__Local__0);
		}
		// --- END default subobject 'VehicleMesh' //
	}
	if(__Local__1)
	{
		// --- Default subobject 'MovementComp' //
		__Local__1->EngineSetup.MaxRPM = 2000.000000f;
		__Local__1->TransmissionSetup.bUseGearAutoBox = false;
		__Local__1->TransmissionSetup.GearSwitchTime = 0.000000f;
		__Local__1->TransmissionSetup.ForwardGears = TArray<FVehicleGearData> ();
		__Local__1->TransmissionSetup.ForwardGears.AddUninitialized(5);
		FVehicleGearData::StaticStruct()->InitializeStruct(__Local__1->TransmissionSetup.ForwardGears.GetData(), 5);
		auto& __Local__2 = __Local__1->TransmissionSetup.ForwardGears[0];
		__Local__2.Ratio = 3.000000f;
		__Local__2.DownRatio = 0.500000f;
		__Local__2.UpRatio = 0.650000f;
		auto& __Local__3 = __Local__1->TransmissionSetup.ForwardGears[1];
		__Local__3.Ratio = 2.000000f;
		__Local__3.DownRatio = 0.500000f;
		__Local__3.UpRatio = 0.650000f;
		auto& __Local__4 = __Local__1->TransmissionSetup.ForwardGears[2];
		__Local__4.Ratio = 1.000000f;
		__Local__4.DownRatio = 0.500000f;
		__Local__4.UpRatio = 0.650000f;
		auto& __Local__5 = __Local__1->TransmissionSetup.ForwardGears[3];
		__Local__5.Ratio = 1.000000f;
		__Local__5.DownRatio = 0.500000f;
		__Local__5.UpRatio = 0.650000f;
		auto& __Local__6 = __Local__1->TransmissionSetup.ForwardGears[4];
		__Local__6.Ratio = 1.000000f;
		__Local__6.DownRatio = 0.500000f;
		__Local__6.UpRatio = 0.650000f;
		__Local__1->WheelSetups = TArray<FWheelSetup> ();
		__Local__1->WheelSetups.AddUninitialized(4);
		FWheelSetup::StaticStruct()->InitializeStruct(__Local__1->WheelSetups.GetData(), 4);
		auto& __Local__7 = __Local__1->WheelSetups[0];
		__Local__7.WheelClass = CastChecked<UClass>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
		__Local__7.BoneName = FName(TEXT("FL"));
		__Local__7.AdditionalOffset = FVector(55.000000, -55.000000, -15.000000);
		auto& __Local__8 = __Local__1->WheelSetups[1];
		__Local__8.WheelClass = CastChecked<UClass>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
		__Local__8.BoneName = FName(TEXT("FR"));
		__Local__8.AdditionalOffset = FVector(55.000000, 55.000000, -15.000000);
		auto& __Local__9 = __Local__1->WheelSetups[2];
		__Local__9.WheelClass = CastChecked<UClass>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
		__Local__9.BoneName = FName(TEXT("RL"));
		__Local__9.AdditionalOffset = FVector(-84.599998, -55.000000, -15.000000);
		__Local__9.bDisableSteering = true;
		auto& __Local__10 = __Local__1->WheelSetups[3];
		__Local__10.WheelClass = CastChecked<UClass>(CastChecked<UDynamicClass>(ADiscoMaluchCar_C__pf1851508772::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
		__Local__10.BoneName = FName(TEXT("RR"));
		__Local__10.AdditionalOffset = FVector(-84.599998, 55.000000, -15.000000);
		__Local__10.bDisableSteering = true;
		__Local__1->Mass = 1000.000000f;
		__Local__1->DragCoefficient = 0.070000f;
		auto& __Local__11 = (*(AccessPrivateProperty<FVehicleInputRate >((__Local__1), UWheeledVehicleMovementComponent::__PPO__ThrottleInputRate() )));
		__Local__11.RiseRate = 99.000000f;
		auto& __Local__12 = (*(AccessPrivateProperty<FVehicleInputRate >((__Local__1), UWheeledVehicleMovementComponent::__PPO__BrakeInputRate() )));
		__Local__12.RiseRate = 9999.000000f;
		auto& __Local__13 = (*(AccessPrivateProperty<FVehicleInputRate >((__Local__1), UWheeledVehicleMovementComponent::__PPO__SteeringInputRate() )));
		__Local__13.RiseRate = 9999.000000f;
		__Local__13.FallRate = 999999.000000f;
		static TWeakObjectPtr<UProperty> __Local__15{};
		const UProperty* __Local__14 = __Local__15.Get();
		if (nullptr == __Local__14)
		{
			__Local__14 = (UNavMovementComponent::StaticClass())->FindPropertyByName(FName(TEXT("bUseAccelerationForPaths")));
			check(__Local__14);
			__Local__15 = __Local__14;
		}
		(((UBoolProperty*)__Local__14)->SetPropertyValue_InContainer((__Local__1), false, 0));
		// --- END default subobject 'MovementComp' //
	}
	bpv__SpringArm__pf = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	bpv__Headlights__pf = CreateDefaultSubobject<USceneComponent>(TEXT("Headlights"));
	bpv__Left__pf = CreateDefaultSubobject<USpotLightComponent>(TEXT("Left"));
	bpv__Right__pf = CreateDefaultSubobject<USpotLightComponent>(TEXT("Right"));
	bpv__Camera__pf = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	bpv__SpringArm__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__SpringArm__pf->AttachToComponent(__Local__0, FAttachmentTransformRules::KeepRelativeTransform );
	bpv__SpringArm__pf->TargetArmLength = 50.000000f;
	bpv__SpringArm__pf->TargetOffset = FVector(-1300.000000, 0.000000, 1560.000000);
	bpv__SpringArm__pf->bInheritPitch = false;
	bpv__SpringArm__pf->bInheritYaw = false;
	bpv__SpringArm__pf->bInheritRoll = false;
	bpv__SpringArm__pf->RelativeRotation = FRotator(-49.999989, 0.000000, 0.000000);
	bpv__Headlights__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Headlights__pf->AttachToComponent(__Local__0, FAttachmentTransformRules::KeepRelativeTransform );
	bpv__Headlights__pf->RelativeLocation = FVector(170.000000, -4.500000, 130.000000);
	bpv__Headlights__pf->Mobility = EComponentMobility::Type::Static;
	bpv__Left__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Left__pf->AttachToComponent(bpv__Headlights__pf, FAttachmentTransformRules::KeepRelativeTransform );
	bpv__Left__pf->OuterConeAngle = 20.000000f;
	bpv__Left__pf->IntensityUnits = ELightUnits::Candelas;
	bpv__Left__pf->LightGuid = FGuid(0x186AA50C, 0x46420F93, 0x39D27BA2, 0xA4E664E8);
	bpv__Left__pf->Intensity = 3000.000000f;
	bpv__Left__pf->LightColor = FColor(220, 224, 255, 255);
	bpv__Left__pf->RelativeLocation = FVector(0.000000, -90.000000, 0.000000);
	bpv__Left__pf->RelativeRotation = FRotator(-9.999968, 0.000000, 0.000000);
	bpv__Right__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Right__pf->AttachToComponent(bpv__Headlights__pf, FAttachmentTransformRules::KeepRelativeTransform );
	bpv__Right__pf->OuterConeAngle = 20.000000f;
	bpv__Right__pf->IntensityUnits = ELightUnits::Candelas;
	bpv__Right__pf->LightGuid = FGuid(0x71C7AD1E, 0x41DD7820, 0xBB1BB7B2, 0x6DA0BD17);
	bpv__Right__pf->Intensity = 3000.000000f;
	bpv__Right__pf->LightColor = FColor(220, 224, 255, 255);
	bpv__Right__pf->RelativeLocation = FVector(0.000000, 90.000000, 0.000000);
	bpv__Right__pf->RelativeRotation = FRotator(-9.999968, 0.000000, 0.000000);
	bpv__Camera__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Camera__pf->AttachToComponent(__Local__0, FAttachmentTransformRules::KeepRelativeTransform );
	bpv__Camera__pf->AspectRatio = 1.333333f;
	bpv__Camera__pf->bConstrainAspectRatio = true;
	bpv__Camera__pf->PostProcessSettings.bOverride_AutoExposureMinBrightness = true;
	bpv__Camera__pf->PostProcessSettings.bOverride_AutoExposureMaxBrightness = true;
	bpv__Camera__pf->PostProcessSettings.AutoExposureMinBrightness = 1.000000f;
	bpv__Camera__pf->PostProcessSettings.AutoExposureMaxBrightness = 1.500000f;
	bpv__Camera__pf->RelativeLocation = FVector(-60.000000, 0.000000, 240.000000);
	bpv__DistancePassed__pf = 0.000000f;
	bpv__CoinsCollected__pf = 0;
	bpv__Fuel__pf = 1.000000f;
	bpv__StartRot__pf = FRotator(0.000000, 0.000000, 0.000000);
	bpv__FuelDescreasingSpeed__pf = 0.030000f;
	bpv__DrivingForce__pf = 2.000000f;
	bpv__SteeringSensitivity__pf = 36.000000f;
	bpv__CameraSteeringSensitivity__pf = 10.000000f;
	bpv__ResettingDistance__pf = 50000.000000f;
	bpv__ChangingGearTrashhold__pf = 0.000000f;
	bpv__AbleToDrive__pf = false;
	bpv__PrevSpeed__pf = 0.000000f;
	bpv__IsGrounded__pf = false;
	bpv__DrivingMultipier__pf = 1.000000f;
	bpv__GameMode__pf = nullptr;
	bpv__PrevX__pf = 0.000000f;
	bpv__YellowTimer__pf = 0.000000f;
	bpv__YellowPressed__pf = false;
	auto& __Local__16 = (*(AccessPrivateProperty<USkeletalMeshComponent*>((this), AWheeledVehicle::__PPO__Mesh() )));
	__Local__16 = __Local__0;
	auto& __Local__17 = (*(AccessPrivateProperty<UWheeledVehicleMovementComponent4W*>((this), AWheeledVehicle::__PPO__VehicleMovement() )));
	__Local__17 = __Local__1;
}
PRAGMA_ENABLE_OPTIMIZATION
void ADiscoMaluchCar_C__pf1851508772::PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph)
{
	Super::PostLoadSubobjects(OuterInstanceGraph);
	if(bpv__SpringArm__pf)
	{
		bpv__SpringArm__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Headlights__pf)
	{
		bpv__Headlights__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Left__pf)
	{
		bpv__Left__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Right__pf)
	{
		bpv__Right__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Camera__pf)
	{
		bpv__Camera__pf->CreationMethod = EComponentCreationMethod::Native;
	}
}
PRAGMA_DISABLE_OPTIMIZATION
void ADiscoMaluchCar_C__pf1851508772::__CustomDynamicClassInitialization(UDynamicClass* InDynamicClass)
{
	ensure(0 == InDynamicClass->ReferencedConvertedFields.Num());
	ensure(0 == InDynamicClass->MiscConvertedSubobjects.Num());
	ensure(0 == InDynamicClass->DynamicBindingObjects.Num());
	ensure(0 == InDynamicClass->ComponentTemplates.Num());
	ensure(0 == InDynamicClass->Timelines.Num());
	ensure(nullptr == InDynamicClass->AnimClassImplementation);
	InDynamicClass->AssembleReferenceTokenStream();
	// List of all referenced converted classes
	InDynamicClass->ReferencedConvertedFields.Add(AMaluchyGameMode_C__pf2132744816::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(UMainUI_C__pf3053510930::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(UMaluchABP_C__pf1851508772::StaticClass());
	FConvertedBlueprintsDependencies::FillUsedAssetsInDynamicClass(InDynamicClass, &__StaticDependencies_DirectlyUsedAssets);
	auto __Local__18 = NewObject<USceneComponent>(InDynamicClass, USceneComponent::StaticClass(), TEXT("DefaultSceneRoot_GEN_VARIABLE"));
	InDynamicClass->ComponentTemplates.Add(__Local__18);
	auto __Local__19 = NewObject<UInputActionDelegateBinding>(InDynamicClass, UInputActionDelegateBinding::StaticClass(), TEXT("InputActionDelegateBinding_1"));
	InDynamicClass->DynamicBindingObjects.Add(__Local__19);
	__Local__19->InputActionDelegateBindings = TArray<FBlueprintInputActionDelegateBinding> ();
	__Local__19->InputActionDelegateBindings.AddUninitialized(2);
	FBlueprintInputActionDelegateBinding::StaticStruct()->InitializeStruct(__Local__19->InputActionDelegateBindings.GetData(), 2);
	auto& __Local__20 = __Local__19->InputActionDelegateBindings[0];
	__Local__20.InputActionName = FName(TEXT("YellowButton"));
	__Local__20.InputKeyEvent = EInputEvent::IE_Released;
	__Local__20.FunctionNameToBind = FName(TEXT("InpActEvt_YellowButton_K2Node_InputActionEvent_3"));
	auto& __Local__21 = __Local__19->InputActionDelegateBindings[1];
	__Local__21.InputActionName = FName(TEXT("YellowButton"));
	__Local__21.FunctionNameToBind = FName(TEXT("InpActEvt_YellowButton_K2Node_InputActionEvent_2"));
}
PRAGMA_ENABLE_OPTIMIZATION
void ADiscoMaluchCar_C__pf1851508772::bpf__ExecuteUbergraph_DiscoMaluchCar__pf_0(int32 bpp__EntryPoint__pf)
{
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf{};
	int32 __CurrentState = bpp__EntryPoint__pf;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpv__YellowTimer__pf = 0.000000;
				__CurrentState = -1;
				break;
			}
		case 6:
			{
				__CurrentState = 7;
				break;
			}
		case 7:
			{
				bool  __Local__22 = false;
				if (!((::IsValid(bpv__GameMode__pf)) ? (bpv__GameMode__pf->bpv__GameStarted__pf) : (__Local__22)))
				{
					__CurrentState = -1;
					break;
				}
			}
		case 8:
			{
				bpf__CallResettingEnviroment__pf();
			}
		case 9:
			{
				bpf__Drive__pf(b0l__K2Node_Event_DeltaSeconds__pf);
			}
		case 10:
			{
				bpf__DecreaseFuel__pf(b0l__K2Node_Event_DeltaSeconds__pf);
			}
		case 11:
			{
				bpf__CheckIfGrounded__pf(b0l__K2Node_Event_DeltaSeconds__pf);
			}
		case 12:
			{
				bpf__CountDistance__pf();
			}
		case 13:
			{
				if (!bpv__YellowPressed__pf)
				{
					__CurrentState = 1;
					break;
				}
			}
		case 14:
			{
				bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Add_FloatFloat(bpv__YellowTimer__pf, b0l__K2Node_Event_DeltaSeconds__pf);
				bpv__YellowTimer__pf = bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf;
			}
		case 15:
			{
				bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Greater_FloatFloat(bpv__YellowTimer__pf, 2.000000);
				if (!bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 16:
			{
				if(::IsValid(bpv__GameMode__pf))
				{
					bpv__GameMode__pf->bpf__GameEnded__pf();
				}
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__ExecuteUbergraph_DiscoMaluchCar__pf_1(int32 bpp__EntryPoint__pf)
{
	FVector bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf(EForceInit::ForceInit);
	check(bpp__EntryPoint__pf == 2);
	// optimized KCST_UnconditionalGoto
	bpf__SetAsMainCar__pf();
	bpf__AddUIWidget__pf();
	bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf = AActor::K2_GetActorLocation();
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf, /*out*/ b0l__CallFunc_BreakVector_X__pf, /*out*/ b0l__CallFunc_BreakVector_Y__pf, /*out*/ b0l__CallFunc_BreakVector_Z__pf);
	bpv__PrevX__pf = b0l__CallFunc_BreakVector_X__pf;
	return; // KCST_GotoReturn
}
void ADiscoMaluchCar_C__pf1851508772::bpf__ExecuteUbergraph_DiscoMaluchCar__pf_2(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 17);
	// optimized KCST_UnconditionalGoto
	b0l__Temp_struct_Variable__pf = b0l__K2Node_InputActionEvent_Key__pf;
	bpv__YellowPressed__pf = true;
	return; // KCST_GotoReturn
}
void ADiscoMaluchCar_C__pf1851508772::bpf__ExecuteUbergraph_DiscoMaluchCar__pf_3(int32 bpp__EntryPoint__pf)
{
	int32 __CurrentState = bpp__EntryPoint__pf;
	do
	{
		switch( __CurrentState )
		{
		case 20:
			{
				__CurrentState = 21;
				break;
			}
		case 21:
			{
				b0l__Temp_struct_Variable__pf = b0l__K2Node_InputActionEvent_Key1__pf;
			}
		case 22:
			{
				bpv__YellowPressed__pf = false;
			}
		case 23:
			{
				bool  __Local__23 = false;
				if (!((::IsValid(bpv__GameMode__pf)) ? (bpv__GameMode__pf->bpv__GameStarted__pf) : (__Local__23)))
				{
					__CurrentState = 24;
					break;
				}
				__CurrentState = -1;
				break;
			}
		case 24:
			{
				if(::IsValid(bpv__GameMode__pf))
				{
					bpv__GameMode__pf->bpv__EventxGameStarted__pfT.Broadcast();
				}
			}
		case 25:
			{
				if(::IsValid(bpv__GameMode__pf))
				{
					bpv__GameMode__pf->bpv__GameStarted__pf = true;
				}
			}
		case 26:
			{
				bpv__AbleToDrive__pf = true;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
{
	b0l__K2Node_Event_DeltaSeconds__pf = bpp__DeltaSeconds__pf;
	bpf__ExecuteUbergraph_DiscoMaluchCar__pf_0(6);
}
void ADiscoMaluchCar_C__pf1851508772::bpf__ReceiveBeginPlay__pf()
{
	bpf__ExecuteUbergraph_DiscoMaluchCar__pf_1(2);
}
void ADiscoMaluchCar_C__pf1851508772::bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf(FKey bpp__Key__pf)
{
	b0l__K2Node_InputActionEvent_Key__pf = bpp__Key__pf;
	bpf__ExecuteUbergraph_DiscoMaluchCar__pf_2(17);
}
void ADiscoMaluchCar_C__pf1851508772::bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf(FKey bpp__Key__pf)
{
	b0l__K2Node_InputActionEvent_Key1__pf = bpp__Key__pf;
	bpf__ExecuteUbergraph_DiscoMaluchCar__pf_3(20);
}
void ADiscoMaluchCar_C__pf1851508772::bpf__UserConstructionScript__pf()
{
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	FRotator bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf(EForceInit::ForceInit);
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				if(::IsValid(bpv__Camera__pf))
				{
					bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf = bpv__Camera__pf->USceneComponent::K2_GetComponentRotation();
				}
				bpv__StartRot__pf = bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf;
			}
		case 2:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 3:
			{
				bpv__GameMode__pf = bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__Move__pf(float bpp__DeltaxTime__pfT)
{
	FVector bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_BreakVector_X__pf{};
	float bpfv__CallFunc_BreakVector_Y__pf{};
	float bpfv__CallFunc_BreakVector_Z__pf{};
	float bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_MakeVector_ReturnValue__pf(EForceInit::ForceInit);
	FHitResult bpfv__CallFunc_K2_SetActorLocation_SweepHitResult__pf{};
	bool bpfv__CallFunc_K2_SetActorLocation_ReturnValue__pf{};
	bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf = AActor::K2_GetActorLocation();
	bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_FloatFloat(bpp__DeltaxTime__pfT, 0.000000);
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
	bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Add_FloatFloat(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf, bpfv__CallFunc_BreakVector_Y__pf);
	bpfv__CallFunc_MakeVector_ReturnValue__pf = UKismetMathLibrary::MakeVector(bpfv__CallFunc_BreakVector_X__pf, bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf, bpfv__CallFunc_BreakVector_Z__pf);
	bpfv__CallFunc_K2_SetActorLocation_ReturnValue__pf = AActor::K2_SetActorLocation(bpfv__CallFunc_MakeVector_ReturnValue__pf, false, /*out*/ bpfv__CallFunc_K2_SetActorLocation_SweepHitResult__pf, true);
}
void ADiscoMaluchCar_C__pf1851508772::bpf__SetAsMainCar__pf()
{
}
void ADiscoMaluchCar_C__pf1851508772::bpf__HitByObstacle__pf()
{
	bpf__CantDrive__pf();
}
void ADiscoMaluchCar_C__pf1851508772::bpf__CollectCoin__pf()
{
	int32 bpfv__Temp_int_Variable__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue__pf{};
	bpfv__CallFunc_Add_IntInt_ReturnValue__pf = UKismetMathLibrary::Add_IntInt(bpv__CoinsCollected__pf, 1);
	bpfv__Temp_int_Variable__pf = bpfv__CallFunc_Add_IntInt_ReturnValue__pf;
	bpv__CoinsCollected__pf = bpfv__Temp_int_Variable__pf;
}
void ADiscoMaluchCar_C__pf1851508772::bpf__AddUIWidget__pf()
{
	UMainUI_C__pf3053510930* bpfv__CallFunc_Create_ReturnValue__pf{};
	bpfv__CallFunc_Create_ReturnValue__pf = CastChecked<UMainUI_C__pf3053510930>(UWidgetBlueprintLibrary::Create(this, UMainUI_C__pf3053510930::StaticClass(), ((APlayerController*)nullptr)), ECastCheckedType::NullAllowed);
	if(::IsValid(bpfv__CallFunc_Create_ReturnValue__pf))
	{
		bpfv__CallFunc_Create_ReturnValue__pf->UUserWidget::AddToViewport(0);
	}
}
void ADiscoMaluchCar_C__pf1851508772::bpf__CollectFuel__pf(float bpp__HowxMuch__pfT)
{
	float bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_FClamp_ReturnValue__pf{};
	bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Add_FloatFloat(bpp__HowxMuch__pfT, bpv__Fuel__pf);
	bpfv__CallFunc_FClamp_ReturnValue__pf = UKismetMathLibrary::FClamp(bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf, 0.000000, 1.000000);
	bpv__Fuel__pf = bpfv__CallFunc_FClamp_ReturnValue__pf;
}
void ADiscoMaluchCar_C__pf1851508772::bpf__DecreaseFuel__pf(float bpp__DeltaxTime__pfT)
{
	bool bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_FloatFloat(bpp__DeltaxTime__pfT, bpv__FuelDescreasingSpeed__pf);
				bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Subtract_FloatFloat(bpv__Fuel__pf, bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf);
				bpv__Fuel__pf = bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf;
			}
		case 2:
			{
				bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf = UKismetMathLibrary::LessEqual_FloatFloat(bpv__Fuel__pf, 0.000000);
				if (!bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 3:
			{
				bpf__CantDrive__pf();
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__CantDrive__pf()
{
	UStaticMeshComponent* bpfv__Target__pf{};
	bpv__AbleToDrive__pf = false;
	if(::IsValid(bpv__GameMode__pf))
	{
		bpv__GameMode__pf->bpf__GameEnded__pf();
	}
}
void ADiscoMaluchCar_C__pf1851508772::bpf__GetTotalMass__pf(/*out*/ float& bpp__TotalxMass__pfT)
{
	float bpfv__CallFunc_GetMass_ReturnValue__pf{};
	if(::IsValid((*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))))
	{
		bpfv__CallFunc_GetMass_ReturnValue__pf = (*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))->GetMass();
	}
	bpp__TotalxMass__pfT = bpfv__CallFunc_GetMass_ReturnValue__pf;
}
void ADiscoMaluchCar_C__pf1851508772::bpf__Drive__pf(float bpp__DeltaxTime__pfT)
{
	FVector bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakVector_X__pf{};
	float bpfv__CallFunc_BreakVector_Y__pf{};
	float bpfv__CallFunc_BreakVector_Z__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_MakeVector_ReturnValue__pf(EForceInit::ForceInit);
	FHitResult bpfv__CallFunc_K2_SetActorLocation_SweepHitResult__pf{};
	bool bpfv__CallFunc_K2_SetActorLocation_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				if (!bpv__AbleToDrive__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf = AActor::K2_GetActorLocation();
				UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
				bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_FloatFloat(bpp__DeltaxTime__pfT, bpv__DrivingForce__pf);
				bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Add_FloatFloat(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf, bpfv__CallFunc_BreakVector_X__pf);
				bpfv__CallFunc_MakeVector_ReturnValue__pf = UKismetMathLibrary::MakeVector(bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf, bpfv__CallFunc_BreakVector_Y__pf, bpfv__CallFunc_BreakVector_Z__pf);
				bpfv__CallFunc_K2_SetActorLocation_ReturnValue__pf = AActor::K2_SetActorLocation(bpfv__CallFunc_MakeVector_ReturnValue__pf, false, /*out*/ bpfv__CallFunc_K2_SetActorLocation_SweepHitResult__pf, true);
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__OldSteer__pf(float bpp__Input__pf)
{
	bool bpfv__CallFunc_NotEqual_FloatFloat_ReturnValue__pf{};
	FRotator bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakRotator_Roll__pf{};
	float bpfv__CallFunc_BreakRotator_Pitch__pf{};
	float bpfv__CallFunc_BreakRotator_Yaw__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Abs_ReturnValue__pf{};
	bool bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf{};
	bool bpfv__CallFunc_BooleanAND_ReturnValue__pf{};
	bool bpfv__CallFunc_BooleanAND_ReturnValue1__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_NotEqual_FloatFloat_ReturnValue__pf = UKismetMathLibrary::NotEqual_FloatFloat(bpp__Input__pf, 0.000000);
				if(::IsValid((*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))))
				{
					bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf = (*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))->USceneComponent::K2_GetComponentRotation();
				}
				UKismetMathLibrary::BreakRotator(bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakRotator_Roll__pf, /*out*/ bpfv__CallFunc_BreakRotator_Pitch__pf, /*out*/ bpfv__CallFunc_BreakRotator_Yaw__pf);
				bpfv__CallFunc_Abs_ReturnValue__pf = UKismetMathLibrary::Abs(bpfv__CallFunc_BreakRotator_Yaw__pf);
				bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Less_FloatFloat(bpfv__CallFunc_Abs_ReturnValue__pf, 80.000000);
				bpfv__CallFunc_BooleanAND_ReturnValue__pf = UKismetMathLibrary::BooleanAND(bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf, bpfv__CallFunc_NotEqual_FloatFloat_ReturnValue__pf);
				bpfv__CallFunc_BooleanAND_ReturnValue1__pf = UKismetMathLibrary::BooleanAND(bpfv__CallFunc_BooleanAND_ReturnValue__pf, bpv__AbleToDrive__pf);
				if (!bpfv__CallFunc_BooleanAND_ReturnValue1__pf)
				{
					__CurrentState = 3;
					break;
				}
			}
		case 2:
			{
				if(::IsValid((*(AccessPrivateProperty<UWheeledVehicleMovementComponent* >((this), AWheeledVehicle::__PPO__VehicleMovement() )))))
				{
					(*(AccessPrivateProperty<UWheeledVehicleMovementComponent* >((this), AWheeledVehicle::__PPO__VehicleMovement() )))->UWheeledVehicleMovementComponent::SetSteeringInput(bpp__Input__pf);
				}
				__CurrentState = -1;
				break;
			}
		case 3:
			{
				if(::IsValid((*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))))
				{
					bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf = (*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))->USceneComponent::K2_GetComponentRotation();
				}
				UKismetMathLibrary::BreakRotator(bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakRotator_Roll__pf, /*out*/ bpfv__CallFunc_BreakRotator_Pitch__pf, /*out*/ bpfv__CallFunc_BreakRotator_Yaw__pf);
				bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_FloatFloat(bpfv__CallFunc_BreakRotator_Yaw__pf, -0.200000);
				if(::IsValid((*(AccessPrivateProperty<UWheeledVehicleMovementComponent* >((this), AWheeledVehicle::__PPO__VehicleMovement() )))))
				{
					(*(AccessPrivateProperty<UWheeledVehicleMovementComponent* >((this), AWheeledVehicle::__PPO__VehicleMovement() )))->UWheeledVehicleMovementComponent::SetSteeringInput(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf);
				}
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__NewSteer__pf(float bpp__Input__pf)
{
	float bpfv__CallFunc_Conv_BoolToFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue1__pf{};
	FRotator bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakRotator_Roll__pf{};
	float bpfv__CallFunc_BreakRotator_Pitch__pf{};
	float bpfv__CallFunc_BreakRotator_Yaw__pf{};
	FRotator bpfv__CallFunc_MakeRotator_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue2__pf{};
	FHitResult bpfv__CallFunc_K2_SetWorldRotation_SweepHitResult__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue3__pf{};
	FVector bpfv__CallFunc_K2_GetComponentLocation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakVector_X__pf{};
	float bpfv__CallFunc_BreakVector_Y__pf{};
	float bpfv__CallFunc_BreakVector_Z__pf{};
	float bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_MakeVector_ReturnValue__pf(EForceInit::ForceInit);
	FHitResult bpfv__CallFunc_K2_SetWorldLocation_SweepHitResult__pf{};
	bpfv__CallFunc_Conv_BoolToFloat_ReturnValue__pf = UKismetMathLibrary::Conv_BoolToFloat(bpv__AbleToDrive__pf);
	bpfv__CallFunc_Multiply_FloatFloat_ReturnValue2__pf = UKismetMathLibrary::Multiply_FloatFloat(bpv__SteeringSensitivity__pf, bpp__Input__pf);
	bpfv__CallFunc_Multiply_FloatFloat_ReturnValue3__pf = UKismetMathLibrary::Multiply_FloatFloat(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue2__pf, bpfv__CallFunc_Conv_BoolToFloat_ReturnValue__pf);
	if(::IsValid((*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))))
	{
		bpfv__CallFunc_K2_GetComponentLocation_ReturnValue__pf = (*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))->USceneComponent::K2_GetComponentLocation();
	}
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetComponentLocation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
	bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Add_FloatFloat(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue3__pf, bpfv__CallFunc_BreakVector_Y__pf);
	bpfv__CallFunc_MakeVector_ReturnValue__pf = UKismetMathLibrary::MakeVector(bpfv__CallFunc_BreakVector_X__pf, bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf, bpfv__CallFunc_BreakVector_Z__pf);
	if(::IsValid((*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))))
	{
		(*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() )))->USceneComponent::K2_SetWorldLocation(bpfv__CallFunc_MakeVector_ReturnValue__pf, false, /*out*/ bpfv__CallFunc_K2_SetWorldLocation_SweepHitResult__pf, true);
	}
	bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_FloatFloat(bpp__Input__pf, bpv__CameraSteeringSensitivity__pf);
	bpfv__CallFunc_Multiply_FloatFloat_ReturnValue1__pf = UKismetMathLibrary::Multiply_FloatFloat(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf, -1.000000);
	if(::IsValid(bpv__Camera__pf))
	{
		bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf = bpv__Camera__pf->USceneComponent::K2_GetComponentRotation();
	}
	UKismetMathLibrary::BreakRotator(bpfv__CallFunc_K2_GetComponentRotation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakRotator_Roll__pf, /*out*/ bpfv__CallFunc_BreakRotator_Pitch__pf, /*out*/ bpfv__CallFunc_BreakRotator_Yaw__pf);
	bpfv__CallFunc_MakeRotator_ReturnValue__pf = UKismetMathLibrary::MakeRotator(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue1__pf, bpfv__CallFunc_BreakRotator_Pitch__pf, 0.000000);
	if(::IsValid(bpv__Camera__pf))
	{
		bpv__Camera__pf->USceneComponent::K2_SetWorldRotation(bpfv__CallFunc_MakeRotator_ReturnValue__pf, false, /*out*/ bpfv__CallFunc_K2_SetWorldRotation_SweepHitResult__pf, false);
	}
}
void ADiscoMaluchCar_C__pf1851508772::bpf__CallResettingEnviroment__pf()
{
	float bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakVector_X__pf{};
	float bpfv__CallFunc_BreakVector_Y__pf{};
	float bpfv__CallFunc_BreakVector_Z__pf{};
	bool bpfv__CallFunc_GreaterEqual_FloatFloat_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf = AActor::K2_GetActorLocation();
				UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
				bpfv__CallFunc_GreaterEqual_FloatFloat_ReturnValue__pf = UKismetMathLibrary::GreaterEqual_FloatFloat(bpfv__CallFunc_BreakVector_X__pf, bpv__ResettingDistance__pf);
				if (!bpfv__CallFunc_GreaterEqual_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				if(::IsValid(bpv__GameMode__pf))
				{
					bpv__GameMode__pf->bpf__ResetEnviroment__pf();
				}
			}
		case 3:
			{
				bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Subtract_FloatFloat(bpv__DistancePassed__pf, bpv__ResettingDistance__pf);
				bpv__DistancePassed__pf = bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__ChangeGear__pf(float bpp__Input__pf)
{
	int32 bpfv__GearToSet__pf{};
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf{};
	bool bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf{};
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue1__pf{};
	bool bpfv__CallFunc_Not_PreBool_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__GearToSet__pf = 1;
			}
		case 2:
			{
				bpfv__CallFunc_Not_PreBool_ReturnValue__pf = UKismetMathLibrary::Not_PreBool(bpv__AbleToDrive__pf);
				if (!bpfv__CallFunc_Not_PreBool_ReturnValue__pf)
				{
					__CurrentState = 5;
					break;
				}
			}
		case 3:
			{
				bpfv__GearToSet__pf = 0;
			}
		case 4:
			{
				if(::IsValid((*(AccessPrivateProperty<UWheeledVehicleMovementComponent* >((this), AWheeledVehicle::__PPO__VehicleMovement() )))))
				{
					(*(AccessPrivateProperty<UWheeledVehicleMovementComponent* >((this), AWheeledVehicle::__PPO__VehicleMovement() )))->UWheeledVehicleMovementComponent::SetTargetGear(bpfv__GearToSet__pf, true);
				}
				__CurrentState = -1;
				break;
			}
		case 5:
			{
				bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Greater_FloatFloat(bpp__Input__pf, 0.500000);
				if (!bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = 7;
					break;
				}
			}
		case 6:
			{
				bpfv__GearToSet__pf = 3;
				__CurrentState = 4;
				break;
			}
		case 7:
			{
				bpfv__CallFunc_Greater_FloatFloat_ReturnValue1__pf = UKismetMathLibrary::Greater_FloatFloat(bpp__Input__pf, 0.250000);
				if (!bpfv__CallFunc_Greater_FloatFloat_ReturnValue1__pf)
				{
					__CurrentState = 9;
					break;
				}
			}
		case 8:
			{
				bpfv__GearToSet__pf = 2;
				__CurrentState = 4;
				break;
			}
		case 9:
			{
				bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Less_FloatFloat(bpp__Input__pf, -0.250000);
				if (!bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = 4;
					break;
				}
			}
		case 10:
			{
				bpfv__GearToSet__pf = -1;
				__CurrentState = 4;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__CheckIfGrounded__pf(float bpp__DeltaxTime__pfT)
{
	int32 bpfv__Sign__pf{};
	TArray<TEnumAsByte<EObjectTypeQuery> > bpfv__ObjectTypes__pf{};
	TArray<AActor*> bpfv__Temp_object_Variable__pf{};
	bool bpfv__CallFunc_Not_PreBool_ReturnValue__pf{};
	TArray<TEnumAsByte<EObjectTypeQuery> > bpfv__K2Node_MakeArray_Array__pf{};
	float bpfv__CallFunc_Conv_IntToFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue1__pf{};
	FRotator bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakRotator_Roll__pf{};
	float bpfv__CallFunc_BreakRotator_Pitch__pf{};
	float bpfv__CallFunc_BreakRotator_Yaw__pf{};
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_GetComponentBounds_Origin__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_GetComponentBounds_BoxExtent__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_GetComponentBounds_SphereRadius__pf{};
	FVector bpfv__CallFunc_Multiply_VectorInt_ReturnValue__pf(EForceInit::ForceInit);
	bool bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_Add_VectorVector_ReturnValue__pf(EForceInit::ForceInit);
	FHitResult bpfv__CallFunc_LineTraceSingleForObjects_OutHit__pf{};
	bool bpfv__CallFunc_LineTraceSingleForObjects_ReturnValue__pf{};
	float bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf{};
	FRotator bpfv__CallFunc_MakeRotator_ReturnValue__pf(EForceInit::ForceInit);
	bool bpfv__CallFunc_K2_SetActorRotation_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__K2Node_MakeArray_Array__pf.SetNum(2, true);
				bpfv__K2Node_MakeArray_Array__pf[0] = EObjectTypeQuery::ObjectTypeQuery2;
				bpfv__K2Node_MakeArray_Array__pf[1] = EObjectTypeQuery::ObjectTypeQuery3;
				bpfv__ObjectTypes__pf = bpfv__K2Node_MakeArray_Array__pf;
			}
		case 2:
			{
				UKismetSystemLibrary::GetComponentBounds((*(AccessPrivateProperty<USkeletalMeshComponent* >((this), AWheeledVehicle::__PPO__Mesh() ))), /*out*/ bpfv__CallFunc_GetComponentBounds_Origin__pf, /*out*/ bpfv__CallFunc_GetComponentBounds_BoxExtent__pf, /*out*/ bpfv__CallFunc_GetComponentBounds_SphereRadius__pf);
				bpfv__CallFunc_Multiply_VectorInt_ReturnValue__pf = UKismetMathLibrary::Multiply_VectorInt(bpfv__CallFunc_GetComponentBounds_BoxExtent__pf, -2);
				bpfv__CallFunc_Add_VectorVector_ReturnValue__pf = UKismetMathLibrary::Add_VectorVector(bpfv__CallFunc_Multiply_VectorInt_ReturnValue__pf, bpfv__CallFunc_GetComponentBounds_Origin__pf);
				bpfv__CallFunc_LineTraceSingleForObjects_ReturnValue__pf = UKismetSystemLibrary::LineTraceSingleForObjects(this, bpfv__CallFunc_GetComponentBounds_Origin__pf, bpfv__CallFunc_Add_VectorVector_ReturnValue__pf, bpfv__ObjectTypes__pf, false, bpfv__Temp_object_Variable__pf, EDrawDebugTrace::None, /*out*/ bpfv__CallFunc_LineTraceSingleForObjects_OutHit__pf, true, FLinearColor(1.000000,0.000000,0.000000,1.000000), FLinearColor(0.000000,1.000000,0.000000,1.000000), 5.000000);
			}
		case 3:
			{
				bpv__IsGrounded__pf = bpfv__CallFunc_LineTraceSingleForObjects_ReturnValue__pf;
			}
		case 4:
			{
				bpfv__CallFunc_Not_PreBool_ReturnValue__pf = UKismetMathLibrary::Not_PreBool(bpv__IsGrounded__pf);
				if (!bpfv__CallFunc_Not_PreBool_ReturnValue__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 5:
			{
				bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf = AActor::K2_GetActorRotation();
				UKismetMathLibrary::BreakRotator(bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakRotator_Roll__pf, /*out*/ bpfv__CallFunc_BreakRotator_Pitch__pf, /*out*/ bpfv__CallFunc_BreakRotator_Yaw__pf);
				bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Greater_FloatFloat(bpfv__CallFunc_BreakRotator_Pitch__pf, 0.000000);
				if (!bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = 8;
					break;
				}
			}
		case 6:
			{
				bpfv__Sign__pf = 1;
			}
		case 7:
			{
				bpfv__CallFunc_Conv_IntToFloat_ReturnValue__pf = UKismetMathLibrary::Conv_IntToFloat(bpfv__Sign__pf);
				bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_FloatFloat(bpfv__CallFunc_Conv_IntToFloat_ReturnValue__pf, 20.000000);
				bpfv__CallFunc_Multiply_FloatFloat_ReturnValue1__pf = UKismetMathLibrary::Multiply_FloatFloat(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf, bpp__DeltaxTime__pfT);
				bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf = AActor::K2_GetActorRotation();
				UKismetMathLibrary::BreakRotator(bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakRotator_Roll__pf, /*out*/ bpfv__CallFunc_BreakRotator_Pitch__pf, /*out*/ bpfv__CallFunc_BreakRotator_Yaw__pf);
				bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Add_FloatFloat(bpfv__CallFunc_BreakRotator_Pitch__pf, bpfv__CallFunc_Multiply_FloatFloat_ReturnValue1__pf);
				bpfv__CallFunc_MakeRotator_ReturnValue__pf = UKismetMathLibrary::MakeRotator(bpfv__CallFunc_BreakRotator_Roll__pf, bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf, bpfv__CallFunc_BreakRotator_Yaw__pf);
				bpfv__CallFunc_K2_SetActorRotation_ReturnValue__pf = AActor::K2_SetActorRotation(bpfv__CallFunc_MakeRotator_ReturnValue__pf, true);
				__CurrentState = -1;
				break;
			}
		case 8:
			{
				bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf = AActor::K2_GetActorRotation();
				UKismetMathLibrary::BreakRotator(bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakRotator_Roll__pf, /*out*/ bpfv__CallFunc_BreakRotator_Pitch__pf, /*out*/ bpfv__CallFunc_BreakRotator_Yaw__pf);
				bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Less_FloatFloat(bpfv__CallFunc_BreakRotator_Pitch__pf, 0.000000);
				if (!bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = 10;
					break;
				}
			}
		case 9:
			{
				bpfv__Sign__pf = -1;
				__CurrentState = 7;
				break;
			}
		case 10:
			{
				bpfv__Sign__pf = 0;
				__CurrentState = 7;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ADiscoMaluchCar_C__pf1851508772::bpf__CountDistance__pf()
{
	FVector bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakVector_X__pf{};
	float bpfv__CallFunc_BreakVector_Y__pf{};
	float bpfv__CallFunc_BreakVector_Z__pf{};
	float bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Abs_ReturnValue__pf{};
	float bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf{};
	bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf = AActor::K2_GetActorLocation();
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
	bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Subtract_FloatFloat(bpv__PrevX__pf, bpfv__CallFunc_BreakVector_X__pf);
	bpfv__CallFunc_Abs_ReturnValue__pf = UKismetMathLibrary::Abs(bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf);
	bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Add_FloatFloat(bpv__DistancePassed__pf, bpfv__CallFunc_Abs_ReturnValue__pf);
	bpv__DistancePassed__pf = bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf;
	bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf = AActor::K2_GetActorLocation();
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
	bpv__PrevX__pf = bpfv__CallFunc_BreakVector_X__pf;
}
PRAGMA_DISABLE_OPTIMIZATION
void ADiscoMaluchCar_C__pf1851508772::__StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{123, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  SkeletalMesh /Game/Blueprints/Car/Meshes/SimplifiedMesh2.SimplifiedMesh2 
		{62, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Material /Game/Assets/Black.Black 
		{124, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/Car/MaluchWheel.MaluchWheel_C 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
PRAGMA_DISABLE_OPTIMIZATION
void ADiscoMaluchCar_C__pf1851508772::__StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	__StaticDependencies_DirectlyUsedAssets(AssetsToLoad);
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{30, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.SpringArmComponent 
		{0, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.SceneComponent 
		{82, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.SpotLightComponent 
		{29, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.CameraComponent 
		{1, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.PointerToUberGraphFrame 
		{103, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/InputCore.Key 
		{6, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Actor 
		{4, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetMathLibrary 
		{125, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/PhysXVehicles.WheeledVehicle 
		{36, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Enum /Script/Engine.EObjectTypeQuery 
		{9, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.HitResult 
		{5, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetSystemLibrary 
		{126, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/PhysXVehicles.WheeledVehicleMovementComponent 
		{28, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.StaticMeshComponent 
		{37, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.WidgetBlueprintLibrary 
		{38, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.UserWidget 
		{2, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.GameModeBase 
		{7, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.GameplayStatics 
		{127, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/ClothingSystemRuntime.ClothingSimulationFactoryNv 
		{128, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  MaluchWheel_C /Game/Blueprints/Car/MaluchWheel.Default__MaluchWheel_C 
		{39, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/AIModule.AIController 
		{40, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/MaluchyGameMode.MaluchyGameMode_C 
		{41, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  WidgetBlueprintGeneratedClass /Game/Blueprints/UI/MainUI.MainUI_C 
		{129, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimBlueprintGeneratedClass /Game/Blueprints/Car/MaluchABP.MaluchABP_C 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
struct FRegisterHelper__ADiscoMaluchCar_C__pf1851508772
{
	FRegisterHelper__ADiscoMaluchCar_C__pf1851508772()
	{
		FConvertedBlueprintsDependencies::Get().RegisterConvertedClass(TEXT("/Game/Blueprints/Car/DiscoMaluchCar"), &ADiscoMaluchCar_C__pf1851508772::__StaticDependenciesAssets);
	}
	static FRegisterHelper__ADiscoMaluchCar_C__pf1851508772 Instance;
};
FRegisterHelper__ADiscoMaluchCar_C__pf1851508772 FRegisterHelper__ADiscoMaluchCar_C__pf1851508772::Instance;
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
