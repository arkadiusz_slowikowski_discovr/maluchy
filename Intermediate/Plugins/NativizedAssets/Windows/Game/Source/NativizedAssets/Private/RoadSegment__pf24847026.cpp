#include "NativizedAssets.h"
#include "RoadSegment__pf24847026.h"
#include "GeneratedCodeHelpers.h"
#include "Runtime/Engine/Classes/Engine/SimpleConstructionScript.h"
#include "Runtime/Engine/Classes/Engine/SCS_Node.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/EngineBaseTypes.h"
#include "Runtime/Engine/Classes/Engine/AssetUserData.h"
#include "Runtime/Engine/Classes/EdGraph/EdGraphPin.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Runtime/Engine/Classes/Engine/NetSerialization.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerInput.h"
#include "Runtime/InputCore/Classes/InputCoreTypes.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"
#include "Runtime/Engine/Classes/GameFramework/Info.h"
#include "Runtime/Engine/Classes/Components/BillboardComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodyInstance.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterial.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsSettingsEnums.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterialPropertyBase.h"
#include "Runtime/Engine/Classes/Vehicles/TireType.h"
#include "Runtime/Engine/Classes/Engine/DataAsset.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Engine/SubsurfaceProfile.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Engine/Public/MaterialShared.h"
#include "Runtime/Engine/Classes/Materials/MaterialExpression.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunction.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunctionInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollection.h"
#include "Runtime/Engine/Classes/Engine/BlendableInterface.h"
#include "Runtime/Engine/Classes/Engine/Texture.h"
#include "Runtime/Engine/Classes/Engine/TextureDefines.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_AssetUserData.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Materials/MaterialLayersFunctions.h"
#include "Runtime/Engine/Classes/Engine/Font.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/Engine/Classes/Engine/FontImportOptions.h"
#include "Runtime/SlateCore/Public/Fonts/CompositeFont.h"
#include "Runtime/SlateCore/Public/Fonts/FontProviderInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceBasePropertyOverrides.h"
#include "Runtime/Engine/Public/StaticParameterSet.h"
#include "Runtime/Engine/Classes/GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/GameFramework/PawnMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/NavMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/MovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PhysicsVolume.h"
#include "Runtime/Engine/Classes/GameFramework/Volume.h"
#include "Runtime/Engine/Classes/Engine/Brush.h"
#include "Runtime/Engine/Classes/Components/BrushComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetup.h"
#include "Runtime/Engine/Classes/PhysicsEngine/AggregateGeom.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphereElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ShapeElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BoxElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphylElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConvexElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/TaperedCapsuleElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetupEnums.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationTypes.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAgentInterface.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Navigation/PathFollowingComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationData.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataInterface.h"
#include "Runtime/AIModule/Classes/AIResourceInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/PathFollowingAgentInterface.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardData.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include "Runtime/AIModule/Classes/Perception/AISenseConfig.h"
#include "Runtime/AIModule/Classes/Perception/AISense.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionSystem.h"
#include "Runtime/AIModule/Classes/Perception/AISenseEvent.h"
#include "Runtime/AIModule/Classes/Actions/PawnActionsComponent.h"
#include "Runtime/AIModule/Classes/Actions/PawnAction.h"
#include "Runtime/AIModule/Classes/AITypes.h"
#include "Runtime/GameplayTasks/Classes/GameplayTasksComponent.h"
#include "Runtime/GameplayTasks/Classes/GameplayTask.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskOwnerInterface.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskResource.h"
#include "Runtime/NavigationSystem/Public/NavFilters/NavigationQueryFilter.h"
#include "Runtime/NavigationSystem/Public/NavAreas/NavArea.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAreaBase.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTCompositeNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTTaskNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTService.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTAuxiliaryNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTDecorator.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionListenerInterface.h"
#include "Runtime/AIModule/Classes/GenericTeamAgentInterface.h"
#include "Runtime/Engine/Public/VisualLogger/VisualLoggerDebugSnapshotInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavRelevantInterface.h"
#include "Runtime/Engine/Classes/GameFramework/LocalMessage.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineReplStructs.h"
#include "Runtime/CoreUObject/Public/UObject/CoreOnline.h"
#include "Runtime/Engine/Classes/GameFramework/EngineMessage.h"
#include "Runtime/Engine/Classes/GameFramework/DamageType.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/SkinnedMeshComponent.h"
#include "Runtime/Engine/Classes/Components/MeshComponent.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMesh.h"
#include "Runtime/Engine/Classes/Animation/Skeleton.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSocket.h"
#include "Runtime/Engine/Classes/Animation/SmartName.h"
#include "Runtime/Engine/Classes/Animation/BlendProfile.h"
#include "Runtime/Engine/Public/BoneContainer.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_PreviewMeshProvider.h"
#include "Runtime/Engine/Public/Components.h"
#include "Runtime/Engine/Public/PerPlatformProperties.h"
#include "Runtime/Engine/Public/SkeletalMeshReductionSettings.h"
#include "Runtime/Engine/Classes/Animation/AnimSequence.h"
#include "Runtime/Engine/Classes/Animation/AnimSequenceBase.h"
#include "Runtime/Engine/Classes/Animation/AnimationAsset.h"
#include "Runtime/Engine/Classes/Animation/AnimMetaData.h"
#include "Runtime/Engine/Public/Animation/AnimTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimLinkableElement.h"
#include "Runtime/Engine/Classes/Animation/AnimMontage.h"
#include "Runtime/Engine/Classes/Animation/AnimCompositeBase.h"
#include "Runtime/Engine/Public/AlphaBlend.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Curves/CurveBase.h"
#include "Runtime/Engine/Classes/Curves/RichCurve.h"
#include "Runtime/Engine/Classes/Curves/IndexedCurve.h"
#include "Runtime/Engine/Classes/Curves/KeyHandle.h"
#include "Runtime/Engine/Classes/Animation/AnimEnums.h"
#include "Runtime/Engine/Classes/Animation/TimeStretchCurve.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotify.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotifyState.h"
#include "Runtime/Engine/Public/Animation/AnimCurveTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsAsset.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicalAnimationComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsConstraintTemplate.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintInstance.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintDrives.h"
#include "Runtime/Engine/Classes/EditorFramework/ThumbnailInfo.h"
#include "Runtime/Engine/Public/Animation/NodeMappingContainer.h"
#include "Runtime/Engine/Public/Animation/NodeMappingProviderInterface.h"
#include "Runtime/Engine/Classes/Animation/MorphTarget.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include "Runtime/Engine/Public/Animation/AnimNotifyQueue.h"
#include "Runtime/Engine/Public/Animation/PoseSnapshot.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingAssetInterface.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSampling.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshLODSettings.h"
#include "Runtime/Engine/Classes/Engine/Blueprint.h"
#include "Runtime/Engine/Classes/Engine/BlueprintCore.h"
#include "Runtime/Engine/Classes/Engine/BlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/TimelineTemplate.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "Runtime/Engine/Classes/Curves/CurveVector.h"
#include "Runtime/Engine/Classes/Curves/CurveLinearColor.h"
#include "Runtime/Engine/Classes/Engine/InheritableComponentHandler.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_CollisionDataProvider.h"
#include "Runtime/Engine/Classes/Animation/AnimBlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/DynamicBlueprintBinding.h"
#include "Runtime/Engine/Classes/Animation/AnimStateMachineTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimClassInterface.h"
#include "Runtime/Engine/Public/SingleAnimationPlayData.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationFactoryInterface.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationInteractor.h"
#include "Runtime/Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationAvoidanceTypes.h"
#include "Runtime/Engine/Classes/GameFramework/RootMotionSource.h"
#include "Runtime/Engine/Public/AI/RVOAvoidanceInterface.h"
#include "Runtime/Engine/Classes/Interfaces/NetworkPredictionInterface.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Runtime/Engine/Classes/Engine/Player.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstDirector.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInst.h"
#include "Runtime/Engine/Classes/GameFramework/HUD.h"
#include "Runtime/Engine/Classes/Engine/Canvas.h"
#include "Runtime/Engine/Classes/Debug/ReporterGraph.h"
#include "Runtime/Engine/Classes/Debug/ReporterBase.h"
#include "Runtime/Engine/Classes/GameFramework/DebugTextInfo.h"
#include "Runtime/Engine/Classes/Camera/PlayerCameraManager.h"
#include "Runtime/Engine/Classes/Camera/CameraTypes.h"
#include "Runtime/Engine/Classes/Engine/Scene.h"
#include "Runtime/Engine/Classes/Engine/TextureCube.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier.h"
#include "Runtime/Engine/Classes/Particles/EmitterCameraLensEffectBase.h"
#include "Runtime/Engine/Classes/Particles/Emitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleEmitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleLODLevel.h"
#include "Runtime/Engine/Classes/Particles/ParticleModuleRequired.h"
#include "Runtime/Engine/Classes/Particles/ParticleModule.h"
#include "Runtime/Engine/Classes/Particles/ParticleSpriteEmitter.h"
#include "Runtime/Engine/Classes/Distributions/DistributionFloat.h"
#include "Runtime/Engine/Classes/Distributions/Distribution.h"
#include "Runtime/Engine/Classes/Particles/SubUVAnimation.h"
#include "Runtime/Engine/Classes/Particles/TypeData/ParticleModuleTypeDataBase.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawn.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawnBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventGenerator.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventSendToGame.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbit.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbitBase.h"
#include "Runtime/Engine/Classes/Distributions/DistributionVector.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventReceiverBase.h"
#include "Runtime/Engine/Public/ParticleHelper.h"
#include "Runtime/Engine/Classes/Engine/InterpCurveEdSetup.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemReplay.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier_CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraAnim.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroup.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrack.h"
#include "Runtime/Engine/Classes/Camera/CameraAnimInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackMove.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstMove.h"
#include "Runtime/Engine/Classes/Camera/CameraActor.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/GameFramework/CheatManager.h"
#include "Runtime/Engine/Classes/Engine/DebugCameraController.h"
#include "Runtime/Engine/Classes/Components/DrawFrustumComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "Runtime/Engine/Classes/Engine/NetConnection.h"
#include "Runtime/Engine/Classes/Engine/ChildConnection.h"
#include "Runtime/Engine/Classes/Engine/PackageMapClient.h"
#include "Runtime/Engine/Classes/Engine/NetDriver.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Engine/Level.h"
#include "Runtime/Engine/Classes/Components/ModelComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelActorContainer.h"
#include "Runtime/Engine/Classes/Engine/LevelScriptActor.h"
#include "Runtime/Engine/Classes/Engine/NavigationObjectBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataChunk.h"
#include "Runtime/Engine/Classes/Engine/MapBuildDataRegistry.h"
#include "Runtime/Engine/Classes/GameFramework/WorldSettings.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemConfig.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPhysicsVolume.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsCollisionHandler.h"
#include "Runtime/Engine/Classes/Sound/SoundBase.h"
#include "Runtime/Engine/Classes/Sound/SoundClass.h"
#include "Runtime/Engine/Classes/Sound/SoundMix.h"
#include "Runtime/Engine/Classes/Sound/SoundConcurrency.h"
#include "Runtime/Engine/Classes/Sound/SoundAttenuation.h"
#include "Runtime/Engine/Classes/Engine/Attenuation.h"
#include "Runtime/Engine/Public/IAudioExtensionPlugin.h"
#include "Runtime/Engine/Classes/Sound/SoundSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectPreset.h"
#include "Runtime/Engine/Public/IAmbisonicsMixer.h"
#include "Runtime/Engine/Classes/Sound/SoundWave.h"
#include "Runtime/AudioPlatformConfiguration/Public/AudioCompressionSettings.h"
#include "Runtime/Engine/Classes/Sound/SoundGroups.h"
#include "Runtime/Engine/Classes/Engine/CurveTable.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSource.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBusSend.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBus.h"
#include "Runtime/Engine/Classes/GameFramework/GameModeBase.h"
#include "Runtime/Engine/Classes/GameFramework/GameSession.h"
#include "Runtime/Engine/Classes/GameFramework/GameStateBase.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawn.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPawn.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshSocket.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavCollisionBase.h"
#include "Runtime/Engine/Classes/Engine/TextureStreamingTypes.h"
#include "Runtime/Engine/Classes/GameFramework/FloatingPawnMovement.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawnMovement.h"
#include "Runtime/Engine/Classes/Engine/ServerStatReplicator.h"
#include "Runtime/Engine/Classes/GameFramework/GameNetworkManager.h"
#include "Runtime/Engine/Classes/Sound/AudioVolume.h"
#include "Runtime/Engine/Classes/Sound/ReverbEffect.h"
#include "Runtime/Engine/Classes/Engine/BookMark.h"
#include "Runtime/Engine/Classes/Components/LineBatchComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelStreaming.h"
#include "Runtime/Engine/Classes/Engine/LevelStreamingVolume.h"
#include "Runtime/Engine/Classes/Engine/DemoNetDriver.h"
#include "Runtime/Engine/Classes/Particles/ParticleEventManager.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemBase.h"
#include "Runtime/Engine/Classes/AI/AISystemBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/AvoidanceManager.h"
#include "Runtime/Engine/Classes/Engine/GameInstance.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "Runtime/Engine/Classes/Engine/GameViewportClient.h"
#include "Runtime/Engine/Classes/Engine/ScriptViewportClient.h"
#include "Runtime/Engine/Classes/Engine/Console.h"
#include "Runtime/Engine/Classes/Engine/DebugDisplayProperty.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/Engine/Classes/GameFramework/GameUserSettings.h"
#include "Runtime/Engine/Classes/Engine/AssetManager.h"
#include "Runtime/Engine/Classes/Engine/EngineCustomTimeStep.h"
#include "Runtime/Engine/Classes/Engine/TimecodeProvider.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineSession.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollectionInstance.h"
#include "Runtime/Engine/Classes/Engine/WorldComposition.h"
#include "Runtime/Engine/Classes/Particles/WorldPSCPool.h"
#include "Runtime/Engine/Classes/Engine/ReplicationDriver.h"
#include "Runtime/Engine/Classes/Engine/Channel.h"
#include "Runtime/Engine/Classes/GameFramework/TouchInterface.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Components/Widget.h"
#include "Runtime/UMG/Public/Components/Visual.h"
#include "Runtime/UMG/Public/Components/PanelSlot.h"
#include "Runtime/UMG/Public/Components/PanelWidget.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "Runtime/UMG/Public/Slate/WidgetTransform.h"
#include "Runtime/SlateCore/Public/Layout/Clipping.h"
#include "Runtime/UMG/Public/Blueprint/WidgetNavigation.h"
#include "Runtime/SlateCore/Public/Input/NavigationReply.h"
#include "Runtime/SlateCore/Public/Types/SlateEnums.h"
#include "Runtime/UMG/Public/Binding/PropertyBinding.h"
#include "Runtime/UMG/Public/Binding/DynamicPropertyPath.h"
#include "Runtime/PropertyPath/Public/PropertyPathHelpers.h"
#include "Runtime/SlateCore/Public/Layout/Geometry.h"
#include "Runtime/SlateCore/Public/Input/Events.h"
#include "Runtime/SlateCore/Public/Styling/SlateColor.h"
#include "Runtime/SlateCore/Public/Styling/SlateBrush.h"
#include "Runtime/SlateCore/Public/Layout/Margin.h"
#include "Runtime/SlateCore/Public/Styling/SlateTypes.h"
#include "Runtime/UMG/Public/Animation/UMGSequencePlayer.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimation.h"
#include "Runtime/MovieScene/Public/MovieSceneSequence.h"
#include "Runtime/MovieScene/Public/MovieSceneSignedObject.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackIdentifier.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSegment.h"
#include "Runtime/MovieScene/Public/MovieSceneTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvalTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackImplementation.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationField.h"
#include "Runtime/MovieScene/Public/MovieSceneFrameMigration.h"
#include "Runtime/MovieScene/Public/MovieSceneSequenceID.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationKey.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceHierarchy.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceTransform.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceInstanceData.h"
#include "Runtime/MovieScene/Public/MovieSceneSection.h"
#include "Runtime/MovieScene/Public/MovieScene.h"
#include "Runtime/MovieScene/Public/MovieSceneSpawnable.h"
#include "Runtime/MovieScene/Public/MovieScenePossessable.h"
#include "Runtime/MovieScene/Public/MovieSceneBinding.h"
#include "Runtime/MovieScene/Public/MovieSceneFwd.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimationBinding.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/Slate/Public/Widgets/Layout/Anchors.h"
#include "Runtime/UMG/Public/Blueprint/DragDropOperation.h"
#include "Runtime/UMG/Public/Components/NamedSlotInterface.h"
#include "Runtime/Engine/Classes/Haptics/HapticFeedbackEffect_Base.h"
#include "Runtime/Engine/Classes/Engine/LatentActionManager.h"
#include "Runtime/Engine/Classes/Matinee/MatineeActor.h"
#include "Runtime/Engine/Classes/Matinee/InterpData.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupDirector.h"
#include "Runtime/Engine/Classes/Components/ChildActorComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/NavigationSystem/Public/NavAreas/NavArea_Obstacle.h"
#include "MaluchyGameMode__pf2132744816.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackAttenuation.h"
#include "Runtime/Engine/Classes/Components/ForceFeedbackComponent.h"
#include "Runtime/Engine/Classes/Sound/DialogueWave.h"
#include "Runtime/Engine/Classes/Sound/DialogueTypes.h"
#include "Runtime/Engine/Classes/Sound/DialogueVoice.h"
#include "Runtime/Engine/Classes/Sound/DialogueSoundWaveProxy.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SaveGame.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStaticsTypes.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/Engine/Classes/Engine/CollisionProfile.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Rotatable__pf2962636854.h"
#include "Runtime/Engine/Classes/Kismet/KismetArrayLibrary.h"
#include "DiscoMaluch__pf1851508772.h"
#include "MainCarController__pf1851508772.h"
#include "Coin__pf2962636854.h"
#include "ObsCar__pf931608196.h"
#include "Canister__pf1442588193.h"
#include "Boost__pf2962636854.h"


#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
PRAGMA_DISABLE_OPTIMIZATION
ARoadSegment_C__pf24847026::ARoadSegment_C__pf24847026(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	if(HasAnyFlags(RF_ClassDefaultObject) && (ARoadSegment_C__pf24847026::StaticClass() == GetClass()))
	{
		ARoadSegment_C__pf24847026::__CustomDynamicClassInitialization(CastChecked<UDynamicClass>(GetClass()));
	}
	
	bpv__DefaultSceneRoot__pf = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	bpv__StaticMesh1__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh1"));
	bpv__StaticMesh__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	bpv__Box__pf = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	bpv__Asphalt__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Asphalt"));
	bpv__Line0__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Line0"));
	bpv__Line1__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Line1"));
	bpv__SmolLine0__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SmolLine0"));
	bpv__SmolLine1__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SmolLine1"));
	bpv__Border0__pf = CreateDefaultSubobject<UBoxComponent>(TEXT("Border0"));
	bpv__Border1__pf = CreateDefaultSubobject<UBoxComponent>(TEXT("Border1"));
	bpv__Cube__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cube"));
	bpv__StaticMesh3__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh3"));
	bpv__StaticMesh4__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh4"));
	bpv__StaticMesh5__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh5"));
	bpv__StaticMesh6__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh6"));
	bpv__Cube1__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cube1"));
	bpv__StaticMesh7__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh7"));
	bpv__StaticMesh2__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh2"));
	bpv__low_poly_buildings_Highrise_1__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("low_poly_buildings_Highrise_1"));
	bpv__low_poly_buildings_Highrise_17__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("low_poly_buildings_Highrise_17"));
	bpv__low_poly_buildings_Highrise_9__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("low_poly_buildings_Highrise_9"));
	bpv__low_poly_buildings_Highrise_2__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("low_poly_buildings_Highrise_2"));
	bpv__low_poly_buildings_Highrise_16__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("low_poly_buildings_Highrise_16"));
	bpv__low_poly_buildings_Highrise_11__pf = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("low_poly_buildings_Highrise_11"));
	RootComponent = bpv__DefaultSceneRoot__pf;
	bpv__DefaultSceneRoot__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh1__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh1__pf->AttachToComponent(bpv__DefaultSceneRoot__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__0 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__StaticMesh1__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__0 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	bpv__StaticMesh1__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__StaticMesh1__pf->OverrideMaterials.Reserve(1);
	bpv__StaticMesh1__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed));
	bpv__StaticMesh1__pf->bCastDynamicShadow = false;
	bpv__StaticMesh1__pf->RelativeLocation = FVector(0.000000, 0.000000, -130.000000);
	bpv__StaticMesh1__pf->RelativeScale3D = FVector(45.000000, 40.000000, 0.500000);
	if(!bpv__StaticMesh1__pf->IsTemplate())
	{
		bpv__StaticMesh1__pf->BodyInstance.FixupData(bpv__StaticMesh1__pf);
	}
	bpv__StaticMesh__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh__pf->AttachToComponent(bpv__StaticMesh1__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__1 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__StaticMesh__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__1 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	bpv__StaticMesh__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__StaticMesh__pf->OverrideMaterials.Reserve(1);
	bpv__StaticMesh__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed));
	bpv__StaticMesh__pf->bCastDynamicShadow = false;
	bpv__StaticMesh__pf->RelativeScale3D = FVector(1.000000, 0.270833, 1.111111);
	bpv__StaticMesh__pf->bVisible = false;
	if(!bpv__StaticMesh__pf->IsTemplate())
	{
		bpv__StaticMesh__pf->BodyInstance.FixupData(bpv__StaticMesh__pf);
	}
	bpv__Box__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Box__pf->AttachToComponent(bpv__StaticMesh__pf, FAttachmentTransformRules::KeepRelativeTransform );
	bpv__Box__pf->AreaClass = UNavArea_Obstacle::StaticClass();
	bpv__Box__pf->BodyInstance.SetCollisionProfileName(FName(TEXT("OverlapAll")));
	bpv__Box__pf->RelativeLocation = FVector(0.000000, 0.000000, 3317.340088);
	bpv__Box__pf->RelativeScale3D = FVector(4.000000, 3.600000, 100.000000);
	if(!bpv__Box__pf->IsTemplate())
	{
		bpv__Box__pf->BodyInstance.FixupData(bpv__Box__pf);
	}
	bpv__Asphalt__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Asphalt__pf->AttachToComponent(bpv__StaticMesh__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__2 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__Asphalt__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__2 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	bpv__Asphalt__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__Asphalt__pf->OverrideMaterials.Reserve(1);
	bpv__Asphalt__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[3], ECastCheckedType::NullAllowed));
	bpv__Asphalt__pf->bCastDynamicShadow = false;
	if(!bpv__Asphalt__pf->IsTemplate())
	{
		bpv__Asphalt__pf->BodyInstance.FixupData(bpv__Asphalt__pf);
	}
	bpv__Line0__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Line0__pf->AttachToComponent(bpv__Asphalt__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__3 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__Line0__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__3 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	bpv__Line0__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__Line0__pf->OverrideMaterials.Reserve(1);
	bpv__Line0__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[4], ECastCheckedType::NullAllowed));
	bpv__Line0__pf->bCastDynamicShadow = false;
	bpv__Line0__pf->RelativeLocation = FVector(0.000000, -110.000000, 1.000000);
	bpv__Line0__pf->RelativeScale3D = FVector(1.000000, 0.050000, 1.000000);
	if(!bpv__Line0__pf->IsTemplate())
	{
		bpv__Line0__pf->BodyInstance.FixupData(bpv__Line0__pf);
	}
	bpv__Line1__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Line1__pf->AttachToComponent(bpv__Asphalt__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__4 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__Line1__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__4 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	bpv__Line1__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__Line1__pf->OverrideMaterials.Reserve(1);
	bpv__Line1__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[4], ECastCheckedType::NullAllowed));
	bpv__Line1__pf->bCastDynamicShadow = false;
	bpv__Line1__pf->RelativeLocation = FVector(0.000000, 110.000000, 1.000000);
	bpv__Line1__pf->RelativeScale3D = FVector(1.000000, 0.050000, 1.000000);
	if(!bpv__Line1__pf->IsTemplate())
	{
		bpv__Line1__pf->BodyInstance.FixupData(bpv__Line1__pf);
	}
	bpv__SmolLine0__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__SmolLine0__pf->AttachToComponent(bpv__Asphalt__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__5 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__SmolLine0__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__5 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	bpv__SmolLine0__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__SmolLine0__pf->OverrideMaterials.Reserve(1);
	bpv__SmolLine0__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[4], ECastCheckedType::NullAllowed));
	bpv__SmolLine0__pf->bCastDynamicShadow = false;
	bpv__SmolLine0__pf->RelativeLocation = FVector(-63.000000, 0.000000, 1.000000);
	bpv__SmolLine0__pf->RelativeScale3D = FVector(0.100000, -0.015000, 1.000000);
	if(!bpv__SmolLine0__pf->IsTemplate())
	{
		bpv__SmolLine0__pf->BodyInstance.FixupData(bpv__SmolLine0__pf);
	}
	bpv__SmolLine1__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__SmolLine1__pf->AttachToComponent(bpv__Asphalt__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__6 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__SmolLine1__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__6 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	bpv__SmolLine1__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__SmolLine1__pf->OverrideMaterials.Reserve(1);
	bpv__SmolLine1__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[4], ECastCheckedType::NullAllowed));
	bpv__SmolLine1__pf->bCastDynamicShadow = false;
	bpv__SmolLine1__pf->RelativeLocation = FVector(63.000000, 0.000000, 1.000000);
	bpv__SmolLine1__pf->RelativeScale3D = FVector(0.100000, 0.015000, 1.000000);
	if(!bpv__SmolLine1__pf->IsTemplate())
	{
		bpv__SmolLine1__pf->BodyInstance.FixupData(bpv__SmolLine1__pf);
	}
	bpv__Border0__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Border0__pf->AttachToComponent(bpv__StaticMesh__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__7 = (*(AccessPrivateProperty<FVector >((bpv__Border0__pf), UBoxComponent::__PPO__BoxExtent() )));
	__Local__7 = FVector(32.299999, 32.000000, 32.000000);
	bpv__Border0__pf->AreaClass = UNavArea_Obstacle::StaticClass();
	bpv__Border0__pf->BodyInstance.SetCollisionProfileName(FName(TEXT("BlockAll")));
	bpv__Border0__pf->RelativeLocation = FVector(0.000000, -256.000000, 3360.000000);
	bpv__Border0__pf->RelativeScale3D = FVector(4.000000, 4.000000, 100.000000);
	if(!bpv__Border0__pf->IsTemplate())
	{
		bpv__Border0__pf->BodyInstance.FixupData(bpv__Border0__pf);
	}
	bpv__Border1__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Border1__pf->AttachToComponent(bpv__StaticMesh__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__8 = (*(AccessPrivateProperty<FVector >((bpv__Border1__pf), UBoxComponent::__PPO__BoxExtent() )));
	__Local__8 = FVector(32.299999, 32.000000, 32.000000);
	bpv__Border1__pf->AreaClass = UNavArea_Obstacle::StaticClass();
	bpv__Border1__pf->BodyInstance.SetCollisionProfileName(FName(TEXT("BlockAll")));
	bpv__Border1__pf->RelativeLocation = FVector(0.000000, 256.000000, 3316.800293);
	bpv__Border1__pf->RelativeScale3D = FVector(4.000000, 4.000000, 100.000000);
	if(!bpv__Border1__pf->IsTemplate())
	{
		bpv__Border1__pf->BodyInstance.FixupData(bpv__Border1__pf);
	}
	bpv__Cube__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Cube__pf->AttachToComponent(bpv__StaticMesh1__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__9 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__Cube__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__9 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	bpv__Cube__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__Cube__pf->OverrideMaterials.Reserve(1);
	bpv__Cube__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed));
	bpv__Cube__pf->bCastDynamicShadow = false;
	bpv__Cube__pf->RelativeLocation = FVector(0.000000, -36.166664, 330.000000);
	bpv__Cube__pf->RelativeScale3D = FVector(0.011111, 0.016667, 4.000000);
	if(!bpv__Cube__pf->IsTemplate())
	{
		bpv__Cube__pf->BodyInstance.FixupData(bpv__Cube__pf);
	}
	bpv__StaticMesh3__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh3__pf->AttachToComponent(bpv__Cube__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__10 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__StaticMesh3__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__10 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	bpv__StaticMesh3__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__StaticMesh3__pf->OverrideMaterials.Reserve(1);
	bpv__StaticMesh3__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed));
	bpv__StaticMesh3__pf->bCastDynamicShadow = false;
	bpv__StaticMesh3__pf->RelativeLocation = FVector(-5760.000000, 0.000000, 0.000000);
	if(!bpv__StaticMesh3__pf->IsTemplate())
	{
		bpv__StaticMesh3__pf->BodyInstance.FixupData(bpv__StaticMesh3__pf);
	}
	bpv__StaticMesh4__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh4__pf->AttachToComponent(bpv__Cube__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__11 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__StaticMesh4__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__11 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	bpv__StaticMesh4__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__StaticMesh4__pf->OverrideMaterials.Reserve(1);
	bpv__StaticMesh4__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed));
	bpv__StaticMesh4__pf->bCastDynamicShadow = false;
	bpv__StaticMesh4__pf->RelativeLocation = FVector(-5760.000000, 4340.000000, 0.000000);
	if(!bpv__StaticMesh4__pf->IsTemplate())
	{
		bpv__StaticMesh4__pf->BodyInstance.FixupData(bpv__StaticMesh4__pf);
	}
	bpv__StaticMesh5__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh5__pf->AttachToComponent(bpv__Cube__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__12 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__StaticMesh5__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__12 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	bpv__StaticMesh5__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__StaticMesh5__pf->OverrideMaterials.Reserve(1);
	bpv__StaticMesh5__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed));
	bpv__StaticMesh5__pf->bCastDynamicShadow = false;
	bpv__StaticMesh5__pf->RelativeLocation = FVector(5760.000000, 4340.000000, 0.000000);
	if(!bpv__StaticMesh5__pf->IsTemplate())
	{
		bpv__StaticMesh5__pf->BodyInstance.FixupData(bpv__StaticMesh5__pf);
	}
	bpv__StaticMesh6__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh6__pf->AttachToComponent(bpv__Cube__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__13 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__StaticMesh6__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__13 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	bpv__StaticMesh6__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__StaticMesh6__pf->OverrideMaterials.Reserve(1);
	bpv__StaticMesh6__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed));
	bpv__StaticMesh6__pf->bCastDynamicShadow = false;
	bpv__StaticMesh6__pf->RelativeLocation = FVector(0.000000, 4340.000000, 0.000000);
	if(!bpv__StaticMesh6__pf->IsTemplate())
	{
		bpv__StaticMesh6__pf->BodyInstance.FixupData(bpv__StaticMesh6__pf);
	}
	bpv__Cube1__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__Cube1__pf->AttachToComponent(bpv__Cube__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__14 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__Cube1__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__14 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	bpv__Cube1__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__Cube1__pf->OverrideMaterials.Reserve(1);
	bpv__Cube1__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed));
	bpv__Cube1__pf->bCastDynamicShadow = false;
	bpv__Cube1__pf->RelativeLocation = FVector(0.000000, 40.000000, 0.000000);
	bpv__Cube1__pf->RelativeScale3D = FVector(230.399994, 1.000000, 0.250000);
	if(!bpv__Cube1__pf->IsTemplate())
	{
		bpv__Cube1__pf->BodyInstance.FixupData(bpv__Cube1__pf);
	}
	bpv__StaticMesh7__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh7__pf->AttachToComponent(bpv__Cube__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__15 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__StaticMesh7__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__15 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	bpv__StaticMesh7__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__StaticMesh7__pf->OverrideMaterials.Reserve(1);
	bpv__StaticMesh7__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed));
	bpv__StaticMesh7__pf->bCastDynamicShadow = false;
	bpv__StaticMesh7__pf->RelativeLocation = FVector(0.000000, 4300.000000, 0.000000);
	bpv__StaticMesh7__pf->RelativeScale3D = FVector(230.399994, 1.000000, 0.250000);
	if(!bpv__StaticMesh7__pf->IsTemplate())
	{
		bpv__StaticMesh7__pf->BodyInstance.FixupData(bpv__StaticMesh7__pf);
	}
	bpv__StaticMesh2__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__StaticMesh2__pf->AttachToComponent(bpv__Cube__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__16 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__StaticMesh2__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__16 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	bpv__StaticMesh2__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__StaticMesh2__pf->OverrideMaterials.Reserve(1);
	bpv__StaticMesh2__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed));
	bpv__StaticMesh2__pf->bCastDynamicShadow = false;
	bpv__StaticMesh2__pf->RelativeLocation = FVector(5760.000000, 0.000000, 0.000000);
	if(!bpv__StaticMesh2__pf->IsTemplate())
	{
		bpv__StaticMesh2__pf->BodyInstance.FixupData(bpv__StaticMesh2__pf);
	}
	bpv__low_poly_buildings_Highrise_1__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__low_poly_buildings_Highrise_1__pf->AttachToComponent(bpv__StaticMesh1__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__17 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__low_poly_buildings_Highrise_1__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__17 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[7], ECastCheckedType::NullAllowed);
	bpv__low_poly_buildings_Highrise_1__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__low_poly_buildings_Highrise_1__pf->OverrideMaterials.Reserve(1);
	bpv__low_poly_buildings_Highrise_1__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[8], ECastCheckedType::NullAllowed));
	bpv__low_poly_buildings_Highrise_1__pf->bCastDynamicShadow = false;
	bpv__low_poly_buildings_Highrise_1__pf->RelativeLocation = FVector(-174.222229, -44.999996, 112.000000);
	bpv__low_poly_buildings_Highrise_1__pf->RelativeScale3D = FVector(0.222222, 0.333333, 10.000000);
	if(!bpv__low_poly_buildings_Highrise_1__pf->IsTemplate())
	{
		bpv__low_poly_buildings_Highrise_1__pf->BodyInstance.FixupData(bpv__low_poly_buildings_Highrise_1__pf);
	}
	bpv__low_poly_buildings_Highrise_17__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__low_poly_buildings_Highrise_17__pf->AttachToComponent(bpv__low_poly_buildings_Highrise_1__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__18 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__low_poly_buildings_Highrise_17__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__18 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[9], ECastCheckedType::NullAllowed);
	bpv__low_poly_buildings_Highrise_17__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__low_poly_buildings_Highrise_17__pf->OverrideMaterials.Reserve(1);
	bpv__low_poly_buildings_Highrise_17__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[8], ECastCheckedType::NullAllowed));
	bpv__low_poly_buildings_Highrise_17__pf->bCastDynamicShadow = false;
	bpv__low_poly_buildings_Highrise_17__pf->RelativeLocation = FVector(1111.000000, 465.500000, 0.000000);
	if(!bpv__low_poly_buildings_Highrise_17__pf->IsTemplate())
	{
		bpv__low_poly_buildings_Highrise_17__pf->BodyInstance.FixupData(bpv__low_poly_buildings_Highrise_17__pf);
	}
	bpv__low_poly_buildings_Highrise_9__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__low_poly_buildings_Highrise_9__pf->AttachToComponent(bpv__low_poly_buildings_Highrise_1__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__19 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__low_poly_buildings_Highrise_9__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__19 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[10], ECastCheckedType::NullAllowed);
	bpv__low_poly_buildings_Highrise_9__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__low_poly_buildings_Highrise_9__pf->OverrideMaterials.Reserve(1);
	bpv__low_poly_buildings_Highrise_9__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[8], ECastCheckedType::NullAllowed));
	bpv__low_poly_buildings_Highrise_9__pf->bCastDynamicShadow = false;
	bpv__low_poly_buildings_Highrise_9__pf->RelativeLocation = FVector(-755.500000, 465.500000, 0.000000);
	if(!bpv__low_poly_buildings_Highrise_9__pf->IsTemplate())
	{
		bpv__low_poly_buildings_Highrise_9__pf->BodyInstance.FixupData(bpv__low_poly_buildings_Highrise_9__pf);
	}
	bpv__low_poly_buildings_Highrise_2__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__low_poly_buildings_Highrise_2__pf->AttachToComponent(bpv__low_poly_buildings_Highrise_1__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__20 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__low_poly_buildings_Highrise_2__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__20 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[11], ECastCheckedType::NullAllowed);
	bpv__low_poly_buildings_Highrise_2__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__low_poly_buildings_Highrise_2__pf->OverrideMaterials.Reserve(1);
	bpv__low_poly_buildings_Highrise_2__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[8], ECastCheckedType::NullAllowed));
	bpv__low_poly_buildings_Highrise_2__pf->bCastDynamicShadow = false;
	bpv__low_poly_buildings_Highrise_2__pf->RelativeLocation = FVector(-83.500000, 0.000000, 0.000000);
	if(!bpv__low_poly_buildings_Highrise_2__pf->IsTemplate())
	{
		bpv__low_poly_buildings_Highrise_2__pf->BodyInstance.FixupData(bpv__low_poly_buildings_Highrise_2__pf);
	}
	bpv__low_poly_buildings_Highrise_16__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__low_poly_buildings_Highrise_16__pf->AttachToComponent(bpv__low_poly_buildings_Highrise_1__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__21 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__low_poly_buildings_Highrise_16__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__21 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[12], ECastCheckedType::NullAllowed);
	bpv__low_poly_buildings_Highrise_16__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__low_poly_buildings_Highrise_16__pf->OverrideMaterials.Reserve(1);
	bpv__low_poly_buildings_Highrise_16__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[8], ECastCheckedType::NullAllowed));
	bpv__low_poly_buildings_Highrise_16__pf->bCastDynamicShadow = false;
	bpv__low_poly_buildings_Highrise_16__pf->RelativeLocation = FVector(2012.000000, 451.000000, 0.000000);
	if(!bpv__low_poly_buildings_Highrise_16__pf->IsTemplate())
	{
		bpv__low_poly_buildings_Highrise_16__pf->BodyInstance.FixupData(bpv__low_poly_buildings_Highrise_16__pf);
	}
	bpv__low_poly_buildings_Highrise_11__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__low_poly_buildings_Highrise_11__pf->AttachToComponent(bpv__low_poly_buildings_Highrise_1__pf, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__22 = (*(AccessPrivateProperty<UStaticMesh* >((bpv__low_poly_buildings_Highrise_11__pf), UStaticMeshComponent::__PPO__StaticMesh() )));
	__Local__22 = CastChecked<UStaticMesh>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[13], ECastCheckedType::NullAllowed);
	bpv__low_poly_buildings_Highrise_11__pf->OverrideMaterials = TArray<UMaterialInterface*> ();
	bpv__low_poly_buildings_Highrise_11__pf->OverrideMaterials.Reserve(1);
	bpv__low_poly_buildings_Highrise_11__pf->OverrideMaterials.Add(CastChecked<UMaterialInterface>(CastChecked<UDynamicClass>(ARoadSegment_C__pf24847026::StaticClass())->UsedAssets[8], ECastCheckedType::NullAllowed));
	bpv__low_poly_buildings_Highrise_11__pf->bCastDynamicShadow = false;
	bpv__low_poly_buildings_Highrise_11__pf->RelativeLocation = FVector(-968.000000, 0.000000, 0.000000);
	if(!bpv__low_poly_buildings_Highrise_11__pf->IsTemplate())
	{
		bpv__low_poly_buildings_Highrise_11__pf->BodyInstance.FixupData(bpv__low_poly_buildings_Highrise_11__pf);
	}
	bpv__IsStart__pf = false;
	bpv__Next__pf = nullptr;
	bpv__Previous__pf = nullptr;
	bpv__CurrObjects__pf = TArray<AActor*> ();
	bpv__ObjectsToRandomize__pf = TArray<UClass*> ();
	bpv__ObjectsToRandomize__pf.Reserve(3);
	bpv__ObjectsToRandomize__pf.Add(AObsCar_C__pf931608196::StaticClass());
	bpv__ObjectsToRandomize__pf.Add(ACoin_C__pf2962636854::StaticClass());
	bpv__ObjectsToRandomize__pf.Add(ACanister_C__pf1442588193::StaticClass());
	bpv__MinDistanceFromAnother__pf = 500.000000f;
	bpv__ObjectsPerSegment__pf = 10;
	bpv__NextSegments__pf = 3;
	PrimaryActorTick.bCanEverTick = true;
}
PRAGMA_ENABLE_OPTIMIZATION
void ARoadSegment_C__pf24847026::PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph)
{
	Super::PostLoadSubobjects(OuterInstanceGraph);
	if(bpv__DefaultSceneRoot__pf)
	{
		bpv__DefaultSceneRoot__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__StaticMesh1__pf)
	{
		bpv__StaticMesh1__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__StaticMesh__pf)
	{
		bpv__StaticMesh__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Box__pf)
	{
		bpv__Box__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Asphalt__pf)
	{
		bpv__Asphalt__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Line0__pf)
	{
		bpv__Line0__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Line1__pf)
	{
		bpv__Line1__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__SmolLine0__pf)
	{
		bpv__SmolLine0__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__SmolLine1__pf)
	{
		bpv__SmolLine1__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Border0__pf)
	{
		bpv__Border0__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Border1__pf)
	{
		bpv__Border1__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Cube__pf)
	{
		bpv__Cube__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__StaticMesh3__pf)
	{
		bpv__StaticMesh3__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__StaticMesh4__pf)
	{
		bpv__StaticMesh4__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__StaticMesh5__pf)
	{
		bpv__StaticMesh5__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__StaticMesh6__pf)
	{
		bpv__StaticMesh6__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__Cube1__pf)
	{
		bpv__Cube1__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__StaticMesh7__pf)
	{
		bpv__StaticMesh7__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__StaticMesh2__pf)
	{
		bpv__StaticMesh2__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__low_poly_buildings_Highrise_1__pf)
	{
		bpv__low_poly_buildings_Highrise_1__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__low_poly_buildings_Highrise_17__pf)
	{
		bpv__low_poly_buildings_Highrise_17__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__low_poly_buildings_Highrise_9__pf)
	{
		bpv__low_poly_buildings_Highrise_9__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__low_poly_buildings_Highrise_2__pf)
	{
		bpv__low_poly_buildings_Highrise_2__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__low_poly_buildings_Highrise_16__pf)
	{
		bpv__low_poly_buildings_Highrise_16__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__low_poly_buildings_Highrise_11__pf)
	{
		bpv__low_poly_buildings_Highrise_11__pf->CreationMethod = EComponentCreationMethod::Native;
	}
}
PRAGMA_DISABLE_OPTIMIZATION
void ARoadSegment_C__pf24847026::__CustomDynamicClassInitialization(UDynamicClass* InDynamicClass)
{
	ensure(0 == InDynamicClass->ReferencedConvertedFields.Num());
	ensure(0 == InDynamicClass->MiscConvertedSubobjects.Num());
	ensure(0 == InDynamicClass->DynamicBindingObjects.Num());
	ensure(0 == InDynamicClass->ComponentTemplates.Num());
	ensure(0 == InDynamicClass->Timelines.Num());
	ensure(nullptr == InDynamicClass->AnimClassImplementation);
	InDynamicClass->AssembleReferenceTokenStream();
	// List of all referenced converted classes
	InDynamicClass->ReferencedConvertedFields.Add(AMaluchyGameMode_C__pf2132744816::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(ACoin_C__pf2962636854::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(AObsCar_C__pf931608196::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(ACanister_C__pf1442588193::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(ABoost_C__pf2962636854::StaticClass());
	FConvertedBlueprintsDependencies::FillUsedAssetsInDynamicClass(InDynamicClass, &__StaticDependencies_DirectlyUsedAssets);
}
PRAGMA_ENABLE_OPTIMIZATION
void ARoadSegment_C__pf24847026::bpf__ExecuteUbergraph_RoadSegment__pf_0(int32 bpp__EntryPoint__pf)
{
	bool bpfv__CallFunc_IsValid_ReturnValue__pf{};
	bool bpfv__CallFunc_BooleanAND_ReturnValue__pf{};
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	bool bpfv__CallFunc_Not_PreBool_ReturnValue__pf{};
	bool bpfv__CallFunc_BooleanAND_ReturnValue1__pf{};
	check(bpp__EntryPoint__pf == 11);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
	b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
	b0l__K2Node_DynamicCast_bSuccess__pf = (b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
	if (!b0l__K2Node_DynamicCast_bSuccess__pf)
	{
		return; //KCST_GotoReturnIfNot
	}
	bpf__IsActorMainCar__pf(b0l__K2Node_Event_OtherActor__pf, /*out*/ b0l__CallFunc_IsActorMainCar_Is1__pf);
	bpfv__CallFunc_IsValid_ReturnValue__pf = UKismetSystemLibrary::IsValid(bpv__Previous__pf);
	bpfv__CallFunc_BooleanAND_ReturnValue__pf = UKismetMathLibrary::BooleanAND(bpfv__CallFunc_IsValid_ReturnValue__pf, b0l__CallFunc_IsActorMainCar_Is1__pf);
	bool  __Local__23 = false;
	bpfv__CallFunc_Not_PreBool_ReturnValue__pf = UKismetMathLibrary::Not_PreBool(((::IsValid(b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf)) ? (b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__IsResetting__pf) : (__Local__23)));
	bpfv__CallFunc_BooleanAND_ReturnValue1__pf = UKismetMathLibrary::BooleanAND(bpfv__CallFunc_BooleanAND_ReturnValue__pf, bpfv__CallFunc_Not_PreBool_ReturnValue__pf);
	if (!bpfv__CallFunc_BooleanAND_ReturnValue1__pf)
	{
		return; //KCST_GotoReturnIfNot
	}
	if(::IsValid(bpv__Previous__pf))
	{
		bpv__Previous__pf->bpf__DestroyRoadSegment__pf();
	}
	return; // KCST_GotoReturn
}
void ARoadSegment_C__pf24847026::bpf__ExecuteUbergraph_RoadSegment__pf_1(int32 bpp__EntryPoint__pf)
{
	int32 __CurrentState = bpp__EntryPoint__pf;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpf__PutAnotherSegment__pf();
				__CurrentState = -1;
				break;
			}
		case 2:
			{
				__CurrentState = 3;
				break;
			}
		case 3:
			{
				if (!bpv__IsStart__pf)
				{
					__CurrentState = 4;
					break;
				}
				__CurrentState = 1;
				break;
			}
		case 4:
			{
				bpf__AddToResetables__pf();
			}
		case 5:
			{
				bpf__RandomizeObjects__pf();
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__ExecuteUbergraph_RoadSegment__pf_2(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 7);
	// optimized KCST_UnconditionalGoto
	bpf__IsActorMainCar__pf(b0l__K2Node_Event_OtherActor1__pf, /*out*/ b0l__CallFunc_IsActorMainCar_Is__pf);
	if (!b0l__CallFunc_IsActorMainCar_Is__pf)
	{
		return; //KCST_GotoReturnIfNot
	}
	bpf__PutAnotherSegment__pf();
	return; // KCST_GotoReturn
}
void ARoadSegment_C__pf24847026::bpf__ExecuteUbergraph_RoadSegment__pf_3(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 6);
	return; // KCST_GotoReturn
}
void ARoadSegment_C__pf24847026::bpf__ReceiveActorEndOverlap__pf(AActor* bpp__OtherActor__pf)
{
	b0l__K2Node_Event_OtherActor__pf = bpp__OtherActor__pf;
	bpf__ExecuteUbergraph_RoadSegment__pf_0(11);
}
void ARoadSegment_C__pf24847026::bpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf)
{
	b0l__K2Node_Event_OtherActor1__pf = bpp__OtherActor__pf;
	bpf__ExecuteUbergraph_RoadSegment__pf_2(7);
}
void ARoadSegment_C__pf24847026::bpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
{
	b0l__K2Node_Event_DeltaSeconds__pf = bpp__DeltaSeconds__pf;
	bpf__ExecuteUbergraph_RoadSegment__pf_3(6);
}
void ARoadSegment_C__pf24847026::bpf__ReceiveBeginPlay__pf()
{
	bpf__ExecuteUbergraph_RoadSegment__pf_1(2);
}
void ARoadSegment_C__pf24847026::bpf__UserConstructionScript__pf()
{
}
void ARoadSegment_C__pf24847026::bpf__MoveSegment__pf()
{
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	FTransform bpfv__CallFunc_GetTransform_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	FVector bpfv__CallFunc_BreakTransform_Location__pf(EForceInit::ForceInit);
	FRotator bpfv__CallFunc_BreakTransform_Rotation__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_BreakTransform_Scale__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_MakeVector_ReturnValue__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_Add_VectorVector_ReturnValue__pf(EForceInit::ForceInit);
	FHitResult bpfv__CallFunc_K2_SetActorLocation_SweepHitResult__pf{};
	bool bpfv__CallFunc_K2_SetActorLocation_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				bpfv__CallFunc_GetTransform_ReturnValue__pf = AActor::GetTransform();
				UKismetMathLibrary::BreakTransform(bpfv__CallFunc_GetTransform_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakTransform_Location__pf, /*out*/ bpfv__CallFunc_BreakTransform_Rotation__pf, /*out*/ bpfv__CallFunc_BreakTransform_Scale__pf);
				float  __Local__24 = 0.000000;
				bpfv__CallFunc_MakeVector_ReturnValue__pf = UKismetMathLibrary::MakeVector(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__RoadSegmentsSpeed__pf) : (__Local__24)), 0.000000, 0.000000);
				bpfv__CallFunc_Add_VectorVector_ReturnValue__pf = UKismetMathLibrary::Add_VectorVector(bpfv__CallFunc_BreakTransform_Location__pf, bpfv__CallFunc_MakeVector_ReturnValue__pf);
				bpfv__CallFunc_K2_SetActorLocation_ReturnValue__pf = AActor::K2_SetActorLocation(bpfv__CallFunc_Add_VectorVector_ReturnValue__pf, false, /*out*/ bpfv__CallFunc_K2_SetActorLocation_SweepHitResult__pf, false);
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__GetOriginForNext__pf(/*out*/ FVector& bpp__Origin__pf)
{
	FVector bpfv__CallFunc_K2_GetComponentLocation_ReturnValue__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_GetComponentBounds_Origin__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_GetComponentBounds_BoxExtent__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_GetComponentBounds_SphereRadius__pf{};
	float bpfv__CallFunc_BreakVector_X__pf{};
	float bpfv__CallFunc_BreakVector_Y__pf{};
	float bpfv__CallFunc_BreakVector_Z__pf{};
	float bpfv__CallFunc_BreakVector_X1__pf{};
	float bpfv__CallFunc_BreakVector_Y1__pf{};
	float bpfv__CallFunc_BreakVector_Z1__pf{};
	float bpfv__CallFunc_BreakVector_X2__pf{};
	float bpfv__CallFunc_BreakVector_Y2__pf{};
	float bpfv__CallFunc_BreakVector_Z2__pf{};
	float bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf{};
	float bpfv__CallFunc_BreakVector_X3__pf{};
	float bpfv__CallFunc_BreakVector_Y3__pf{};
	float bpfv__CallFunc_BreakVector_Z3__pf{};
	float bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_MakeVector_ReturnValue__pf(EForceInit::ForceInit);
	if(::IsValid(bpv__DefaultSceneRoot__pf))
	{
		bpfv__CallFunc_K2_GetComponentLocation_ReturnValue__pf = bpv__DefaultSceneRoot__pf->USceneComponent::K2_GetComponentLocation();
	}
	UKismetSystemLibrary::GetComponentBounds(bpv__StaticMesh1__pf, /*out*/ bpfv__CallFunc_GetComponentBounds_Origin__pf, /*out*/ bpfv__CallFunc_GetComponentBounds_BoxExtent__pf, /*out*/ bpfv__CallFunc_GetComponentBounds_SphereRadius__pf);
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetComponentLocation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_GetComponentBounds_BoxExtent__pf, /*out*/ bpfv__CallFunc_BreakVector_X1__pf, /*out*/ bpfv__CallFunc_BreakVector_Y1__pf, /*out*/ bpfv__CallFunc_BreakVector_Z1__pf);
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_GetComponentBounds_Origin__pf, /*out*/ bpfv__CallFunc_BreakVector_X2__pf, /*out*/ bpfv__CallFunc_BreakVector_Y2__pf, /*out*/ bpfv__CallFunc_BreakVector_Z2__pf);
	bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_IntFloat(2, bpfv__CallFunc_BreakVector_X1__pf);
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_GetComponentBounds_Origin__pf, /*out*/ bpfv__CallFunc_BreakVector_X3__pf, /*out*/ bpfv__CallFunc_BreakVector_Y3__pf, /*out*/ bpfv__CallFunc_BreakVector_Z3__pf);
	bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Add_FloatFloat(bpfv__CallFunc_BreakVector_X3__pf, bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf);
	bpfv__CallFunc_MakeVector_ReturnValue__pf = UKismetMathLibrary::MakeVector(bpfv__CallFunc_Add_FloatFloat_ReturnValue__pf, bpfv__CallFunc_BreakVector_Y2__pf, bpfv__CallFunc_BreakVector_Z__pf);
	bpp__Origin__pf = bpfv__CallFunc_MakeVector_ReturnValue__pf;
}
void ARoadSegment_C__pf24847026::bpf__PutAnotherSegment__pf()
{
	ARoadSegment_C__pf24847026* bpfv__RoadSegment__pf{};
	FVector bpfv__CallFunc_GetOriginForNext_Origin__pf(EForceInit::ForceInit);
	FTransform bpfv__CallFunc_MakeTransform_ReturnValue__pf{};
	AActor* bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf{};
	bool bpfv__CallFunc_IsValid_ReturnValue__pf{};
	ARoadSegment_C__pf24847026* bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf{};
	int32 bpfv__Temp_int_Variable__pf{};
	bool bpfv__CallFunc_LessEqual_IntInt_ReturnValue__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue__pf{};
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	bool bpfv__CallFunc_Not_PreBool_ReturnValue__pf{};
	TArray< int32, TInlineAllocator<8> > __StateStack;

	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 2:
			{
				bool  __Local__25 = false;
				bpfv__CallFunc_Not_PreBool_ReturnValue__pf = UKismetMathLibrary::Not_PreBool(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__IsResetting__pf) : (__Local__25)));
				if (!bpfv__CallFunc_Not_PreBool_ReturnValue__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 3:
			{
				bpfv__RoadSegment__pf = this;
			}
		case 4:
			{
				bpfv__Temp_int_Variable__pf = 0;
			}
		case 5:
			{
				bpfv__CallFunc_LessEqual_IntInt_ReturnValue__pf = UKismetMathLibrary::LessEqual_IntInt(bpfv__Temp_int_Variable__pf, bpv__NextSegments__pf);
				if (!bpfv__CallFunc_LessEqual_IntInt_ReturnValue__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 6:
			{
				__StateStack.Push(9);
			}
		case 7:
			{
				ARoadSegment_C__pf24847026*  __Local__26 = ((ARoadSegment_C__pf24847026*)nullptr);
				bpfv__CallFunc_IsValid_ReturnValue__pf = UKismetSystemLibrary::IsValid(((::IsValid(bpfv__RoadSegment__pf)) ? (bpfv__RoadSegment__pf->bpv__Next__pf) : (__Local__26)));
				if (!bpfv__CallFunc_IsValid_ReturnValue__pf)
				{
					__CurrentState = 10;
					break;
				}
			}
		case 8:
			{
				ARoadSegment_C__pf24847026*  __Local__27 = ((ARoadSegment_C__pf24847026*)nullptr);
				bpfv__RoadSegment__pf = ((::IsValid(bpfv__RoadSegment__pf)) ? (bpfv__RoadSegment__pf->bpv__Next__pf) : (__Local__27));
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 9:
			{
				bpfv__CallFunc_Add_IntInt_ReturnValue__pf = UKismetMathLibrary::Add_IntInt(bpfv__Temp_int_Variable__pf, 1);
				bpfv__Temp_int_Variable__pf = bpfv__CallFunc_Add_IntInt_ReturnValue__pf;
				__CurrentState = 5;
				break;
			}
		case 10:
			{
				if(::IsValid(bpfv__RoadSegment__pf))
				{
					bpfv__RoadSegment__pf->bpf__GetOriginForNext__pf(/*out*/ bpfv__CallFunc_GetOriginForNext_Origin__pf);
				}
			}
		case 11:
			{
				bpfv__CallFunc_MakeTransform_ReturnValue__pf = UKismetMathLibrary::MakeTransform(bpfv__CallFunc_GetOriginForNext_Origin__pf, FRotator(0.000000,0.000000,0.000000), FVector(1.000000,1.000000,1.000000));
				bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf = UGameplayStatics::BeginDeferredActorSpawnFromClass(this, GetClass(), bpfv__CallFunc_MakeTransform_ReturnValue__pf, ESpawnActorCollisionHandlingMethod::Undefined, ((AActor*)nullptr));
			}
		case 12:
			{
				bpfv__CallFunc_MakeTransform_ReturnValue__pf = UKismetMathLibrary::MakeTransform(bpfv__CallFunc_GetOriginForNext_Origin__pf, FRotator(0.000000,0.000000,0.000000), FVector(1.000000,1.000000,1.000000));
				bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf = CastChecked<ARoadSegment_C__pf24847026>(UGameplayStatics::FinishSpawningActor(bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf, bpfv__CallFunc_MakeTransform_ReturnValue__pf), ECastCheckedType::NullAllowed);
			}
		case 13:
			{
				if(::IsValid(bpfv__RoadSegment__pf))
				{
					bpfv__RoadSegment__pf->bpv__Next__pf = bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf;
				}
			}
		case 14:
			{
				if(::IsValid(bpfv__RoadSegment__pf) && ::IsValid(bpfv__RoadSegment__pf->bpv__Next__pf))
				{
					bpfv__RoadSegment__pf->bpv__Next__pf->bpv__Previous__pf = bpfv__RoadSegment__pf;
				}
				__CurrentState = 8;
				break;
			}
		default:
			check(false); // Invalid state
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__IsActorMainCar__pf(AActor* bpp__Actor__pf, /*out*/ bool& bpp__Is__pf)
{
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	bool bpfv__CallFunc_EqualEqual_ObjectObject_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				ADiscoMaluch_C__pf1851508772*  __Local__28 = ((ADiscoMaluch_C__pf1851508772*)nullptr);
				bpfv__CallFunc_EqualEqual_ObjectObject_ReturnValue__pf = UKismetMathLibrary::EqualEqual_ObjectObject(bpp__Actor__pf, ((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Car__pf) : (__Local__28)));
				bpp__Is__pf = bpfv__CallFunc_EqualEqual_ObjectObject_ReturnValue__pf;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__RandomizeObjects__pf()
{
	int32 bpfv__RandomInt__pf{};
	bool bpfv__CanxSpawnxHere__pfTT{};
	float bpfv__CallFunc_RandomFloatInRange_ReturnValue__pf{};
	FRotator bpfv__CallFunc_MakeRotator_ReturnValue__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_GetSpawnableLocation_Location__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_RandomFloatInRange_ReturnValue1__pf{};
	float bpfv__CallFunc_RandomFloatInRange_ReturnValue2__pf{};
	FRotator bpfv__CallFunc_MakeRotator_ReturnValue1__pf(EForceInit::ForceInit);
	FRotator bpfv__CallFunc_MakeRotator_ReturnValue2__pf(EForceInit::ForceInit);
	FTransform bpfv__CallFunc_MakeTransform_ReturnValue__pf{};
	FVector bpfv__CallFunc_GetSpawnableLocation_Location1__pf(EForceInit::ForceInit);
	FTransform bpfv__CallFunc_MakeTransform_ReturnValue1__pf{};
	int32 bpfv__Temp_int_Variable__pf{};
	bool bpfv__CallFunc_LessEqual_IntInt_ReturnValue__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue__pf{};
	FVector bpfv__CallFunc_GetSpawnableLocation_Location2__pf(EForceInit::ForceInit);
	FTransform bpfv__CallFunc_MakeTransform_ReturnValue2__pf{};
	int32 bpfv__CallFunc_RandomInteger_ReturnValue__pf{};
	int32 bpfv__Temp_int_Variable1__pf{};
	int32 bpfv__Temp_int_Variable2__pf{};
	float bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_MakeVector_ReturnValue__pf(EForceInit::ForceInit);
	bool bpfv__CallFunc_LessEqual_IntInt_ReturnValue1__pf{};
	FVector bpfv__CallFunc_Add_VectorVector_ReturnValue__pf(EForceInit::ForceInit);
	FTransform bpfv__CallFunc_MakeTransform_ReturnValue3__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue1__pf{};
	bool bpfv__CallFunc_LessEqual_IntInt_ReturnValue2__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue2__pf{};
	FVector bpfv__CallFunc_GetSpawnableLocation_Location3__pf(EForceInit::ForceInit);
	FTransform bpfv__CallFunc_MakeTransform_ReturnValue4__pf{};
	bool bpfv__CallFunc_EqualEqual_IntInt_ReturnValue__pf{};
	TArray< int32, TInlineAllocator<8> > __StateStack;

	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpf__GetSpawnableLocation__pf(/*out*/ bpfv__CallFunc_GetSpawnableLocation_Location1__pf);
			}
		case 2:
			{
				bpfv__CallFunc_RandomFloatInRange_ReturnValue2__pf = UKismetMathLibrary::RandomFloatInRange(0.000000, 360.000000);
				bpfv__CallFunc_MakeRotator_ReturnValue2__pf = UKismetMathLibrary::MakeRotator(0.000000, 0.000000, bpfv__CallFunc_RandomFloatInRange_ReturnValue2__pf);
				bpfv__CallFunc_MakeTransform_ReturnValue1__pf = UKismetMathLibrary::MakeTransform(bpfv__CallFunc_GetSpawnableLocation_Location1__pf, bpfv__CallFunc_MakeRotator_ReturnValue2__pf, FVector(1.000000,1.000000,1.000000));
				bpf__SpawnChild__pf(ACoin_C__pf2962636854::StaticClass(), bpfv__CallFunc_MakeTransform_ReturnValue1__pf);
			}
		case 3:
			{
				bpfv__Temp_int_Variable1__pf = 1;
			}
		case 4:
			{
				bpfv__CallFunc_LessEqual_IntInt_ReturnValue2__pf = UKismetMathLibrary::LessEqual_IntInt(bpfv__Temp_int_Variable1__pf, 5);
				if (!bpfv__CallFunc_LessEqual_IntInt_ReturnValue2__pf)
				{
					__CurrentState = 7;
					break;
				}
			}
		case 5:
			{
				__StateStack.Push(21);
			}
		case 6:
			{
				bpfv__CallFunc_RandomFloatInRange_ReturnValue2__pf = UKismetMathLibrary::RandomFloatInRange(0.000000, 360.000000);
				bpfv__CallFunc_MakeRotator_ReturnValue2__pf = UKismetMathLibrary::MakeRotator(0.000000, 0.000000, bpfv__CallFunc_RandomFloatInRange_ReturnValue2__pf);
				bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_IntFloat(bpfv__Temp_int_Variable1__pf, 500.000000);
				bpfv__CallFunc_MakeVector_ReturnValue__pf = UKismetMathLibrary::MakeVector(bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf, 0.000000, 0.000000);
				bpfv__CallFunc_Add_VectorVector_ReturnValue__pf = UKismetMathLibrary::Add_VectorVector(bpfv__CallFunc_GetSpawnableLocation_Location1__pf, bpfv__CallFunc_MakeVector_ReturnValue__pf);
				bpfv__CallFunc_MakeTransform_ReturnValue3__pf = UKismetMathLibrary::MakeTransform(bpfv__CallFunc_Add_VectorVector_ReturnValue__pf, bpfv__CallFunc_MakeRotator_ReturnValue2__pf, FVector(1.000000,1.000000,1.000000));
				bpf__SpawnChild__pf(ACoin_C__pf2962636854::StaticClass(), bpfv__CallFunc_MakeTransform_ReturnValue3__pf);
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 7:
			{
				bpfv__Temp_int_Variable__pf = 0;
			}
		case 8:
			{
				bpfv__CallFunc_LessEqual_IntInt_ReturnValue__pf = UKismetMathLibrary::LessEqual_IntInt(bpfv__Temp_int_Variable__pf, 3);
				if (!bpfv__CallFunc_LessEqual_IntInt_ReturnValue__pf)
				{
					__CurrentState = 12;
					break;
				}
			}
		case 9:
			{
				__StateStack.Push(22);
			}
		case 10:
			{
				bpf__GetSpawnableLocation__pf(/*out*/ bpfv__CallFunc_GetSpawnableLocation_Location__pf);
			}
		case 11:
			{
				bpfv__CallFunc_RandomFloatInRange_ReturnValue1__pf = UKismetMathLibrary::RandomFloatInRange(-45.000000, 45.000000);
				bpfv__CallFunc_MakeRotator_ReturnValue1__pf = UKismetMathLibrary::MakeRotator(0.000000, 0.000000, bpfv__CallFunc_RandomFloatInRange_ReturnValue1__pf);
				bpfv__CallFunc_MakeTransform_ReturnValue__pf = UKismetMathLibrary::MakeTransform(bpfv__CallFunc_GetSpawnableLocation_Location__pf, bpfv__CallFunc_MakeRotator_ReturnValue1__pf, FVector(1.500000,1.500000,1.500000));
				bpf__SpawnChild__pf(AObsCar_C__pf931608196::StaticClass(), bpfv__CallFunc_MakeTransform_ReturnValue__pf);
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 12:
			{
				bpfv__Temp_int_Variable2__pf = 0;
			}
		case 13:
			{
				bpfv__CallFunc_LessEqual_IntInt_ReturnValue1__pf = UKismetMathLibrary::LessEqual_IntInt(bpfv__Temp_int_Variable2__pf, 2);
				if (!bpfv__CallFunc_LessEqual_IntInt_ReturnValue1__pf)
				{
					__CurrentState = 17;
					break;
				}
			}
		case 14:
			{
				__StateStack.Push(23);
			}
		case 15:
			{
				bpf__GetSpawnableLocation__pf(/*out*/ bpfv__CallFunc_GetSpawnableLocation_Location2__pf);
			}
		case 16:
			{
				bpfv__CallFunc_RandomFloatInRange_ReturnValue__pf = UKismetMathLibrary::RandomFloatInRange(0.000000, 360.000000);
				bpfv__CallFunc_MakeRotator_ReturnValue__pf = UKismetMathLibrary::MakeRotator(0.000000, 0.000000, bpfv__CallFunc_RandomFloatInRange_ReturnValue__pf);
				bpfv__CallFunc_MakeTransform_ReturnValue2__pf = UKismetMathLibrary::MakeTransform(bpfv__CallFunc_GetSpawnableLocation_Location2__pf, bpfv__CallFunc_MakeRotator_ReturnValue__pf, FVector(1.000000,1.000000,1.000000));
				bpf__SpawnChild__pf(ACanister_C__pf1442588193::StaticClass(), bpfv__CallFunc_MakeTransform_ReturnValue2__pf);
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 17:
			{
				bpfv__CallFunc_RandomInteger_ReturnValue__pf = UKismetMathLibrary::RandomInteger(2);
				bpfv__RandomInt__pf = bpfv__CallFunc_RandomInteger_ReturnValue__pf;
			}
		case 18:
			{
				bpfv__CallFunc_EqualEqual_IntInt_ReturnValue__pf = UKismetMathLibrary::EqualEqual_IntInt(bpfv__RandomInt__pf, 1);
				if (!bpfv__CallFunc_EqualEqual_IntInt_ReturnValue__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 19:
			{
				bpf__GetSpawnableLocation__pf(/*out*/ bpfv__CallFunc_GetSpawnableLocation_Location3__pf);
			}
		case 20:
			{
				bpfv__CallFunc_MakeTransform_ReturnValue4__pf = UKismetMathLibrary::MakeTransform(bpfv__CallFunc_GetSpawnableLocation_Location3__pf, FRotator(0.000000,0.000000,0.000000), FVector(1.000000,1.000000,1.000000));
				bpf__SpawnChild__pf(ABoost_C__pf2962636854::StaticClass(), bpfv__CallFunc_MakeTransform_ReturnValue4__pf);
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 21:
			{
				bpfv__CallFunc_Add_IntInt_ReturnValue2__pf = UKismetMathLibrary::Add_IntInt(bpfv__Temp_int_Variable1__pf, 1);
				bpfv__Temp_int_Variable1__pf = bpfv__CallFunc_Add_IntInt_ReturnValue2__pf;
				__CurrentState = 4;
				break;
			}
		case 22:
			{
				bpfv__CallFunc_Add_IntInt_ReturnValue__pf = UKismetMathLibrary::Add_IntInt(bpfv__Temp_int_Variable__pf, 1);
				bpfv__Temp_int_Variable__pf = bpfv__CallFunc_Add_IntInt_ReturnValue__pf;
				__CurrentState = 8;
				break;
			}
		case 23:
			{
				bpfv__CallFunc_Add_IntInt_ReturnValue1__pf = UKismetMathLibrary::Add_IntInt(bpfv__Temp_int_Variable2__pf, 1);
				bpfv__Temp_int_Variable2__pf = bpfv__CallFunc_Add_IntInt_ReturnValue1__pf;
				__CurrentState = 13;
				break;
			}
		default:
			check(false); // Invalid state
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__AddToResetables__pf()
{
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	int32 bpfv__CallFunc_Array_Add_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				TArray<AActor*>  __Local__29 = TArray<AActor*> {};
				bpfv__CallFunc_Array_Add_ReturnValue__pf = FCustomThunkTemplates::Array_Add(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Resetables__pf) : (__Local__29)), this);
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__RemoveFromResetables__pf()
{
	AGameModeBase* bpfv__CallFunc_GetGameMode_ReturnValue__pf{};
	AMaluchyGameMode_C__pf2132744816* bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	bool bpfv__CallFunc_Array_RemoveItem_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_GetGameMode_ReturnValue__pf = UGameplayStatics::GetGameMode(this);
				bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf = Cast<AMaluchyGameMode_C__pf2132744816>(bpfv__CallFunc_GetGameMode_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				TArray<AActor*>  __Local__30 = TArray<AActor*> {};
				bpfv__CallFunc_Array_RemoveItem_ReturnValue__pf = FCustomThunkTemplates::Array_RemoveItem(((::IsValid(bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf)) ? (bpfv__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf->bpv__Resetables__pf) : (__Local__30)), this);
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__ResetLocation__pf(float bpp__By__pf)
{
	FVector bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakVector_X__pf{};
	float bpfv__CallFunc_BreakVector_Y__pf{};
	float bpfv__CallFunc_BreakVector_Z__pf{};
	float bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf{};
	FVector bpfv__CallFunc_MakeVector_ReturnValue__pf(EForceInit::ForceInit);
	FHitResult bpfv__CallFunc_K2_SetActorLocation_SweepHitResult__pf{};
	bool bpfv__CallFunc_K2_SetActorLocation_ReturnValue__pf{};
	bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf = AActor::K2_GetActorLocation();
	UKismetMathLibrary::BreakVector(bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
	bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Subtract_FloatFloat(bpfv__CallFunc_BreakVector_X__pf, bpp__By__pf);
	bpfv__CallFunc_MakeVector_ReturnValue__pf = UKismetMathLibrary::MakeVector(bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf, bpfv__CallFunc_BreakVector_Y__pf, bpfv__CallFunc_BreakVector_Z__pf);
	bpfv__CallFunc_K2_SetActorLocation_ReturnValue__pf = AActor::K2_SetActorLocation(bpfv__CallFunc_MakeVector_ReturnValue__pf, false, /*out*/ bpfv__CallFunc_K2_SetActorLocation_SweepHitResult__pf, false);
}
void ARoadSegment_C__pf24847026::bpf__DestroyRoadSegment__pf()
{
	AActor* bpfv__ToDestroy__pf{};
	int32 bpfv__Temp_int_Array_Index_Variable__pf{};
	int32 bpfv__Temp_int_Loop_Counter_Variable__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue__pf{};
	AActor* bpfv__CallFunc_Array_Get_Item__pf{};
	int32 bpfv__CallFunc_Array_Length_ReturnValue__pf{};
	bool bpfv__CallFunc_IsValid_ReturnValue__pf{};
	bool bpfv__CallFunc_Less_IntInt_ReturnValue__pf{};
	TArray< int32, TInlineAllocator<8> > __StateStack;

	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpf__RemoveFromResetables__pf();
			}
		case 2:
			{
				bpfv__Temp_int_Loop_Counter_Variable__pf = 0;
			}
		case 3:
			{
				bpfv__Temp_int_Array_Index_Variable__pf = 0;
			}
		case 4:
			{
				bpfv__CallFunc_Array_Length_ReturnValue__pf = FCustomThunkTemplates::Array_Length(bpv__CurrObjects__pf);
				bpfv__CallFunc_Less_IntInt_ReturnValue__pf = UKismetMathLibrary::Less_IntInt(bpfv__Temp_int_Loop_Counter_Variable__pf, bpfv__CallFunc_Array_Length_ReturnValue__pf);
				if (!bpfv__CallFunc_Less_IntInt_ReturnValue__pf)
				{
					__CurrentState = 9;
					break;
				}
			}
		case 5:
			{
				bpfv__Temp_int_Array_Index_Variable__pf = bpfv__Temp_int_Loop_Counter_Variable__pf;
			}
		case 6:
			{
				__StateStack.Push(10);
			}
		case 7:
			{
				FCustomThunkTemplates::Array_Get(bpv__CurrObjects__pf, bpfv__Temp_int_Array_Index_Variable__pf, /*out*/ bpfv__CallFunc_Array_Get_Item__pf);
				bpfv__CallFunc_IsValid_ReturnValue__pf = UKismetSystemLibrary::IsValid(bpfv__CallFunc_Array_Get_Item__pf);
				if (!bpfv__CallFunc_IsValid_ReturnValue__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 8:
			{
				FCustomThunkTemplates::Array_Get(bpv__CurrObjects__pf, bpfv__Temp_int_Array_Index_Variable__pf, /*out*/ bpfv__CallFunc_Array_Get_Item__pf);
				if(::IsValid(bpfv__CallFunc_Array_Get_Item__pf))
				{
					bpfv__CallFunc_Array_Get_Item__pf->K2_DestroyActor();
				}
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 9:
			{
				K2_DestroyActor();
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 10:
			{
				bpfv__CallFunc_Add_IntInt_ReturnValue__pf = UKismetMathLibrary::Add_IntInt(bpfv__Temp_int_Loop_Counter_Variable__pf, 1);
				bpfv__Temp_int_Loop_Counter_Variable__pf = bpfv__CallFunc_Add_IntInt_ReturnValue__pf;
				__CurrentState = 4;
				break;
			}
		default:
			check(false); // Invalid state
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__GetSpawnableLocation__pf(/*out*/ FVector& bpp__Location__pf)
{
	FVector bpfv__TestedLocation__pf(EForceInit::ForceInit);
	bool bpfv__CanSpawnHere__pf{};
	int32 bpfv__Temp_int_Array_Index_Variable__pf{};
	int32 bpfv__Temp_int_Loop_Counter_Variable__pf{};
	int32 bpfv__CallFunc_Add_IntInt_ReturnValue__pf{};
	bool bpfv__CallFunc_Not_PreBool_ReturnValue__pf{};
	FVector bpfv__CallFunc_GetComponentBounds_Origin__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_GetComponentBounds_BoxExtent__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_GetComponentBounds_SphereRadius__pf{};
	float bpfv__CallFunc_BreakVector_X__pf{};
	float bpfv__CallFunc_BreakVector_Y__pf{};
	float bpfv__CallFunc_BreakVector_Z__pf{};
	float bpfv__CallFunc_BreakVector_X1__pf{};
	float bpfv__CallFunc_BreakVector_Y1__pf{};
	float bpfv__CallFunc_BreakVector_Z1__pf{};
	float bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_RandomFloatInRange_ReturnValue__pf{};
	float bpfv__CallFunc_Multiply_IntFloat_ReturnValue1__pf{};
	float bpfv__CallFunc_RandomFloatInRange_ReturnValue1__pf{};
	FVector bpfv__CallFunc_MakeVector_ReturnValue__pf(EForceInit::ForceInit);
	AActor* bpfv__CallFunc_Array_Get_Item__pf{};
	FVector bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf(EForceInit::ForceInit);
	int32 bpfv__CallFunc_Array_Length_ReturnValue__pf{};
	FVector bpfv__CallFunc_Subtract_VectorVector_ReturnValue__pf(EForceInit::ForceInit);
	bool bpfv__CallFunc_Less_IntInt_ReturnValue__pf{};
	float bpfv__CallFunc_VSize_ReturnValue__pf{};
	bool bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf{};
	TArray< int32, TInlineAllocator<8> > __StateStack;

	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_Not_PreBool_ReturnValue__pf = UKismetMathLibrary::Not_PreBool(bpfv__CanSpawnHere__pf);
				if (!bpfv__CallFunc_Not_PreBool_ReturnValue__pf)
				{
					__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
					break;
				}
			}
		case 2:
			{
				__StateStack.Push(1);
			}
		case 3:
			{
				bpfv__CanSpawnHere__pf = true;
			}
		case 4:
			{
				UKismetSystemLibrary::GetComponentBounds(bpv__Box__pf, /*out*/ bpfv__CallFunc_GetComponentBounds_Origin__pf, /*out*/ bpfv__CallFunc_GetComponentBounds_BoxExtent__pf, /*out*/ bpfv__CallFunc_GetComponentBounds_SphereRadius__pf);
				UKismetMathLibrary::BreakVector(bpfv__CallFunc_GetComponentBounds_BoxExtent__pf, /*out*/ bpfv__CallFunc_BreakVector_X__pf, /*out*/ bpfv__CallFunc_BreakVector_Y__pf, /*out*/ bpfv__CallFunc_BreakVector_Z__pf);
				UKismetMathLibrary::BreakVector(bpfv__CallFunc_GetComponentBounds_Origin__pf, /*out*/ bpfv__CallFunc_BreakVector_X1__pf, /*out*/ bpfv__CallFunc_BreakVector_Y1__pf, /*out*/ bpfv__CallFunc_BreakVector_Z1__pf);
				bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_IntFloat(-1, bpfv__CallFunc_BreakVector_Y__pf);
				bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Subtract_FloatFloat(bpfv__CallFunc_BreakVector_Z1__pf, bpfv__CallFunc_BreakVector_Z__pf);
				bpfv__CallFunc_RandomFloatInRange_ReturnValue__pf = UKismetMathLibrary::RandomFloatInRange(bpfv__CallFunc_Multiply_IntFloat_ReturnValue__pf, bpfv__CallFunc_BreakVector_Y__pf);
				bpfv__CallFunc_Multiply_IntFloat_ReturnValue1__pf = UKismetMathLibrary::Multiply_IntFloat(-1, bpfv__CallFunc_BreakVector_X__pf);
				bpfv__CallFunc_RandomFloatInRange_ReturnValue1__pf = UKismetMathLibrary::RandomFloatInRange(bpfv__CallFunc_Multiply_IntFloat_ReturnValue1__pf, bpfv__CallFunc_BreakVector_X__pf);
				bpfv__CallFunc_MakeVector_ReturnValue__pf = UKismetMathLibrary::MakeVector(bpfv__CallFunc_RandomFloatInRange_ReturnValue1__pf, bpfv__CallFunc_RandomFloatInRange_ReturnValue__pf, bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf);
				bpfv__TestedLocation__pf = bpfv__CallFunc_MakeVector_ReturnValue__pf;
			}
		case 5:
			{
				bpfv__Temp_int_Loop_Counter_Variable__pf = 0;
			}
		case 6:
			{
				bpfv__Temp_int_Array_Index_Variable__pf = 0;
			}
		case 7:
			{
				bpfv__CallFunc_Array_Length_ReturnValue__pf = FCustomThunkTemplates::Array_Length(bpv__CurrObjects__pf);
				bpfv__CallFunc_Less_IntInt_ReturnValue__pf = UKismetMathLibrary::Less_IntInt(bpfv__Temp_int_Loop_Counter_Variable__pf, bpfv__CallFunc_Array_Length_ReturnValue__pf);
				if (!bpfv__CallFunc_Less_IntInt_ReturnValue__pf)
				{
					__CurrentState = 12;
					break;
				}
			}
		case 8:
			{
				bpfv__Temp_int_Array_Index_Variable__pf = bpfv__Temp_int_Loop_Counter_Variable__pf;
			}
		case 9:
			{
				__StateStack.Push(13);
			}
		case 10:
			{
				FCustomThunkTemplates::Array_Get(bpv__CurrObjects__pf, bpfv__Temp_int_Array_Index_Variable__pf, /*out*/ bpfv__CallFunc_Array_Get_Item__pf);
				if(::IsValid(bpfv__CallFunc_Array_Get_Item__pf))
				{
					bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf = bpfv__CallFunc_Array_Get_Item__pf->AActor::K2_GetActorLocation();
				}
				bpfv__CallFunc_Subtract_VectorVector_ReturnValue__pf = UKismetMathLibrary::Subtract_VectorVector(bpfv__TestedLocation__pf, bpfv__CallFunc_K2_GetActorLocation_ReturnValue__pf);
				bpfv__CallFunc_VSize_ReturnValue__pf = UKismetMathLibrary::VSize(bpfv__CallFunc_Subtract_VectorVector_ReturnValue__pf);
				bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Less_FloatFloat(bpfv__CallFunc_VSize_ReturnValue__pf, bpv__MinDistanceFromAnother__pf);
				if (!bpfv__CallFunc_Less_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = 12;
					break;
				}
			}
		case 11:
			{
				bpfv__CanSpawnHere__pf = false;
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 12:
			{
				bpp__Location__pf = bpfv__TestedLocation__pf;
				__CurrentState = -1;
				break;
			}
		case 13:
			{
				bpfv__CallFunc_Add_IntInt_ReturnValue__pf = UKismetMathLibrary::Add_IntInt(bpfv__Temp_int_Loop_Counter_Variable__pf, 1);
				bpfv__Temp_int_Loop_Counter_Variable__pf = bpfv__CallFunc_Add_IntInt_ReturnValue__pf;
				__CurrentState = 7;
				break;
			}
		default:
			check(false); // Invalid state
			break;
		}
	} while( __CurrentState != -1 );
}
void ARoadSegment_C__pf24847026::bpf__SpawnChild__pf(UClass* bpp__Class__pf, FTransform bpp__SpawnTransform__pf)
{
	AActor* bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf{};
	AActor* bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf{};
	int32 bpfv__CallFunc_Array_Add_ReturnValue__pf{};
	bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf = UGameplayStatics::BeginDeferredActorSpawnFromClass(this, bpp__Class__pf, bpp__SpawnTransform__pf, ESpawnActorCollisionHandlingMethod::Undefined, ((AActor*)nullptr));
	bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf = UGameplayStatics::FinishSpawningActor(bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf, bpp__SpawnTransform__pf);
	if(::IsValid(bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf))
	{
		bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf->AActor::K2_AttachToActor(this, FName(TEXT("None")), EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, true);
	}
	bpfv__CallFunc_Array_Add_ReturnValue__pf = FCustomThunkTemplates::Array_Add(bpv__CurrObjects__pf, bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf);
}
PRAGMA_DISABLE_OPTIMIZATION
void ARoadSegment_C__pf24847026::__StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{56, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  StaticMesh /Engine/EngineMeshes/Cube.Cube 
		{57, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Material /Game/Blueprints/RoadSegment/grassMat.GrassMat 
		{58, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Material /Game/Assets/Graphics/Misc/Texture/roadMat.roadMat 
		{59, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Material /Game/Blueprints/RoadSegment/AsphaltMat.AsphaltMat 
		{60, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Material /Game/Blueprints/RoadSegment/LineMat.LineMat 
		{61, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  StaticMesh /Engine/BasicShapes/Cube.Cube 
		{62, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Material /Game/Assets/Black.Black 
		{63, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  StaticMesh /Game/Assets/Buildings/low_poly_buildings_Highrise_1.low_poly_buildings_Highrise_1 
		{64, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Material /Game/Assets/Buildings/BuildingDefaultMaterial.BuildingDefaultMaterial 
		{65, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  StaticMesh /Game/Assets/Buildings/low_poly_buildings_Highrise_17.low_poly_buildings_Highrise_17 
		{66, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  StaticMesh /Game/Assets/Buildings/low_poly_buildings_Highrise_9.low_poly_buildings_Highrise_9 
		{67, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  StaticMesh /Game/Assets/Buildings/low_poly_buildings_Highrise_2.low_poly_buildings_Highrise_2 
		{68, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  StaticMesh /Game/Assets/Buildings/low_poly_buildings_Highrise_16.low_poly_buildings_Highrise_16 
		{69, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  StaticMesh /Game/Assets/Buildings/low_poly_buildings_Highrise_11.low_poly_buildings_Highrise_11 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
PRAGMA_DISABLE_OPTIMIZATION
void ARoadSegment_C__pf24847026::__StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	__StaticDependencies_DirectlyUsedAssets(AssetsToLoad);
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{0, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.SceneComponent 
		{28, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.StaticMeshComponent 
		{70, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.BoxComponent 
		{71, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/NavigationSystem.NavArea_Obstacle 
		{1, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.PointerToUberGraphFrame 
		{6, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Actor 
		{2, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.GameModeBase 
		{7, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.GameplayStatics 
		{5, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetSystemLibrary 
		{4, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetMathLibrary 
		{3, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetArrayLibrary 
		{9, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.HitResult 
		{40, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/MaluchyGameMode.MaluchyGameMode_C 
		{72, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/Collectables/Coin.Coin_C 
		{73, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/Car/OppCars/ObsCar.ObsCar_C 
		{74, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/Collectables/Canister/Canister.Canister_C 
		{75, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Blueprints/Collectables/Boost.Boost_C 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
struct FRegisterHelper__ARoadSegment_C__pf24847026
{
	FRegisterHelper__ARoadSegment_C__pf24847026()
	{
		FConvertedBlueprintsDependencies::Get().RegisterConvertedClass(TEXT("/Game/Blueprints/RoadSegment/RoadSegment"), &ARoadSegment_C__pf24847026::__StaticDependenciesAssets);
	}
	static FRegisterHelper__ARoadSegment_C__pf24847026 Instance;
};
FRegisterHelper__ARoadSegment_C__pf24847026 FRegisterHelper__ARoadSegment_C__pf24847026::Instance;
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
