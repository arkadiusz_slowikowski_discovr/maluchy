#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/InputCore/Classes/InputCoreTypes.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
class ADiscoMaluch_C__pf1851508772;
class AMaluchyGameMode_C__pf2132744816;
#include "MainCarController__pf1851508772.generated.h"
UCLASS(config=Game, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/Car/MainCarController.MainCarController_C", OverrideNativeName="MainCarController_C"))
class AMainCarController_C__pf1851508772 : public APlayerController
{
public:
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Turning Speed", Category="Default", OverrideNativeName="TurningSpeed"))
	float bpv__TurningSpeed__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Rolling Sensitivity", Category="Default", OverrideNativeName="RollingSensitivity"))
	float bpv__RollingSensitivity__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Yellow Pressed", Category="Default", OverrideNativeName="YellowPressed"))
	bool bpv__YellowPressed__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Yellow Timer", Category="Default", OverrideNativeName="YellowTimer"))
	float bpv__YellowTimer__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputActionEvent_Key1"))
	FKey b0l__K2Node_InputActionEvent_Key1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsDisco_Maluch"))
	ADiscoMaluch_C__pf1851508772* b0l__K2Node_DynamicCast_AsDisco_Maluch__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess"))
	bool b0l__K2Node_DynamicCast_bSuccess__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputActionEvent_Key"))
	FKey b0l__K2Node_InputActionEvent_Key__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="Temp_struct_Variable"))
	FKey b0l__Temp_struct_Variable__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputAxisEvent_AxisValue1"))
	float b0l__K2Node_InputAxisEvent_AxisValue1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputAxisEvent_AxisValue"))
	float b0l__K2Node_InputAxisEvent_AxisValue__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsDisco_Maluch1"))
	ADiscoMaluch_C__pf1851508772* b0l__K2Node_DynamicCast_AsDisco_Maluch1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess1"))
	bool b0l__K2Node_DynamicCast_bSuccess1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsDisco_Maluch2"))
	ADiscoMaluch_C__pf1851508772* b0l__K2Node_DynamicCast_AsDisco_Maluch2__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess2"))
	bool b0l__K2Node_DynamicCast_bSuccess2__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsMaluchy_Game_Mode"))
	AMaluchyGameMode_C__pf2132744816* b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess3"))
	bool b0l__K2Node_DynamicCast_bSuccess3__pf;
	AMainCarController_C__pf1851508772(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_MainCarController__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_MainCarController__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_MainCarController__pf_2(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_MainCarController__pf_3(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_MainCarController__pf_4(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(OverrideNativeName="InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0"))
	virtual void bpf__InpAxisEvt_Horizontal_K2Node_InputAxisEvent_0__pf(float bpp__AxisValue__pf);
	UFUNCTION(meta=(OverrideNativeName="InpAxisEvt_Vertical_K2Node_InputAxisEvent_1"))
	virtual void bpf__InpAxisEvt_Vertical_K2Node_InputAxisEvent_1__pf(float bpp__AxisValue__pf);
	UFUNCTION(meta=(DisplayName="BeginPlay", ToolTip="Event when play begins for this actor.", CppFromBpEvent, OverrideNativeName="ReceiveBeginPlay"))
	virtual void bpf__ReceiveBeginPlay__pf();
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_YellowButton_K2Node_InputActionEvent_2"))
	virtual void bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf(FKey bpp__Key__pf);
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_YellowButton_K2Node_InputActionEvent_3"))
	virtual void bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf(FKey bpp__Key__pf);
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", DisplayName="Construction Script", ToolTip="Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation.", Category, CppFromBpEvent, OverrideNativeName="UserConstructionScript"))
	virtual void bpf__UserConstructionScript__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="MoveHor"))
	virtual void bpf__MoveHor__pf(float bpp__Input__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="RollCar"))
	virtual void bpf__RollCar__pf(float bpp__Input__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Restart"))
	virtual void bpf__Restart__pf();
public:
};
