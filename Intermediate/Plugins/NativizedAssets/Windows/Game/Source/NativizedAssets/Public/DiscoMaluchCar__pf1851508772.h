#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/InputCore/Classes/InputCoreTypes.h"
#include "Runtime/CoreUObject/Public/UObject/NoExportTypes.h"
#include "../Plugins/Runtime/PhysXVehicles/Source/PhysXVehicles/Public/WheeledVehicle.h"
class UCameraComponent;
class USpotLightComponent;
class USceneComponent;
class USpringArmComponent;
class AMaluchyGameMode_C__pf2132744816;
#include "DiscoMaluchCar__pf1851508772.generated.h"
UCLASS(config=Game, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/Car/DiscoMaluchCar.DiscoMaluchCar_C", OverrideNativeName="DiscoMaluchCar_C"))
class ADiscoMaluchCar_C__pf1851508772 : public AWheeledVehicle
{
public:
	GENERATED_BODY()
	UDELEGATE(meta=(OverrideNativeName="BecameUnableToDrive__DelegateSignature"))
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate );
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Camera"))
	UCameraComponent* bpv__Camera__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Right"))
	USpotLightComponent* bpv__Right__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Left"))
	USpotLightComponent* bpv__Left__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Headlights"))
	USceneComponent* bpv__Headlights__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="SpringArm"))
	USpringArmComponent* bpv__SpringArm__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Distance Passed", Category="Default", OverrideNativeName="DistancePassed"))
	float bpv__DistancePassed__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Coins Collected", Category="Default", OverrideNativeName="CoinsCollected"))
	int32 bpv__CoinsCollected__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Fuel", Category="Default", OverrideNativeName="Fuel"))
	float bpv__Fuel__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Start Rot", Category="Default", OverrideNativeName="StartRot"))
	FRotator bpv__StartRot__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Fuel Descreasing Speed", Category="Default", OverrideNativeName="FuelDescreasingSpeed"))
	float bpv__FuelDescreasingSpeed__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Driving Force", Category="Default", OverrideNativeName="DrivingForce"))
	float bpv__DrivingForce__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Steering Sensitivity", Category="Default", OverrideNativeName="SteeringSensitivity"))
	float bpv__SteeringSensitivity__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Camera Steering Sensitivity", Category="Default", OverrideNativeName="CameraSteeringSensitivity"))
	float bpv__CameraSteeringSensitivity__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Resetting Distance", Category="Default", OverrideNativeName="ResettingDistance"))
	float bpv__ResettingDistance__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Changing Gear Trashhold", Category="Default", OverrideNativeName="ChangingGearTrashhold"))
	float bpv__ChangingGearTrashhold__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Able to Drive", Category="Default", OverrideNativeName="AbleToDrive"))
	bool bpv__AbleToDrive__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta=(DisplayName="Became Unable to Drive", Category="Default", OverrideNativeName="BecameUnableToDrive"))
	FBecameUnableToDrive__pf__DiscoMaluchCar_C__pf__MulticastDelegate bpv__BecameUnableToDrive__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Prev Speed", Category="Default", OverrideNativeName="PrevSpeed"))
	float bpv__PrevSpeed__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Is Grounded", Category="Default", OverrideNativeName="IsGrounded"))
	bool bpv__IsGrounded__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Driving Multipier", Category="Default", OverrideNativeName="DrivingMultipier"))
	float bpv__DrivingMultipier__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Game Mode", Category="Default", OverrideNativeName="GameMode"))
	AMaluchyGameMode_C__pf2132744816* bpv__GameMode__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Prev X", Category="Default", OverrideNativeName="PrevX"))
	float bpv__PrevX__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Yellow Timer", Category="Default", OverrideNativeName="YellowTimer"))
	float bpv__YellowTimer__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Yellow Pressed", Category="Default", OverrideNativeName="YellowPressed"))
	bool bpv__YellowPressed__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DeltaSeconds"))
	float b0l__K2Node_Event_DeltaSeconds__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="Temp_struct_Variable"))
	FKey b0l__Temp_struct_Variable__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputActionEvent_Key"))
	FKey b0l__K2Node_InputActionEvent_Key__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputActionEvent_Key1"))
	FKey b0l__K2Node_InputActionEvent_Key1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_BreakVector_X"))
	float b0l__CallFunc_BreakVector_X__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_BreakVector_Y"))
	float b0l__CallFunc_BreakVector_Y__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_BreakVector_Z"))
	float b0l__CallFunc_BreakVector_Z__pf;
	ADiscoMaluchCar_C__pf1851508772(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_DiscoMaluchCar__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_DiscoMaluchCar__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_DiscoMaluchCar__pf_2(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_DiscoMaluchCar__pf_3(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(DisplayName="Tick", ToolTip="Event called every frame", CppFromBpEvent, OverrideNativeName="ReceiveTick"))
	virtual void bpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf);
	UFUNCTION(meta=(DisplayName="BeginPlay", ToolTip="Event when play begins for this actor.", CppFromBpEvent, OverrideNativeName="ReceiveBeginPlay"))
	virtual void bpf__ReceiveBeginPlay__pf();
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_YellowButton_K2Node_InputActionEvent_2"))
	virtual void bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_2__pf(FKey bpp__Key__pf);
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_YellowButton_K2Node_InputActionEvent_3"))
	virtual void bpf__InpActEvt_YellowButton_K2Node_InputActionEvent_3__pf(FKey bpp__Key__pf);
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", DisplayName="Construction Script", ToolTip="Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation.", Category, CppFromBpEvent, OverrideNativeName="UserConstructionScript"))
	virtual void bpf__UserConstructionScript__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Move"))
	virtual void bpf__Move__pf(float bpp__DeltaxTime__pfT);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="SetAsMainCar"))
	virtual void bpf__SetAsMainCar__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="HitByObstacle"))
	virtual void bpf__HitByObstacle__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="CollectCoin"))
	virtual void bpf__CollectCoin__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="AddUIWidget"))
	virtual void bpf__AddUIWidget__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="CollectFuel"))
	virtual void bpf__CollectFuel__pf(float bpp__HowxMuch__pfT);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="DecreaseFuel"))
	virtual void bpf__DecreaseFuel__pf(float bpp__DeltaxTime__pfT);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="CantDrive"))
	virtual void bpf__CantDrive__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="GetTotalMass"))
	virtual void bpf__GetTotalMass__pf(/*out*/ float& bpp__TotalxMass__pfT);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Drive"))
	virtual void bpf__Drive__pf(float bpp__DeltaxTime__pfT);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="OldSteer"))
	virtual void bpf__OldSteer__pf(float bpp__Input__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="NewSteer"))
	virtual void bpf__NewSteer__pf(float bpp__Input__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="CallResettingEnviroment"))
	virtual void bpf__CallResettingEnviroment__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="ChangeGear"))
	virtual void bpf__ChangeGear__pf(float bpp__Input__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="CheckIfGrounded"))
	virtual void bpf__CheckIfGrounded__pf(float bpp__DeltaxTime__pfT);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="CountDistance"))
	virtual void bpf__CountDistance__pf();
public:
};
