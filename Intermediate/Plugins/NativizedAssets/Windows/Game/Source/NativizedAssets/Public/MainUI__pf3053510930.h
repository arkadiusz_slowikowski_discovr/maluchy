#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/SlateCore/Public/Layout/Geometry.h"
#include "Runtime/SlateCore/Public/Styling/SlateColor.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
class UTextBlock;
class UCanvasPanel;
class UProgressBar;
class UImage;
class UHorizontalBox;
class AMaluchyGameMode_C__pf2132744816;
#include "MainUI__pf3053510930.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/UI/MainUI.MainUI_C", OverrideNativeName="MainUI_C"))
class UMainUI_C__pf3053510930 : public UUserWidget
{
public:
	GENERATED_BODY()
	UDELEGATE(meta=(OverrideNativeName="Event GameStarted__DelegateSignature"))
	DECLARE_DYNAMIC_DELEGATE(FEventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate );
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Circle", Category="MainUI", OverrideNativeName="Circle"))
	UImage* bpv__Circle__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="CoinsBox", Category="MainUI", OverrideNativeName="CoinsBox"))
	UHorizontalBox* bpv__CoinsBox__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="DistanceBox", Category="MainUI", OverrideNativeName="DistanceBox"))
	UHorizontalBox* bpv__DistanceBox__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="FuelBox", Category="MainUI", OverrideNativeName="FuelBox"))
	UHorizontalBox* bpv__FuelBox__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_46", Category="MainUI", OverrideNativeName="Image_46"))
	UImage* bpv__Image_46__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_202", Category="MainUI", OverrideNativeName="Image_202"))
	UImage* bpv__Image_202__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="InitInfo", Category="MainUI", OverrideNativeName="InitInfo"))
	UCanvasPanel* bpv__InitInfo__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Pointer", Category="MainUI", OverrideNativeName="Pointer"))
	UImage* bpv__Pointer__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="ProgressBar_0", Category="MainUI", OverrideNativeName="ProgressBar_0"))
	UProgressBar* bpv__ProgressBar_0__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Speedometer", Category="MainUI", OverrideNativeName="Speedometer"))
	UCanvasPanel* bpv__Speedometer__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_1", OverrideNativeName="TextBlock_1"))
	UTextBlock* bpv__TextBlock_1__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_56", OverrideNativeName="TextBlock_56"))
	UTextBlock* bpv__TextBlock_56__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_123", OverrideNativeName="TextBlock_123"))
	UTextBlock* bpv__TextBlock_123__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_619", OverrideNativeName="TextBlock_619"))
	UTextBlock* bpv__TextBlock_619__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_CreateDelegate_OutputDelegate"))
	FEventxGameStarted__pfT__MaluchyGameMode_C__pf__SinglecastDelegate b0l__K2Node_CreateDelegate_OutputDelegate__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_MyGeometry"))
	FGeometry b0l__K2Node_Event_MyGeometry__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_InDeltaTime"))
	float b0l__K2Node_Event_InDeltaTime__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsMaluchy_Game_Mode"))
	AMaluchyGameMode_C__pf2132744816* b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess"))
	bool b0l__K2Node_DynamicCast_bSuccess__pf;
	UMainUI_C__pf3053510930(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_MainUI__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_MainUI__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_MainUI__pf_2(int32 bpp__EntryPoint__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Event HandleGameStarted"))
	virtual void bpf__EventxHandleGameStarted__pfT();
	UFUNCTION(BlueprintCosmetic, meta=(Category="User Interface", ToolTip="Ticks this widget.  Override in derived classes, but always call the parent implementation.@param  MyGeometry The space allotted for this widget@param  InDeltaTime  Real time passed since last tick", CppFromBpEvent, OverrideNativeName="Tick"))
	virtual void bpf__Tick__pf(FGeometry bpp__MyGeometry__pf, float bpp__InDeltaTime__pf);
	UFUNCTION(BlueprintCosmetic, meta=(Category="User Interface", Keywords="Begin Play", ToolTip="Called after the underlying slate widget is constructed.  Depending on how the slate object is usedthis event may be called multiple times due to adding and removing from the hierarchy.", CppFromBpEvent, OverrideNativeName="Construct"))
	virtual void bpf__Construct__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetText_0"))
	virtual FText  bpf__GetText_0__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetPercent_0"))
	virtual float  bpf__GetPercent_0__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetColorAndOpacity_0"))
	virtual FSlateColor  bpf__GetColorAndOpacity_0__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetText_1"))
	virtual FText  bpf__GetText_1__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="GetCarSpeed"))
	virtual float  bpf__GetCarSpeed__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="HandleGameStarted"))
	virtual void bpf__HandleGameStarted__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetText_2"))
	virtual FText  bpf__GetText_2__pf();
public:
	virtual void GetSlotNames(TArray<FName>& SlotNames) const override;
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
	virtual void InitializeNativeClassData() override;
};
