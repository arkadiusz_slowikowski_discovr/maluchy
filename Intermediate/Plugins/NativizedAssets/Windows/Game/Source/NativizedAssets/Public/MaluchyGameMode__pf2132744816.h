#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/Engine/Classes/GameFramework/GameModeBase.h"
class ARotatable_C__pf2962636854;
class AActor;
class ADiscoMaluch_C__pf1851508772;
class USceneComponent;
#include "MaluchyGameMode__pf2132744816.generated.h"
UCLASS(config=Game, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/MaluchyGameMode.MaluchyGameMode_C,/Game/Blueprints/CarGameMode.CarGameMode_C,/Game/CarGameMode.CarGameMode_C", OverrideNativeName="MaluchyGameMode_C"))
class AMaluchyGameMode_C__pf2132744816 : public AGameModeBase
{
public:
	GENERATED_BODY()
	UDELEGATE(meta=(OverrideNativeName="Event GameStarted__DelegateSignature"))
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate );
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="DefaultSceneRoot"))
	USceneComponent* bpv__DefaultSceneRoot__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Car", Category="Default", OverrideNativeName="Car"))
	ADiscoMaluch_C__pf1851508772* bpv__Car__pf;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Road Segments Speed", Category="Default", OverrideNativeName="RoadSegmentsSpeed"))
	float bpv__RoadSegmentsSpeed__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Resetables", Category="Default", OverrideNativeName="Resetables"))
	TArray<AActor*> bpv__Resetables__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Is Resetting", Category="Default", OverrideNativeName="IsResetting"))
	bool bpv__IsResetting__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Game Started", Category="Default", OverrideNativeName="GameStarted"))
	bool bpv__GameStarted__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta=(DisplayName="Event Game Started", Category="Default", OverrideNativeName="Event GameStarted"))
	FEventxGameStarted__pfT__MaluchyGameMode_C__pf__MulticastDelegate bpv__EventxGameStarted__pfT;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Game Already Ended", Category="Default", OverrideNativeName="GameAlreadyEnded"))
	bool bpv__GameAlreadyEnded__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Rotatables", Category="Default", OverrideNativeName="Rotatables"))
	TArray<ARotatable_C__pf2962636854*> bpv__Rotatables__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DeltaSeconds"))
	float b0l__K2Node_Event_DeltaSeconds__pf;
	AMaluchyGameMode_C__pf2132744816(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_MaluchyGameMode__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_MaluchyGameMode__pf_1(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(DisplayName="Tick", ToolTip="Event called every frame", CppFromBpEvent, OverrideNativeName="ReceiveTick"))
	virtual void bpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf);
	UFUNCTION(meta=(DisplayName="BeginPlay", ToolTip="Event when play begins for this actor.", CppFromBpEvent, OverrideNativeName="ReceiveBeginPlay"))
	virtual void bpf__ReceiveBeginPlay__pf();
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", DisplayName="Construction Script", ToolTip="Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation.", Category, CppFromBpEvent, OverrideNativeName="UserConstructionScript"))
	virtual void bpf__UserConstructionScript__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="ResetEnviroment"))
	virtual void bpf__ResetEnviroment__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="GameEnded"))
	virtual void bpf__GameEnded__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="ResetGame"))
	virtual void bpf__ResetGame__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="RotateAllRotatables"))
	virtual void bpf__RotateAllRotatables__pf(float bpp__DeltaTime__pf);
public:
};
