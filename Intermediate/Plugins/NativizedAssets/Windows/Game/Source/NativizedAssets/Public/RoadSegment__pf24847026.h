#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/CoreUObject/Public/UObject/NoExportTypes.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
class UStaticMeshComponent;
class UBoxComponent;
class USceneComponent;
class ARoadSegment_C__pf24847026;
class AActor;
class UClass;
class AMaluchyGameMode_C__pf2132744816;
#include "RoadSegment__pf24847026.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/RoadSegment/RoadSegment.RoadSegment_C", OverrideNativeName="RoadSegment_C"))
class ARoadSegment_C__pf24847026 : public AActor
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="low_poly_buildings_Highrise_1"))
	UStaticMeshComponent* bpv__low_poly_buildings_Highrise_1__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Cube"))
	UStaticMeshComponent* bpv__Cube__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="StaticMesh2"))
	UStaticMeshComponent* bpv__StaticMesh2__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="StaticMesh7"))
	UStaticMeshComponent* bpv__StaticMesh7__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Cube1"))
	UStaticMeshComponent* bpv__Cube1__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="StaticMesh6"))
	UStaticMeshComponent* bpv__StaticMesh6__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="StaticMesh5"))
	UStaticMeshComponent* bpv__StaticMesh5__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="StaticMesh4"))
	UStaticMeshComponent* bpv__StaticMesh4__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="StaticMesh3"))
	UStaticMeshComponent* bpv__StaticMesh3__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="low_poly_buildings_Highrise_11"))
	UStaticMeshComponent* bpv__low_poly_buildings_Highrise_11__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="low_poly_buildings_Highrise_16"))
	UStaticMeshComponent* bpv__low_poly_buildings_Highrise_16__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="low_poly_buildings_Highrise_2"))
	UStaticMeshComponent* bpv__low_poly_buildings_Highrise_2__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="low_poly_buildings_Highrise_9"))
	UStaticMeshComponent* bpv__low_poly_buildings_Highrise_9__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="low_poly_buildings_Highrise_17"))
	UStaticMeshComponent* bpv__low_poly_buildings_Highrise_17__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Border1"))
	UBoxComponent* bpv__Border1__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Border0"))
	UBoxComponent* bpv__Border0__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="SmolLine1"))
	UStaticMeshComponent* bpv__SmolLine1__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="SmolLine0"))
	UStaticMeshComponent* bpv__SmolLine0__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Line1"))
	UStaticMeshComponent* bpv__Line1__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Line0"))
	UStaticMeshComponent* bpv__Line0__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="Asphalt"))
	UStaticMeshComponent* bpv__Asphalt__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Components", OverrideNativeName="Box"))
	UBoxComponent* bpv__Box__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Components", OverrideNativeName="StaticMesh"))
	UStaticMeshComponent* bpv__StaticMesh__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="StaticMesh1"))
	UStaticMeshComponent* bpv__StaticMesh1__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Components", OverrideNativeName="DefaultSceneRoot"))
	USceneComponent* bpv__DefaultSceneRoot__pf;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Is Start", Category="Default", OverrideNativeName="IsStart"))
	bool bpv__IsStart__pf;
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, meta=(DisplayName="Next", Category="Default", OverrideNativeName="Next"))
	ARoadSegment_C__pf24847026* bpv__Next__pf;
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, meta=(DisplayName="Previous", Category="Default", OverrideNativeName="Previous"))
	ARoadSegment_C__pf24847026* bpv__Previous__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Curr Objects", Category="Default", OverrideNativeName="CurrObjects"))
	TArray<AActor*> bpv__CurrObjects__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Objects to Randomize", Category="Default", OverrideNativeName="ObjectsToRandomize"))
	TArray<UClass*> bpv__ObjectsToRandomize__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Min Distance from Another", Category="Default", OverrideNativeName="MinDistanceFromAnother"))
	float bpv__MinDistanceFromAnother__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Objects Per Segment", Category="Default", OverrideNativeName="ObjectsPerSegment"))
	int32 bpv__ObjectsPerSegment__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Next Segments", Category="Default", OverrideNativeName="NextSegments"))
	int32 bpv__NextSegments__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DeltaSeconds"))
	float b0l__K2Node_Event_DeltaSeconds__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_OtherActor1"))
	AActor* b0l__K2Node_Event_OtherActor1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_IsActorMainCar_Is"))
	bool b0l__CallFunc_IsActorMainCar_Is__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_OtherActor"))
	AActor* b0l__K2Node_Event_OtherActor__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_IsActorMainCar_Is1"))
	bool b0l__CallFunc_IsActorMainCar_Is1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsMaluchy_Game_Mode"))
	AMaluchyGameMode_C__pf2132744816* b0l__K2Node_DynamicCast_AsMaluchy_Game_Mode__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess"))
	bool b0l__K2Node_DynamicCast_bSuccess__pf;
	ARoadSegment_C__pf24847026(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_RoadSegment__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_RoadSegment__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_RoadSegment__pf_2(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_RoadSegment__pf_3(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(Category="Collision", DisplayName="ActorEndOverlap", ToolTip="Event when an actor no longer overlaps another actor, and they have separated.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events.", CppFromBpEvent, OverrideNativeName="ReceiveActorEndOverlap"))
	virtual void bpf__ReceiveActorEndOverlap__pf(AActor* bpp__OtherActor__pf);
	UFUNCTION(meta=(Category="Collision", DisplayName="ActorBeginOverlap", ToolTip="Event when this actor overlaps another actor, for example a player walking into a trigger.For events when objects have a blocking collision, for example a player hitting a wall, see \'Hit\' events.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events.", CppFromBpEvent, OverrideNativeName="ReceiveActorBeginOverlap"))
	virtual void bpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf);
	UFUNCTION(meta=(DisplayName="Tick", ToolTip="Event called every frame", CppFromBpEvent, OverrideNativeName="ReceiveTick"))
	virtual void bpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf);
	UFUNCTION(meta=(DisplayName="BeginPlay", ToolTip="Event when play begins for this actor.", CppFromBpEvent, OverrideNativeName="ReceiveBeginPlay"))
	virtual void bpf__ReceiveBeginPlay__pf();
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", DisplayName="Construction Script", ToolTip="Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation.", Category, CppFromBpEvent, OverrideNativeName="UserConstructionScript"))
	virtual void bpf__UserConstructionScript__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="MoveSegment"))
	virtual void bpf__MoveSegment__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="GetOriginForNext"))
	virtual void bpf__GetOriginForNext__pf(/*out*/ FVector& bpp__Origin__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="PutAnotherSegment"))
	virtual void bpf__PutAnotherSegment__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="IsActorMainCar"))
	virtual void bpf__IsActorMainCar__pf(AActor* bpp__Actor__pf, /*out*/ bool& bpp__Is__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="RandomizeObjects"))
	virtual void bpf__RandomizeObjects__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="AddToResetables"))
	virtual void bpf__AddToResetables__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="RemoveFromResetables"))
	virtual void bpf__RemoveFromResetables__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="ResetLocation"))
	virtual void bpf__ResetLocation__pf(float bpp__By__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="DestroyRoadSegment"))
	virtual void bpf__DestroyRoadSegment__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="GetSpawnableLocation"))
	virtual void bpf__GetSpawnableLocation__pf(/*out*/ FVector& bpp__Location__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="SpawnChild"))
	virtual void bpf__SpawnChild__pf(UClass* bpp__Class__pf, FTransform bpp__SpawnTransform__pf);
public:
};
