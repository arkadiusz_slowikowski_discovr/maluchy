#pragma once
#include "GeneratedCodeHelpers.h"
#include "Blueprint/BlueprintSupport.h"
#include "Parametry__pf1851508772.generated.h"

USTRUCT(BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/Car/Parametry.Parametry", OverrideNativeName="Parametry"))
struct FParametry__pf1851508772
{
public:
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="SteeringSensitivity", Tooltip, OverrideNativeName="SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76"))
	float bpv__SteeringSensitivity_5_B056D05B4781A5BE60A4839117C36D76__pf;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="DrivingForce", Tooltip, OverrideNativeName="DrivingForce_7_9788A03E425C90DD905F5CA4274F056A"))
	float bpv__DrivingForce_7_9788A03E425C90DD905F5CA4274F056A__pf;
	FParametry__pf1851508772();
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	bool operator== (const FParametry__pf1851508772& __Other) const
	{
		return FParametry__pf1851508772::StaticStruct()->CompareScriptStruct(this, &__Other, 0);
	};
	friend uint32 GetTypeHash(const FParametry__pf1851508772& __Other) { return UUserDefinedStruct::GetUserDefinedStructTypeHash( &__Other, FParametry__pf1851508772::StaticStruct()); }
};
