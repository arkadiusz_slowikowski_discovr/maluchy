#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Rotatable__pf2962636854.h"
class UStaticMeshComponent;
class AActor;
class ADiscoMaluch_C__pf1851508772;
#include "Canister__pf1442588193.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/Collectables/Canister/Canister.Canister_C", OverrideNativeName="Canister_C"))
class ACanister_C__pf1442588193 : public ARotatable_C__pf2962636854
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="canister_Group_003"))
	UStaticMeshComponent* bpv__canister_Group_003__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="canister_Group_001"))
	UStaticMeshComponent* bpv__canister_Group_001__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="canister_Reservekanister_001"))
	UStaticMeshComponent* bpv__canister_Reservekanister_001__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_OtherActor"))
	AActor* b1l__K2Node_Event_OtherActor__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DeltaSeconds"))
	float b1l__K2Node_Event_DeltaSeconds__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsDisco_Maluch"))
	ADiscoMaluch_C__pf1851508772* b1l__K2Node_DynamicCast_AsDisco_Maluch__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess"))
	bool b1l__K2Node_DynamicCast_bSuccess__pf;
	ACanister_C__pf1442588193(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_Canister__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_Canister__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_Canister__pf_2(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(DisplayName="Tick", ToolTip="Event called every frame", CppFromBpEvent, OverrideNativeName="ReceiveTick"))
	virtual void bpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf);
	UFUNCTION(meta=(Category="Collision", DisplayName="ActorBeginOverlap", ToolTip="Event when this actor overlaps another actor, for example a player walking into a trigger.For events when objects have a blocking collision, for example a player hitting a wall, see \'Hit\' events.@note Components on both this and the other Actor must have bGenerateOverlapEvents set to true to generate overlap events.", CppFromBpEvent, OverrideNativeName="ReceiveActorBeginOverlap"))
	virtual void bpf__ReceiveActorBeginOverlap__pf(AActor* bpp__OtherActor__pf);
	UFUNCTION(meta=(DisplayName="BeginPlay", ToolTip="Event when play begins for this actor.", CppFromBpEvent, OverrideNativeName="ReceiveBeginPlay"))
	virtual void bpf__ReceiveBeginPlay__pf();
	virtual void bpf__UserConstructionScript__pf() override;
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Rotate"))
	virtual void bpf__Rotate__pf(float bpp__DeltaxTime__pfT);
public:
};
