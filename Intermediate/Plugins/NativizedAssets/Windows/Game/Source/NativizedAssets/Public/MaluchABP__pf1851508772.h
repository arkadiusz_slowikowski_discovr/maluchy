#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Animation/AnimClassData.h"
#include "Runtime/AnimGraphRuntime/Public/AnimNodes/AnimNode_RefPose.h"
#include "Runtime/Engine/Classes/Animation/AnimNodeSpaceConversions.h"
#include "../Plugins/Runtime/PhysXVehicles/Source/PhysXVehicles/Public/AnimNode_WheelHandler.h"
#include "Runtime/AnimGraphRuntime/Public/AnimNodes/AnimNode_Root.h"
#include "../Plugins/Runtime/PhysXVehicles/Source/PhysXVehicles/Public/VehicleAnimInstance.h"
#include "MaluchABP__pf1851508772.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/Car/MaluchABP.MaluchABP_C", OverrideNativeName="MaluchABP_C"))
class UMaluchABP_C__pf1851508772 : public UVehicleAnimInstance
{
public:
	GENERATED_BODY()
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_Root_F5F975D549CA8B4829EF77B58E9E1259"))
	FAnimNode_Root bpv__AnimGraphNode_Root_F5F975D549CA8B4829EF77B58E9E1259__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_WheelHandler_FBA3090E480E93354C4244A9406B927A"))
	FAnimNode_WheelHandler bpv__AnimGraphNode_WheelHandler_FBA3090E480E93354C4244A9406B927A__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_ComponentToLocalSpace_A347C4D2415FC6F1536AF0ACDAEA7CF5"))
	FAnimNode_ConvertComponentToLocalSpace bpv__AnimGraphNode_ComponentToLocalSpace_A347C4D2415FC6F1536AF0ACDAEA7CF5__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_MeshRefPose_304B03444A0EF236EE0F6E944A102D1B"))
	FAnimNode_MeshSpaceRefPose bpv__AnimGraphNode_MeshRefPose_304B03444A0EF236EE0F6E944A102D1B__pf;
	UMaluchABP_C__pf1851508772(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void __InitAllAnimNodes();
	void __InitAnimNode__AnimGraphNode_Root_F5F975D549CA8B4829EF77B58E9E1259();
	void __InitAnimNode__AnimGraphNode_WheelHandler_FBA3090E480E93354C4244A9406B927A();
	void __InitAnimNode__AnimGraphNode_ComponentToLocalSpace_A347C4D2415FC6F1536AF0ACDAEA7CF5();
	void __InitAnimNode__AnimGraphNode_MeshRefPose_304B03444A0EF236EE0F6E944A102D1B();
	UFUNCTION(meta=(OverrideNativeName="ExecuteUbergraph_MaluchABP"))
	void bpf__ExecuteUbergraph_MaluchABP__pf(int32 bpp__EntryPoint__pf);
public:
};
