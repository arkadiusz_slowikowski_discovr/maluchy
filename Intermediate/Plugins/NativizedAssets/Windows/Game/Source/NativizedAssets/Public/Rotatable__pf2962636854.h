#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
class USceneComponent;
#include "Rotatable__pf2962636854.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/Collectables/Rotatable.Rotatable_C", OverrideNativeName="Rotatable_C"))
class ARotatable_C__pf2962636854 : public AActor
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="DefaultSceneRoot"))
	USceneComponent* bpv__DefaultSceneRoot__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Rotating Speed", Category="Default", OverrideNativeName="RotatingSpeed"))
	float bpv__RotatingSpeed__pf;
	ARotatable_C__pf2962636854(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", DisplayName="Construction Script", ToolTip="Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation.", Category, CppFromBpEvent, OverrideNativeName="UserConstructionScript"))
	virtual void bpf__UserConstructionScript__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="AddToRotatables"))
	virtual void bpf__AddToRotatables__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="RemoveFromRotatables"))
	virtual void bpf__RemoveFromRotatables__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="RandomStartRot"))
	virtual void bpf__RandomStartRot__pf();
public:
};
