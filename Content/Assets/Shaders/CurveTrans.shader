// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "CurioAssets/Curve_Transparent"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
	}

		SubShader
	{
		 Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }

		CGPROGRAM
		#pragma surface surf NoLighting noambient vertex:vert alpha
		#pragma multi_compile __ BEND_ON

		// Global properties to be set by BendControllerRadial script
		uniform half3 _CurveOrigin;
		uniform half _Curvature;
		uniform fixed3 _Scale;
		uniform half _FlatMargin;
		uniform half _HorizonWaveFrequency;

		// Per material properties
		sampler2D _MainTex;
		fixed4 _Color;

		struct Input
		{
			float2 uv_MainTex;
		};

		half4 Bend(half4 v)
		{
			half4 wpos = mul(unity_ObjectToWorld, v);
			half2 xzDist = (wpos.xz - _WorldSpaceCameraPos.xz) / _Scale.xz;
			half dist = length(xzDist);
			dist = max(0, dist - _FlatMargin);
			wpos.y -= dist * dist * _Curvature;
			wpos = mul(unity_WorldToObject, wpos);
			return wpos;
		}

		void vert(inout appdata_full v)
		{
			#if defined(BEND_ON)
			v.vertex = Bend(v.vertex);
			#endif
		}

		void surf(Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) *_Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten) {
			fixed4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}
		ENDCG
	}

		Fallback "Mobile/Diffuse"
}
